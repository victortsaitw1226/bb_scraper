# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
import requests
from bs4 import BeautifulSoup
from datetime import datetime
import calendar
import json
import re


#class YoutubeSpider(scrapy.Spider):
class YoutubeSpider(RedisSpider):
    name = "youtube"

    def __init__(self, user_id='', since=''):
        self.headers = {
            'x-youtube-client-name': '1',
            'x-youtube-client-version': '2.20191211.04.00',
            'x-youtube-identity-token': 'QUFFLUhqbXdJU05xMHh1bkpxT3NaRXB0djk2NVlEb2xPQXw=',
            'cookie': 'VISITOR_INFO1_LIVE=KH5npNfZjDE; CONSENT=YES+TW.zh-TW+20161105-12-1; PREF=al=zh-TW&f1=50000000; YSC=ajeMx3aDDAE; SID=rQd_SFLYM9vtI2ju2YWl9ngXsUI5F6r7zLp0LBMiLl4dwVlT2zajSoBSUdC1PAw86YcfJw.; HSID=Ay5vGybPcH_wsZ-aI; SSID=AA_1nLKL8f87Jg_zr; APISID=hOrwwnbbA9DCOgnI/Atm91O13-rBdV33xI; SAPISID=YFVpQtywWhXbe9s7/Ak6TLougG6DnJ5lny; LOGIN_INFO=AFmmF2swRQIgLfdNS-h0M3q2GtAaJP9_i70dykf0XCjL8Ep2xTkXHCUCIQDX-meUzxOpg-Zpz0_1jwJAcr9l66ntGwddItlhvtVpoQ:QUQ3MjNmek5kcUNJSzdfb0l1d1V1Sm1RcGRULUVYWl9adVZqOTlHNkhxZ1pfU05TOEtvMzk1M0xWZTJZYk9pZWloSU9iZmtJQTZWYm5OS1dqMDlta2ppV3VYTTdCM3Jjelp4eS1CM1BMTEpMUjBydk9LOUhPbkF0RklxVVQxQmRCRk1ocklybVQ3TlZxQW9BeGtrT3YteTJGcGhqcF9SN1g4ekNRb3RyNENQZ0RrV0dFWnBYZGE4; SIDCC=AN0-TYvSM4XOEoAFbpNt0Xbp2AaJpfKw0Bq9lgEr941iz6LK3oZX086DNNJVQbN79wNqz0va4Q',
            'Cache-Control': 'no-cache', 
            'Content-Encoding': 'gzip', 
            'X-Frame-Options': 'SAMEORIGIN', 
            'Expires': 'Tue, 27 Apr 1971 19:44:06 GMT', 
            'Strict-Transport-Security': 'max-age=31536000', 
            'X-Content-Type-Options': 'nosniff', 
            'Content-Type': 'text/html; charset=utf-8', 
            'Server': 'YouTube Frontend Proxy', 
            'X-XSS-Protection': '0', 
            'Alt-Svc': 'quic=":443"; ma=2592000; v="46,43",h3-Q050=":443"; ma=2592000,h3-Q049=":443"; ma=2592000,h3-Q048=":443"; ma=2592000,h3-Q046=":443"; ma=2592000,h3-Q043=":443"; ma=2592000', 
            'Transfer-Encoding': 'chunked'
        } 
        
        

    def start_requests(self):

        if isinstance(self, RedisSpider):
            return

        requests = [{
            "media": "youtube",
            "name": "youtube",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.youtube.com/channel/UCjZCm3uuhkCvTYE9txucVcw/videos",
            "priority": 1
        }]

        for request in requests:
            yield scrapy.FormRequest(url=request['url'],
                                headers = self.headers,
                                method='GET',
                                meta=request,
                                dont_filter=True,
                                callback=self.parse_list)
                
    def parse_list(self, response):
        meta = response.meta

        # get links to crawl video_info
        soup = BeautifulSoup(response.body, 'html.parser')
        links = soup.find_all('div','yt-lockup-content')
        All_links = [ 'https://www.youtube.com/' +  l.find('a')['href'] for l in links]

        # past = datetime.now() - timedelta(seconds=meta['days_limit'])
        # if latest_datetime < past:
        #     return

        # get token to crawl other pages
        continuation = re.search(r';continuation\=(.*?)"',response.text).group(1)
        continuation = continuation.replace('%253D','%3D')
        itct = re.search(r'"itct=(.*?)"',response.text).group(1)
        while True:
            params = (
                ('ctoken', continuation),
                ('continuation', continuation),
                ('itct', itct),
            )
            
            response = requests.get('https://www.youtube.com/browse_ajax', headers=self.headers, params=params)
    
            raw = json.loads(response.text)
            items = raw[1]['response']['continuationContents']['gridContinuation']['items']
            links = [ 'https://www.youtube.com/watch?v=' + item['gridVideoRenderer']['videoId'] for item in items]
            All_links.extend(links)
            
            try:
                params_info = raw[1]['response']['continuationContents']['gridContinuation']['continuations'][0]['nextContinuationData']
                continuation = params_info['continuation']
                itct = params_info['clickTrackingParams']
            except:
                break

        for url in All_links:
            yield scrapy.FormRequest(url=url,
                                headers = self.headers,
                                method='GET',
                                # meta=url,
                                dont_filter=True,
                                callback=self.parse_article)

    def parse_article(self, response):
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')
        
        # comments
        continuation = re.search(r'\'COMMENTS_TOKEN\': "(.*?)",\n',html).group(1)
        itct = re.search(r'clickTrackingParams\\":\\"(.*?)\\"',html).group(1)
        session_token = re.search(r'XSRF_TOKEN\': "(.*?)",\n',html).group(1)

        tmp = {'url':response.url,
                'continuation':continuation,
                'itct':itct,
                'session_token':session_token}

        with open("youtube_comments.txt", "w") as text_file:
            text_file.write(str(response.url) + str(response.body))

        messages_list = self.crawl_replies(session_token,continuation,itct)    
        messages =  [self.parse_reply(r) for r in messages_list]



        # video_info
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = self.parse_content(soup)
        item['comment'] = messages
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,html)
        item['content_type'] = 0
        item['media'] = 'youtube'
        item['proto'] =  'YOUTUBE_PARSE_ITEM'
        yield item

        

        # for message in messages:
        #     item = NewsItem()
        #     item['url'] = response.url
        #     item['metadata'] = message['metadata']
        #     item['article_title'] = self.parse_title(soup)
        #     item['author'] = message['author']
        #     item['author_url'] = message['author_url']
        #     item['date'] = message['datetime']
        #     item['content'] = message['pushcontent_content']
        #     item['content_type'] = 1
        #     item['media'] = 'youtube'
        #     item['proto'] = 'YOUTUBE_PARSE_ITEM'
        #     item['comment'] = []
        #     yield item


    def crawl_replies(self, session_token, continuation, itct):  
        data = {'session_token':session_token}
        comments_list = []
        while True:
            params = (
                ('action_get_comments', '1'),
                ('pbj', '1'),
                ('ctoken', continuation),
                ('continuation',continuation ),
                ('itct',itct ),
            )
            
            response = requests.post('https://www.youtube.com/comment_service_ajax', headers=self.headers, params=params, data=data)
            print('params:',str(params))
            print('data:',str(data))
            # with open("youtube_comments.txt", "w") as text_file:
            #     text_file.write()
            print('response:',str(response.text))
            raw = json.loads(response.text)
            comments = raw['response']['continuationContents']['itemSectionContinuation']['contents']
            
            for i in range(len(comments)):
                comments_list.append(comments[i]['commentThreadRenderer']['comment']['commentRenderer'])
            
            try:
                params_info = raw['response']['continuationContents']['itemSectionContinuation']['continuations'][0]['nextContinuationData']
                continuation = params_info['continuation']
                itct = params_info['clickTrackingParams']
            except:
                break
        return comments_list


    def parse_reply(self, raw):
        reply_dict =  {
            'comment_id': raw['commentId'],
            'datetime': raw['publishedTimeText']['runs'][0]['text'], #not finished yet
            'author': raw['authorText']['simpleText'],
            'author_url': 'https://www.youtube.com'+raw['authorEndpoint']['browseEndpoint']['canonicalBaseUrl'],
            'content':''.join( c['text']for c in raw['contentText']['runs']),
            'source': 'youtube',
            'metadata':{'like_count':'' if 'likeCount' not in raw.keys() else raw['likeCount'], 
                        'reply_count':'' if 'replyCount' not in raw.keys() else raw['replyCount']},
            'comments':[] }
        return reply_dict


    def parse_title(self, soup):
        head = soup.find('head')
        info_title = head.find('title')
        if info_title is None:
            title = ''
        title = info_title.get_text()
        return title
        
    def parse_author(self, soup):
        author = soup.find('div','yt-user-info').text.strip()
        return author
        
    def parse_author_url(self, soup):
        author_url = []
        author_url.append('https://www.youtube.com' + soup.find('div','yt-user-info').find('a')['href']) 
        return author_url   
    
    def parse_content(self, soup):
        return soup.find('div',{'id':'watch-description-text'}).text
    
    def parse_datetime(self, soup):
        date = soup.find('strong','watch-time-text').text
        splitStr = date.split()
        abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
        dt = splitStr[4] + '/' + str(abbr_to_num[splitStr[2]]) + '/' + splitStr[3]  
        dt = datetime.strptime( dt, '%Y/%m/%d,')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_metadata(self, soup,html):
        metadata={}
        metadata['view_count'] = soup.find('meta',{'itemprop':'interactionCount'})['content']
        metadata['agree_count'] = re.search(r'likeCount\\":(.*?)\,',html).group(1)
        metadata['disagree_count'] = re.search(r'dislikeCount\\":(.*?)\,',html).group(1)
        return metadata
