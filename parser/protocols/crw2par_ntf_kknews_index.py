#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_KKNEWS_INDEX(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_KKNEWS_INDEX, self).__init__('CRW2PAR_NTF_KKNEWS_INDEX')
        self.update({
          'content': '',
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

