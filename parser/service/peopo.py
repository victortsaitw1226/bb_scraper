#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments


class PEOPO:
    def __init__(self, server):
        print('init peopo.py')
        self.server = server

    def on_CRW2PAR_NTF_PEOPO_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_peopo_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_PEOPO_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_peopo_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_peopo_list(self, content):

        soup = BeautifulSoup(content, 'html.parser')
        peopo = 'https://www.peopo.org'

        try:
            next_page = peopo + soup.find('a', {'title':'到下一頁'})['href']
        except:
            next_page = None
            
        links = [{'url':peopo+s.find('a')['href'],'metadata':[]} for s in soup.findAll('h3', 'view-list-title')]
        link_date = [datetime.strptime(s.text, '%Y-%m-%d %H:%M') for s in soup.findAll('div','view-list-date')]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_peopo_content(self, content):

        def peopo_comments(soup):
            comments = []
            comment_author = [s.find('a')['title'] for s in soup.findAll('div','user-picture')]
            comment_author_url = ['https://www.peopo.org'+s.find('a')['href'] for s in soup.findAll('div','user-picture')]
            comment_content = [s.find('p').text for s in soup.findAll('div','comment-inner')]
            comment_datetime = [s.text.split(' 於 ')[1] for s in soup.findAll('div','s-lt floatleft')]
            comment_datetime = [datetime.strptime(s, '%Y/%m/%d - %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800') for s in comment_datetime]
            for cont,auth,auth_url,date in zip(comment_content,comment_author,comment_author_url,comment_datetime):
                comments.append({'comment_id':'', 'datetime':date, 'author':auth, 'author_url':auth_url , 'content':cont , 
                                'metadata':{},'source':'peopo','comments':[]})
            return comments
        
        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        date = datetime.strptime(soup.find('div','submitted').text, '%Y.%m.%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find("meta",  property="og:title")['content'].strip().replace(u'\u3000',u' ')
        author = soup.find('div','user-infos floatleft').text.split('\n')[1]
        content = ''.join([s.text for s in soup.find('div','post_text_s').findAll('p')]).replace('\xa0','').replace('\n\t','')
        
        #metadata
        keywords = ''.join([s.text for s in soup.find(id='node-terms').findAll('ul', {'class','inline'})])[:-1].split('\n')
        category = soup.find(id='node-terms').findAll('div', {'class','field-items'})[1].findAll('a')[0].text
        view = soup.find('div','amount amount-view').text[:-2]
        share = soup.find('div','amount amount-comment').text[:-2]
        peopo_vote_count = soup.find('span','vote-amount').text
        peopo_comment_count = soup.find('div','dialog-number').text
        fb_like_count = fb_like_count_html.find(class_='_5n6h _2pih').text
        try:
            video_url = re.split('src:  "|" }',str(soup.find(id='h5p').find('script')))[1]
        except:
            video_url = ''
    
        metadata = {'tag': keywords, 'category':category,'view_count': view, 'share_count': share, 'like_count':peopo_vote_count,
            'reply_count':peopo_comment_count,'video_url': video_url, 'fb_like_count':fb_like_count}

        peopo_comment = peopo_comments(soup)
        fb_comment = parse_comments(comment_html)
        comment = peopo_comment + fb_comment

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
