#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback
import requests
import sys


class EYNY:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()
        # self.headers = {
        #     "Host": "www07.eyny.com",
        #     "origin": "http://www07.eyny.com",
        #     "Referer": "http://www07.eyny.com/member.php?mod=logging&action=login",
        #     "Upgrade-Insecure-Requests": "1",
        #     'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
        # }

    def detect_error(self, html):
        return 'padding:0px;' in html

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    # def login(self):
        
    #     login_data = {
    #             "formhash": "formhash",
    #             "referer":'http://www07.eyny.com/./',
    #             'loginfield':'username',
    #             "username": "ibdo.dev",
    #             "password": "ibdo2018!",
    #             "questionid": 0, 
    #             "answer": '',
    #             "cookietime":2592000,
    #             "g-recaptcha-response":'03AOLTBLTACQfkGkDrGJ6eFioWrnyLkaUI3GiyX7tbPVqN3qyryRFFonrAfRD9OHQKfaZs_zT9jFYhMwnSwquES1UkfXEpLowFAjaLpC4gzxykDIRAHDPmVSVM1KX2jzUe9Zv0uOBYLlZJBD5IFbu-Luupgyt0Yie2ML5j6cmjjdHenBgzW2FDUQisKPjih0l8oRItshOCHSo6er0EHF6cWsQyLcRHx1Jctxw2HcQnbdmWcJ9d9TNtT9LO0PTGG4z_2CvgjTrcNaVfuv3ZJ_4sJRdru_jNyimxpc6tW5nG5aeCJ9aEQBiO61vIihRSpPF-pz9pXrQpLrH_rRkjQRm3-O0bUcbaOTkel089aEEX15MttzxB80r_Sfpc62S_5_VgnAxWD1H1aLXQ'
    #             }
        
    #     login_url = 'http://www07.eyny.com/member.php?mod=logging&action=login'
    #     login_request_url = 'http://www07.eyny.com/member.php?mod=logging&action=login&loginsubmit=yes&loginhash=LakeK&inajax=1'
        
    #     # get the formhash
    #     r = self.session.get(login_url,headers = self.headers)
    #     soup = BeautifulSoup(r.content, 'html.parser')

    #     try:
    #         login_data["formhash"] = soup.find('input',{'name':'formhash'})['value']
    #         # post login request
    #         r = self.session.post(login_request_url, data = login_data, headers = self.headers)
    #     except:
    #         print('fail_login:',r.content)
        




    def on_MNG2CRW_NTF_EYNY_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = self.goto(url)
            # html = self.server.new_requests(url, self.detect_error)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_EYNY_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:

            # self.login()

            html = []
            link_prefix = 'http://www07.eyny.com/'
            
            #first page
            content_html = self.goto(url)
            # content_html = self.server.new_requests(url, self.detect_error)
            html.append(content_html)
            
            # alert error
            if 'alert_error' in content_html:
                html = 'alert_error'
            elif 'alert_info' in content_html: 
                html = 'alert_info'
            else:
                # other page
                soup = BeautifulSoup(content_html, 'html.parser')
                nxt = soup.find('div','pgs mtm mbm cl').find('a','nxt')
                
                while nxt != None:
                    content_html = self.goto(link_prefix + nxt['href'])
                    # content_html = self.server.new_requests(link_prefix + nxt['href'], self.detect_error)
                    html.append(content_html)
                    soup = BeautifulSoup( content_html, 'html.parser')
                    nxt = soup.find('div','pgs mtm mbm cl').find('a','nxt')
                html = {'html': html, 'url': url}

        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        if html != 'alert_error' and html != 'alert_info':
            to_notify = CRW2PAR_NTF_NEWS_POST() 
            to_notify.set_content(html)
            to_notify.set_media(from_notify.get_media())
            self.server.send(to_notify)
