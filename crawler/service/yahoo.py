#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback, sys
import requests
import json
import time
import re

class YAHOO:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        raw.encoding = "utf-8"
        return raw.text  

    def on_MNG2CRW_NTF_YAHOO_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            archive = 'https://tw.news.yahoo.com/' + re.split(r'newsTab=(\D*);',url)[1] + '/archive'
            data = []
            data.extend(json.loads(self.goto(url))['data'])
            for i in range(len(data)):
                try:
                    data[i]['url'] = archive + data[i]['url']
                except:
                    pass #json找不到'url'
            html = {'list_html':data, 'current_page':url, 'category': re.split(r'newsTab=(\D*);',url)[1]} 
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_YAHOO_GET_POST(self, packet):

        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # detail
            detail_html = self.goto(url['url'])
            
            # comments
            url_top3 = "https://tw.news.yahoo.com/_td/api/resource/canvass.getMessageListForContext_ns;context={uuid};count=3;index=null;lang=zh-Hant-TW;namespace=yahoo_content;oauthConsumerKey=frontpage.oauth.canvassKey;oauthConsumerSecret=frontpage.oauth.canvassSecret;rankingProfile=;region=TW;sortBy=popular;type=null;userActivity=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=cacheContentCanvas%2CenableCMP%2CenableConsentData%2CenableGDPRFooter%2CenableGuceJs%2CenableGuceJsOverlay%2CvideoDocking%2CnewContentAttribution%2Clivecoverage&intl=tw&lang=zh-Hant-TW&partner=none&prid=c573hbdepajf2&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.26449&returnMeta=true"
            url_message = "https://tw.news.yahoo.com/_td/api/resource/canvass.getMessageListForContext_ns;context={uuid};count=10;index=v%3D1%3As%3Dpopular%3Asl%3D1570065901%3Aoff%3D{count};lang=zh-Hant-TW;namespace=yahoo_content;oauthConsumerKey=frontpage.oauth.canvassKey;oauthConsumerSecret=frontpage.oauth.canvassSecret;rankingProfile=;region=TW;sortBy=popular;type=null;userActivity=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=cacheContentCanvas%2CenableCMP%2CenableConsentData%2CenableGDPRFooter%2CenableGuceJs%2CenableGuceJsOverlay%2CvideoDocking%2CnewContentAttribution%2Clivecoverage&intl=tw&lang=zh-Hant-TW&partner=none&prid=c573hbdepajf2&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.26471&returnMeta=true"
            
            soup = BeautifulSoup(detail_html, 'html.parser')
            uuid = soup.find('meta',{'name':'apple-itunes-app'})['content'].split('/')[-1]
            
            res = self.goto(url_top3.format(uuid = uuid))
            res = json.loads(res)
            all_msg = [x for x in res['data']['canvassMessages']]
            
            count = res['data']['total']['count']
            for i in range(int(count/10) + 1):
                res = self.goto(url_message.format(uuid = uuid ,count= i*10 +2 ))
                res = json.loads(res)
                all_msg.extend([x for x in res['data']['canvassMessages']])
            
            # get replies replies
            url_message_message  = 'https://tw.news.yahoo.com/_td/api/resource/canvass.getReplies_ns;action=showNext;context={uuid};count=10;index=null;lang=zh-Hant-TW;messageId={messageID};namespace=yahoo_content;oauthConsumerKey=frontpage.oauth.canvassKey;oauthConsumerSecret=frontpage.oauth.canvassSecret;region=TW;sortBy=createdAt;tags=?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=cacheContentCanvas%2CenableCMP%2CenableConsentData%2CenableGDPRFooter%2CenableGuceJs%2CenableGuceJsOverlay%2CvideoDocking%2CnewContentAttribution%2Clivecoverage&intl=tw&lang=zh-Hant-TW&partner=none&prid=fk7nodpepasfj&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.26471&returnMeta=true'
            for m in range(len(all_msg)):
                if all_msg[m]['reactionStats']['replyCount'] !=0:
                    res = self.goto(url_message_message .format(uuid = uuid,messageID = all_msg[m]['messageId'] ))
                    res = json.loads(res)
                    all_msg[m]['replies_list'] = res['data']
                else:
                    all_msg[m]['replies_list'] = []
            
            html = {'html': detail_html, 'comments_json':all_msg, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
