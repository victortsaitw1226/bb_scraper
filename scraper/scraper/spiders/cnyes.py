# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class CnyesSpider(RedisSpider):
    name = "cnyes"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "cnyes",
            "name": "cnyes",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600 * 3,
            "url": "https://news.cnyes.com/api/v2/news?limit=30",
            "scrapy_key": "cnyes:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        html = json.loads(response.body)
        
        posts_date = []
        for h in html['items']['data']:
            url = 'https://news.cnyes.com/news/id/' + str(h['newsId'])
            posts_date.append(datetime.fromtimestamp(h['publishAt']))
            yield scrapy.Request(url,
                meta=meta,
                callback=self.parse_article)

        if len(posts_date) == 0:
            return

        latest_datetime = max(posts_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        next_page = self.parse_next_page(html,latest_datetime)

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self,html,latest_post_date):
        if html['items']['next_page_url'] != None:
            next_page = html['items']['next_page_url']
        else:
            start = (latest_post_date.date()-timedelta(1)).strftime('%s')
            end = str(int(start) + 86399)
            next_page = 'https://news.cnyes.com/api/v2/news?limit=30&startAt='+start+'&endAt='+end
        return next_page
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'cnyes'
        item['proto'] = 'CNYES_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = soup.find("meta",  property="article:published_time")['content'].replace('+08:00','+0800')
        return date
    
    def parse_author(self, soup):
        author = soup.find("span",  {'itemprop':'author'}).text.split('台')[0].split(' ')[0].replace('鉅亨網編譯','').replace('鉅亨網編輯','').replace('鉅亨網記者','') 
        return author
    
    def parse_title(self, soup):
        title = soup.find("meta",  property="og:title")['content'].split(' | ')[0]
        return title
    
    def parse_content(self, soup):
        content = soup.find("div",  {'itemprop':'articleBody'})
        for div in content.findAll('figure'): 
            div.decompose()
        content = content.text
        return content

    def parse_metadata(self, soup):
        try:
            keywords = soup.find("meta",  itemprop="keywords")['content'].split(',')
        except:
            keywords = []
        category = [s.text for s in soup.find("nav",  {'class':'_9QBS'}).findAll('span')]
        category = category[-1]
        metadata = {'tag': keywords, 'category':category}
        return metadata
    
