#!/usr/bin/env python
# -*- coding: utf-8 -*-
import random
import sys
import traceback
import time
import requests
import json
import re
from bs4 import BeautifulSoup

class Proxy:

    def generate_proxies(self, redis):
        #response = requests.get('https://www.us-proxy.org')
        #response = requests.get('https://www.us-proxy.org')
        response = requests.get('https://www.sslproxies.org')
        soup = BeautifulSoup(response.text, 'lxml')
        trs = soup.select("#proxylisttable tr")
        proxies_list = list()
        for tr in trs:
            tds = tr.select("td")
            if len(tds) <= 6:
                continue
            ip = tds[0].text
            port = tds[1].text
            code = tds[2].text
            anonymity = tds[4].text
            ifScheme = tds[6].text
            if ifScheme != 'yes':
                continue
            scheme = 'https'
            proxy = "%s://%s:%s" % (scheme, ip, port)
            meta = {
              'port': port,
              'proxy': proxy,
              'code': code,
              'dont_retry': True,
              'download_timeout': 3,
              '_proxy_scheme': scheme,
              '_proxy_ip': '{}:{}'.format(ip, port)
            }
            #redis.hmset(proxy, 'ip_port', '{}:{}'.format(ip, port), 'code', code)
            redis.sadd('all_proxies', '{}:{}'.format(ip, port))
            if code in ['US', 'CA', 'GB', 'ZA', 'PH']:
                redis.sadd('eng_proxies', '{}:{}'.format(ip, port))

            #redis.hset('proxies', proxy, '{}:{}'.format(ip, port))
            
            proxies_list.append(meta)
        return proxies_list

    def remove_illegal_proxy(self, redis):
        validate_proxies = list()
        url = 'http://dcard.tw/_api/forums'
        #proxies = redis.hkeys('proxies')
        #ips = redis.hvals('proxies')
        proxies = list(redis.smembers('all_proxies'))
        for i in range(len(proxies)):
            try:
                proxy = str(proxies[i], 'utf-8')
                #ip = str(ips[i], 'utf-8')
                #ip = proxy.split(':')[0]
                response = requests.get(url, proxies={
                    'https': 'https://{}'.format(proxy)
                }, timeout=(5, 5))
            except:
                #redis.hdel('proxies', proxy)
                #traceback.print_exc(file=sys.stdout)
                print('remove proxy:{}'.format(proxy))
                redis.srem('all_proxies', proxy)
                redis.srem('eng_proxies', proxy)

    def random_proxy(self, redis, eng=False):
        if eng:
            proxies = redis.smembers('eng_proxies')
        else:
            proxies = redis.smembers('all_proxies')
        proxies = list(proxies)
        proxy_index = random.randint(0, len(proxies) - 1)
        ip_port = proxies[proxy_index]
        return str(ip_port, 'utf-8')

    def add_proxy(self, redis, proxy):
        splited_proxy = proxy.split(':')
        if 2 != len(splited_proxy):
            raise Exception('Proxy notation is illegal')
        ip_str = splited_proxy[0]
        pattern = r"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b"
        if not re.match(pattern, ip_str):
            raise Exception('IP is illegal')
        port_str = splited_proxy[1]
        port_number = int(port_str)
        if port_number < 1 or port_number > 65535:
           raise Exception('Port is illegal')

        try:
            url = 'https://api.ipify.org?format=json'
            response = requests.get(url, proxies={
                'https': 'https://{}'.format(proxy)
            }, timeout=(5, 5))
            print('response from ipify:{}'.format(response.text))
        except:
            raise Exception('Proxy cannot work')

        print('add_proxy:{}'.format(ip_str, port_number))
        redis.sadd('all_proxies', '{}:{}'.format(ip_str, port_number))
        redis.sadd('eng_proxies', '{}:{}'.format(ip_str, port_number))
        return True

    def list_proxy(self, redis):
        proxies = {}
        proxies['eng'] = [str(ip_port, 'utf-8') for ip_port
                          in list(redis.smembers('eng_proxies'))]
        proxies['all'] = [str(ip_port, 'utf-8') for ip_port
                          in list(redis.smembers('all_proxies'))]
        return proxies
