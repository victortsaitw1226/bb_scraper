#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class YOUTUBE_PARSE_COMMENT(Protocol):

    def __init__(self):
        super(YOUTUBE_PARSE_COMMENT, self).__init__('YOUTUBE_PARSE_COMMENT')
        self.update({
          'channel_url': '',
          'video_url': '',
          'author': '',
          'date': '',
          'content': '',
          'p': 0,
          'replies': []
        })

    def set_p(self, p):
        self.set('p', p.strip())

    def get_p(self):
        return self.get('p')

    def set_channel_url(self, channel_url):
        self.set('channel_url', channel_url)

    def get_channel_url(self):
        return self.get('channel_url')

    def set_video_url(self, video_url):
        self.set('video_url', video_url)

    def get_video_url(self):
        return self.get('video_url')

    def set_author(self, author):
        self.set('author', author.strip())

    def get_author(self):
        return self.get('author')

    def set_date(self, date):
        self.set('date', date.strip())

    def get_date(self):
        return self.get('date')

    def set_content(self, content):
        self.set('content', content.strip())

    def get_content(self):
        return self.get('content')

    def set_replies(self, replies):
        self.set('replies', replies)

    def get_replies(self):
        return self.get('replies')
