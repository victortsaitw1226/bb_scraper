#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_KKNEWS_POST_URLS(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_KKNEWS_POST_URLS, self).__init__('PAR2MNG_NTF_KKNEWS_POST_URLS')
        self.update({
          'urls': [],
          'board': ''
        })

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')

    def set_next_page_url(self, url):
        self.set('next_page_url', url)

    def get_next_page_url(self):
        return self.get('next_page_url')

    def set_latest_post_date(self, date):
        self.set('latest_post_date', date)

    def get_latest_post_date(self):
        return self.get('latest_post_date')