#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from utils.parse_fb_comment import parse_comments
from datetime import datetime
import requests

class ETFORUM:
    def __init__(self, server):
        self.server = server

    def on_CRW2PAR_NTF_ETFORUM_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_etforum_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ETFORUM_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_etforum_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_etforum_list(self, content):        
        soup = BeautifulSoup(content, 'html.parser')
        # get links
        links = []
        for subsoup in soup.find_all('div', {'class': 'block_content'}):
            for news in subsoup.find_all('div', {'class': 'piece clearfix'}):
                for url in news.find_all('a', {'class': 'pic', 'ref': False}):
                    links.append({'url': url.get('href')})
        # get next page
        current_page = int(soup.find_all('span', {'class': 'current'})[0].text)
        
        for ele in soup.find_all('div', {'class': 'menu_page'}):
            page_display = ele.find_all('p', {'class': 'info'})[0].text
        last_page = int(re.findall("\d+",page_display.split(' ')[2])[0])
        
        if current_page != last_page:
            next_page = 'https://forum.ettoday.net/newslist/675/'+str(current_page+1)

        def parse_datetime(soup):
            datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
            datetime = datetime[:22]+datetime[-2:] #remove ':'
            return datetime
        html = requests.get(links[0]['url']).text
        soup = BeautifulSoup(html, 'html.parser')
        latest_post_date = parse_datetime(soup)
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_etforum_content(self, content):
           
        def parse_datetime(soup):
            datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
            datetime = datetime[:22]+datetime[-2:] #remove ':'
            return datetime
            
        def parse_metadata(soup):
            label = soup.find_all('a', {'class': 'btn current'})
            if len(label):
                return {
                        'label': label[0].text
                        }
            else:
                return {}
            
        def parse_author(soup): 
            author = ''
            try:
                # try Columnist
                block = soup.find_all('div', {'class': 'penname_news clearfix'})
                for ele in block:
                    penname = ele.find_all('a', {'class': 'pic'})
                href = penname[0]['href']
                author = href.split('/')[-1]
                return {'author_name': author, 'link': 'https://forum.ettoday.net'+href}
    
            except:
                paragraph=[]
                for content in soup.find_all('div', {'itemprop': 'articleBody'}):
                    for ele in content.find_all('p'):
                        text = ele.text
                        if len(text) and ('●' in list(text)):
                            paragraph.append(text)
                if(len(paragraph)):
                    author_line = paragraph[0].split('●')[1]
                    author_line = author_line.split('／')[0]
                    author = author_line.split('，')[0]
                    author = [author, None][author=='作者']
                    return {'author_name': author, 'link': ''}

        def parse_title(soup):
            return soup.select('h1')[0].text
        
        def parse_content(soup):
            article = soup.find('div', class_='story')
            paragraph = []
            for p in article.find_all('p'):
                text = p.text.strip()
                if len(text) == 0 or text[0]=='►':
                    continue
                paragraph.append(text)
                
            content = '\n'.join(paragraph)
            content = content.split('【更多鏡週刊相關報導】')[0]
            return content

        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        metadata = { 'fb_like_count':fb_like_count_html.find('span',{'id':'u_0_1'}).text ,
                      'category': soup.find('meta',{'property':'article:section'})['content'] }
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(comment_html))
        item.set_metadata(metadata)
        
        return item
