#!/usr/bin/env python
# -*- coding: utf-8 -*-
import string
import random
import pymongo
from nameko.extensions import DependencyProvider
from bson.objectid import ObjectId
from datetime import datetime

class Mongo(DependencyProvider):
    class __MongoAgent:
        def __init__(self, host, port, user, password):
            if user:
                self.client = pymongo.MongoClient(host, port, username=user, password=password)
            else:
                self.client = pymongo.MongoClient(host, port)

        def getDB(self, db):
            return self.client[db]

        def getCollection(self, db, collection):
            return self.client[db][collection]

        def find(self, db, collection, query):
            collection = self.getCollection(db, collection)
            cursor = collection.find(query)
            return cursor

        def find_last_one(self, db, collection):
            collection = self.getCollection(db, collection)
            return collection.find_one({}, sort=[('_id',pymongo.DESCENDING)])

        def find_greater_than_id(self, db, collection, _id):
            collection = self.getCollection(db, collection)
            cursor = collection.find({'_id': {'$gt': ObjectId(_id)}})
            return cursor

        def find_greater_than(self, db, collection, col, value):
            collection = self.getCollection(db, collection)
            cursor = collection.find({col: {'$gt': value}})
            return cursor

        def find_all(self, db, collection):
            collection = self.getCollection(db, collection)
            cursor = collection.find({})
            return cursor

        def insert(self, db, collection, data):
            collection = self.getCollection(db, collection)
            _id = collection.insert_one(data).inserted_id
            return _id

        def upsert(self, db, collection, query, data):
            collection = self.getCollection(db, collection)
            return collection.update(query, {'$set': data}, upsert=True)

        def find_and_modify(self, db, collection, query, data):
            collection = self.getCollection(db, collection)
            return collection.find_and_modify(query, data)

        def update(self, db, collection, query, data):
            collection = self.getCollection(db, collection)
            return collection.update(query, data)

        def update_many(self, db, collection, query, data):
            collection = self.getCollection(db, collection)
            return collection.update_many(query, {'$set': data})

        def find_one(self, db, collection, query):
            collection = self.getCollection(db, collection)
            return collection.find_one(query)

        def delete_field(self, db, collection, query, field):
            collection = self.getCollection(db, collection)
            d = {}
            d[field] = ""
            return collection.update_many(query, {'$unset': d})

        def createCollection(self, db, collection):
            collist = self.client[db].list_collection_names()
            if collection in collist:
                return
            return self.client[db][collection].ensure_index([('url', pymongo.ASCENDING)], unique=True, dropDups=True)

        def upsert_post(self, db, collection, url, data):
            now = datetime.now()
            ''' Calculate the changing stats'''
            #stat_collection = self.getCollection(db, 'stats')
            #stat_collection.insert({
            #    'url': url,
            #    'created_dt': now,
            #    'media': data.get('media', '')
            #})

            ''' Change DB Name '''
            collection = '{}_{}_{}'.format(collection, now.year, now.month)

            self.createCollection(db, collection)

            data_collection = self.getCollection(db, collection)
            db_result = data_collection.update(
                {'url': url}, {'$set': data}, upsert=True)
            print(db_result)
            if not db_result.get('updatedExisting') or db_result.get('nModified'):
                return url
            return None

    def __init__(self, **kwargs):
        self.kwargs=kwargs

    def setup(self):
        self.host = self.container.config['MONGO_HOST']
        self.port = self.container.config['MONGO_PORT']
        self.user = self.container.config.get('MONGO_USER')
        self.password = self.container.config.get('MONGO_PASSWORD')
        self.instance = Mongo.__MongoAgent(self.host, self.port, self.user, self.password)

    def stop(self):
        pass

    def kill(self):
        pass

    def get_dependency(self, worker_ctx):
        return self.instance
