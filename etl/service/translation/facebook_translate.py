from googletrans import Translator
from pymongo import MongoClient

url = '210.202.47.189:27017'
db = MongoClient(url, username = 'ibdo', password = 'ibdo2018').get_database('history_v2')


str_time_start = '2019-07-01T00:00:00.000Z'
str_time_end = '2019-07-10T23:59:59.000Z'

query = {'$and': [
        { 'createdAt': {'$gte': str_time_start} },
        { 'createdAt': {'$lt': str_time_end} }
]}

records = []
for data in db.data.find({'media':{'$eq': 'facebook'}}):
    records.append(data)

ct_ls = []
for comment in records:
    if ('Comment' not in comment['comments_count']) and ('comment' not in comment['comments_count']):
        result = ''.join([i for i in comment['comments_count'] if not i.isdigit()])
        result = result.replace(u'\xa0', '')    
        ct_ls.append(result) 
ct_ls = set(ct_ls)

sh_ls = []
for comment in records:
    if ('Share' not in comment['shares_count']) and ('share' not in comment['shares_count']):
        result = ''.join([i for i in comment['shares_count'] if not i.isdigit()])
        result = result.replace(u'\xa0', '')    
        sh_ls.append(result) 
sh_ls = set(sh_ls)

ts = Translator()

ts_ls = []
for ct in ct_ls:
    ts_tx = ts.translate(ct, dest= 'en').text
    ct_dict = {
        'english' : ts_tx,
        'original': ct
    }
    ts_ls.append(ct_dict)

for sh in sh_ls:
    ts_tx = ts.translate(sh, dest= 'en').text
    sh_dict = {
        'english' : ts_tx,
        'original': sh
    }
    ts_ls.append(sh_dict)