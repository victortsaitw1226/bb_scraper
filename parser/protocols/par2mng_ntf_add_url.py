#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_ADD_URL(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_ADD_URL, self).__init__('PAR2MNG_NTF_ADD_URL')
        self.update({
          'content': '',
        })

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')
