#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class GRINEWS:
    def __init__(self, server):
        print('init grinews.py')
        self.server = server

    def on_CRW2PAR_NTF_GRINEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_grinews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_GRINEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_grinews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_grinews_list(self, content):

        def parse_next_page(soup):
            next_page =soup.find("span","page-numbers current").find_next_sibling()["href"]
            return next_page

        def parse_links_dates(soup):
            data = {
            'latest_post_date': None,
            'links': []}

            news=soup.find_all("div","article-img col-md-4")
            news_dates=soup.find_all("div","article-info col-md-8")
            links =[]
            dt_list=[]
            for i,matadata in (zip(news,news_dates)):
                auth=matadata.find("div","author").find("a")["href"]
                for time_ in matadata.find_all("div","date"):
                    dt=datetime.strptime(time_.text, ' %Y / %m / %d')
                    dt_list.append(dt)
                for url in i.find_all("a"):
                    urls=url["href"]
                    links.append({"url":urls,"matadata":{"author":auth}})

            data["links"] =links
            data["time"]= max(dt_list).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return data

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_grinews_content(self, content):
        
        def parse_datetime(soup):
            time_ =soup.find("li","date").text
            dt = datetime.strptime(time_, " %Y/%m/%d")
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author_url(soup):
            try:
                writer = soup.find("li","author").find("a")["href"]
            except:
                writer = []
            return writer

        def parse_author(soup):
            try:
                writer = soup.find("li","author").find("a").text
            except:
                writer = ''
            return writer

        def parse_title(soup):
            return soup.find("div","post-title").text.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            after_soup = soup
            [s.extract() for s in after_soup('strong')]
            content =soup.find("div","post-content").text
            return content.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_metadata(soup):
            keys=[]
            try:
                for tag in soup.find('li',"cat").find_all("a"):
                    keys.append(tag.text)
            except:
                pass
            category = soup.find('ul','post-categories').find('li').text
            return {
                'tag': keys,
                'category':category
            }

        def parse_comments(soup):
            coments_list=[]
            comment_list = soup.find_all("ol","comment-list")
            comment=""
            for i in comment_list:
                for content in i.find_all("p"):
                    comment+= content.text
                coments_list.append({"comment_id":'',
                                    "author": i.find("cite","comment-author").text,
                                    "author_url":'',
                                    "content": comment,
                                    "datetime": i.find("a","comment-date").text,
                                    "source": "grinews",
                                    "matadata": {},
                                    "comments":[],
                })

            return coments_list

        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_author_url(parse_author_url(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(soup))
        item.set_metadata(parse_metadata(soup))
        return item
