#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nameko.extensions import DependencyProvider
from cmreslogging.handlers import CMRESHandler
import logging
import time
import os
import traceback
import sys
import platform
import json
import datetime
import re
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.proxy import Proxy, ProxyType
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from .common import transform_xpath
from .utils.redis_agent import RedisAgent

class Browser(DependencyProvider):
    class __Browser:
        def __init__(self, **kwargs):
            self.account = 'c5662899@urhen.com'
            self.password = 'FFgg17'
            self.driver = self.create_driver()
            self.login()

        def create_driver(self, proxy_require=False):
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument('--no-sandbox')
            chrome_options.add_argument('--window-size=1420,1080')
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('--disable-dev-shm-usage')
            chrome_options.add_argument('--lang=en-US')
            fake_ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
            chrome_options.add_argument('--user-agent={}'.format(fake_ua))
            ## close ad
            prefs = {  
                'profile.default_content_setting_values' :  {  
                    'notifications' : 2  
                }  
            }  
            chrome_options.add_experimental_option('prefs', prefs)
            desired_caps = chrome_options.to_capabilities()
            # Add Proxy
            driver = webdriver.Chrome(chrome_options=chrome_options,
                                desired_capabilities=desired_caps)
            driver.implicitly_wait(10) # seconds
            return driver

        def login(self):
            self.driver.get('https://m.facebook.com')
            self.driver.maximize_window()
            self.driver.find_element_by_name('email').send_keys(self.account)
            self.driver.find_element_by_name('pass').send_keys(self.password)
            self.driver.find_element_by_name('login').click()
            time.sleep(3)
            self.driver.maximize_window()
            self.output('after_fb_login.html')
            return self.driver.page_source

        def count_fb(self, url):
            fb_url = 'https://developers.facebook.com/tools/debug/sharing/?q={}'.format(url)
            self.driver.get(fb_url)
            self.output('after_fb_count.html')
            text = self.soup().find('div', class_='_3-8x').get_text()
            return text.split(' ')[0]

        def get_driver(self):
            return self.driver

        def recreate_driver(self):
            self.driver.quit()
            self.driver = self.create_driver()

        def get(self, url, wait=2, path=''):
            if not url:
                raise Exception('Browser.goto:url is None')
            try:
                start_time = time.time()
                self.driver.get(url)
                elapsed_time = time.time() - start_time
                if wait > 0 and path == '':
                    time.sleep(wait)
                if wait > 0 and path != '':
                    element = WebDriverWait(self.driver, wait).until(
                        EC.presence_of_element_located((By.XPATH, path)))
                #if elapsed_time > 60:
                #    print('Browser._goto:self.driver.get is more than 60 seconds:{}:{}'.format(elapsed_time, url))
                print('Browser._goto:self.driver.get seconds:{}:{}'.format(elapsed_time, url))
                return self.driver.page_source
            except:
                traceback.print_exc(file=sys.stdout)
                print(url)
                self.recreate_driver()
                return self.get(url, wait, path)   

        def goto(self, url, wait=2, tag=''):
            if not url:
                raise Exception('Browser.goto:url is None')
            try:
                html = self._goto(url, wait, tag)
                return html
            except:
                traceback.print_exc(file=sys.stdout)
                print(url)
                self.recreate_driver()
                return self.goto(url, wait, tag)

        def _goto(self, url, wait, tag):
            start_time = time.time()
            self.driver.get(url)
            elapsed_time = time.time() - start_time
            if wait > 0 and tag == '':
                time.sleep(wait)
            if wait > 0 and tag != '':
                element = WebDriverWait(self.driver, wait).until(
                    EC.presence_of_element_located((By.TAG_NAME, tag))
                )
            #if elapsed_time > 60:
            #    print('Browser._goto:self.driver.get is more than 60 seconds:{}:{}'.format(elapsed_time, url))
            print('Browser._goto:self.driver.get seconds:{}:{}'.format(elapsed_time, url))
            return self.driver.page_source

        def switch_to(self, url):
            self.driver.switch_to.frame(url)

        def output(self, filename):
            html = self.driver.page_source
            with open(filename, 'w') as f:
                f.write(html)

        def screenshot(self, filename='facebook.png'):
            self.driver.save_screenshot(filename)

        def oldclick(self, element):
            element_xpath = transform_xpath(element)
            element = WebDriverWait(self.driver, 20).until(
                EC.element_to_be_clickable((By.XPATH, element_xpath))
            )
            element.click()
            #self.driver.find_element(By.XPATH, element_xpath).click()
            time.sleep(1)
            return self.driver.page_source

        def click(self, element):
            self.driver.execute_script("arguments[0].click();", element)
            return self.driver.page_source

        def _click(self, element):
            element.click()

        def html(self):
            return self.driver.page_source

        def url(self):
            return self.driver.current_url

        def soup(self):
            return BeautifulSoup(self.driver.page_source, 'html.parser')

        def quit(self):
            self.driver.quit()

        def beautifulsoup_to_xpath(self, element):
            return transform_xpath(element)

        def find_element_by_xpath(self, path):
            return self.driver.find_element_by_xpath(path)

        def find_elements_by_xpath(self, path):
            return self.driver.find_elements_by_xpath(path)

        def find_element_by_tag(self, tag):
            return self.driver.find_element_by_tag_name(tag)

        def get_attribute(self, element, attribute):
            return element.get_attribute(attribute)

        def move_to_element(self, element):
            #actions = ActionChains(self.driver)
            #actions.move_to_element(element).perform()
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
            return self.driver.page_source
        
        def get_window_screen_height(self):
            return self.driver.execute_script("return window.screen.availHeight") 
        
        def get_dicumentelement_scrolltop(self):
            return self.driver.execute_script("return document.documentElement.scrollTop")  
        
        def scroll_customized_height(self,height):
            return self.driver.execute_script("window.scrollTo(0," + str(height)+ ");")

        def scrollDown(self, scroll=-1, pause=1):
            try:
                url = self.url()
                return self._scrollDown(scroll, pause)
            except WebDriverException:
                traceback.print_exc(file=sys.stdout)
                self.recreate_driver()
                self.goto(url)
                return self.scrollDown(scroll, pause)

        def _scrollDown(self, scroll=-1, pause=1):
            """
            Function to scroll down till end of page.
            """
            import time
            lastHeight = self.driver.execute_script("return document.documentElement.scrollHeight")
            print('Browser Last Height: %d' % lastHeight)
            i = 0
            while True:
                self.driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
                time.sleep(pause)
                newHeight = self.driver.execute_script("return document.documentElement.scrollHeight")
                print('Browser New Height: %d' % newHeight)
                if newHeight == lastHeight:
                    break
                if scroll > 0 and i >= scroll:
                    break
                lastHeight = newHeight
                i = i + 1
            return lastHeight

        def check_ip(self):
            url = 'http://lumtest.com/myip.json'
            self.driver.get(url)
            html = self.driver.page_source
            print('my ip: {}'.format(html))

        def fb_scrollDown_click_button(self, pause=1):
            """
            Function to scroll down till end of page and press buttons.
            """
            lastHeight = self.driver.execute_script("return document.documentElement.scrollTop")
            print('Browser Last scrollTop: %d' % lastHeight)  
            window_height = self.driver.execute_script("return window.screen.availHeight") 
            i=0
            while True:
                #顯示討論串的另1則回覆
                try:
                    buttons = self.driver.find_elements_by_xpath("//*[@class=' _50f7 _2iep']")
                    for bt in buttons:
                        try:
                            bt.click()
                            time.sleep(pause)
                        except:
                            pass
                except:
                    pass
                #查看更多
                try:
                    buttons = self.driver.find_elements_by_xpath("//*[@class='_5v47 fss']")
                    for bt in buttons:
                        bt.click()
                except:
                    pass
                self.driver.execute_script("window.scrollTo(0," + str(window_height*(i+1)) + ");")
                #載入其他10則留言
                try:
                    self.driver.find_element_by_class_name('_5o4h').click()
                    clk = 1
                except:
                    clk = 0
            
                time.sleep(pause)
                newHeight = self.driver.execute_script("return document.documentElement.scrollTop")
                print('Browser New scrollTop: %d' % newHeight)
                if newHeight == lastHeight and clk == 0:
                    break
                lastHeight = newHeight
                i = i + 1

    def setup(self):
        print('start browser')
        try:
            self.instance = Browser.__Browser(
                **self.container.config
            )
        except:
            traceback.print_exc(file=sys.stdout)

    def get_dependency(self, worker_ctx):
        return self.instance
