#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback
import requests
import sys
from bs4 import BeautifulSoup


class ETTODAY:

    def __init__(self, server):
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def send_xhr(self, url, page):
        json_dic = {
            r'%E6%94%BF%E6%B2%BB': '1.json',
            r'%E7%A4%BE%E6%9C%83': '6.json',
            r'%E5%9C%8B%E9%9A%9B': '2.json',
            r'%E5%A4%A7%E9%99%B8': '3.json',
            r'%E8%B2%A1%E7%B6%93': '104.json',
            r'%E7%94%9F%E6%B4%BB': '5.json'
        }
        board = url.split('/')[-2]
        response  = self.session.post(
            url = 'https://www.ettoday.net/show_roll.php', 
            data = {
                'offset': page,
                'tPage': 2, 
                'tFile': json_dic.get(board)
            }, 
            headers = {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15',
                'Referer': url,
            }
        )
        return response.text 

    def on_MNG2CRW_NTF_ETTODAY_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        try:
            html_list = []
            for i in range(1, 11):
                html_list.append(self.send_xhr(url, i))
            
            # get latest_post_date in newest post
            latest_page = html_list[0]
            soup = BeautifulSoup(latest_page, "lxml") 
            prefix = 'https://www.ettoday.net'   
            page = [prefix + i['href'] for i in soup.select('a')]
            latest_url = list(set(page))[0]
            
            def parse_datetime(soup):
                try:
                    datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
                except:
                    datetime = soup.find_all('time', {'class': 'news-time'})[0]['datetime']
                datetime = datetime[:22]+datetime[-2:] #remove ':'
                return datetime
        
            latest_html = requests.get(latest_url).text
            soup = BeautifulSoup(latest_html, 'lxml')
            latest_post_date = parse_datetime(soup)


            html = {'html': html_list, 'latest_post_date': latest_post_date}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
        
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_ETTODAY_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # detail_html = self.server.browser.goto(url)
            detail_html = requests.get(url).text
            
            # fb_like_count_element = self.server.browser.find_element_by_xpath('//*[@id="star"]/div[3]/div[2]/div[5]/div/div/div[1]/article/div/div[3]/div[2]/div/span/iframe')
            # fb_like_count_src = self.server.browser.get_attribute( fb_like_count_element, 'src')
            
            # fb_comment_element = self.server.browser.find_element_by_xpath('//*[@title="fb:comments Facebook Social Plugin"]')
            # fb_comment_src = self.server.browser.get_attribute( fb_comment_element, 'src')

            # fb_like_count_html = self.server.browser.goto(fb_like_count_src)
            
            # self.server.browser.get(fb_comment_src)
            # self.server.browser.fb_scrollDown_click_button()
            # fb_comment_html = self.server.browser.html()

            html = {'html': detail_html, 'fb_like_count':0,  
                 'comment_html':0, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
