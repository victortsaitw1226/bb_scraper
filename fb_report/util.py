import json
import random

def binary2json(binary_input):
    string = binary_input.decode().split("for (;;);")[-1]
    json_output = json.loads(string)
    return json_output

def random_sec():
    return random.choice([0.5, 1.2, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5])
