#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_PTT_GET_POST(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_PTT_GET_POST, self).__init__('MNG2CRW_NTF_PTT_GET_POST')
        self.update({
          'url': '',
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')
