#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_YOUTUBE_VIDEO_LIST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_YOUTUBE_VIDEO_LIST, self).__init__('CRW2PAR_NTF_YOUTUBE_VIDEO_LIST')
        self.update({
          'url': '',
          'channel': '',
          'content': '',
          'days_limit': 0
        })
    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_channel(self, channel):
        self.set('channel', channel)

    def get_channel(self):
        return self.get('channel')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
