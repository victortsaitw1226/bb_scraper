#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_FB_POST_URLS(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_FB_POST_URLS, self).__init__('CRW2PAR_NTF_FB_POST_URLS')
        self.update({
          'fan_page': '',
          'post_urls': []
        })

    def set_post_urls(self, post_urls):
        self.set('post_urls', post_urls)

    def get_post_urls(self):
        return self.get('post_urls')

    def set_fan_page(self, fan_page):
        self.set('fan_page', fan_page)

    def get_fan_page(self):
        return self.get('fan_page')
