# -*- coding: utf-8 -*-
#from scrapy_redis.spiders import RedisSpider
from .redis_spiders import RedisSpider

class ExampleSpider(RedisSpider):
    name = "example"

    def parse(self, response):

        return {
            'name': response.css('title::text').extract_first(),
            'url': response.url,
            'priority': response.request.priority
        }
