#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class MATCH:
    def __init__(self, server):
        print('init match.py')
        self.server = server

    def on_CRW2PAR_NTF_MATCH_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_match_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        # print(str(to_notify))
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_MATCH_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_match_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_match_list(self, content):

        def parse_next_page(soup):
            prefix_html = 'http://m.match.net.tw'
            suffix_url = soup.find("div", "tab-pager").find("a")["href"]
            current_page = int(soup.find("li", "active").text)
            suffix_url = re.sub("list.(\d+)", "list/{}".format(current_page + 1), suffix_url)
            next_page = prefix_html + suffix_url
            return next_page

        def parse_links_dates(soup):
            prefix_html = 'http://m.match.net.tw'
            data = {'next_page': None, 'latest_post_date': None, 'links': []}
            links = []
            dt_list = []
            news=soup.find("div","single-con1").find_all('li')
            for ind in range(len(news)):
                suffix = news[ind].find("a")['href']
                link = prefix_html + suffix
                links.append({"url": link, "matadata": {}})

                date_=news[ind].find("span","date").text
                date_=re.search("(\d+月\d+日.星期[\u4E00-\u9FFF].[\u4E00-\u9FFF]+.\d+.\d+)",date_).group()
                map_table =  {"星期一": "Mon" ,
                "星期二": "Tue",
                "星期三": "Wed",
                "星期四": "Thu",
                "星期五": "Fri",
                "星期六": "Sat",
                "星期日": "Sun",
                "下午":"PM",
                "上午":"AM"}
                res = ' '.join([map_table.get(i, i) for i in date_.split()])
                year = datetime.today().year
                dt=datetime.strptime(str(year)+res,"%Y%m月%d日 %a %p %I:%M")
                dt_list.append(dt)
            
            data["links"] = links
            data["time"] = max(dt_list).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return data

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])
        return item

    def parse_match_content(self, content):
        
        def parse_datetime(soup):
            published_time = soup.find(
                    "meta", {"property": "article:published_time"})["content"]
            published_time = published_time[:-1] + "+0800"
            return published_time

        def parse_author(soup):
            try:
                writer=soup.find("p","date").text
                writer =writer.split("\xa0\xa0")[0]
            except:
                writer = ''
            return writer

        def parse_title(soup):
            return soup.h3.text.strip().replace(u'\u3000',
                                            u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            content=''
            content_sec = soup.select('#content-main .newbox.stock li .txt')      
            content = '\n'.join([x.text for x in content_sec ])   
            content =  '\n'.join(content.split())
            return content

        def parse_metadata(soup):
            try:
                updated_time = soup.find(
                    "meta", {"property": "article:modified_time"})["content"]
                # print(updated_time)
                updated_time = updated_time + "+0800"

            except:
                updated_time = ""

            category = soup.find('div','newbox-hd').find('h2').text

            return {"update_time": updated_time, "category": category}


        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        return item
