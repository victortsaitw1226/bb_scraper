# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId
from googletrans import Translator
import datetime

from pymongo import MongoClient


# 用不同區的 proxy 所抓的資料會有不同

def parse(data):
    source_id = str(data.get('_id', ''))
    docurl = data.get('url', '')
    pdate = datetime.datetime.fromtimestamp(data['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
    
    try:
        docboard = data['fan_page']
    except:
        docboard = ''    

    comment_body = []
    
    ts = Translator()
    
    # Comment Counts
    if data['comments_count'] != '':
        if ('K' in data['comments_count']) or ('k' in data['comments_count']):
            try:
                comment_count = int(float(data['comments_count'].split('K')[0])*1000)
            except:
                comment_count = int(float(data['comments_count'].split('k')[0])*1000)

        elif ('M' in data['comments_count']) or ('m' in data['comments_count'].split(' ')[0]):
            try:
                comment_count = int(float(data['comments_count'].split('M')[0])*1000000)
            except:
                comment_count = int(float(data['comments_count'].split('m')[0])*1000000)

        else:
            comment_count = int(data['comments_count'].split(' ')[0])
    else:
        comment_count = 0

    # Shares
    if data['shares_count'] == '':
        share = 0
    else:
        if ('K' in data['shares_count']) or ('k' in data['shares_count']):
            try:
                share = int(float(data['shares_count'].split('K')[0])) * 1000
            except:
                share = int(float(data['shares_count'].split('k')[0])) * 1000

        elif ('M' in data['shares_count']) or ('m' in data['shares_count']):
            try:
                share = int(float(data['shares_count'].split('M')[0])) * 1000000
            except:
                share = int(float(data['shares_count'].split('m')[0])) * 1000000
        else:
            share = int(data['shares_count'].split(' ')[0])

    # Reaction Count

    if data['reactions_count'] == '' :
        reaction = 0        

    elif ('K' in data['reactions_count']) or ('k' in data['reactions_count']):
        try:
            reaction = int(float(data['reactions_count'].split('K')[0])) * 1000
        except:
            reaction = int(float(data['reactions_count'].split('k')[0])) * 1000

    elif ('M' in data['reactions_count']) or ('m' in data['reactions_count']):
        try:
            reaction = int(float(data['reactions_count'].split('M')[0])) * 1000000
        except:
            reaction = int(float(data['reactions_count'].split('m')[0])) * 1000000
    else:
        reaction = int(data['reactions_count'])


    cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    content = data.get('post', '')
    title = content[:30]
    body = dict({
        "data_id": source_id,
        "postauthor": data["fan_page"],
        "Author_ID": data["fan_page"],
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": docboard,
        "content": content,
        "media": data["media"],
        "SourceType": data["media"],
        "docurl": docurl,
        "PostDate": pdate or cdate,
        "contentType": 0,
        "candidate": '',
        "mediatype": 2,
        "datafrom": 2,
        "title": title,
        "brocount": comment_count + reaction + share,
        "repcount": comment_count,
        "datatype": 0,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2
    })

    if len(data['comments']) > 0:
        for comment in data['comments']:
            content = comment.get('text', '')
            pdate = datetime.datetime.fromtimestamp(comment['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
            dummy_body = dict({
                "data_id": source_id,
                "postauthor": comment.get("author", ''),
                "Author_ID": comment.get("author", ''),
                "createdate": cdate,
                "docid": source_id,
                "MainID": source_id,
                "docboard": docboard,
                "content": content,
                "media": data.get("media", ''),
                "SourceType": data.get("media", ''),
                "docurl": docurl,
                "PostDate": pdate or cdate,
                "contentType": 1,
                "candidate": '',
                "mediatype": 2,
                "datafrom": 2,
                "title": title + '::' + content[:50],
                "brocount": 0,
                "repcount": len(comment['replies']),
                "datatype": 0,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 2
            })
            comment_body.append(dummy_body)
            
            if comment['replies']:
                for reply in comment['replies']:
                    content = reply.get("text", '')
                    pdate = datetime.datetime.fromtimestamp(comment['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
                    dummy_reply_body = dict({
                        "data_id": source_id,
                        "postauthor": reply.get("author", ''),
                        "Author_ID": reply.get("author", ''),
                        "createdate": cdate,
                        "docid": source_id,
                        "MainID": source_id,
                        "docboard": docboard,
                        "content": content,
                        "media": data.get("media",''),
                        "SourceType": data.get("media", ''),
                        "docurl": docurl,
                        "PostDate": pdate or cdate,
                        "contentType": 1,
                        "candidate": '',
                        "mediatype": 2,
                        "datafrom": 2,
                        "title": title + '::' + content[:50],
                        "brocount": 0,
                        "repcount": 0,
                        "datatype": 0,
                        "posscore": 0,
                        "negscore": 0,
                        "evaluation": 2
                    })
                    comment_body.append(dummy_reply_body)
            
    comment_body.append(body)
    return comment_body



if __name__ == '__main__':
    url = '210.202.47.189:27017'
    db = MongoClient(url, username = 'ibdo', password = 'ibdo2018').get_database('history_v2')

    str_time_start = '2019-07-01T00:00:00.000Z'
    str_time_end = '2019-07-10T23:59:59.000Z'

    query = {'$and': [
            { 'createdAt': {'$gte': str_time_start} },
            { 'createdAt': {'$lt': str_time_end} }
    ]}

    records = []
    for datas in db.data.find({'media' : 'facebook'}):
        records.append(datas)
    
    # print(records[76:78])

    for shit in range(101,200):
        try:
            print(shit)
            print(parse(records[shit]))  
        except Exception as e:
            print(e)

    # for shit in range(0,3000):
    #     print(shit)
    #     print(parse(records[shit]))
