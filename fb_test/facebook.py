#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
import re
import sys
import time
from  datetime import datetime, timedelta
from six.moves import urllib
from bs4 import BeautifulSoup
from browser import Browser

class Facebook:

    def __init__(self, browser):
        self.browser = browser
   
    '''
    post content and comments
    '''
    def crawl_post_urls(self, url, days_limit):
        #self.check_ip()
        post_urls = []
        self.goto_and_validate_url(url)
        self.browser.scrollDown(15)
        soup = BeautifulSoup(self.browser.html(), 'html.parser')

        if soup is None:
            print('crawl_post_urls:html is empty:{}'.format(url))
            return post_urls

        profile_id = soup.find('meta', {'property': 'al:ios:url'}).get('content')
        profile_id = profile_id.split('fb://page/?id=')[1]
        
        #posts = soup.findAll('div', {'role': 'article'})
        posts = soup.findAll('div', class_= re.compile('userContentWrapper'))
        # Loop All Posts
        for post in posts:
            #print(post)
            #print('-' * 60)
            #content_wrapper = post.find('div', class_= re.compile('userContentWrapper'))
            #if not content_wrapper:
            #    print('[FACEBOOK.crawl_post_urls]content_wrapper is None:{}'.format(url))
            #    continue
            timestamp_content = post.find('span', class_='timestampContent')

            if timestamp_content is None:
                print('[FACEBOOK.crawl_post_urls]timstampContent is None:{}'.format(url))
                continue

            timestamp_content = timestamp_content.parent

            if timestamp_content is None:
                print('[FACEBOOK.crawl_post_urls]timstampContent.parent is None:{}'.format(url))
                continue

            post_utime = timestamp_content.get('data-utime')
            post_utime = int(post_utime)
            post_datetime = datetime.fromtimestamp(post_utime)
            if post_datetime < datetime.now() - timedelta(seconds=days_limit):
                print('[FACEBOOK.crawl_post_urls]post_datetime is less than days_limit:{}'.format(url))
                continue
        
            comment_item = post.find('form', class_='commentable_item')
            post_tag = comment_item.find('input', {'name': 'ft_ent_identifier'})
            if post_tag is None:
                print('[FACEBOOK.crawl_post_urls]post_tag is None:{}'.format(url))
                continue

            post_id = post_tag.get('value')
            post_url = 'https://www.facebook.com/{profile}/posts/{post}'.format(profile=profile_id, post=post_id)
            print(post_url)
            post_urls.append({
                'post_url': post_url, 
                'post_utime': post_utime,
                'display_time': post_datetime.isoformat()
            })

        return post_urls


    def check_ip(self):
        ip = self.browser.check_my_ip()


    def crawl_content(self, url):
        # self.check_ip()
        self.goto_and_validate_url(url)
        soup = BeautifulSoup(self.browser.html(), 'html.parser')

        if soup is None:
            raise Exception('[FACEBOOK.crawl_content]:html is empty:{}'.format(url))

        comment_table = soup.find('form', class_ = 'commentable_item')

        if comment_table is None:
            return self.browser.html()
            #raise Exception('crawl_content:comment_table is None:{}'.format(url))

        comment_btn = comment_table.find('a', {'data-testid': 'UFI2CommentsCount/root'})
        if comment_btn:
            element_xpath = self.browser.beautifulsoup_to_xpath(comment_btn)
            btn = self.browser.find_element_by_xpath(element_xpath)
            self.browser.click(btn)

        for i in range(10):
            self.browser.scrollDown()
            soup = BeautifulSoup(self.browser.html(), 'html.parser')

            if soup is None:
                print('[FACEBOOK.crawl_content]:scroll_down:html is empty:{}'.format(url))
                continue

            comment_table = soup.find('form', class_ = re.compile('commentable_item'))

            sub_comments = comment_table.find_all('a', {'data-testid': 'UFI2CommentsPagerRenderer/pager_depth_1'})
            sub_comments = sub_comments[:10]
            for sub_comment in sub_comments:
                element_xpath = self.browser.beautifulsoup_to_xpath(sub_comment)
                btn = self.browser.find_element_by_xpath(element_xpath)
                self.browser.click(btn)

            next_page = comment_table.find('a', {'data-testid': 'UFI2CommentsPagerRenderer/pager_depth_0'})
            if not next_page:
                break

            element_xpath = self.browser.beautifulsoup_to_xpath(next_page)
            btn = self.browser.find_element_by_xpath(element_xpath)
            self.browser.click(btn)

        return self.browser.html()


    def goto_and_validate_url(self, url, max_retry=3):
        for i in range(max_retry):
            ip = self.browser.getProxyAddress()
            html = self.browser.goto(url, 10, 'div')

            soup = BeautifulSoup(html, 'html.parser')
            div = soup.find('div')
            if div is None:
                self.browser.recreate_driver()
                continue

            captcha = soup.find('div', {'id': "captcha"})
            if captcha:
                self.browser.recreate_driver()
                continue

            return html
    
    def parse_content(self, url, html):
        result = {
          'post': '',
          'post_date': '',
          'reactions_count': '',
          'comments_count': '',
          'shares_count': '',
          'comments': []
        }
        soup = BeautifulSoup(html, 'html.parser')

        content_wrapper = soup.find('div', class_= re.compile('userContentWrapper'))

        if not content_wrapper:
            print('[Facebook:parse_content]content_wrapper is None:{}'.format(url))

        # Parse Content
        post = content_wrapper.find('div', {'data-testid': 'post_message'})
        if not post:
            print('[Facebook:parse_content]post is None:{}'.format(url))

        result['post'] = post.get_text()

        post_times = content_wrapper.find('span', class_ = 'timestampContent')
        if not post_times:
            print('[Facebook:parse_content]post_times is None:{}'.format(url))

        result['post_date'] = post_times.get_text()

        reactions_count = content_wrapper.find('span', {'data-testid': 'UFI2ReactionsCount/sentenceWithSocialContext'})
        if reactions_count:
            result['reactions_count'] = reactions_count.get_text()

        comment_count = content_wrapper.find('a', {'data-testid': 'UFI2CommentsCount/root'})
        if comment_count:
            result['comments_count'] = comment_count.get_text()

        share_count = content_wrapper.find('a', {'data-testid': 'UFI2SharesCount/root'})
        if share_count:
            result['shares_count'] = share_count.get_text()

        # Parse Comments
        comments = content_wrapper.find('div', {'data-testid': 'UFI2CommentsList/root_depth_0'})
        if not comments:
            return result

        comments = comments.find('ul')
        if not comments:
            print('[Facebook:parse_content]Second Comments is None:{}'.format(url))
      
        comments = comments.find_all('li', recursive=False)
        for comment in comments:
            comment_item = {
              'author': '',
              'text': '',
              'replies': []
            }
            comment_body = comment.find('div', {'data-testid': 'UFI2Comment/body'})
            if not comment_body:
                print('Facebook:comment_body is None:{}'.format(url))

            comment_author = comment_body.find(class_= "_6qw4")
            if not comment_author:
                print('Facebook:comment_author is None:{}'.format(url))

            comment_author_name = comment_author.get_text()
            comment_author.extract()
            comment_item['author'] = comment_author_name
            comment_item['text'] = comment_body.get_text()
            comment_utime = comment.find('abbr', class_='livetimestamp').get('data-utime')
            comment_item['post_utime'] = int(comment_utime)

            comment_link_tag = comment.find('ul', {'data-testid': 'UFI2CommentActionLinks/root'})
            comment_link = comment_link_tag.find('a').get('href')
            comment_item['url'] = comment_link

            #print('comment: %s' % comment.find('div', {'data-testid': 'UFI2Comment/body'}).get_text())
            sub_comments = comment.find_all('div', {'data-testid': 'UFI2Comment/root_depth_1'})
            for sub_comment in sub_comments:
                sub_comment_item = {
                  'author': '',
                  'text': ''
                }
                sub_comment_body = sub_comment.find('div', {'data-testid': 'UFI2Comment/body'})

                if not sub_comment_body:
                    print('Facebook:sub_comment_body is None:{}'.format(url))

                sub_comment_author = sub_comment_body.find(class_= "_6qw4")

                if not sub_comment_author:
                    print('Facebook:sub_comment_author is None:{}'.format(url))

                sub_comment_author_name = sub_comment_author.get_text()
                sub_comment_author.extract()
                sub_comment_item['author'] = sub_comment_author_name
                sub_comment_item['text'] = sub_comment_body.get_text()
                sub_comment_utime = sub_comment.find('abbr', class_='livetimestamp').get('data-utime')
                sub_comment_item['post_utime'] = int(sub_comment_utime)
                
                sub_comment_link_tag = sub_comment.find('ul', {'data-testid': 'UFI2CommentActionLinks/root'})
                sub_comment_link = sub_comment_link_tag.find('a').get('href')
                sub_comment_item['url'] = sub_comment_link
                comment_item['replies'].append(sub_comment_item)
            result['comments'].append(comment_item)
        return result

if __name__ == '__main__':
    f = Facebook(Browser())
    try:
        urls = f.crawl_post_urls('https://www.facebook.com/twherohan/', 3600 * 24 * 2)
        print(urls)
        for url in urls:
            html = f.crawl_content(url['post_url'])
            print(f.parse_content(url['post_url'], html))
            print('-' * 70)
            print('-' * 70)
            print('-' * 70)
    except:
        traceback.print_exc(file=sys.stdout)
    
