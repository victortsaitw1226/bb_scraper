#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class NEWS_PARSE_ITEM(Protocol):

    def __init__(self):
        super(NEWS_PARSE_ITEM, self).__init__('NEWS_PARSE_ITEM')
        self.update({
            'url': '',
            'article_title': '',
            'author': '',
            'author_url': [],
            'date': '',
            'content': '',
            'comment': [],
            'metadata':{},
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_title(self ,title):
        self.set('article_title', title)

    def get_title(self):
        return self.get('article_title')

    def set_author(self, author):
        self.set('author', author)

    def get_author(self):
        return self.get('author')
    
    def set_author_url(self, author_url):
        self.set('author_url', author_url)

    def get_author_url(self):
        return self.get('author_url')

    def set_date(self, date):
        self.set('date', date)

    def get_date(self):
        return self.get('date')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_comment(self, comment):
        self.set('comment', comment)

    def get_comment(self):
        return self.get('comment')

    def set_metadata(self, metadata):
        self.set('metadata', metadata)

    def get_metadata(self):
        return self.get('metadata')
        