#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class CK101:
    def __init__(self, server):
        print('init ck101.py')
        self.server = server

    def on_CRW2PAR_NTF_CK101_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ck101_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CK101_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        # try:
        #     item = self.parse_ck101_content(content)
        #     # self.server.log.debug(str(item))
        # except Exception as e:
        #     self.server.log.exception(e)
        #     # self.server.log.error(str(from_notify))

        item = self.parse_ck101_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ck101_list(self, content):

        def parse_next_page(html_bs4):
            try:
                url_next = html_bs4.find('a', class_ = 'nxt').attrs['href']
            except:
                url_next = ''
            
            return url_next
        
        # def parse_latest_date(html_bs4):
        #     reg = re.compile(r'normalthread_\d+', re.VERBOSE)
        #     latest_post_date = html_bs4.find('tbody', id = reg).find('span').find('span').text
        #     latest_post_date = datetime.strptime(latest_post_date, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')        
        #     return latest_post_date
        
        def parse_latest_date(html_bs4):
            reg = re.compile(r'normalthread_\d+', re.VERBOSE)
            post_date_list = html_bs4.find_all('tbody', id = reg)

            link_post_date = []
            for i in range(len(post_date_list)):
                post_date = post_date_list[i].find('div','postInfo')
                post_date = post_date.find_all('span')[1].text
                post_date = datetime.strptime(post_date, '%Y-%m-%d %H:%M')
                link_post_date.append(post_date)   

            latest_post_date = max(link_post_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date

        def parse_link(html_bs4):

            def parse_list_meta(post):
                ''' parse url and metadata of news list '''
                
                tmp = post.find('a', class_ = 's xst').attrs['href']
                
                meta = {
                    'url': tmp,
                    'metadata': {}
                }
                return meta
            
            reg = re.compile(r'normalthread_\d+', re.VERBOSE)
            post_entries = html_bs4.find_all('tbody', id = reg)
        
            list_meta = []
            for post in post_entries:
                    list_meta.append(parse_list_meta(post))
            
            return list_meta
        
        clean_html = BeautifulSoup(content, 'lxml')
        news_body =  clean_html.find('div',id = 'ct')
        
        next_p = parse_next_page(news_body)
        latest_post_date = parse_latest_date(news_body)
        list_meta = parse_link(news_body)
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_p)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)
        return item

    def parse_ck101_content(self, detail_content):
        
        def parse_time(clean_html):
            date_org = clean_html.find('span', class_ = 'postDateLine').text[4:]
            date = datetime.strptime(date_org, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date_org, date
        
        def parse_title(clean_html):
            return clean_html.find('h1', id = 'thread_subject').text
        
        def parse_author_url(clean_html):        
            return 'https://ck101.com/' + clean_html.find('a', class_ = 'authorName').attrs['href']
        
        def parse_author(clean_html):        
            return clean_html.find('a', class_ = 'authorName').attrs['title']
        
        def parse_content(clean_html):
            '''
                [content includes ads]
            '''
            reg = re.compile(r'postmessage*')
            
            try:
                tmp = clean_html.find(class_ = reg)
                content = ' '.join(tmp.text.split())
                js = ' '.join([' '.join(j.text.split()) for j in tmp.find_all('script',type = 'text/javascript')])
                content = content.replace(js, '')

            except AttributeError:
                tmp = clean_html.find(id = reg)           
                content = ' '.join(tmp.text.split())
                js = ' '.join([' '.join(j.text.split()) for j in tmp.find_all('script',type = 'text/javascript')])
                content = content.replace(js, '')
            
            content = re.sub(r'(圖片\D+)', '', content)
            content = re.sub(r'\(ads\D+\);', '', content)
            
            return content
   

        def parse_metadata(clean_html):
            ''' parse metadata for article '''
            try:
                keywords = [tag.text for tag in clean_html.find(class_ = 'tagBox').find_all('a')]
            except:
                keywords = []
            
            #scoring = parse_scoring(clean_html)
            scoring = []
            
            metadata = {'category': clean_html.find('div', class_ = 'ts').find('a').attrs['title'],
                        'view_count': clean_html.find(class_ = 'viewNum').text,
                        'reply_count': clean_html.find(class_ = 'replayNum').text,
                        'like_count': clean_html.find(id = 'favoritenumber').text,
                        'thanks_count': clean_html.find(class_ = 'thankNum').text,
                        'agree_count': clean_html.find(id = 'recommendv_add').text,
                        'disagree_count': clean_html.find(id = 'recommendv_subtract').text,
                        'tag': keywords,
                        'scoring': scoring}    
            
            return metadata
        
        
        def parse_comments(clean_html):
            
            def parse_cmt_cmt(k, auth_tmp, cmt_cmt):
                
                id_ = k.attrs['id']
                tmp = k.find(class_ = 'postDateLine').text[4:]
                date = datetime.strptime(tmp, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')           
                author_url = 'https://ck101.com/' + k.find('a', class_ = 'authorName').attrs['href']
                author = k.find('a', class_ = 'authorName').attrs['title']
                metadata = []
                
                # content
                quote = k.find(class_ = 'quote').text.split()
                quoted_author = quote[0]
                quote = ' '.join(quote)
                content_ = ' '.join(k.find('td', class_ = 't_f').text.split()).replace(quote, '')
                
                cmt_cmt_dict = {'comment_id': id_,
                                'datetime': date,
                                'author': author,
                                'author_url': author_url,
                                'content': content_,
                                'metadata': metadata}
                
                cmt_cmt[auth_tmp.index(quoted_author)].append(cmt_cmt_dict)
                
                return cmt_cmt
            
            def parse_metadata_cmt(cmt_html):
                ''' parse metadata for comments '''
                
                #scoring = parse_scoring(cmt_html)
                #return {'scoring': scoring}
                return {}
            

            id_ = []; date = []; author_url = []; author = []; _content = []; metadata = []
            cmt_cmt = []; auth_tmp = []
            
            p = 1
            # print(type(content['detail_html']))
            html_ls = detail_content['detail_html']
            # print(len(html_ls))
            for html_next in html_ls:
                reg = re.compile(r'post_\d+')
                if p == 1:
                    cmt_html = clean_html.find_all('div', id = reg)[1:]
                else:
                    clean_html = BeautifulSoup(html_next, 'lxml')  
                    cmt_html = clean_html.find_all('div', id = reg)
                    
        
                for k in cmt_html:
                    try:
                        ''' 
                            if it is comment of comment (quote exists), 
                            append it under normal comments 
                        '''
                        cmt_cmt = parse_cmt_cmt(k, auth_tmp, cmt_cmt)
                        auth_tmp.append(k.find('a', class_ = 'authorName').text)
                        continue
                        
                    except:
                        ''' normal comments  '''
                        cmt_cmt.append([])
                        id_.append(k.attrs['id'])
                        tmp = k.find(class_ = 'postDateLine').text[4:]
                        date.append(datetime.strptime(tmp, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800'))
                        author.append(k.find('a', class_ = 'authorName').attrs['title'])
                        author_url.append('https://ck101.com/' + k.find('a', class_ = 'authorName').attrs['href'])
                        auth_tmp.append(k.find('a', class_ = 'authorName').text)
                        _content.append(' '.join(k.find('td', class_ = 't_f').text.split()))
                        metadata.append(parse_metadata_cmt(k))
                p += 1

                

            cmt = []
            for i in range(len(id_)):
                cmt.append({'comment_id': id_[i],
                            'datetime': date[i],
                            'author': author[i],
                            'author_url': author_url[i],
                            'content': _content[i],
                            'metadata': metadata[i],
                            'source': 'ck101',
                            'comments': cmt_cmt[i]})
            return cmt

        # detail
        clean_html = BeautifulSoup(detail_content['detail_html'][0], 'lxml')
        url = detail_content['url']
    
        date_org, date = parse_time(clean_html)
        title = parse_title(clean_html)
        author_url = parse_author_url(clean_html)
        author = parse_author(clean_html)
        content = parse_content(clean_html) 
        metadata = parse_metadata(clean_html)
        comment = parse_comments(clean_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_author_url(author_url)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
