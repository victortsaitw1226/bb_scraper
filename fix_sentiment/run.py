from elasticsearch import Elasticsearch
import argparse
import requests
from datetime import datetime, timedelta

def evaluate(body):
    url = 'http://35.236.135.46:9002/predict'

    value = {
        'sentences': [body]        
    }
    response = requests.post(url, json=value)
    j = response.json()
    return j.get('results', [0])[0]


def get_data_by_media(es, index, start, end):
    query = {
      "query": {
        "range": {
          "PostDate": {
            "gte": start,
            "lte": end
          }
        }
      }
    }
    print(query)
    data = es.search(index=index, body=query, size = 1000,
            _source=['content'],
            request_timeout=40, scroll='1m')

    sid = data['_scroll_id']
    scroll_size = len(data['hits']['hits'])
    i = 0
    hits = []
    while scroll_size > 0:
        _sources = data['hits']['hits']
        hits.extend(data['hits']['hits'])
        data = es.scroll(scroll_id=sid, scroll='1m',request_timeout=40)
        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])

    for i in range(0, len(hits)):
        _data = hits[i]

        i = i + 1
        _id = _data['_id']
        _source = _data['_source']
        evaluation = evaluate(_source['content'])
        print(i, _id, evaluation)
        doc = {
            "doc": {
                "evaluation": evaluation 
            }
        }
        es.update(index='crawl_new', doc_type='data', body=doc, id=_id, request_timeout=40)

if __name__ == '__main__':
    es = Elasticsearch(['210.202.47.189:9200'],http_auth=('elastic','ibdo2018'))
    current = datetime.now()
    for i in range(16, 0, -1):
        last_date = current - timedelta(hours=24 * i)
        last_date_text = last_date.strftime('%Y-%m-%d')

        for h in range(0, 24):
            if h < 10:
                start = last_date_text + 'T0%s:00:00' % h
                end = last_date_text + 'T0%s:59:59' % h
            else:
                start = last_date_text + 'T%s:00:00' % h
                end = last_date_text + 'T%s:59:59' % h
            get_data_by_media(es, 'crawl_new', start, end)
        


