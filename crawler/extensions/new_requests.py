import traceback
import sys
from nameko.extensions import DependencyProvider
from .utils.redis_agent import RedisAgent
from fake_useragent import UserAgent
import requests


class New_Requests(DependencyProvider):
    def setup(self):
        print('New_Requests setup')
        self.proxy = None
        try:
            self.url = self.container.config['REDIS_URL']
            self.port = self.container.config['REDIS_PORT']
            self.db = self.container.config['REDIS_DB']
            self.password = self.container.config['REDIS_PASSWORD']
            self.redis = RedisAgent(self.url, self.port, self.db, self.password)
        except:
            traceback.print_exc(file=sys.stdout)

    def check_ip(self, headers, proxies):
        url = 'https://api.ipify.org'
        response = requests.get(
            url, headers=headers, proxies=proxies, timeout=(3, 3))
        print('My public IP address is: {}'.format(response.text))

    def get_dependency(self, worker_ctx):
        def goto(url, detect_error, max_retry=10):
            print('goto_and_validate_url: ', url)
            html = ''
            if self.proxy is None:
                self.proxy = self.redis.random_proxy()
            for i in range(max_retry):
                print('try ', i)
                ua = UserAgent(verify_ssl=False, use_cache_server=False)
                fake_ua = ua.random
                headers = {'user-agent': fake_ua}
                proxies = {
                    'http': 'http://{}'.format(self.proxy),
                    'https': 'https://{}'.format(self.proxy)
                }
                
                try:
                    response = requests.get(
                        url, headers=headers, proxies=proxies, timeout=(4, 4))
                    response.encoding = response.apparent_encoding
                    # response.encoding = 'utf-8'
                    html = response.text

                    # for detecting error (anti-crawler) page
                    if detect_error(html):
                        break
                    else:
                        print('[encountering anti-crawler page] error:{}:{}:'.format(
                            url, self.proxy))
                        self.proxy = self.redis.random_proxy()
                        html = ''
                        continue
                except Exception as e:
                    print('error:{}:{}:{}:html:{}'.format(
                        url, str(e), self.proxy, html))
                    self.proxy = self.redis.random_proxy()
            return html
        return goto
