from bs4 import BeautifulSoup


def youtbe_parse_comments(content):
    print('parser youtube parse_comments')
    soup = BeautifulSoup(content, 'html.parser')
    comment_thread = soup.find_all('ytd-comment-thread-renderer')
    #print(comment_thread)
    results = []
    for comment in comment_thread:
        #print(comment)
        post = comment.find('ytd-comment-renderer', {'id':'comment'})
        item = dict()
        item['author'] = post.find('a', {'id': 'author-text'}).get_text()
        item['content'] = post.find('div', {'id': 'content'}).get_text()
        item['date'] = post.find('yt-formatted-string', {'class': 'published-time-text'}).get_text()
        item['replies'] = []

        replies = comment.find('div', {'id': 'replies'})
        if not replies:
            results.append(item)
            continue

        replies = replies.find_all('ytd-comment-renderer')
        reply_results = []
        for reply in replies:
            reply_item = dict()
            reply_item['author'] = reply.find('a', {'id': 'author-text'}).get_text()
            reply_item['content'] = reply.find('div', {'id': 'content'}).get_text()
            reply_item['date'] = reply.find('yt-formatted-string', {'class': 'published-time-text'}).get_text()
            reply_results.append(reply_item)
        item['replies'] = reply_results
        results.append(item)
    return results


def parse_content(html):
    result = {
        'post': '',
        'reactions_count': '',
        'comments_count': '',
        'shares_count': '',
        'comments': [],
    }
    soup = BeautifulSoup(html, 'html.parser')

    # Parse Content
    result['post'] = soup.find('div', {'data-testid': 'post_message'}).get_text()

    reactions_count = soup.find('span', {'data-testid': 'UFI2ReactionsCount/sentenceWithSocialContext'})
    if reactions_count:
        result['reactions_count'] = reactions_count.get_text()

    comment_count = soup.find('a', {'data-testid': 'UFI2CommentsCount/root'})
    if comment_count:
        result['comments_count'] = comment_count.get_text()

    share_count = soup.find('a', {'data-testid': 'UFI2SharesCount/root'})
    if share_count:
        result['shares_count'] = share_count.get_text()

    # Parse Comments
    comments = soup.find('div', {'data-testid': 'UFI2CommentsList/root_depth_0'})
    comments = comments.find('ul')
    comments = comments.find_all('li', recursive=False)
    for comment in comments:
        comment_item = {
          'author': '',
          'text': '',
          'replies': []
        }
        comment_body = comment.find('div', {'data-testid': 'UFI2Comment/body'})
        comment_author = comment_body.find(class_= "_6qw4")
        comment_author_name = comment_author.get_text()
        comment_author.extract()
        comment_item['author'] = comment_author_name
        comment_item['text'] = comment_body.get_text()
        #print('comment: %s' % comment.find('div', {'data-testid': 'UFI2Comment/body'}).get_text())
        sub_comments = comment.find_all('div', {'data-testid': 'UFI2Comment/root_depth_1'})
        for sub_comment in sub_comments:
            sub_comment_item = {
              'author': '',
              'text': ''
            }
            sub_comment_body = sub_comment.find('div', {'data-testid': 'UFI2Comment/body'})
            sub_comment_author = sub_comment_body.find(class_= "_6qw4")
            sub_comment_author_name = sub_comment_author.get_text()
            sub_comment_author.extract()
            sub_comment_item['author'] = sub_comment_author_name
            sub_comment_item['text'] = sub_comment_body.get_text()
            comment_item['replies'].append(sub_comment_item)
        result['comments'].append(comment_item)
    return result

with open('expand_comment.html', 'r') as f:
    html = f.read()
    results = parse_content(html)
    print(results)
