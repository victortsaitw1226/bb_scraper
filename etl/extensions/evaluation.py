import traceback
import sys
from nameko.extensions import DependencyProvider
import requests


class Evaluation(DependencyProvider):
    def setup(self):
        self.url = self.container.config['EVALUATION_URL']

    def get_dependency(self, worker_ctx):
        def goto(body):
            value = {
              'sentences': [body]        
            }
            try:
                response = requests.post(self.url, json=value, timeout=0.5)
                j = response.json()
                return j.get('results', [0])[0]
            except:
                traceback.print_exc(file=sys.stdout)
                return -1
        return goto
