#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import json


class WOWNEWS:
    def __init__(self, server):
        print('init wownews.py')
        self.server = server

    def on_CRW2PAR_NTF_WOWNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_wownews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_WOWNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_wownews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_wownews_list(self, content):
        def parse_json(html):
            res = json.loads(html)
            max_page=max(res["pages"])
            return res, max_page

        def handle_next_page(data, url, max_page):
            re_list = re.search(r'page=[0-9]+', url).group().split("=")
            cur_page_num = int(re_list[1])
            if cur_page_num < max_page:
                re_list[1] = str(cur_page_num + 1)
                next = "=".join(re_list)
                data["next_page"] = url.replace(
                    re.search('page=[0-9]', url).group(), next)
            else:
                data["next_page"] = None
                
        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        links = []
        dt_list = []
        res, max_page = parse_json(content['html'])
        handle_next_page(data, content['url'], max_page)
        items = res["results"]
        html_base = "http://www.wownews.tw/newsdetail/"
        for item in items:
            links.append({'url': html_base +item["category"][1]["desc"]+"/"+ item["slug"], 'metadata': {}})
            dt = datetime.strptime(item["publish_start"][:-1],"%Y-%m-%dT%H:%M:%S.%f")#"2019-06-11T20:18:15.114Z"
            dt_list.append(dt)

        data["links"] =links
        data["time"]= max(dt_list).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['time'])
        item.set_urls(data['links'])

        return item

    def parse_wownews_content(self, content):
            
        def parse_datetime(soup):
            time_ =soup.find("span","ng-binding").text
            dt = datetime.strptime(time_, "%Y/%m/%d %H:%M")
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            writer = ''
            return writer

        def parse_title(soup):
            return soup.find("div","title ng-binding").text.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            content=soup.find("div",{"id":"textform"}).text
            return content.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_metadata(soup):
            category=''
            try:
                category = soup.find("span","item ng-binding").text
            except:
                pass
            return {
                'category': category
            }

        # def parse_comments(soup):
        #     pass


        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        #item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        return item
