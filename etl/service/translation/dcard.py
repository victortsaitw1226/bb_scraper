 # -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId
from pymongo import MongoClient

def parse(data):
    source_id = str(data.get('_id', ''))
    forum = data.get('forumAlias', '')
    id_ = data.get('id', '')
    if (forum != '') and (id_ != ''):
        docurl = 'https://www.dcard.tw/f/' + forum + '/p/' + str(id_)
    else:
        docurl = ''
    comment_body = []
    docboard = data.get('forumName', '')
    author = data.get('school', 'anonymous')
    content = data.get('content', '')
    
    # try:
    #     author = data['school']
    # except KeyError:
    #     author = None
    # except Exception as e:
    #     print(('=' * 20) + str(e) + '1')
    # try:
    #     content = data['content']
    # except KeyError:
    #     content = None
    # except Exception as e:
    #     print('=' * 20  + str(e) + '2')

    comment_count = data.get('commentCount', 0)
    like_count = data.get('likeCount', 0)
    title = data.get("title", '')
    body = dict({
        "data_id": source_id,
        "postauthor": author,
        "Author_ID": author,
        "createdate": data.get('createdAt', datetime.datetime.now()),
        "docid": source_id,
        "MainID": str(data.get('id', source_id)),
        "docboard": docboard,
        "content": content,
        "media": "Dcard",
        "SourceTypeV2": data.get("media", ''),
        "SourceType": 0,
        "docurl": docurl,
        "coverurl": '',
        "PostDate": data.get('updatedAt', datetime.datetime.now()),
        "contentType": '0',
        "candidate": '',
        "mediatype": 2,
        "datafrom": 2,
        "title": title,
        "brocount": comment_count + like_count,
        "repcount": comment_count,
        "datatype": 0,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2,
        "candidates": []
    })

    if (comment_count > 0) and (type(data['comments']) == 'list'):
        for comment in data['comments']:

            author2 = comment.get('school', 'anonymous')
            content2 = comment.get('content', '')
            # try:
            #     author = comment['school']
            # except KeyError:
            #     author = None
            # except Exception as e:
            #     print('=' * 20 + str(e) + '3')
            #     print(e)

            # try:
            #     content = comment['content']
            # except KeyError:
            #     content = None
            # except Exception as e:
            #     print('=' * 20 + str(e) + '4')
            #     print(e)

            dummy_body = dict({
                "data_id": source_id,
                "postauthor": author2,
                "Author_ID": author2,
                "createdate": comment.get('createdAt', ''),
                "docid": source_id,
                "MainID": str(comment.get("floor", '')),
                "docboard": docboard,
                "content": content2,
                "media": "Dcard",
                "SourceTypeV2": data.get("media", ''),
                "SourceType": 0,
                "docurl": docurl,
                "coverurl": '',
                "PostDate": comment.get('updatedAt', ''),
                "contentType": '1',
                "candidate": '',
                "mediatype": 2,
                "datafrom": 2,
                "title": title,
                "brocount": 0,
                "repcount": 0,
                "datatype": 0,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 2,
                "candidates": []
            })
            comment_body.append(dummy_body)
    comment_body.append(body)
    return comment_body
