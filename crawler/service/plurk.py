#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from plurk_oauth import PlurkAPI
import traceback
import time
import sys


class PLURK:

    def __init__(self, server):   

        def plurkApiLogin():
            CONSUMER_KEY =  'jmnSbfmM2pmm'
            CONSUMER_SECRET = 'XnjjCEQ6M8uGlMY6Lo7uTbw2AQ8SiE4M'
            ACCESS_TOKEN = 'tYJh887ctMlY'
            ACCESS_TOKEN_SECRET = '0pmhL8NIcuzb1iFqomniT4WAEIuZXLSb'

            _plurk = PlurkAPI(CONSUMER_KEY, CONSUMER_SECRET)
            _plurk.authorize(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
            checkToken = _plurk.callAPI('/APP/checkToken')
            if (checkToken is None):
                print("Your CONSUMER_KEY or CONSUMER_SECRET is wrong!")
                time.sleep(1)
                raise SystemExit
                exit()
            return _plurk

        self.server = server
        self.plurk = plurkApiLogin()


    def on_MNG2CRW_NTF_PLURK_GET_INDEX(self, packet):

        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            _id = url['id']
            time_Offset = url['time_Offset']
            rawJson = self.plurk.callAPI('/APP/Timeline/getPublicPlurks',{'user_id':_id, 'offset':time_Offset, 'limit':30, 'favorers_detail':False, 'limited_detail':False, 'replurkers_detail':False})['plurks']
        
            html = {'plurk_rawJson':rawJson,'user_id':_id}
        except:
            traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_PLURK_GET_POST(self, packet):

        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        plurk_ = from_notify.get_url()

        try:
            replies = self.plurk.callAPI('/APP/Responses/get', {'plurk_id':plurk_['plurk_id']} )['responses']
            url = plurk_['plurk_id']
            html = {'content':plurk_, 'replies':replies, 'url': url}
        except:
            traceback.print_exc(file=sys.stdout)

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
