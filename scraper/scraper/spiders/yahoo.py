# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class YahooSpider(RedisSpider):
    name = "yahoo"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "yahoo",
            "name": "yahoo",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://tw.news.yahoo.com/_td-news/api/resource/IndexDataService.getExternalMediaNewsList;count=10;loadMore=true;mrs=%7B%22size%22%3A%7B%22w%22%3A220%2C%22h%22%3A128%7D%7D;newsTab=politics;start=0;tag=%5B%22yct%3A001000661%22%5D;usePrefetch=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=videoDocking&intl=tw&lang=zh-Hant-TW&partner=none&prid=75lv959eff255&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.1210&returnMeta=true",
            "url_pattern": "https://tw.news.yahoo.com/_td-news/api/resource/IndexDataService.getExternalMediaNewsList;count=10;loadMore=true;mrs=%7B%22size%22%3A%7B%22w%22%3A220%2C%22h%22%3A128%7D%7D;newsTab=politics;start={page};tag=%5B%22yct%3A001000661%22%5D;usePrefetch=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=videoDocking&intl=tw&lang=zh-Hant-TW&partner=none&prid=75lv959eff255&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.1210&returnMeta=true",
            "page": 1,
            "scrapy_key": "yahoo:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        archive = 'https://tw.news.yahoo.com/' + re.split(r'newsTab=(\D*);',response.url)[1] + '/archive'
        data = []
        data.extend(json.loads(response.body)['data'])

        # the end of news_list
        if data==[]:
            return

        # crawl article_url
        for url in data:
            yield response.follow(url['url'],
                    meta = {'category':re.split(r'newsTab=(\D*);',response.url)[1]},
                    callback=self.parse_article)

        # latest datetime
        link_date = []
        for i in range(len(data)):
            link_date.append( datetime.fromtimestamp(data[i]['published_at']) )  
        latest_datetime = max(link_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return
        
        # crawl next page
        _url_pattern = meta['url_pattern']
        meta['page']+=1
        next_page = _url_pattern.format(page=str(meta['page']))
        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)
    

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.text, 'html.parser')
        item = NewsItem()
        title = self.parse_title(soup)
        metadata = self.parse_metadata(soup,meta)
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = title
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = 'yahoo'
        item['proto'] = 'YAHOO_PARSE_ITEM'
        yield item

        # crawl comments
        url_top3 = "https://tw.news.yahoo.com/_td/api/resource/canvass.getMessageListForContext_ns;context={uuid};count=3;index=null;lang=zh-Hant-TW;namespace=yahoo_content;oauthConsumerKey=frontpage.oauth.canvassKey;oauthConsumerSecret=frontpage.oauth.canvassSecret;rankingProfile=;region=TW;sortBy=popular;type=null;userActivity=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=cacheContentCanvas%2CenableCMP%2CenableConsentData%2CenableGDPRFooter%2CenableGuceJs%2CenableGuceJsOverlay%2CvideoDocking%2CnewContentAttribution%2Clivecoverage&intl=tw&lang=zh-Hant-TW&partner=none&prid=c573hbdepajf2&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.26449&returnMeta=true"
        uuid = soup.find('meta',{'name':'apple-itunes-app'})['content'].split('/')[-1]
        meta = {'uuid':uuid,
                'url' : response.url,
                'article_title':title,
                'metadata':metadata,
                'page':-1}
        yield scrapy.Request(url_top3.format(uuid = uuid),
                dont_filter=True,
                meta=meta,
                callback=self.parse_comments)

    def parse_comments(self, response):
        meta = response.meta
        res = json.loads(response.text)

        # no more replies    
        if res['data']['canvassMessages'] ==[]:
            return

        all_msg = [x for x in res['data']['canvassMessages']]
        
        for r in all_msg:
            item = NewsItem()

            reply_metadata = {'category':meta['metadata']['category'],
                'news_source':meta['metadata']['news_source'],
                'like_count':r['reactionStats']['upVoteCount'],
                'dislike_count':r['reactionStats']['downVoteCount'],
                'reply_count':r['reactionStats']['replyCount']}

            item['url'] = meta['url']
            item['metadata'] = reply_metadata
            item['article_title'] = meta['article_title']
            item['author'] = r['meta']['author']['nickname']
            item['author_url'] = []
            item['date'] = datetime.fromtimestamp(r['meta']['createdAt']).strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = r['details']['userText']
            item['content_type'] = 1
            item['media'] = 'yahoo'
            item['proto'] = 'YAHOO_PARSE_ITEM'
            item['comment'] = []
            yield item

        url_message = "https://tw.news.yahoo.com/_td/api/resource/canvass.getMessageListForContext_ns;context={uuid};count=10;index=v%3D1%3As%3Dpopular%3Asl%3D1570065901%3Aoff%3D{count};lang=zh-Hant-TW;namespace=yahoo_content;oauthConsumerKey=frontpage.oauth.canvassKey;oauthConsumerSecret=frontpage.oauth.canvassSecret;rankingProfile=;region=TW;sortBy=popular;type=null;userActivity=true?bkt=news-TW-zh-Hant-TW-def&device=desktop&feature=cacheContentCanvas%2CenableCMP%2CenableConsentData%2CenableGDPRFooter%2CenableGuceJs%2CenableGuceJsOverlay%2CvideoDocking%2CnewContentAttribution%2Clivecoverage&intl=tw&lang=zh-Hant-TW&partner=none&prid=c573hbdepajf2&region=TW&site=news&tz=Asia%2FTaipei&ver=2.0.26471&returnMeta=true"
        count = res['data']['total']['count']
        meta['page'] +=1 
        yield scrapy.Request(url_message.format(uuid = meta['uuid'] ,count= meta['page']*10 +2 ),
                dont_filter=True,
                meta=meta,
                callback=self.parse_comments)

    def parse_title(self,soup):
        title = soup.find('meta',{'property':'og:title'})['content']
        return title 
    
    def parse_datetime(self,soup):
        date = soup.find('time')['datetime']
        date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.000Z')
        date = date + timedelta(hours=8)
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_author(self,soup):
        if soup.find('div','author D(tbc) Va(m) Pend(5px)')!=None:
            author = soup.find('div','author D(tbc) Va(m) Pend(5px)').text
        elif soup.find('span','provider-link')!=None:
            author = soup.find('span','provider-link').text
        elif soup.find('img','auth-logo Mah(70px)')!=None:
            author = soup.find('img','auth-logo Mah(70px)')['title']
        else:
            author = ''
        return author
    
    def parse_metadata(self,soup,meta):            
        if soup.find('a','C(#222)')!=None:
            publisher = soup.find('a','C(#222)').text
        elif soup.find('img','auth-logo Mah(70px)')!=None:
            if 'title' in soup.find('img','auth-logo Mah(70px)').attrs.keys():
                publisher = soup.find('img','auth-logo Mah(70px)')['title']
            else:
                publisher = ''
        else:
            publisher = ''

        medata = {'news_source':publisher,
                  'category':meta['category']}
        return medata
    
    def parse_content(self,soup):
        content = soup.find('article')
        try:
            for span_tag in content.findAll('a'):
                span_tag.clear()
            
            content = content.find_all('p')
            content = ' '.join([x.text for x in content ])
            content = ' '.join(content.split())
            content = re.split(r'更多(.*)新聞',content)[0]
            content = re.split(r'更多(.*)報導',content)[0]
        except:
            content = content.text
        return content
