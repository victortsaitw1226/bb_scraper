#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class FT:
    def __init__(self, server):
        print('init ft.py')
        self.server = server

    def on_CRW2PAR_NTF_FT_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ft_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_FT_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ft_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ft_list(self, content):

        def parse_next_page(soup):
            try:
                next_page_postfix = soup.find(
                    'div', class_='pagination-inner').find_all('a')[-3].get('href')
            except:
                return ''
            return domain_prefix + '/channel/' + next_page_postfix

        def parse_links_dates(soup):
            times = []
            links = []
            # find exact match 'item-headline-link' instead of 'item-headline-link locked'
            # to filter premium only (locked) articles
            
            article_links = soup.find_all('a','item-headline-link')
            article_links = [ li for li in article_links if 'locked' not in str(li)]
            
            if not article_links:
                element = soup.find(lambda tag: tag.name == 'a' and
                                        tag.get('class') == ['item-headline-link locked'])
                element = soup.find('a','item-headline-link locked')
                
                t = datetime.now()
                offset = element.parent.parent.find('div', class_='item-time').text
                if '分鐘前' in offset:
                    times = t - timedelta(minutes=int(offset.split('分鐘前')[0]))
                elif '小時前' in offset:
                    times = t - timedelta(hours=int(offset.split('小時前')[0]))
                elif '天前' in offset:
                    times = t - timedelta(days=int(offset.split('天前')[0]))
                else:
                    time = datetime.strptime(offset, '%Y年%m月%d日')
                return {'time': time, 'links': None, }

            for element in article_links:
                links.append({
                    'url': domain_prefix + element['href'],
                    'metadata': {}
                })

                t = datetime.now()
                try:
                    offset = element.parent.parent.find('div', class_='item-time').text
                except:
                    continue
                if '分鐘前' in offset:
                    times.append(
                        t - timedelta(minutes=int(offset.split('分鐘前')[0])))
                elif '小時前' in offset:
                    times.append(
                        t - timedelta(hours=int(offset.split('小時前')[0])))
                elif '天前' in offset:
                    times.append(
                        t - timedelta(days=int(offset.split('天前')[0])))
                else:
                    times.append(datetime.strptime(offset, '%Y年%m月%d日'))

            return {'time': max(
                times).strftime('%Y-%m-%dT%H:%M:%S+0800'), 'links': links, }

        domain_prefix = 'http://big5.ftchinese.com'
        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_ft_content(self, content):
        
        def parse_datetime(soup):
            time = soup.find('span', class_='story-time')
            date = datetime.strptime(time.text, '更新於%Y年%m月%d日 %H:%M')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            authors = []
            result = soup.find('span', class_='story-author').find_all('a')

            for auth in result:
                authors.append(''.join(auth.text.split()))

            return authors[0]
            # data['metadata']['author'] = authors
            # data['author'] = data['metadata']['author'][0]

        def parse_title(soup):
            return soup.find('h1', class_='story-headline').text

        def parse_content(soup):
            content = []
            content = soup.find('div', id='story-body-container')
            content = ''.join(content.text.split())
            return content

        def parse_metadata(soup):

            def parse_tag(soup):
                tags = []
                tag_content = soup.find_all(class_='story-theme')
                for tag in tag_content[1:]:
                    tags.append(''.join(tag.a.text.split()))
                return tags

            # 文章分類、人氣、關鍵字、分享數
            category = content['url'].split('=')[-1]
            metadata = {
                'tag': parse_tag(soup),
                'category':category
            }
            return metadata

        def parse_comments(soup):
            all_comments = soup.find_all('div', class_='commentcontainer')

            comments = []  # post time, author name, content, where they come from
            for element in all_comments:
                dt = ''.join(element.span.text.split())
                if '今天' in dt:
                    dt = datetime.strptime(dt, '今天%H:%M')
                    dt = datetime.combine(datetime.today(), dt.time())
                elif '昨天' in dt:
                    dt = datetime.strptime(dt, '昨天%H:%M')
                    dt = datetime.combine(
                        datetime.today() - timedelta(days=1), dt.time())
                elif '前天' in dt:
                    dt = datetime.strptime(dt, '前天%H:%M')
                    dt = datetime.combine(
                        datetime.today() - timedelta(days=2), dt.time())
                else:
                    dt = datetime.strptime(dt, '%m-%d%H:%M')
                    dt = dt.replace(year=datetime.today().year)
                    
                comment = {
                    'comment_id': '',
                    'datetime': dt.strftime('%Y-%m-%dT%H:%M:%S+0800'),
                    'author': ''.join(element.b.text.split()),
                    'author_url':'',
                    'content': ''.join(element.dd.text.split()).replace('null', ''),
                    'source': 'ft',
                    'metadata': {
                        'from_area': ''.join(element.font.text.split('来自')[1]),
                    },
                    'comments':[],
                }
                comments.append(comment)
            
            return comments

        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(soup))
        item.set_metadata(parse_metadata(soup))
        return item
