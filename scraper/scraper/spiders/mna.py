# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class MnaSpider(RedisSpider):
    name = "mna"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "mna",
            "name": "mna",
            "enabled": True,
            "days_limit": 3600 * 24 * 10,
            "interval": 3600,
            "url": "https://mna.gpwb.gov.tw/all.php",
            "scrapy_key": "mna:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        next_page = self.parse_next_page(soup)

        posts_datetime = []
        links_block = soup.find('div', class_='ctn--search-list')
        for link in links_block.find_all('li'):
            title_tag = link.find('h3', class_='title')
            title = title_tag.get_text()

            url = title_tag.find('a', class_='link').get('href')
            meta['title'] = title

            content_tag = link.find('div', class_='content')
            post_date_text = content_tag.find('div').get_text()
            rex = r"(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})"
            post_date = re.search(rex, post_date_text).group(0)
            posts_datetime.append(datetime.strptime(post_date, '%Y-%m-%d %H:%M:%S'))

            yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        latest_datetime = max(posts_datetime)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)
    
    def parse_next_page(self,soup):
        current_page = int(soup.select('nav li .active')[0].text)   
        last_page_href = soup.select('nav li .last')[0]['href']
        last_page = int(last_page_href.split('=')[-1])
        
        if current_page != last_page:
            next_page = 'https://mna.gpwb.gov.tw/all.php?id=1&Num=' + str(current_page+1)
        else:
            next_page = None
        return next_page
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'mna'
        item['proto'] =  'MNA_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        timestamp = soup.select('.ctn .cf span')[0].text
        timestamp = timestamp.split('民國')[1]
        year, mon_date = timestamp.split('年')
        date = str(int(year)+1911)+mon_date   # change Republic Era into A.D
        date = datetime.strptime(date, '%Y%m月%d日')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup): 
        try:
            first_sentence = soup.select('.ctn p')[0].text.split('）')[0]
            ind_first = first_sentence.find('軍聞社記者')
            last_sentence = soup.select('.ctn p')[-1].text.split('。')[-1]
            ind_last = last_sentence.find('軍聞社')
            if ind_first == 1:
                return first_sentence[6:9]
            elif ind_last == 1:
                return last_sentence[4:7]
            elif ind_last == 3:
                return last_sentence[6:9]
            else:
                return None
        except:
            return None

    def parse_title(self,soup):
        title = soup.select('.ctn h2')[0].text
        return title.replace(u'\u3000' , ',')

    def parse_content(self,soup):
        try:
            paragraph = []
            for p in soup.select('.ctn p'):
                paragraph.append(p.text.replace(u'\u3000', ','))
                
            content = '\n'.join(paragraph)
        except:
            content = None
        return content

    def parse_metadata(self,soup):
        category = soup.find('ul','breadcrumbs--default').find('li','breadcrumb--current').text
        return {'category':category}
