#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2MNG_RSP_CRAWL(Protocol):

    def __init__(self):
        super(CRW2MNG_RSP_CRAWL, self).__init__('CRW2MNG_RSP_CRAWL')
        self.update({
          'result': ''
        })

    def set_result(self, result):
        self.set('result', result)
