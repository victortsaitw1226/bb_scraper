# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class UhoArticleSpider(RedisSpider):
    name = "uhoarticle"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "uho",
            "name": "uho",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600 * 3,
            "url": "http://www.uho.com.tw/article.asp?sor=&page=1",
            "scrapy_key": "uhoarticle:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links, latest_datetime = self.parse_links_dates(soup)

        if len(links) == 0:
            return

        for url in links:
            yield response.follow(url,
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    
    def parse_links_dates(self,soup):
        times = []
        links = []
        for ele in soup.find_all(class_='cont_title'):
            regex = re.search(r'>日期：(.*)</div>-->',
                            str(ele.parent.parent), re.M | re.S)
            times.append(datetime.strptime(regex.group(1), '%Y.%m.%d'))

            links.append(ele['href'])

        return links, max(times) 
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'uho'
        item['proto'] = 'UHO_ARTICLE_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        time = soup.find('div', class_='tilr').text
        date = re.search(r"\d{4}.\d{1,2}.\d{1,2}", time).group(0)
        time = datetime.strptime(date, '%Y.%m.%d')
        return time.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup):
        author = soup.find('div', class_='cont_dr')
        author = ''.join(author.text.split())
        author = author.replace('◎', '').replace('記者','')
        try:
            author = author.split('／')[0]
        except:
            pass
        try:
            author = author.split('/')[0]
        except:
            pass
        return author

    def parse_title(self,soup):
        title = soup.find('div', class_='till')
        title = ''.join(title.text.split())
        title = title.replace('》', '')
        return title

    def parse_content(self,soup):
        return ''.join(soup.find('div', class_='cont_all').text.split())

    def parse_metadata(self,soup,fb_like_soup=None):
        category = soup.find('h1','addr').find_all('a','addr_f')[-1].text
        return {
            'category':category,
            'fb_like_count': '',
        }
