#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class URCOSME:
    def __init__(self, server):
        print('init urcosme.py')
        self.server = server

    def on_CRW2PAR_NTF_URCOSME_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_urcosme_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_URCOSME_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_urcosme_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_urcosme_list(self, content):

        urcosme = 'https://www.urcosme.com'
        soup = BeautifulSoup(content, "lxml")
        
        try:
            next_page = urcosme + soup.find('a',text = '下一頁＞')['href']
        except:
            next_page = None
            
        links = soup.findAll('div', 'uc-news-item')
        links_view = soup.findAll('div', 'uc-news-time')
        links = [{'url':urcosme +i.find('a')['href'],'metadata':{'views':j.text.split(' / 人氣')[1]}} for i,j in zip(links, links_view)]
        
        link_date = [datetime.strptime(s.text.split(' / ')[0], '%Y.%m.%d') for s in soup.findAll('div','uc-news-time')]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_urcosme_content(self, content):

        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        date = datetime.strptime(soup.find("meta",  {'name':'title'})['content'].split(' | ')[-1], '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find("meta",  property="og:title")['content']
        author = soup.find("meta",  {'name':'title'})['content'].split(' | ')[1]
        content = soup.find('div','content-desc')
        for div in content.findAll('em'): 
            div.decompose()
        for div in content.findAll('div','buttons'): 
            div.decompose()
        content = content.text.replace('\uf059','')

        #metadata
        keywords = [s.text for s in soup.find('div','uc-news-tag').findAll('a')]
        try:
            keywords.extend(soup.find('div','uc-news-tag-list').text[1:].split('#'))
        except:
            pass           
        
        brand = soup.find("meta",  {'name':'title'})['content'].split(' | ')[2].replace('的美妝報導','')
        metadata = {'tag': keywords, 'category': brand}        
        #comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
