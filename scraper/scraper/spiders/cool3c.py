# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
from pytz import timezone
import re

class Cool3cSpider(RedisSpider):
    name = "cool3c"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "cool3c",
            "name": "cool3c",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "https://www.cool3c.com/news/p0",
            "scrapy_key": "cool3c:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        #next_page = self.parse_next_page(soup)
        links, latest_datetime = self.parse_links_dates(soup)

        if len(links) == 0:
            return

        for url in links:
            yield scrapy.Request(url,
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now(timezone('Asia/Taipei')) - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("p(\d+)", response.url).group(1)
        next_page = re.sub("p(\d+)", "p{}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    #def parse_next_page(self, soup):
    #    try:
    #        next_page = soup.find_all('li', class_='page-item near')[-1].a['href']
    #    except:
    #        next_page = None
    #    return next_page

    def parse_links_dates(self, soup):
        times = []
        links = []

        for elem in soup.find_all('div', class_='created'):
            # parse post time
            now = datetime.now(timezone('Asia/Taipei'))
            offset = ''.join(elem.text.split())
            if '就在剛剛' in offset:
                times.append(now)
            elif '秒前' in offset:
                times.append(
                    now - timedelta(seconds=int(offset.split('秒前')[0])))
            elif '分鐘前' in offset:
                times.append(
                    now - timedelta(minutes=int(offset.split('分鐘前')[0])))
            elif '個小時前' in offset:
                times.append(
                    now - timedelta(hours=int(offset.split('個小時前')[0])))
            elif '天前' in offset:
                times.append(
                    now - timedelta(days=int(offset.split('天前')[0])))
            elif '個月前' in offset:
                times.append(
                    now - timedelta(months=int(offset.split('個月前')[0])))
            elif '年前' in offset:
                times.append(
                    now - timedelta(years=int(offset.split('年前')[0])))
            else:
                continue
            # parse link
            links.append(elem.a['href'])

        return links, max(times)
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')

        # get user_token to crawl comments    
        user_source = soup.find('script',{'id':'user-source'}).text
        user_token = json.loads(user_source)['token']


        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'cool3c'
        item['proto'] = 'COOL3C_PARSE_ITEM'
        yield item

        # crawl and parse comments
        target_id = response.url.split('/')[-1]
        comments_json = self.crawl_comments(target_id,user_token)

        for reply in comments_json:
            item = NewsItem()

            main_metadata = self.parse_metadata(soup)
            reply_metadata = {
                'category':main_metadata['category'],
                'tag':main_metadata['tag'],
                'reply_count':reply['count']['children']
            }

            item['url'] = response.url
            item['metadata'] = reply_metadata
            item['article_title'] = self.parse_title(soup)
            item['author'] = reply['author']['name']
            item['author_url'] = []
            item['date'] = datetime.fromtimestamp(int(reply['created'])).strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = reply['teaser']
            item['content_type'] = 1
            item['media'] = 'cool3c'
            item['proto'] = 'COOL3C_PARSE_ITEM'
            item['comment'] = []
            yield item



    def parse_datetime(self, soup):
        time = soup.find('div', class_='created slacken')
        time = ''.join(time.text.split())
        time = datetime.strptime(time, '%Y.%m.%d%I:%M%p')
        return time.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self, soup):
        auth = soup.find('div', class_='author').find('a')
        return auth.text

    def parse_author_url(self, soup):
        author_url = []
        author_url.append(soup.find('div', class_='author').a['href'])
        return author_url

    def parse_title(self, soup):
        return soup.find('h1', class_='col-12').text

    def parse_content(self, soup):
        try:
            return ''.join(soup.find('div', class_='lucy-content').text.split())
        except:
            return ''.join(soup.find('div', class_='row content').text.split())

    def parse_category(self, soup):
        cat = soup.find('li', class_='list-inline-item type-primary term-category')
        return ''.join(cat.text.split())

    def parse_tag(self, soup):
        tags = []
        for tag in soup.find_all('li', class_='list-inline-item type-default term-tag'):
            tags.append(''.join(tag.text.split()))

        return tags

    def parse_metadata(self, soup):
        metadata = {
            'category': self.parse_category(soup),
            'tag': self.parse_tag(soup),
            'fb_like_count': '',
            'view_count': soup.find(class_='views').text.replace('\n', '')
        }
        return metadata


    def crawl_comments(self, target_id, user_token):
        start = '0'
        headers = {
            'authority': 'www.cool3c.com',
            'accept': 'application/json',
            'authorization': user_token, 
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36',
        }
        
        comments_all = []
        
        while True:
            params = (
                ('target_id',target_id ),
                ('target_type', 'posts'),
                ('parent', '0'),
                ('status', 'general'),
                ('start', start),
                ('limit', '16'),
            )
            
            response = requests.get('https://www.cool3c.com/api/i/comments', headers=headers, params=params)
            raw = json.loads(response.text)
            
            if len(raw['data'])==1 or len(raw['data'])==0:
                break
            else:
                if start =='0':
                    comments_all.extend(raw['data'])
                else:
                    comments_all.extend(raw['data'][1:])
                start = str(raw['data'][-1]['created'])
        
        return comments_all
