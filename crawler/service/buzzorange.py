#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback
import requests
import json
import sys
import re


class BUZZORANGE:

    def __init__(self, server):
        print('init BUZZORANGE')
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        raw.encoding = "utf-8"
        return raw.text

    def send_xhr(self, url_dic):
        url = url_dic['url']
        page = url_dic['page']

        # get nonce
        soup = BeautifulSoup(requests.get('https://buzzorange.com/').text)
        script = soup.find_all('script',{'type':'text/javascript'})
        index = [i for i, e in enumerate(script) if 'nonce' in str(e)][0]
        nonce = re.split(r'"nonce":"(.*)","url"',str(script[index]))[1]

        res = requests.session().post(
                url = url,
                data = {
                    'action': 'fm_ajax_load_more',
                    'nonce':nonce,
                    'page': page,
                },
                headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                    'Referer': 'https://buzzorange.com/',
                }
                )

        html = json.loads(res.text)['data']
        return {'html':html, 'page':page+1, 'url':url }

    def on_MNG2CRW_NTF_BUZZORANGE_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        # print('url')
        try:
            html = self.send_xhr(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
        
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_BUZZORANGE_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            detail_html = self.goto(url)
            html = {'html': detail_html, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
