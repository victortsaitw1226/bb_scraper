#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class YOUTUBE_PARSE_INFO(Protocol):

    def __init__(self):
        super(YOUTUBE_PARSE_INFO, self).__init__('YOUTUBE_PARSE_INFO')
        self.update({
          'channel_url': '',
          'video_url': '',
          'title': '',
          'p': '0',
          'n': '0',
          'views': '',
          'date': ''
        })

    def set_channel_url(self, url):
        self.set('channel_url', url)

    def get_channel_url(self):
        return self.get('channel_url')

    def set_video_url(self, url):
        self.set('video_url', url)

    def get_video_url(self):
        return self.get('video_url')

    def set_title(self, title):
        self.set('title', title)

    def get_title(self):
        return self.get('title')

    def set_p(self, p):
        self.set('p', p)

    def get_p(self):
        return self.get('p')

    def set_n(self, n):
        self.set('n', n)

    def get_n(self):
        return self.get('n')

    def set_views(self, count):
        self.set('views', count)

    def get_views(self):
        return self.get('views')

    def set_date(self, date):
        self.set('date', date)

    def get_date(self):
        return self.get('date')
  
