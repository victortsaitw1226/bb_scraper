# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

#class ITHelpSpider(scrapy.Spider):
class ITHelpSpider(RedisSpider):
    name = "ithelp"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ithelp",
            "name": "ithelp",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            #"url": "https://ithelp.ithome.com.tw/articles?tab=tech&page=1",
            "url": "https://ithelp.ithome.com.tw/questions?page=1",
            "scrapy_key": "ithelp:start_urls",
            "priority": 1
        },
        {
            "media": "ithome_ithelp",
            "name": "ithome_ithelp",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://ithelp.ithome.com.tw/articles?tab=job",
            "scrapy_key": "ithome_ithelp:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        links = []
        posts_date = []
        for link in soup.find_all('div','qa-list'):
            url = link.find('a').get('href')
            condition = link.find('div', class_='qa-list__condition')
            metrics = condition.find_all('span', class_='qa-condition__count')
            post_meta = {
                'href': url,
                'likes': metrics[0].get_text(),
                'reply': metrics[1].get_text(),
                'view': metrics[2].get_text()
            }
            links.append(post_meta)
            title = link.find('a', 'qa-list__info-time').get('title')
            posts_date.append(datetime.strptime(title, '%Y-%m-%d %H:%M:%S'))
        
        if 0 == len(links):
            return

        for post in links:
            yield scrapy.Request(post['href'],
                    meta=post,
                    callback=self.parse_article)

        latest_datetime = max(posts_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_article(self, response):
        meta = response.meta
        # main content
        soup = BeautifulSoup(response.body, 'html.parser')

        if 'ir-article__topic' in response.text:
            main_metadata = self.parse_ir_metadata(soup)
            main_metadata.update({
                'like_count': meta['likes'],
                'reply_count': meta['reply'],
                'view_count': meta['view']
            })
            title = self.parse_title(soup)
            item = NewsItem()
            item['url'] = response.url
            item['author'] = self.parse_ir_author(soup)
            item['article_title'] = title
            item['author_url'] = []
            item['content'] = self.parse_content(soup)
            item['comment'] = []
            item['date'] = self.parse_datetime(soup)
            item['metadata'] = main_metadata
            item['content_type'] = 0
            item['media'] = 'ithome'
            item['proto'] = 'ITHOME_ITHELP_PARSE_ITEM'
            yield item
        
        else:
            main_metadata = self.parse_metadata(soup)
            main_metadata.update({
                'like_count': meta['likes'],
                'reply_count': meta['reply'],
                'view_count': meta['view']
            })
            title = self.parse_title(soup)
            item = NewsItem()
            item['url'] = response.url
            item['author'] = self.parse_author(soup)
            item['article_title'] = title
            item['author_url'] = []
            item['content'] = self.parse_content(soup)
            item['comment'] = []
            item['date'] = self.parse_datetime(soup)
            item['metadata'] = main_metadata
            item['content_type'] = 0
            item['media'] = 'ithome'
            item['proto'] = 'ITHOME_ITHELP_PARSE_ITEM'
            yield item

        # replies
        for reply in soup.find_all('div','qa-panel ans clearfix'):
            item = NewsItem()
            try:
                reply_count = reply.find('span','qa-action__link-num').text
            except:
                reply_count = '0'
            item['url'] = response.url
            item['metadata'] = {
                'tag': main_metadata['tag'],
                'category': main_metadata['category'],
                'reply_count': reply_count
            }
            item['article_title'] = title
            item['author'] = reply.find('a','ans-header__person').text.strip()
            item['author_url'] = []
            item['date'] = datetime.strptime(reply.find('a','ans-header__time')['title'], '%Y-%m-%d %H:%M:%S')
            item['content'] = ' '.join(reply.find('div','markdown__style').text.split())
            item['content_type'] = 1
            item['media'] = 'ithome'
            item['proto'] = 'ITHOME_ITHELP_PARSE_ITEM'
            item['comment'] = []
            yield item

                
        for reply in soup.find_all('div','qa-panel response clearfix'):
            item = NewsItem()
            try:
                reply_count = reply.find('span','qa-action__link-num').text.strip()
            except:
                reply_count = '0'
            item['url'] = response.url
            item['metadata'] = {'tag':main_metadata['tag'], 'category':main_metadata['category'], 'reply_count':reply_count}
            item['article_title'] = title
            item['author'] = reply.find('a','response-header__person').text.strip()
            item['author_url'] = []
            item['date'] = datetime.strptime(reply.find('a','ans-header__time')['title'], '%Y-%m-%d %H:%M:%S')
            item['content'] = ' '.join(reply.find('div','markdown__style').text.split())
            item['content_type'] = 1
            item['media'] = 'ithome'
            item['proto'] = 'ITHOME_ITHELP_PARSE_ITEM'
            item['comment'] = []
            yield item

    def parse_datetime(self, soup):
        date = soup.find('div','qa-panel clearfix').find('a','qa-header__info-time')['title']
        date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('div','qa-panel clearfix').find('a','qa-header__info-person').text.strip()
        return author
    
    def parse_ir_author(self, soup):
        author = soup.find('div','qa-panel clearfix').find('a','ir-article-info__name').text.strip()
        return author
    
    def parse_title(self, soup):
        title = soup.find('div','qa-panel clearfix').find('h2','qa-header__title').text
        return title.strip()
    
    def parse_content(self, soup):
        content = soup.find('div','qa-panel clearfix').find('div','markdown__style').text
        content = ' '.join(content.split())
        return content

    def parse_metadata(self, soup):
        tags = [t.text for t in soup.find('div','qa-panel clearfix').find('div','qa-header__tagGroup').find_all('a')]
        category = soup.find('a',re.compile(r' menu__item-link--active')).text
        metadata = {
            'tag': tags,
            'category': category
        }
        return metadata

    def parse_ir_metadata(self, soup):
        try:
            view_count = soup.find('div','qa-panel clearfix').find('div','ir-article-info__view').text.split(' 瀏覽')[0]
        except:
            view_count = ''
        tags = [ t.text for t in soup.find('div','qa-panel clearfix').find('div','qa-header__tagGroup').find_all('a')]
        category = soup.find('a',re.compile(r' menu__item-link--active')).text + '-鐵人賽'
        metadata = {'view_count':view_count,'tag':tags, 'category':category}
        return metadata
