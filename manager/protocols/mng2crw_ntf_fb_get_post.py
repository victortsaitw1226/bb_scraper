#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_FB_GET_POST(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_FB_GET_POST, self).__init__('MNG2CRW_NTF_FB_GET_POST')
        self.update({
          'url': '',
          'post_utime': 0,
          'fan_page': ''
        })

    def set_post_utime(self, utime):
        self.set('post_utime', utime)

    def get_post_utime(self):
        return self.get('post_utime')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_fan_page(self, fan_page):
        self.set('fan_page', fan_page)

    def get_fan_page(self):
        return self.get('fan_page')
