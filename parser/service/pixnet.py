#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
import calendar
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class PIXNET:
    def __init__(self, server):
        print('init pixnet.py')
        self.server = server

    def on_CRW2PAR_NTF_PIXNET_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_pixnet_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_PIXNET_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_pixnet_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_pixnet_list(self, content):
        def parse_next_page(soup):
            try:
                next_page = soup.find('div','page').find('a','page-next')['href']
                next_page = 'https://www.pixnet.net' + next_page
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            
            def get_date_text(d):
                d = d.split()
                d = d[d.index('|')-1]
                d = datetime.strptime( d, '%Y.%m.%d')
                return d
            
            links = soup.find('div','box-body').find_all('li')
            first_link = soup.find('div','featured')
            

            links_date = []
            
            if first_link!=None:
                links_date.append(get_date_text(first_link.find('span','publish').text))
                
            [links_date.append(get_date_text(l.find('span','meta').text)) for l in links]
            
            latest_post_date = max(links_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        def parse_links(soup):
            links = soup.find('div','box-body').find_all('li')
            first_link = soup.find('div','featured')
            links_list = []
            if first_link!=None:
                links_list.append( { 'url':first_link.find('a')['href'], 'metadata':[] } )
            [links_list.append( { 'url':l.find('a')['href'] , 'metadata':[] } ) for l in links]
            return links_list
        
        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_latest_post_date(soup))
        item.set_urls(parse_links(soup))

        return item


    def parse_pixnet_content(self, content):
        
        def parse_datetime(soup):
        
            abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
            publish = soup.find('li','publish')
            year = publish.find('span','year').text.strip()
            month = str(abbr_to_num[publish.find('span','month').text.strip()])
            date = publish.find('span','date').text.strip()
            HM = publish.find('span','time').text.strip()
            
            date = datetime.strptime( year + '-' + month + '-' + date + ' ' + HM , '%Y-%m-%d %H:%M')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
            return date
        
        
        def parse_content(soup):
            content = soup.find('div','article-content-inner')
            for s in content.find_all('script'):
                s.clear()
            for s in content.find_all('noscript'):
                s.clear()
            content = ' '.join(content.text.split())
            if '延伸閱讀' in content:
                content = content.split('延伸閱讀')[0]
            return content
        
        def parse_metadata(soup):
            if soup.find('div','tag__main') != None:
                tag = soup.find('div','tag__main').find_all('a','tag')
                tag = [t['data-tag']for t in tag]
            else:
                tag = []
            category = soup.find('p',{'id':'blog-category'}).find('a').text
            return{'tag':tag, 'category':category,'fb_like':''}
        
        def parse_comments(soup):
            
            def parse_reply_reply(rr):
                replies = []
                if rr!=None:
                    auth = rr.find('a').text
                    rr.find_all('a')[-1].clear()
                    date = datetime.strptime( rr.find('p').text.strip(), '於 %Y/%m/%d %H:%M 回覆').strftime('%Y-%m-%dT%H:%M:%S+0800')
                    rr.find('p').clear()
                    cont = ' '.join(rr.text.split())
                    reply = {
                            'comment_id': '',
                            'datetime': date,
                            'author': auth,
                            'author_url': '',
                            'content': cont,
                            'metadata': {}
                            }
                    replies.append(reply)
                return replies
                
            single_post = soup.find_all('ul','single-post')
            comments = []
            for sp in single_post:
                if sp.find('li','post-info')!=None:
                    reply_dict =  {
                            'comment_id': re.split('-',sp.find('a')['name'])[1],
                            'datetime': datetime.strptime( sp.find('span','post-time').text, '於 %Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800'),
                            'author': sp.find('span','user-name').text.strip(),
                            'author_url': '',
                            'content':sp.find('li','post-text').text.strip(),
                            'source': 'pixnet',
                            'metadata':{},
                            'comments':parse_reply_reply(sp.find('li','reply-text')) }
                    comments.append(reply_dict)
            return comments
       

        soup = BeautifulSoup(content['html'][0], 'html.parser')
        comment_json = BeautifulSoup(content['comment_json'], 'html.parser')
        
        title = ' '.join(soup.find('h2',{'itemprop':'headline'}).text.split())
        author = soup.find('meta',{'name':'author'})['content']
        author_url = []
        author_url.append(soup.find('div','author-profile__content').find('a')['href'])

        content_detail = []
        for ch in content['html']:
            content_soup = BeautifulSoup(ch, 'html.parser')
            content_detail.append(parse_content(content_soup))
        content_detail = '    * next-page *    '.join(content_detail)

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(title)
        item.set_author(author)
        item.set_author_url(author_url)
        item.set_date(parse_datetime(soup))
        item.set_content(content_detail)
        item.set_comment(parse_comments(comment_json))
        item.set_metadata(parse_metadata(soup))
        return item
