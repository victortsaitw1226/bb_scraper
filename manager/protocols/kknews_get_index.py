#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class KKNEWS_GET_INDEX(Protocol):

    def __init__(self):
        super(KKNEWS_GET_INDEX, self).__init__('KKNEWS_GET_INDEX')
        self.update({
          'board': ''
        })

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')