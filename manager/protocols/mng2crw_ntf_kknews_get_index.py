#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_KKNEWS_GET_INDEX(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_KKNEWS_GET_INDEX, self).__init__('MNG2CRW_NTF_KKNEWS_GET_INDEX')
        self.update({
          'url': '',
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')