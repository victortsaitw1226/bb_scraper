#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
import requests

class CTS:

    def __init__(self, server):        
        print('init cts')
        self.session = requests.session()
        self.server = server

    def detect_error(self, html):
        return 'cts-logo' in html

    def on_MNG2CRW_NTF_CTS_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        self.server.log.debug(from_notify)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        if type(url) is str:
            pass
        else:
            return
        
        try:
            html = self.server.new_requests(url, self.detect_error)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_CTS_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        # self.server.log.debug(from_notify)
        url = from_notify.get_url()
        if type(url) is str:
            pass
        else:
            return

        try:
            # print('POST url:', url)
            # detail_html = self.server.browser.goto(url)
            
            # fb_like_count_element = self.server.browser.find_element_by_xpath('//*[@title="fb:like Facebook Social Plugin"]')
            # fb_like_count_src = self.server.browser.get_attribute( fb_like_count_element, 'src')
            
            # line_share_count_element = self.server.browser.find_element_by_xpath('//*[@title="Share this page on LINE."]')
            # line_share_count_src = self.server.browser.get_attribute( line_share_count_element, 'src')

            # fb_comment_element = self.server.browser.find_element_by_xpath('//*[@title="fb:comments Facebook Social Plugin"]')
            # fb_comment_src = self.server.browser.get_attribute( fb_comment_element, 'src')

            # # print('POST fb_like_count_src:', fb_like_count_src)
            # fb_like_count_html = self.server.browser.goto(fb_like_count_src)
            # # print('POST line_share_count_src:', line_share_count_src)
            # line_share_count_html = self.server.browser.goto(line_share_count_src)
            
            # self.server.browser.get(fb_comment_src)
            # self.server.browser.fb_scrollDown_click_button()
            # fb_comment_html = self.server.browser.html()

            # html = {'html': detail_html, 'fb_like_count':fb_like_count_html, 'line_share_count':line_share_count_html, 
            #      'comment_html':fb_comment_html, 'url': url}
            html = {'html': self.server.new_requests(url, self.detect_error),  'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
