#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_PTT_GET_INDEX(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_PTT_GET_INDEX, self).__init__('MNG2CRW_NTF_PTT_GET_INDEX')
        self.update({
          'url': '',
          'days_limit': 1
        })

    def set_days_limit(self, days):
        self.set('days_limit', days)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')
