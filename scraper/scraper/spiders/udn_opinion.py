# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

#class UDNOpinionSpider(scrapy.Spider):
class UDNOpinionSpider(RedisSpider):
    name = "udnopinion"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "udnopinion",
            "name": "udnopinion",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://opinion.udn.com/rank/ajax_newest/1008/0/1",
            "scrapy_key": "udnopinion:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
 
        if soup.text is '':
            return

        links = self.parse_links(soup)
        next_page = self.parse_next_page(response.url)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_latest_post_date(self, html_bs4):
        pub_date = html_bs4.find_all('time')
        link_date = []
        for date in pub_date:
            date = datetime.strptime( date['datetime'] , '%Y-%m-%d')
            link_date.append(date)
        latest_post_date = max(link_date)
        return latest_post_date
    
    def parse_links(self, html_bs4):
        links = html_bs4.find_all('h2')
        links = [li.find('a')['href'] for li in links] 
        return links
    
    def parse_next_page(self, current_page):
        url_split = re.split('/',current_page)
        url_split[-1] = str(int(url_split[-1])+1)
        next_page = '/'.join(url_split)
        return next_page

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'udn'
        item['proto'] =  'UDN_OPINION_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date_org = soup.find('time').text
        date = datetime.strptime(date_org, '%d %b, %Y').strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date

    def parse_title(self, soup):
        return soup.find('h1', class_='story_art_title').text

    def parse_content(self, soup):
        for tags in soup.find_all('style', {'type': 'text/css'}):
            tags.decompose()
        return ''.join([ent.text for ent in soup.find_all('p')[:-1]]).replace('\n', '')

    def parse_author(self, soup):
        author = soup.find('div', class_='story_bady_info_author').find('a').text
        return author
    
    def parse_metadata(self, soup):
        tmp = soup.find('div', id='keywords').find_all('a')
        metadata = {
            'category':'',
            'tag':[],
            'fb_like_count':''
        }
        try: 
            metadata['category'] = tmp[0].text
        except AttributeError:
            pass
        try: 
            metadata['tag'] = [i.text for i in tmp[1:]]
        except AttributeError:
            pass
        return metadata
