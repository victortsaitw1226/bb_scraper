#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
import re
import sys
from six.moves import urllib
from bs4 import BeautifulSoup
sys.path.append("..")
from utils.browser import Browser
from protocols.youtube_parse_comment import YOUTUBE_PARSE_COMMENT
from protocols.mng2crw_ntf_youtube_get_video_list import MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST
from protocols.mng2crw_ntf_youtube_get_comments import MNG2CRW_NTF_YOUTUBE_GET_COMMENTS
from protocols.mng2crw_ntf_youtube_get_info import MNG2CRW_NTF_YOUTUBE_GET_INFO
from protocols.crw2par_ntf_youtube_video_list import CRW2PAR_NTF_YOUTUBE_VIDEO_LIST
from protocols.crw2par_ntf_youtube_comments import CRW2PAR_NTF_YOUTUBE_COMMENTS
from protocols.crw2par_ntf_youtube_info import CRW2PAR_NTF_YOUTUBE_INFO

class Youtube:
    browser = None
    api = None

    def __init__(self, server):
        print('init Youtube')
        self.server = server
        if not Youtube.browser:
            # Youtube.browser = Browser(server.redis)
            Youtube.browser = Browser(None)

    def routine(self):
        proxy_list = Youtube.browser.renew_proxy_list()

    def crawl_video_list(self, url):
        # html = Youtube.browser.goto(url)
        self.goto_and_validate_url(url)
        Youtube.browser.scrollDown(5)
        return Youtube.browser.html()

    def goto_and_validate_url(self, url):
        for i in range(10):
            #ip = Youtube.browser.check_my_ip()
            #print('Server IP : %s' % ip)
            html = Youtube.browser.goto(url)
            soup = BeautifulSoup(html, 'html.parser')
            div = soup.find('div')
            if div is None:
                Youtube.browser.recreate_driver()
            else:
                break

        return html
   
    def crawl_info(self, url):
        # return Youtube.browser.goto(url)
        return self.goto_and_validate_url(url)

    def crawl_comment(self, url):
        html = self.goto_and_validate_url(url)
        Youtube.browser.scrollDown(10)
        
        replies = Youtube.browser.soup().find_all('div', {'id': 'replies'})
        for reply in replies:
            reply_button = reply.find('ytd-button-renderer', {'id': 'more-replies'})
            if reply_button is None:
                continue
            element_xpath = Youtube.browser.beautifulsoup_to_xpath(reply_button.find('a'))
            btn = Youtube.browser.find_element_by_xpath(element_xpath)
            Youtube.browser.click(btn)

        return Youtube.browser.html()

    def on_MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST(self, packet):
        from_notify = MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        channel = from_notify.get_channel()

        data = self.crawl_video_list(url)

        to_notify = CRW2PAR_NTF_YOUTUBE_VIDEO_LIST()
        to_notify.set_url(url)
        to_notify.set_channel(channel)
        to_notify.set_content(data)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_YOUTUBE_GET_COMMENTS(self, packet):
        from_notify = MNG2CRW_NTF_YOUTUBE_GET_COMMENTS()
        from_notify.parse(packet)
        channel_url = from_notify.get_channel_url()
        channel = from_notify.get_channel()
        video_url = from_notify.get_video_url()

        data = self.crawl_comment(video_url)

        to_notify = CRW2PAR_NTF_YOUTUBE_COMMENTS()
        to_notify.set_channel_url(channel_url)
        to_notify.set_channel(channel)
        to_notify.set_video_url(video_url)
        to_notify.set_content(data)
        self.server.send(to_notify)

    def parse_comments(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        comment_thread = soup.find_all('ytd-comment-thread-renderer')
        results = []
        for comment in comment_thread:
            item = YOUTUBE_PARSE_COMMENT()
            #print(comment)
            post = comment.find('ytd-comment-renderer', {'id':'comment'})
            item.set_author(post.find('a', {'id': 'author-text'}).get_text())
            item.set_content(post.find('div', {'id': 'content'}).get_text())
            item.set_date(post.find('yt-formatted-string', {'class': 'published-time-text'}).get_text())
            item.set_p(post.find('span', {'id': 'vote-count-middle'}).get_text())

            replies = comment.find('div', {'id': 'replies'})
            if not replies:
                results.append(item)
                continue

            replies = replies.find_all('ytd-comment-renderer')
            reply_results = []
            for reply in replies:
                reply_item = dict()
                reply_item['author'] = reply.find('a', {'id': 'author-text'}).get_text().strip()
                reply_item['content'] = reply.find('yt-formatted-string', {'id': 'content-text'}).get_text()
                reply_item['date'] = reply.find('yt-formatted-string', {'class': 'published-time-text'}).get_text().strip()
                reply_item['p'] = reply.find('span', {'id': 'vote-count-middle'}).get_text().strip()
                reply_results.append(reply_item)
            item.set_replies(reply_results)
            results.append(item)
        return results

if __name__ == '__main__':
    y = Youtube(None)
    html = y.crawl_comment('https://www.youtube.com/watch?v=ma7r2HGqwXs')
    result = y.parse_comments(html)
    for c in result:
        print(str(c))
