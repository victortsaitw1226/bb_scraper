# -*- coding: utf-8 -*-
'''
run with user_id and since:
scrapy crawl twitter -a user_id='@KP_Taipei' -a since='2019-12-01'
'''

import scrapy
from bs4 import BeautifulSoup
import traceback, sys
from datetime import datetime, timedelta
import calendar
import requests
import re
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
import json
from .redis_spiders import RedisSpider
from urllib.parse import quote

class TwitterSpider(scrapy.Spider): #(RedisSpider):
    name = "twitter"

    def __init__(self, user_id='', since=''):
        self.user_id = user_id
        self.since = since
        self.url = "https://twitter.com/search/"
        self.url = self.url + "?q={direction}%3A%40{id}%20since%3A{since}&src=typed_query&f=live&max_position={p}"
        self.gt = '1204320271739449344'
        self.authorization = "Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA"

    def start_requests(self):
        return
        url = self.url.format(direction = 'from',
                                id = self.user_id.split('@')[-1], 
                                since = self.since,
                                p = '')
        # url = 'https://twitter.com/search?q=from%3A%40KP_Taipei%20since%3A2019-11-20&src=typed_query'
        yield scrapy.http.Request(url, 
                                dont_filter=True, 
                                callback=self.parse_page)



    def parse_page(self, response):
        ## parse current page article
        html = response.body.decode("utf-8")
        soup = BeautifulSoup(html,'html.parser')
        items = soup.find_all('li',{'data-item-type':'tweet'})

        for item in items:
            article = {}
            if item.find('div','ReplyingToContextBelowAuthor')==None: # filter replies
                article['author'] = item.find('span','username u-dir u-textTruncate').text.split('@')[-1]
                
                article['ID'] = item.find('div')['data-conversation-id']
                
                article['url'] = 'https://twitter.com/' + item.find('div')['data-permalink-path']
                
                date = item.find('div','stream-item-header').find('small','time').find('a').find(
                        'span')['data-time']
                article['date'] = datetime.fromtimestamp(int(date)).strftime('%Y-%m-%dT%H:%M:%S+0800')
                
                article['content'] = item.find('div','js-tweet-text-container').find('p').text
                
                article['article_title'] =  article['content'][:30]
                
                nbr_retweet = item.find('span','ProfileTweet-action--retweet').find('span','ProfileTweet-actionCount').text.split()[0]
                nbr_favorite = item.find('span','ProfileTweet-action--favorite').find('span','ProfileTweet-actionCount').text.split()[0]
                nbr_reply = item.find('span','ProfileTweet-action--reply').find('span','ProfileTweet-actionCount').text.split()[0]
                article['metadata'] = {'like_count':nbr_favorite,
                                        'reply_count':nbr_reply,
                                        'retweet_count':nbr_retweet}
                
                ## request for reply
                reply_url = "https://api.twitter.com/2/timeline/conversation/{id}.json".format(id = article['ID'])
                
                querystring = {"include_can_media_tag":"1",
                            "include_composer_source":"true",
                            "tweet_mode":"extended",
                            "include_entities":"true",
                            "include_user_entities":"true"}
                
                headers = {'Authorization': self.authorization,
                            'x-guest-token': self.gt}
                                                
                yield scrapy.FormRequest(url=reply_url,
                                        formdata= querystring,
                                        headers = headers,
                                        method='GET',
                                        meta=article,
                                        dont_filter=True,
                                        callback=self.handle_replies)
            else:
                pass


        ## get next page
        try:
            min_position = soup.find('div','stream-container')['data-max-position']
            url = self.url.format(direction = 'from',
                                    id = self.user_id.split('@')[-1], 
                                    since = self.since,
                                    p = min_position)
            yield scrapy.Request(url, dont_filter=True, callback=self.parse_page)
        except:
            pass
    
    def handle_replies(self, response):

        def filter_reply(tweets):
            reply_subset = []
            tweet_id = list(tweets.keys())
            for t in tweet_id:
                if self.user_id in tweets[t]['full_text']:
                    reply_subset.append(tweets[t])
            return reply_subset

        def get_cursor(raw):
            try:
                cursor = raw['timeline']['instructions'][0]['addEntries']['entries'][-1]['content']['operation']['cursor']['value']
            except:
                cursor = ''
            return cursor

        def parse_reply(reply_json):
            def parse_datetime(t):
                splitStr = t.split()
                abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
                dt = splitStr[5] + '/' + str(abbr_to_num[splitStr[1]]) + '/' + splitStr[2] + ' ' + splitStr[3] 
                dt = datetime.strptime( dt, '%Y/%m/%d %H:%M:%S')   
                dt = dt + timedelta(hours=8)
                return  dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

            reply_list = []
            for reply in reply_json:
                reply_dict =  {
                        'comment_id': reply['id_str'],
                        'datetime': parse_datetime(reply['created_at']),
                        'author': reply['user_id_str'],
                        'author_url': '',
                        'content':''.join(reply['full_text'].split()),
                        'source': 'twitter',
                        'metadata':{'like_count':reply['favorite_count'], 'retweet_count':reply['retweet_count']},
                        'comments':[] }      
                reply_list.append(reply_dict)
            return reply_list
        
        
        ## crawl_replies
        reply = []
        tweet = response.meta
        reply_raw = json.loads(response.body)
        reply.extend ( filter_reply(reply_raw['globalObjects']['tweets']))
        cursor = get_cursor(reply_raw)
        
        
        while cursor!='':
            reply_url = "https://api.twitter.com/2/timeline/conversation/{id}.json".format(id = tweet['ID'])

            querystring = {"include_can_media_tag":"1",
                       "include_composer_source":"true",
                       "tweet_mode":"extended",
                       "include_entities":"true",
                       "include_user_entities":"true",
                       "cursor":cursor}

            headers = {'Authorization': self.authorization,
                        'x-guest-token': self.gt}
                                                
            response = requests.request("GET", reply_url, headers=headers, params=querystring).text

            reply_raw = json.loads(response)
            reply.extend ( filter_reply(reply_raw['globalObjects']['tweets']))
            cursor = get_cursor(reply_raw)

        
        ## parse replies
        comments = parse_reply(reply)
        print('length of comments:',len(comments))
        print('1st comment:',comments[0])
        
        
        ## write info into NewsItem
        item = NewsItem()
        item['url'] = tweet['url']
        item['article_title'] = tweet['article_title']
        item['author'] = tweet['author']
        item['comment'] = comments
        item['date'] = tweet['date']
        item['content'] = tweet['content']
        item['metadata'] = tweet['metadata']
        item['media'] = 'twitter'
        item['proto'] =  'TWITTER_PARSE_ITEM'
        return item
        


        
