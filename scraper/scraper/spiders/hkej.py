# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta, date
from bs4 import BeautifulSoup
import json
import re

class HkejSpider(RedisSpider):
    name = "hkej"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "hkej",
            "name": "hkej",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www2.hkej.com/instantnews?&page=1",
            "url_pattern": "https://www2.hkej.com/instantnews?date={date}&page={page}#top",
            "scrapy_key": "hkej:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        now = datetime.now()
        past = datetime.now()
        while True:
            url_pattern = meta['url_pattern']
            url = url_pattern.format(date=past.strftime('%Y-%m-%d'), page=1)
            yield scrapy.Request(url,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_list)
            past = past - timedelta(hours=24)
            if (now - timedelta(seconds=meta['days_limit'])) > past:
                break

    def parse_list(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = soup.findAll('div', class_='hkej_toc_listingAll_news2_2014')
        for link in links:
            href = link.find('a').get('href')
            yield response.follow(href,
                    meta = meta,
                    callback=self.parse_article)

        if len(links) == 0:
            return

        current_page = re.search(r'page=(\d+)', response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)
        yield scrapy.Request(next_page,
             meta=meta,
             dont_filter=True,
             callback=self.parse_list)


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'hkej'
        item['proto'] = 'HKEJ_PARSE_ITEM'
        return item
        

    def parse_datetime(self, soup):
        dat = soup.find('span','date').text.strip()
        if '今日' in dat:
            dat = dat.replace('今日', date.today().strftime('%Y年%m月%d日'))
        elif '昨日' in dat:
            dat = dat.replace('昨日', (date.today()-timedelta(1)).strftime('%Y年%m月%d日'))
        try:
            dat = datetime.strptime(dat, '%Y年%m月%d日 %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            dat = datetime.strptime(dat, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
        return dat
    
    def parse_author(self, soup):
        author = soup.find("meta",  property="og:site_name")['content'].strip()
        return author
    
    def parse_title(self, soup):
        title = soup.find("h1",  id="article-title").text
        return title
    
    def parse_content(self, soup):
        content = soup.find("div",  id="article-content").text.strip()
        return content

    def parse_metadata(self, soup):
        category = soup.find('span','cate').text.strip()
        metadata = {'category':category}
        return metadata
    

