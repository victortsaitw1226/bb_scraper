#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
import re
import sys
import time
from six.moves import urllib
from bs4 import BeautifulSoup
from protocols.mng2crw_ntf_youtube_get_video_list import MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST
from protocols.mng2crw_ntf_youtube_get_comments import MNG2CRW_NTF_YOUTUBE_GET_COMMENTS
from protocols.mng2crw_ntf_youtube_get_info import MNG2CRW_NTF_YOUTUBE_GET_INFO
from protocols.crw2par_ntf_youtube_video_list import CRW2PAR_NTF_YOUTUBE_VIDEO_LIST
from protocols.crw2par_ntf_youtube_comments import CRW2PAR_NTF_YOUTUBE_COMMENTS
from protocols.crw2par_ntf_youtube_info import CRW2PAR_NTF_YOUTUBE_INFO

class YOUTUBE:

    def __init__(self, server):
        print('init Youtube')
        self.server = server

    def goto_and_validate_url(self, url, max_retry=10):
        for i in range(max_retry):
            print('goto_and_validate_url:retry:{}:{}'.format(i, url))
            html = self.server.browser.goto(url)
            soup = BeautifulSoup(html, 'html.parser')
            div = soup.find('div')
            if div is None:
                self.server.browser.recreate_driver()
                continue
            recaptcha = soup.find('div', {'id': 'recaptcha'})
            if recaptcha:
                self.server.browser.recreate_driver()
                continue
            break

        return html
   
    def crawl_video_list(self, url):
        print('crawl video list')
        self.goto_and_validate_url(url)
        self.server.browser.scrollDown(5)
        return self.server.browser.html()

    def crawl_comment(self, url):
        html = self.goto_and_validate_url(url)
        self.server.browser.scrollDown(10)
        
        replies = self.server.browser.soup().find_all('div', {'id': 'replies'})
        for reply in replies:
            reply_button = reply.find('ytd-button-renderer', {'id': 'more-replies'})
            if reply_button is None:
                continue
            element_xpath = self.server.browser.beautifulsoup_to_xpath(reply_button.find('a'))
            btn = self.server.browser.find_element_by_xpath(element_xpath)
            self.server.browser.click(btn)

        return self.server.browser.html()


    def on_MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST(self, packet):
        from_notify = MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        channel = from_notify.get_channel()
        days_limit = from_notify.get_days_limit()

        start_time = time.time()

        data = self.crawl_video_list(url)

        elapsed_time = time.time() - start_time

        print("--- on_MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST: {} seconds:{} ---".format(elapsed_time, url))

        to_notify = CRW2PAR_NTF_YOUTUBE_VIDEO_LIST()
        to_notify.set_url(url)
        to_notify.set_channel(channel)
        to_notify.set_content(data)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_YOUTUBE_GET_COMMENTS(self, packet):
        from_notify = MNG2CRW_NTF_YOUTUBE_GET_COMMENTS()
        from_notify.parse(packet)
        channel_url = from_notify.get_channel_url()
        channel = from_notify.get_channel()
        video_url = from_notify.get_video_url()
        video_img = from_notify.get_video_img()

        start_time = time.time()

        data = self.crawl_comment(video_url)

        elapsed_time = time.time() - start_time
        print("--- on_MNG2CRW_NTF_YOUTUBE_GET_COMMENTS: channel:{}, {} seconds:{} ---".format(channel, elapsed_time, video_url))

        to_notify = CRW2PAR_NTF_YOUTUBE_COMMENTS()
        to_notify.set_channel_url(channel_url)
        to_notify.set_channel(channel)
        to_notify.set_video_url(video_url)
        to_notify.set_content(data)
        to_notify.set_video_img(video_img)
        self.server.send(to_notify)
