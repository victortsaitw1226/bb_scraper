#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_YOUTUBE_GET_COMMENTS(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_YOUTUBE_GET_COMMENTS, self).__init__('MNG2CRW_NTF_YOUTUBE_GET_COMMENTS')
        self.update({
          'channel_url': '',
          'channel': '',
          'video_url': '',
          'video_img': ''
        })

    def set_channel_url(self, url):
        self.set('channel_url', url)

    def get_channel_url(self):
        return self.get('channel_url')

    def set_channel(self, channel):
        self.set('channel', channel)

    def get_channel(self):
        return self.get('channel')

    def set_video_url(self, video_url):
        self.set('video_url', video_url)

    def get_video_url(self):
        return self.get('video_url')

    def set_video_img(self, video_img):
        self.set('video_img', video_img)

    def get_video_img(self):
        return self.get('video_img')
