#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class TWTIMES:
    def __init__(self, server):
        print('init twtimes.py')
        self.server = server

    def on_CRW2PAR_NTF_TWTIMES_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_twtimes_list(content)
        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TWTIMES_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_twtimes_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_twtimes_list(self, content):

        twtimes = 'http://www.twtimes.com.tw'
        soup = BeautifulSoup(content, "lxml")
        
        page = soup.find('div', 'pagination megas512').find('li').text.split(':')[-1].split('/')
        if page[0] != page[1]:
            next_page = twtimes + '/' + soup.find('a', text = '下一頁 ')['href']
        else:
            next_page = None
            
        links = [{'url':twtimes+'/index.php'+s['href'],'metadata':[]} for s in soup.find('div','newsli').findAll('a')]
        
        table = soup.find('div','newsli')
        for div in table.findAll('li','topnews'): 
            div.decompose()
        for div in table.findAll('a'): 
            div.decompose()
        link_date = [datetime.strptime(s.text.replace('\xa0',''), '%Y-%m-%d') for s in table.findAll('li')]
        
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_twtimes_content(self, content):
        # detail
        soup = BeautifulSoup(content['detail_html'], 'lxml')
        url = content['url'] 
    
        date = soup.find('td',{'width':'50%'}).text.split('\t')[-1].strip()
        date = datetime.strptime(date, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find('meta', {'name':'title'})['content']
            
        content = soup.find('span', {'name':'zoom'}).find('br').parent.text.strip()

        if re.search(r'〔記者(.*?)報導〕', content):
            author = re.search(r'〔記者(.*?)報導〕', content).group(1)
        elif re.search(r'︹記者(.*?)報導︺', content):
            author = re.search(r'︹記者(.*?)報導︺', content).group(1)
        elif re.search(r'（記者(.*?)）', content):
            author = re.search(r'（記者(.*?)）', content).group(1)
        else:
            author = ''
            
        #metadata        
        category = soup.find('div',{'id':'main_poti'}).text.split(' > ')[-1]
        metadata = {'category':category}

        comment = []
              
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
