#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class EYNY:
    def __init__(self, server):
        print('init eyny.py')
        self.server = server

    def on_CRW2PAR_NTF_EYNY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_eyny_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_EYNY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_eyny_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_eyny_list(self, content):

        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        link_prefix = 'http://www07.eyny.com/'
        links = []
        datetimes = []
        
        soup = BeautifulSoup(content, 'html.parser')
        
        link_sections = soup.find_all('th', class_='new')  # first_page:new, others_page: common
        if len(link_sections)==0:
            link_sections = soup.find_all('th', class_='common')

        for element in link_sections:
            
            for sub in element.find_all('a', class_='xst'):
                link = sub.get('href')
                links.append({
                    'url': link_prefix + link,
                    'metadata': {}
                })
            
            time_ = element.nextSibling.find('em')
            if not time_:
                continue
    
            dt = datetime.strptime(time_.text, '%Y-%m-%d')
            datetimes.append(dt)
        
        if soup.find('a', class_='nxt') is not None:
            next_page = soup.find('a', class_='nxt').get('href')
            data['next_page'] = link_prefix + next_page 

        data['links'] = links
        data['latest_post_date'] = max(datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_eyny_content(self, content):
        
        def parse_author_url(soup):
            author_url = []
            if (soup.find('div', class_='authi') != None):    
                author_url.append( link_prefix + soup.find('div', class_='authi').find('a').get('href') )
            return author_url
        
        def parse_author(soup):
            author = ''
            if (soup.find('div', class_='authi') != None):    
                author = soup.find('div', class_='authi').find('a','xw1').text.strip()
            return author
        
        def parse_datetime(soup):
            if soup.find('div', class_='pti').find('span').get('title') is not None:
                time_ = soup.find('div', class_='pti').find('span')
                dt = datetime.strptime(time_.get('title'), '%Y-%m-%d %H:%M %p')           
            else:
                time_ = soup.find('div', class_='pti').find('em').text[4:]
                dt = datetime.strptime(time_, '%Y-%m-%d %H:%M %p')


            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_title(soup):
            board = soup.find('td', class_='plc ptm pbn').find('h1')
            title = board.find(id = 'thread_subject')
            return title.text.strip()

        def parse_content(soup):
            
            for script in soup.find_all('script'):
                script.extract()
            
            for edit in soup.find_all('i', class_='pstatus'):
                edit.extract()
            
            paragraphs = soup.find('td', class_='t_f').get_text(strip = True)
            return paragraphs
        
        def parse_metadata(soup):
            
            metadata = {
    #            'entry_crumb': None,
                'category': '',
                'view_count': '',
                'reply_count': '',
                'share_count' : '',
                'like_count': '',
                'upvotes_count': '',
                'fb_like_count': ''
            }
            
    #        entry_crumb = soup.find('div', class_='bm cl').find_all('a')
    #        entry = [x.text for x in entry_crumb]
    #        entry = list(dict.fromkeys(entry))
    #        metadata['entry_crumb'] = entry[2:-1]
            
            # category 
            if  soup.find('td', class_='plc ptm pbn') is not None:
                cat = soup.find('td', class_='plc ptm pbn').find('h1').find('a')
                category = cat.text.replace('[','')
                category = category.replace(']','')
                metadata['category'] = category
            
            
            # view_count, reply_count
            view_reply = soup.find('td', class_='pls ptm pbm').find('div','hm').find_all('span', class_='xi1')
            metadata['view_count'] = int(view_reply[0].text)
            metadata['reply_count'] = int(view_reply[1].text)
            
            
            # share_count, favorite_count, upvotes_count
            other_meta = soup.find('div', class_='mtw mbm cl')
            if other_meta is not None:
                metadata['share_count'] = int(other_meta.find('span', id='sharenumber').text)
                metadata['like_count'] = int(other_meta.find('span', id='favoritenumber').text)
                metadata['upvotes_count'] = int(other_meta.find('span', id='recommendv_add').text)
                        
    #        metadata['fb_like_count'] = int(html['likes'])
            
            return metadata   
        
        def parse_reply(html):
        
            soup = BeautifulSoup(html, 'lxml')
            link_prefix = 'http://www07.eyny.com/'
        
            # Extract the useless strings
            for comment_tag in soup.find_all('h3', class_='psth xs1'):
                comment_tag.extract()
        
            for quote in soup.find_all('div', class_='quote'):
                quote.extract()
            
            for edit in soup.find_all('i', class_='pstatus'):
                edit.extract()
            
            # 留言樓層
            floor = soup.find('div', class_='wp cl').find_all('div', id=re.compile(r'post_'))
            
            whole_comment = []
        
            for ele in floor:
                try:
                    whole_comment_data = {
                            'author':'',
                            'author_url':'',
                            'comment_id':'',
                            'content':'',
                            'datetime':'',
                            'source':'eney',
                            'metadata': {},
                            'comments':[]
                            }
                    
                    comment_author_url = ele.find('a', class_='xw1').get('href')
                    comment = ele.find('td', id=re.compile(r'postmessage_')).text
                    comment_date = ele.find('div', class_='pti').find('span').get('title')
                    dt = datetime.strptime(comment_date, '%Y-%m-%d %H:%M %p')
                    
                    whole_comment_data['author_url'] = link_prefix + comment_author_url
                    whole_comment_data['author'] = ele.find('a', class_='xw1').text
                    whole_comment_data['content'] = comment.replace('\n', '')
                    whole_comment_data['datetime'] = dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
                    
                    comment_replies = []
                    for cm_reply in ele.find('div', class_='cm'):
                        for replier in cm_reply.find_all('div', class_='psti'):
                            data = {
                                    'author':'',
                                    'author_url':'',
                                    'comment_id':'',
                                    'content':'',
                                    'datetime':'',
                                    'metadata': {},
                                    }
                            
                            cm_reply_time = replier.contents[2].next.next.get('title')
                            dt = datetime.strptime(cm_reply_time, '%Y-%m-%d %H:%M %p')
                            cm_reply_author_url = replier.find('a', class_='xi2 xw1')
                            comment_reply = replier.text.split('\xa0')
                            
                            data['content'] = comment_reply[1:-2][0].replace('\n', '')
                            data['author_url'] = link_prefix + cm_reply_author_url.get('href')
                            data['author'] = cm_reply_author_url.text
                            data['datetime'] = dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
                            
                            comment_replies.append(data)
                        whole_comment_data['comments']= comment_replies
                            
                    whole_comment.append(whole_comment_data)
                    
                except:
                    pass
            
            return whole_comment
        
        def parse_comments(html):
            comment = []
            for pg in range(len(html)):
                comment.extend(parse_reply(html[pg]))
            if len(comment)!=0: # first comment is the main content
                comment.pop(0)
            return comment
        
        soup = BeautifulSoup(content['html'][0], 'html.parser')
        link_prefix = 'http://www07.eyny.com/'


        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_author_url(parse_author_url(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(content['html']))
        item.set_metadata(parse_metadata(soup))
        return item
