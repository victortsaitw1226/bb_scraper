#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PTT_GET_INDEX(Protocol):

    def __init__(self):
        super(PTT_GET_INDEX, self).__init__('PTT_GET_INDEX')
        self.update({
          'board': ''
        })

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')
