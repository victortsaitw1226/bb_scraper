# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class NextmagSpider(RedisSpider):
    name = "nextmag"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "nextmag",
            "name": "nextmag",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://tw.nextmgz.com/breakingnews/beauty",
            "scrapy_key": "nextmag:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        for element in soup.find_all('a', class_='fill-link'):
            time_ = element.find('time')
            if not time_:
                continue

            link = element.get('href')
            yield response.follow(link,
                    meta=meta,
                    callback=self.parse_article)
    
    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        content, author = self.parse_content_author(soup)
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = content
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'nextmag'
        item['proto'] = 'NEXTMAG_PARSE_ITEM'
        return item
    
    def parse_datetime(self,soup):
        time_ = soup.find('time')
        dt = datetime.strptime(time_.text, '%Y年%m月%d日')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_metadata(self,soup):
        return {
            'category': soup.find('div', class_='category').text
        }

    def parse_title(self,soup):
        board = soup.find('div', class_='category')
        title = board.find_next_siblings()[0]
        return ''.join(title.text.split())
    
    def parse_content_author(self,soup):
        content = soup.find('article').find_all('p')
        content = '\n'.join(x.text for x in content)
        content = content.split('更多壹週刊')[0]
        content = ''.join(content.split())

        regex = re.search('撰文：([^）　)]+)', content)
        if regex:
            author = regex.groups()[0]
        else:
            author = ''
        return content, author
