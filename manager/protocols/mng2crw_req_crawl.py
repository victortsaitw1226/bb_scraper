#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_REQ_CRAWL(Protocol):

    def __init__(self):
        super(MNG2CRW_REQ_CRAWL, self).__init__('MNG2CRW_REQ_CRAWL')
        self.update({
          'url': '',
          'site': ''
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_site(self, site):
        self.set('site', site)

    def get_site(self):
        return self.get('site')

if __name__ == '__main__':
    print(MNG2CRW_REQ_CRAWL.__name__)
