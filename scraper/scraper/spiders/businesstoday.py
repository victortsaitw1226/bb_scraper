# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class BusinesstodaySpider(RedisSpider):
    name = "businesstoday"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "businesstoday",
            "name": "businesstoday",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600,
            "url": "https://www.businesstoday.com.tw/catalog/80391/list/page/1/ajax",
            "scrapy_key": "businesstoday:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)

        if len(links) == 0: # the end of list page
            return

        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        next_page = self.parse_next_page(response.url)
        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        return [li['href'] for li in soup.find_all('a', 'article__item')]
    
    def parse_next_page(self, current_page):
        page = re.search(r'page/(.*)/ajax', current_page).group(1)
        next_page_count = str(int(page) + 1)
        return re.split('page/', current_page)[0] + 'page/'+ next_page_count + '/ajax'
    
    def parse_latest_post_date(self, soup):
        posts_date = []
        for dat in soup.find_all('p', 'article__item-date'):
            posts_date.append(date_parser(dat.text))
            #posts_date.append(datetime.strptime(dat.text, '%Y-%m-%d'))
        return max(posts_date)

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'businesstoday'
        item['proto'] = 'BUSINESSTODAY_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        post_time = soup.find('p', class_='context__info-item context__info-item--date')
        #date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M')
        date = date_parser(post_time.text)
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self,soup):
        return soup.find('p', class_='context__info-item context__info-item--author').text
    
    def parse_title(self,soup):
        title = soup.find('h1', class_='article__maintitle').text
        title = ' '.join(title.split())
        return title
    
    def parse_content(self,soup):
        content = []
        segment = soup.find('div', class_='cke_editable font__select-content')
        content = ''.join(x for x in segment.text)
        content = ''.join(content.split())
        return content

    def parse_metadata(self,soup):

        def parse_tag(soup):
            tags_content = []
            tags = soup.find_all('a', class_='context__tag')
            for tag in tags:
                tags_content.append(tag.text)
            return tags_content

        # 文章分類、關鍵字
        metadata = {
            'category': soup.find('p', class_='context__info-item context__info-item--type').text,
            'tag': parse_tag(soup),
        }
        return metadata
    

