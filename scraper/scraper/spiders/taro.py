# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class TaroSpider(RedisSpider):
    name = "taro"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return

        requests = [{
            "media": "taro",
            "name": "taro",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "https://taronews.tw/category/politics/",
            "scrapy_key": "taro:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        next_page = self.parse_next_page(soup)
        latest_datetime, links = self.parse_links_dates(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self, soup):
        if soup.find('div', class_='older').a is not None:
            next_page = soup.find('div', class_='older').a.get('href')
        return next_page

    def parse_links_dates(self, soup):
        links = []
        datetimes = []
        
        # Older ones
        for ele in soup.find_all('div', class_='col-sm-8 content-column'):
            for news in ele.find_all('div', class_='item-inner'):
                link = news.find('a', class_='post-title post-url').get('href')
                links.append(link)
            
                time_ = news.find('time', class_='post-published updated')

                if not time_:
                    continue
        
                dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+08:00')
                datetimes.append(dt)
        
        latest_post_date = max(datetimes)

        return latest_post_date, links
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'taro'
        item['proto'] =  'TARO_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        time_ = soup.find('time', class_='post-published updated')
        dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+08:00')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self, soup):
        if (soup.find('span', class_='post-author-name') == None):
            authors = ''
        else:
            authors = soup.find('span', class_='post-author-name').find('b').get_text(strip=True)
        return authors

    def parse_title(self, soup):
        board = soup.find('h1', class_='single-post-title')
        return board.get_text(strip=True)

    def parse_content(self, soup):
        for script in soup.find_all('script'):
            script.extract()
        paragraphs = soup.find('div', class_='entry-content clearfix single-post-content').find_all('p')
        contents = []
        for pp in paragraphs:
            text = pp.get_text(strip=True)
            contents.append(text)
        contents = '\n'.join(contents)
        return contents

    def parse_metadata(self, soup):
        metadata = {
            'category': '',
            'tag': [],
        }
        category_tag = soup.find('div', class_='term-badges floated')
        category = [y.get_text() for y in category_tag]   
        keyword_tag = soup.find('div', class_='entry-terms post-tags clearfix')
        if  keyword_tag is not None:
            keywords = keyword_tag.find_all('a')
            kword = [x.get_text() for x in keywords]
            metadata['tag'] = kword
        metadata['category'] = category[0]
        return metadata
