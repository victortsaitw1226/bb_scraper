#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback
import requests
import sys


class GAMER:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def on_MNG2CRW_NTF_GAMER_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_GAMER_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            content = self.goto(url)
            comment_url = url.replace('detail.php','ajax/gnn_list_comment.php')
            comments = self.goto(comment_url)
            html = {'html': content, 'comment_html': comments, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))


        if 'location.replace' in html['html'] :
            print('url location change')

        else:
            to_notify = CRW2PAR_NTF_NEWS_POST() 
            to_notify.set_content(html)
            to_notify.set_media(from_notify.get_media())
            self.server.send(to_notify)

            
