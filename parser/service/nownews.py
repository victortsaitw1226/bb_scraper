#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class NOWNEWS:
    def __init__(self, server):
        print('init nownews.py')
        self.server = server

    def on_CRW2PAR_NTF_NOWNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_nownews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_NOWNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_nownews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_nownews_list(self, content):
    
        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        links = []
        datetimes = []
        dummy = []
        soup = BeautifulSoup(content, 'lxml')
        
        main_content = soup.find('div', class_='td-main-content-wrap td-container-wrap')
        #all_news_div = main_content.find('div', class_= 'wpb_wrapper')
        for element in main_content.find_all('div', class_ = 'td-module-thumb'):
            
            link = element.a.get('href')
            
            if link not in dummy:
                links.append({
                    'url': link,
                    'metadata': {}
                })
            
            dummy.append(link)
            
            time_ = element.findNext('div').find('time', class_ = 'entry-date updated td-module-date')
            if not time_:
                continue
    
            dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+00:00')
            datetimes.append(dt)
        
        next_page = soup.find('div', class_='page-nav td-pb-padding-side')
        
        if next_page is not None:
            if next_page.find('i', class_='td-icon-menu-right') is not None:
                next_page_link = next_page.find('i', class_='td-icon-menu-right').previous.get('href')
                data['next_page'] = next_page_link
        
        data['links'] = links
        data['latest_post_date'] = min(datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'] )
        item.set_urls(data['links'])

        return item

    def parse_nownews_content(self, content):

        def parse_datetime(soup):
            time_ = soup.find('time')
            dt = datetime.strptime(time_.text, '%Y-%m-%d %H:%M:%S')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_metadata(soup, fb_like_count_html=None):
            
            metadata = {
                'category': '',
                'tag': [],
                'fb_like_count':''
            }
            
            metadata['category'] = soup.find('span','td-bred-no-url-last').text
            #metadata['fb_like_count'] = fb_like_count_html.find('span',{'id':'u_0_3'}).text

            if  soup.find('ul', class_='td-tags td-post-small-box clearfix') is not None:
                keywords = soup.find('ul', class_='td-tags td-post-small-box clearfix').find_all('li')
                kword = [x.text for x in keywords]
                metadata['tag'] = kword[1:]
                        
            return metadata

        def parse_title(soup):
            board = soup.find('header', class_='td-post-title').find('h1')
            return board.text.strip()

        def parse_author(soup):
            if (soup.find('div', class_='td-post-author-name') == None):
                author = ''
            else:
                author = soup.find('div', class_='td-post-author-name').get_text(strip = True)
                if '記者' in author:
                    authors = re.search(u"[\u4e00-\u9fa5]+", author).group()
                    authors = authors[2:]
                else:
                    authors = author
            return authors

        def parse_content(soup):
            
            for script in soup.find_all('script'):
                script.extract()
            
            paragraph = []
            paragraphs = soup.find('div', class_='td-post-content').find_all('p')
            
            for pp in paragraphs:
                if any(text not in pp.text for text in ('Decrease font size', 'Increase font size', 'Reset font size')):
                    paragraph.append(pp.get_text(strip=True))
            
            content = '\n'.join(paragraph)
                    
            return content
        
        

        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        author = parse_author(soup)
        title =  parse_title(soup)
        date = parse_datetime(soup)
        content = parse_content(soup)
        metadata = parse_metadata(soup)
        #metadata = parse_metadata(soup, fb_like_count_html)
        #comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item
