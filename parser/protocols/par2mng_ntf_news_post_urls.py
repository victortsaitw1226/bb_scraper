#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_NEWS_POST_URLS(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_NEWS_POST_URLS, self).__init__('PAR2MNG_NTF_NEWS_POST_URLS')
        self.update({
          'urls': [],
          'name': '',
          'media': '',
          'days_limit': 0,
          'next_page_url': 1,
          'latest_post_date': ''
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')

    def set_next_page_url(self, url):
        self.set('next_page_url', url)

    def get_next_page_url(self):
        return self.get('next_page_url')

    def set_latest_post_date(self, date):
        self.set('latest_post_date', date)

    def get_latest_post_date(self):
        return self.get('latest_post_date')

    def set_media(self, media):
        self.set('media', media)

    def get_media(self):
        return self.get('media')

    def set_name(self, name):
        self.set('name', name)

    def get_name(self):
        return self.get('name')
