# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId

def parse(data):
    comment_body = []
        
    source_id = str(data.get('_id', ''))
    docurl = data.get('SourceUrl', '')
     
    pdate = data.get('PostDate', '1971-01-01T00:00:00')
    cdate = data.get('CreateDate', '1971-01-01T00:00:00')
    title = data.get('Title', '')
    author = data.get('Author', 'anonymous')
    author_id = data.get('Author_ID', 'anonymous')
    board = data.get('SourceAreaName', '')
    content_type = data.get('ContentType', '')

    body = dict({
        "data_id": source_id,
        "postauthor": author,
        "Author_ID": author_id,
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": board,
        "content": data.get("SourceContent", ''),
        "media": data.get("SourceName", ''),
        "SourceType": data.get("SourceType", ''),
        "docurl": docurl,
        "coverurl": docurl,
        "PostDate": pdate,
        "contentType": content_type,
        "candidate": '',
        "mediatype": 3,
        "datafrom": 3,
        "title": title,
        "brocount": 0,
        "repcount": 0,
        "datatype": 2,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2,
        "candidates": []
    })

    return body

