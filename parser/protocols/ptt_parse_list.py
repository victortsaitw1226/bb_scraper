#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PTT_PARSE_LIST(Protocol):

    def __init__(self):
        super(PTT_PARSE_LIST, self).__init__('PTT_PARSE_LIST')
        self.update({
          'urls': [],
          'next_page_url': '',
          'latest_post_date': '',
          'board': ''
        })

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')

    def set_next_page_url(self, url):
        self.set('next_page_url', url)

    def get_next_page_url(self):
        return self.get('next_page_url')

    def set_latest_post_date(self, date):
        self.set('latest_post_date', date)

    def get_latest_post_date(self):
        return self.get('latest_post_date')
