#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
import requests
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class LIFE:
    def __init__(self, server):
        print('init life.py')
        self.server = server

    def on_CRW2PAR_NTF_LIFE_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_life_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_LIFE_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_life_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_life_list(self, content):

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(content['next_page'])
        item.set_latest_post_date(content['latest_post_date'])
        item.set_urls(content['links'])

        return item

    def parse_life_content(self, content):
        
        def parse_datetime(soup):
            post_time = soup.find('span', class_='d-num')
            post_time = ''.join(post_time.text.split())
            date = datetime.strptime(post_time, '%Y-%m-%d')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            author_name = soup.find('a', class_='corange tdu').text
            return author_name

        def parse_author_url(soup):
            author_postfix = soup.find('a', class_='corange tdu')['href']
            return 'https://life.tw' + author_postfix

        def parse_title(soup):
            title = soup.find('div', class_='aricle-detail-top').find('h1').text
            return ''.join(title.split())
        
        def parse_content(soup):
            text = []
            text = soup.find_all('p')
            text = ''.join([x.text for x in text])
            text = ''.join(text.split())
            return text

        def parse_category(soup):
                category = soup.find(
                    'div', class_='path shadow radius5 mb20 cb').find_all('strong')[1]
                return ''.join(category.text.split())


        soup = BeautifulSoup(content['html'], 'html.parser')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        #fb_share_count_html =  BeautifulSoup(content['fb_share_count'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        article_html = BeautifulSoup(content['article_html'], 'html.parser')

        url = content['url']
        title = parse_title(soup)
        author = parse_author(soup)
        author_url = parse_author_url(soup)
        date = parse_datetime(soup)
        content = parse_content(article_html)
        
        #fb_like_count = fb_like_count_html.find('span',{'id':'u_0_1'}).text
        #fb_share_count = fb_share_count_html.find('span',{'id':'u_0_1'}).text
        category = parse_category(soup)
        #metadata = {'category': category,'fb_like_count':fb_like_count,'fb_share_count':fb_share_count}
        metadata = {'category': category}
        
        #comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_author_url(author_url)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item
