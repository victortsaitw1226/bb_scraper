# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class MatchSpider(RedisSpider):
#class MatchSpider(scrapy.Spider):
    name = "match"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "match",
            "name": "match",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://m.match.net.tw/pc/news/list/1/104",
            "scrapy_key": "match:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        next_page = self.parse_next_page(soup)
        #links, latest_datetime= self.parse_links_dates(soup)

        dt_list = []
        news=soup.find("div","single-con1").find_all('li')
        for ind in range(len(news)):
            link = news[ind].find("a")['href']

            date_=news[ind].find("span","date").text
            date_=re.search("(\d+月\d+日.星期[\u4E00-\u9FFF].[\u4E00-\u9FFF]+.\d+.\d+)",date_).group()
            map_table =  {"星期一": "Mon" ,
            "星期二": "Tue",
            "星期三": "Wed",
            "星期四": "Thu",
            "星期五": "Fri",
            "星期六": "Sat",
            "星期日": "Sun",
            "下午":"PM",
            "上午":"AM"}
            res = ' '.join([map_table.get(i, i) for i in date_.split()])
            year = datetime.today().year
            dt=datetime.strptime(str(year)+res,"%Y%m月%d日 %a %p %I:%M")

            meta['post_datetime'] = dt
            yield response.follow(link,
                    meta = meta,
                    callback=self.parse_article)
            

            dt_list.append(dt)

        latest_datetime = max(dt_list)


        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self, soup):
        try:
            next_page = soup.find("div", "tab-pager").find('a','tabnext')['href']
        except:
            next_page = None
        return next_page

    def parse_links_dates(self, soup):
        links = []
        dt_list = []
        news=soup.find("div","single-con1").find_all('li')
        for ind in range(len(news)):
            link = news[ind].find("a")['href']
            links.append(link)

            date_=news[ind].find("span","date").text
            date_=re.search("(\d+月\d+日.星期[\u4E00-\u9FFF].[\u4E00-\u9FFF]+.\d+.\d+)",date_).group()
            map_table =  {"星期一": "Mon" ,
            "星期二": "Tue",
            "星期三": "Wed",
            "星期四": "Thu",
            "星期五": "Fri",
            "星期六": "Sat",
            "星期日": "Sun",
            "下午":"PM",
            "上午":"AM"}
            res = ' '.join([map_table.get(i, i) for i in date_.split()])
            year = datetime.today().year
            dt=datetime.strptime(str(year)+res,"%Y%m月%d日 %a %p %I:%M")
            dt_list.append(dt)
        
        return links, max(dt_list)
    

    def parse_article(self, response):
        meta=response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
       
        author = self.parse_author(soup)
        metadata = self.parse_metadata(soup)
        metadata['news_source'] = author

        item = NewsItem()
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = meta['post_datetime'] #self.parse_datetime(soup)
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = 'match'
        item['proto'] =  'MATCH_PARSE_ITEM'
        return item


    def parse_datetime(self, soup):
        published_time = soup.find(
                "meta", {"property": "article:published_time"})["content"]
        published_time = datetime.strptime( published_time,'%Y-%m-%dT%H:%M:%SZ')
        published_time = published_time+timedelta(hours=8)
        published_time = published_time.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return published_time

    def parse_author(self, soup):
        try:
            writer=soup.find("p","date").text
            writer =writer.split("\xa0\xa0")[0]
        except:
            writer = ''
        return writer

    def parse_title(self, soup):
        return soup.h3.text.strip().replace(u'\u3000',
                                        u' ').replace(u'\xa0', u' ')

    def parse_content(self, soup):
        if soup.find('div','more-read')!=None:
            soup.find('div','more-read').decompose()

        if soup.find('div',{'id':'_popIn_recommend_word'})!=None:
            soup.find('div',{'id':'_popIn_recommend_word'}).decompose()
        
        if soup.find('div','relnav')!=None:
            soup.find('div','relnav').decompose()

        content_sec = soup.find('div',{'itemprop':'articleBody'}) 
        content = ' '.join(content_sec.text.split())
        content = content.replace('\x00','')

        if '【延伸閱讀】' in content:
            content = content.split('【延伸閱讀】')[0]
        return content

    def parse_metadata(self, soup):
        try:
            updated_time = soup.find(
                "meta", {"property": "article:modified_time"})["content"]
            updated_time = updated_time + "+0800"
        except:
            updated_time = ""
        category = soup.find('div','newbox-hd').find('h2').text
        return {"update_time": updated_time, "category": category}
    
