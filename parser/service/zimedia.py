#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class ZIMEDIA:
    def __init__(self, server):
        self.server = server

    def on_CRW2PAR_NTF_ZIMEDIA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_zimedia_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())        
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ZIMEDIA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        if not content['html']:
            return

        item = self.parse_zimedia_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', result['url'], result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_zimedia_list(self, content):     

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(content['next_page'])
        item.set_latest_post_date(content['latest_post_date'])
        item.set_urls(content['links'])
        
        return item

    def parse_zimedia_content(self, content):

        def parse_datetime(soup):
            info = soup.find('div', class_='gl_mc-outer1000 zi_ae2-article-outer')
            time_ = info.get('data-published-time')
            dt = datetime.strptime(time_.split('.')[0], '%Y-%m-%dT%H:%M:%S')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
        def parse_metadata(soup):
            
            metadata = {
                'category': '',
                'tag': []
            }
            
            entry_crumb = soup.find('ul', class_='zi_breadcrumbs').find_all('a')
            entry = [x.get_text(strip=True) for x in entry_crumb]
            entry = list(dict.fromkeys(entry))
            
            if  soup.find('div', class_='zi_ae2-tag-wrap cl-118FAA') is not None:
                keywords = soup.find('div', class_='zi_ae2-tag-wrap cl-118FAA').find_all('a')
                kword = [x.get_text(strip=True) for x in keywords]
                metadata['tag'] = kword
            
            metadata['category'] = entry[-1]
            
            return metadata
    
        def parse_title(soup):
            board = soup.find('h1', class_='zi_fz24')
            title = board.get_text(strip=True)
            return title
    
        def parse_content(soup):
            
            contents = soup.find(itemprop="articleBody").get_text(strip=True)
            
            return contents
        
        soup = BeautifulSoup(content['html'], 'html.parser')
        
        url = content['url']
        
        try:
            info = soup.find('div', class_='gl_mc-outer1000 zi_ae2-article-outer')
            authors = info.get('data-author')
        except:
            authors = ''
            pass
        
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(authors)
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        #item.set_comment(comment)
        item.set_metadata(parse_metadata(soup))
        return item

