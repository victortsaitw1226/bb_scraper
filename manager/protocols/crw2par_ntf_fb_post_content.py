#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_FB_POST_CONTENT(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_FB_POST_CONTENT, self).__init__('CRW2PAR_NTF_FB_POST_CONTENT')
        self.update({
          'post_url': '',
          'content': '',
        })

    def set_post_url(self, post_url):
        self.set('post_url', post_url)

    def get_post_url(self):
        return self.get('post_url')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
