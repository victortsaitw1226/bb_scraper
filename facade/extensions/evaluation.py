import traceback
import sys
from nameko.extensions import DependencyProvider
import requests


class Evaluation(DependencyProvider):
    def setup(self):
        self.url = self.container.config['EVALUATION_URL']

    def get_dependency(self, worker_ctx):
        def goto(body):
            value = {
              'sentences': [body]        
            }
            response = requests.post(self.url, json=value)
            j = response.json()
            return j.get('results', [0])[0]
        return goto
