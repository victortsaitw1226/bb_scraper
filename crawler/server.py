#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import sys
import importlib
from nameko.web.handlers import http
from nameko.events import EventDispatcher, event_handler, SINGLETON
from nameko.timer import timer
from protocols.protocol import Protocol
from protocols.mng2crw_ntf_ptt_get_index import MNG2CRW_NTF_PTT_GET_INDEX
from protocols.mng2crw_ntf_ptt_get_post import MNG2CRW_NTF_PTT_GET_POST
from protocols.mng2crw_ntf_fb_get_fan_page import MNG2CRW_NTF_FB_GET_FAN_PAGE
from protocols.mng2crw_ntf_fb_get_post import MNG2CRW_NTF_FB_GET_POST
from protocols.mng2crw_ntf_youtube_get_video_list import MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST
from protocols.mng2crw_ntf_youtube_get_comments import MNG2CRW_NTF_YOUTUBE_GET_COMMENTS
from protocols.mng2crw_ntf_youtube_get_info import MNG2CRW_NTF_YOUTUBE_GET_INFO
from protocols.mng2crw_ntf_dcard_get_forum_list import MNG2CRW_NTF_DCARD_GET_FORUM_LIST
from protocols.mng2crw_ntf_dcard_get_post_list import MNG2CRW_NTF_DCARD_GET_POST_LIST
from protocols.mng2crw_ntf_dcard_get_post import MNG2CRW_NTF_DCARD_GET_POST
from datetime import datetime
from extensions.logger import Logger
from extensions.browser import Browser
from extensions.redis import Redis
from extensions.new_requests import New_Requests

from nameko.dependency_providers import Config
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST


class Crawler:
    name = 'crawler'
    dispatch = EventDispatcher()
    config = Config()
    log = Logger(
        name=os.environ.get('POD_NAME', 'crawler'),
        env=os.environ.get('ENV', 'debug')
    )
    redis = Redis()
    browser = Browser()
    new_requests = New_Requests()
    instances = {}

    def get_media_instance(self, media):
        if media in Crawler.instances:
            instance = Crawler.instances[media]
        else:
            my_module = importlib.import_module('service.{}'.format(media))
            MyClass = getattr(my_module, media.upper())
            instance = MyClass(self)
            Crawler.instances[media] = instance
        return instance

    def __init__(self):
        super(Crawler, self).__init__()

    def send(self, protocol):
        proto = protocol.get_proto_name()
        self.dispatch(proto, str(protocol))

    '''
    NEWS
    '''
    @event_handler(
        'manager', MNG2CRW_NTF_NEWS_GET_INDEX.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_NEWS_GET_INDEX(self, packet):
        request = MNG2CRW_NTF_NEWS_GET_INDEX()
        request.parse(packet)
        try:
            site = request.get_media()
            instance = self.get_media_instance(site)
            protocol_name = ('_').join(
                ['on_MNG2CRW_NTF', site.upper(), 'GET_INDEX'])
            getattr(instance, protocol_name)(packet)
        except Exception as e:
            self.log.exception(e)

    @event_handler(
        'manager', MNG2CRW_NTF_NEWS_GET_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_NEWS_GET_POST(self, packet):
        
        request = MNG2CRW_NTF_NEWS_GET_POST()
        request.parse(packet)
        try:
            site = request.get_media()
            instance = self.get_media_instance(site)
            protocol_name = ('_').join(
                ['on_MNG2CRW_NTF', site.upper(), 'GET_POST'])
            now = datetime.now()
            getattr(instance, protocol_name)(packet)
            diff = datetime.now() - now
            print('time spending {}:{}'.format(protocol_name, diff.seconds))
        except Exception as e:
            self.log.exception(e)
            #self.log.error("media: {}, proto: {}, error: {}".format(
            #    request.get_media(), request.get_proto_name(), traceback.format_exc()))

    '''
    Youtube
    '''
    @event_handler(
        'manager', MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST(self, packet):
        try:
            instance = self.get_media_instance('youtube')
            getattr(instance, 'on_MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))


    @event_handler(
        'manager', MNG2CRW_NTF_YOUTUBE_GET_COMMENTS.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_YOUTUBE_GET_COMMENTS(self, packet):
        try:
            instance = self.get_media_instance('youtube')
            getattr(instance, 'on_MNG2CRW_NTF_YOUTUBE_GET_COMMENTS')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    '''
    Facebook
    '''
    @event_handler(
        'manager', MNG2CRW_NTF_FB_GET_FAN_PAGE.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_FB_GET_FAN_PAGE(self, packet):
        try:
            instance = self.get_media_instance('facebook')
            getattr(instance, 'on_MNG2CRW_NTF_FB_GET_FAN_PAGE')(packet)
        except Exception as e:
            self.log.error("error:{}, packet:{}".format(str(e), packet))

    @event_handler(
        'manager', MNG2CRW_NTF_FB_GET_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_FB_GET_POST(self, packet):
        try:
            instance = self.get_media_instance('facebook')
            getattr(instance, 'on_MNG2CRW_NTF_FB_GET_POST')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    '''
    PTT
    '''
    @event_handler(
        'manager', MNG2CRW_NTF_PTT_GET_INDEX.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_PTT_GET_INDEX(self, packet):
        try:
            instance = self.get_media_instance('ptt')
            getattr(instance, 'on_MNG2CRW_NTF_PTT_GET_INDEX')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    @event_handler(
        'manager', MNG2CRW_NTF_PTT_GET_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_PTT_GET_POST(self, packet):
        try:
            instance = self.get_media_instance('ptt')
            getattr(instance, 'on_MNG2CRW_NTF_PTT_GET_POST')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    '''
    DCard
    '''
    @event_handler(
        'manager', MNG2CRW_NTF_DCARD_GET_FORUM_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_DCARD_GET_FORUM_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_MNG2CRW_NTF_DCARD_GET_FORUM_LIST')(packet)
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    @event_handler(
        'manager', MNG2CRW_NTF_DCARD_GET_POST_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_DCARD_GET_POST_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            now = datetime.now()
            getattr(instance, 'on_MNG2CRW_NTF_DCARD_GET_POST_LIST')(packet)
            diff = datetime.now() - now
            print('time spending {}:{}'.format('MNG2CRW_NTF_DCARD_GET_POST_LIST', diff.seconds))
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))

    @event_handler(
        'manager', MNG2CRW_NTF_DCARD_GET_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_MNG2CRW_NTF_DCARD_GET_POST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            now = datetime.now()
            getattr(instance, 'on_MNG2CRW_NTF_DCARD_GET_POST')(packet)
            diff = datetime.now() - now
            print('time spending {}:{}'.format('MNG2CRW_NTF_DCARD_GET_POST', diff.seconds))
        except Exception as e:
            self.log.exception(e)
            #self.log.error("error:{}, packet:{}".format(str(e), packet))
