#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import json
import requests


class TVBS:
    def __init__(self, server):
        print('init tvbs.py')
        self.server = server
        self.session = requests.session()

    def on_CRW2PAR_NTF_TVBS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_tvbs_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TVBS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_tvbs_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_tvbs_list(self, content):

        def send_xhr(url, startID):
            # send XHR to get latest 100 news of each board

            board = url.split('/')[-1]
            cateID = {
                'politics': '7', 
                'local': '1', 
                'entertainment': '5',
                'life': '2'
            }
            response  = self.session.post(
                url= ('&').join(['https://news.tvbs.com.tw/news/LoadMoreOverview?limit=100&offset=11', 
                                    'cateid='+cateID.get(board), 'cate='+board, 'newsid='+str(startID)]))
            response = json.loads(response.text)
            urls = [startID]
            news_id_list = response['news_id_list'].split(',')
            news_id_list = [i.replace("'", '') for i in news_id_list]
            urls.extend(news_id_list)

            prefix = 'https://news.tvbs.com.tw'
            urls = [ {'url':('/').join([prefix, board, i]) , 'metadata':{'category':board}} for i in urls]
            
            return urls

        def get_startID(soup):
            startID = soup.select('.content_center_contxt_box_news a')[0]['data-news-id']
            startID = startID.replace("'", '')
            return startID

        def parse_latest_post_date(soup):
            timestamp = soup.select('.content_center_contxt_box_news .icon_time.time')[0].text
            timestamp = datetime.strptime(timestamp, '%Y/%m/%d %H:%S')
            return timestamp.strftime('%Y-%m-%dT%H:%M:%S+0800')

        soup = BeautifulSoup(content['html'], 'html.parser')
        startID = get_startID(soup)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(None)
        item.set_latest_post_date(parse_latest_post_date(soup))
        item.set_urls(send_xhr(content['url'], startID))

        return item

    def parse_tvbs_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find("div","icon_time time leftBox2").text
            dt = datetime.strptime(time_, '%Y/%m/%d %H:%M')
            dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            try:
                writer = soup.find("h4",'font_color5 leftBox1').a.text
            except:
                writer = ''
            return writer

        def parse_title(soup):
            return soup.h1.text.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            content = soup.find(id='news_detail_div').text.strip().replace(u'\u3000',
                                        u' ').replace(u'\xa0', u' ').replace("最HOT話題在這！想跟上時事，快點我加入TVBS新聞LINE好友！"," ")

            return content

        def parse_metadata(soup):
            try :
                updated_time=soup.find('div','icon_time time margin_b5').text
                updated_time=re.search("[0-9]{4,}/[0-9]{2,}/[0-9]{2,} [0-9]{2,}:[0-9]{2,}",updated_time).group()
                dt = datetime.strptime(updated_time, '%Y/%m/%d %H:%M')
                updated_time=dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                updated_time=''
            try:
                keys=soup.find("div","newsdetail_content_adWords_box margin_b45").text
                keys =re.findall(r"[\u4e00-\u9fa5]+" ,keys)
            except:
                keys =''
            return {
                'key': keys,
                "updated_time" :updated_time,
                'category':content['url']['metadata']['category']
            }

        soup = BeautifulSoup(content['html'], 'html.parser')
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url']['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment('')
        item.set_metadata(parse_metadata(soup))
        return item
