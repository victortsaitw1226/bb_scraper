# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class LinetodaySpider(RedisSpider):
    name = "linetoday"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "linetoday",
            "name": "linetoday",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://today.line.me/TW/pc/main/100259",
            "scrapy_key": "linetoday:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)

        for url in links:
            yield scrapy.Request(url['url'],
                    meta = {'articleid':url['articleid']},
                    callback=self.parse_article)
        

    def parse_links(self,soup):
        group = soup.find('li',{'class':'on _link _category'}).find('a')['href'].split('/')[-1]
        links = [{'url':s['href'], 'articleid':s['data-articleid']} for s in soup.find('div', {'class': 'left-area'}).findAll('a')]
        try:
            links.extend([{'url':s['href'], 'articleid':s['data-articleid']} for s in soup.find('ol', {'class': 'list-type-rk _side_popular_'+group}).findAll('a')])
        except:
            pass
        links = [dict(t) for t in set(tuple(sorted(d.items())) for d in links)]
        return links

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        metadata = self.parse_metadata(soup,meta['articleid'])
        title = self.parse_title(soup)
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = title
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = 'linetoday'
        item['proto'] = 'LINETODAY_PARSE_ITEM'
        yield item

        # crawl comments
        comment_url_pattern = "https://api.today.line.me/webapi/comment/list?articleId={articleid}&limit=10&country=TW&replyCount=true&sort=POPULAR&pivot={pivot}"
        pivot = 0
        articleid = meta['articleid']
        meta = {'comment_url_pattern':comment_url_pattern, 
                'pivot':pivot, 
                'articleid':articleid, 
                'metadata':metadata,
                'url':response.url, 
                'article_title':title}
        yield scrapy.Request(comment_url_pattern.format(articleid = articleid, pivot = pivot),
                dont_filter=True,
                meta=meta,
                callback=self.parse_comments)


    def parse_comments(self, response):
        meta = response.meta
        res = json.loads(response.text)

        # no replies
        if res['message'] != 'SUCCESS':
            return

        all_msg = res['result']['comments']['comments']
        
        # no more replies    
        if all_msg ==[]:
            return
    
        for r in all_msg:
            item = NewsItem()

            reply_metadata = {
                'category':meta['metadata']['category'],
                'news_source':meta['metadata']['news_source'],
                'like_count': r['ext']['likeCount']['up'] if 'up' in r['ext']['likeCount'].keys() else '',
                'dislike_count': r['ext']['likeCount']['down'] if 'down' in r['ext']['likeCount'].keys() else '',
                'reply_count':r['ext']['replyCount']}

            item['url'] = meta['url']
            item['metadata'] = reply_metadata
            item['article_title'] = meta['article_title']
            item['author'] = r['displayName'] if 'displayName' in r.keys() else ''
            item['author_url'] = []
            item['date'] = datetime.fromtimestamp(r['createdDate']/1000).strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = r['contents'][0]['extData']['content']
            item['content_type'] = 1
            item['media'] = 'linetoday'
            item['proto'] = 'LINETODAY_PARSE_ITEM'
            item['comment'] = []
            yield item

        meta['pivot'] +=10 
        yield scrapy.Request(meta['comment_url_pattern'].format(articleid = meta['articleid'], pivot = meta['pivot']),
                dont_filter=True,
                meta=meta,
                callback=self.parse_comments)



    def parse_datetime(self, soup):
        try:
            date = soup.find('dd',{'itemprop':'datePublished'}).text
            date = re.split(' ',date)[1]
            date = datetime.strptime(date , '%Y年%m月%d日%H:%M')
            date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        except: # LINE TV has no datetime
            date = ''
        return date
    
    def parse_author(self, soup):
        try:
            author = soup.find('dd','name').text.strip()
            if '|' in author:
                author = author.split('|')[1]
        except:
            author = soup.find('meta', {'name':'author'})['content']
        return author
    
    def parse_title(self, soup):
        title = soup.find('head').find('title').text
        title = ' '.join(title.split())
        return title
    
    def parse_content(self, soup):
        try:      
            content = soup.find('div',{'id':'container'}).find('article')
            for span_tag in content.findAll('a'):
                span_tag.replace_with('')
            content = content.find_all('p')    
            content = '\n'.join([x.text for x in content ])
            content = content.replace('\xa0','').replace('【延伸閱讀】','').replace('<延伸閱讀>','').replace('延伸閱讀：','')
        except:
            content = soup.find('head').find('meta',{'name':'description'})['content']
        return content

    def parse_category(self, soup):
        site_name = soup.find('meta',{'property':'og:site_name'})['content']
        if site_name=="LINE TV":
            category = 'LINE TV'
        else:
            news_keywords = soup.find('meta',{'name':'news_keywords'})['content']
            category = news_keywords.split()[-1]
        return category

    def parse_update_date(self, soup):
        try:
            date = soup.find_all('dd','date')[1].text
            date = re.split(' ',date)[1]
            date = datetime.strptime(date , '%Y年%m月%d日%H:%M')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            date = ''
        return date
        
    def parse_metadata(self, soup, articleid):
        url = "https://api.today.line.me/webapi/article/dinfo?articleIds={articleid}&country=TW".format(articleid=articleid)
        response = requests.request("GET", url)
        raw_json = json.loads(response.text)
        metadata = {
            'like_count': raw_json['result']['commentLikes'][0]['likeViews']['count'] ,
            'reply_count': raw_json['result']['commentLikes'][0]['commentViews']['count'],
            'update_time': self.parse_update_date(soup),
            'news_source': soup.find('meta', {'name':'author'})['content'],
            'category': self.parse_category(soup)
        }
        return metadata
    
