#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
import requests

class APPLEDAILY:

    def __init__(self, server):        
        print('init appledaily')
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        raw.encoding = raw.apparent_encoding
        return raw.text 

    def on_MNG2CRW_NTF_APPLEDAILY_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            #traceback.print_exc(file=sys.stdout)
            return

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)


    def on_MNG2CRW_NTF_APPLEDAILY_GET_POST(self, packet):
        
        # def login(driver):
        #     #Login
        #     user = ('ibdo.dev@gmail.com')
        #     passw = ('ibdo2018!')
        #     driver.get("https://tw.appledaily.com/")
        #     login_button = driver.find_element_by_xpath('//*[@id="login-button"]/a[1]')
        #     driver._click(login_button)

        #     username = driver.find_element_by_xpath('//*[@name="email"]')
        #     driver.send_keys(username, user)
        #     password = driver.find_element_by_xpath('//*[@name="password"]')
        #     driver.send_keys(password, passw)
        #     driver.send_keys_return(password)

        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # login(self.server.browser)
            # try:
            #     login(self.server.browser)
            # except Exception as e:
            #     self.server.log.exception(e)
            #     #print(self.server.browser.html())
            # detail_html = self.server.browser.goto(url)  
            # fb_comment_element = self.server.browser.find_element_by_xpath('//*[@title="fb:comments Facebook Social Plugin"]')
            # fb_comment_src = self.server.browser.get_attribute( fb_comment_element, 'src')
            # self.server.browser.get(fb_comment_src)
            # self.server.browser.fb_scrollDown_click_button()
            # fb_comment_html = self.server.browser.html()
            # html = {'html': detail_html, 'comment_html':fb_comment_html, 'url': url}

            detail_html = self.goto(url)

            html = {'html': detail_html, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)

