#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class NEXTMAG:
    def __init__(self, server):
        print('init nextmag.py')
        self.server = server

    def on_CRW2PAR_NTF_NEXTMAG_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_nextmag_list(content)
        # print('parse_list: ', item)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_NEXTMAG_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_nextmag_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_nextmag_list(self, content):        

        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }

        links = []
        datetimes = []

        domain_prefix = 'https://www.nextmag.com.tw'
        soup = BeautifulSoup(content, 'html.parser')
        for element in soup.find_all('a', class_='fill-link'):
            time_ = element.find('time')
            if not time_:
                continue

            link = domain_prefix + element.get('href')
            links.append({
                'url': link,
                'metadata': {}
            })

            dt = datetime.strptime(time_.text, '%Y年%m月%d日 %H:%M')
            datetimes.append(dt)

        data['links'] = links
        data['latest_post_date'] = max(
            datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_nextmag_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find('time')
            dt = datetime.strptime(time_.text, '%Y年%m月%d日')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_metadata(soup):
            return {
                'category': soup.find('div', class_='category').text
            }

        def parse_title(soup):
            board = soup.find('div', class_='category')
            title = board.find_next_siblings()[0]
            return ''.join(title.text.split())

        # def parse_content(soup):
        #     article = soup.find('div', class_='article-content')
            
        #     paragraph = []
        #     for p in article.find_all('p'):
        #         text = ''.join(p.text.split())
        #         if len(text) == 0:
        #             continue
        #         paragraph.append(text)

        #     content = '\n'.join(paragraph)
        #     content = content.split('更多壹週刊')[0]
        #     return ''.join(content.split())

        def parse_content(soup):
            content = soup.find('article').find_all('p')
            content = '\n'.join(x.text for x in content)
            content = content.split('更多壹週刊')[0]
            return ''.join(content.split())


        # detail
        soup = BeautifulSoup(content['html'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        
        data = {
            'datetime': parse_datetime(soup),
            'author': '',
            'title': parse_title(soup),
            'content': parse_content(soup),
            'metadata': parse_metadata(soup),
            'comments': [],
        }

        regex = re.search('撰文：([^）　)]+)', data['content'])
        if regex:
            authors = regex.groups()[0]
            data['author'] = authors

        # comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(data['title'])
        item.set_author(data['author'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        # item.set_comment(comment)
        item.set_metadata(data['metadata'])
        return item
