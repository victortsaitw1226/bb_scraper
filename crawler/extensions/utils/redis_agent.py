#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import string
import random
import traceback
import sys
from .proxy import Proxy
from .bloomfilter import BloomFilter

class RedisAgent:
    def __init__(self, url, port, db, password):
        self._redis = redis.Redis(
            host=url, port=port, db=db, password=password
        )
        self.set('test_redis_key', 'test_redis_value')
        v = self.get('test_redis_key')
        self.delete('test_redis_key')
        print('init redis at:{}'.format(url))

    def get_redis(self):
        return self._redis

    def set(self, k, v):
        self._redis.set(k, v)

    def get(self, k):
        return self._redis.get(k)
     
    def delete(self, k):
        self._redis.delete(k)

    def hset(self, k, f, v):
        self._redis.hset(k, f, v)

    def hget(self, k, f):
        return self._redis.hget(k, f)

    def lpush(self, k, v):
        return self._redis.lpush(k, v)

    def rpop(self, k, v):
        return self._redis.rpop(k, v)

    def ltrim(self, k, begin=0, end=-1):
        return self._redis.ltrim(k, begin, end)

    def lrange(self, k, begin=0, end=-1):
        return self._redis.lrange(k, begin, end)

    def get_unique_id(self):
        CHARACTERS = string.ascii_letters + string.digits
        return ''.join(random.choice(CHARACTERS) for _ in range(22)).encode()

    def lock(self, lock_name, ttl=3600*24):
        lock_id = self.get_unique_id()
        if self._redis.set(lock_name, lock_id, nx=True, ex=ttl):
            return lock_id
        return None

    def unlock(self, lock_name, lock_id):
        unlock_script = """
            if redis.call("get",KEYS[1]) == ARGV[1] then
                return redis.call("del",KEYS[1])
            else
                return 0
        end"""
        self._redis.eval(unlock_script, 1, lock_name, lock_id)

    def generate_proxies(self):
        p = Proxy()
        return p.generate_proxies(self._redis)

    def remove_illegal_proxy(self):
        p = Proxy()
        return p.remove_illegal_proxy(self._redis)

    def random_proxy(self, eng=False):
        p = Proxy()
        return p.random_proxy(self._redis, eng)

    def exists_url(self, url):
        b = BloomFilter(self._redis)
        return b.exists(url)

    def insert_url(self, url):
        b = BloomFilter(self._redis)
        return b.insert(url)

    def stop(self):
        self._redis = None
