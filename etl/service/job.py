#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback
import sys
import json
import hashlib
import requests
import datetime
from bson.objectid import ObjectId
from .translation import Parser
import os
import random

class Job:
    ES_TEST_INDEX = 'validation_data'
    ES_INDEX = 'crawl_new'
    ES_DATA_INDEX = 'data'
    ES_TIMELINE_INDEX = 'youtube_timeline'
    MONGO_DB = 'history_v2'
    MONGO_DATA_COLLECTION = 'data'
    MONGO_LOGS_COLLECTION = 'logs'
    MONGO_ASR_DATA_COLLECTION = 'youtube_data'
    MONGO_ASR_TIMELINE_COLLECTION = 'youtube_timeline'
    MONGO_OCR_DATA_COLLECTION = 'paper_data'

    def __init__(self, server):
        self.server = server
        self.parser = Parser()

    def hash_code(self, data):
        str_data = json.dumps({
          'postauthor': data['postauthor'],
          'url': data['docurl'],
          'body': data['content']
        }, sort_keys=True, indent=2)
        return hashlib.md5(str_data.encode("utf-8")).hexdigest()

    def post_evaluation(self, data):
        body = data['content']
        res = self.server.evaluation(body)
        return res

    def process_internet_data(self, url, collection=None):
        now = datetime.datetime.now()
        if not collection:
            collection = '{}_{}_{}'.format(Job.MONGO_DATA_COLLECTION, now.year, now.month)
        data = self.server.mongo.find_one(
            Job.MONGO_DB, collection, {'url': url}
        )

        if data is None:
            return
        try:
            self.insertInternetDataToES(data)
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

    def insertInternetDataToES(self, data):
        media = data.get('media', '').lower()
        comment_body = self.parser.parse(media, data)

        if comment_body is not None:
            for upload_body in comment_body:
                try:
                    media = upload_body.get('SourceTypeV2', '')
                    content_type = upload_body.get('contentType', '')
                    data_id = self.hash_code(upload_body)

                    # No Need to 
                    env = os.environ.get('ENV', 'debug')
                    if env == 'validate':
                        self.server.elasticsearch.upsert(
                            Job.ES_TEST_INDEX, Job.ES_DATA_INDEX, data_id, upload_body
                        )

                    redis_result = self.server.redis.exists_es_id(data_id)
                    # Check the data id is already existed.
                    if redis_result and 0 != content_type:
                        continue
                     
                    upload_body['evaluation'] = self.post_evaluation(upload_body)

                    es_r = self.server.elasticsearch.upsert(
                        Job.ES_INDEX, Job.ES_DATA_INDEX, data_id, upload_body
                    )
                    if not redis_result:
                        self.server.redis.insert_es_id(data_id)

                    #self.process_data_for_alert(es_r['_id'], upload_body)
                except Exception as e:
                    traceback.print_exc(file=sys.stdout)
                    #raise e

    def process_data_for_alert(self, _id, data):
        media = data.get('media', '')
        content_type = data.get('contentType', '')
        if content_type != '0' or media != 'Youtube':
            return

        docboard = data.get('docboard', '')
        result = self.server.mysql.select_one(
            'SELECT * FROM ob_board WHERE board LIKE "%s"' % docboard  
        )
        if not result:
            raise Exception('MYSQL: data {} cannot find in ob_board'.format(docboard))

        now = datetime.datetime.now()
        sql = 'INSERT INTO ob_data(data_id, create_date, board_id, board,' + \
              'media, title, url, media_type, poscount, negcount, brocount,' + \
              'repcount, broadcast_date, host, category, cover_img_path) VALUES' + \
              "('" + _id + "', '" + now.strftime('%Y-%m-%d %H:%M:%S') + "', '" + result['id'] + "', '" + result['board'] + "'," + \
              "'" + result['media'] + "', '" + data['title'] + "', '" + data['docurl'] + "'," + \
              "'電視節目', " + str(data['like']) + ", " + str(data['unlike']) + ", " + str(data['brocount']) + ", " + \
              str(data['repcount']) + ",'" + data['PostDate'] + "', '" + result['host'] + "', '政論', '" + data["video_img"]+ "')"
        self.server.mysql.insert(sql)


    def process_ocr_data(self):
        while True:
            data = self.server.mongo.find_one(
                        Job.MONGO_DB,
                        Job.MONGO_OCR_DATA_COLLECTION,
                        { 'processed': { '$exists': False } })

            if data is None:
                break

            _id = str(data['_id'])

            lock = self.server.redis.lock(_id)
            if not lock:
                continue
            
            try:
                self.update_ocr_to_es(_id, data)
            except:
                traceback.print_exc(file=sys.stdout)

            self.server.redis.unlock(_id, lock)

    def update_ocr_to_es(self, _id, data):
        doc = self.parser.parse('paper', data)
        data_id = self.hash_code(doc)
        self.server.elasticsearch.upsert(
            Job.ES_INDEX, Job.ES_DATA_INDEX, data_id, doc 
        )
        self.server.mongo.update(
            Job.MONGO_DB, 
            Job.MONGO_OCR_DATA_COLLECTION,
            {'_id': ObjectId(_id)},
            {'$set': {'processed': True}}
        )


    def process_asr_data(self):
        lock_name = 'process_asr_data_lock'
        lock = self.server.redis.lock(lock_name)
        if not lock:
            return

        cursor = self.server.mongo.find(
            Job.MONGO_DB,
            Job.MONGO_ASR_DATA_COLLECTION,
            { 'processed': { '$exists': False } })

        for data in cursor:

            _id = str(data['_id'])
            try:
                self.update_asr_to_es(_id, data)
            except:
                traceback.print_exc(file=sys.stdout)

        self.server.redis.unlock(lock_name, lock)

    def update_asr_to_es(self, _id, data):
        # Get the Mongo Data
        url = data['SourceUrl']
        content = data['SourceContent']
        coverurl = data['CoverUrl']

        # Update date to ES
        doc_id, es_result = self.server.elasticsearch.update_content_cover_by_url(
            Job.ES_INDEX, Job.ES_DATA_INDEX, content, coverurl, url
        )
        if not es_result:
            return
        cursor = self.server.mongo.find(
            Job.MONGO_DB,
            Job.MONGO_ASR_TIMELINE_COLLECTION,
            {'Youtube_id': ObjectId(_id)})

        for timeline in cursor:
            timeline_id = timeline['_id']
            self.server.elasticsearch.upsert(
                Job.ES_TIMELINE_INDEX,
                Job.ES_DATA_INDEX,
                str(timeline_id), {
                    "youtube_id": doc_id,
                    "begin": int(timeline.get('Begin', '0')),
                    "end": int(timeline.get('End', '0')),
                    "content": timeline.get('Content', ''),
                    "url": timeline.get('Url', '')
            })

        self.server.mongo.update(
            Job.MONGO_DB, 
            Job.MONGO_ASR_DATA_COLLECTION,
            {'_id': ObjectId(_id)},
            {'$set': {'processed': True}}
        )
