#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class EBC:
    def __init__(self, server):
        print('init EBC.py')
        self.server = server

    def on_CRW2PAR_NTF_EBC_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ebc_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_EBC_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        
        item = self.parse_ebc_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ebc_list(self, content):

        soup = BeautifulSoup(content, 'html.parser')

        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
                
        # Get Articles url
        post_list = soup.find_all('div','style1 white-box')
        for i in range(len(post_list)):
            news={'url': None, 'metadata': {}}
            news['url'] = 'https://news.ebc.net.tw' + post_list[i].find('a')['href']
            data['links'].append(news)
            
        # Get latest_post_date
        if(data['latest_post_date']==None):
            time_stamp = soup.select('.news-list-box .text .small-gray-text')[0].text
            dat = datetime.strptime(time_stamp , '%m/%d %H:%M')
            dat = dat.replace(year=datetime.today().year )
            data['latest_post_date'] = dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        # Get next_page
        page_area = soup.find('div','page-area white-box').find_all('a')
        current_page_index = [i for i, elem in enumerate(page_area) if 'font-weight:bold' in str(elem)][0]
        page_area = page_area[int(current_page_index)+1:]
        
        next_page_index = [i for i, elem in enumerate(page_area) if 'white-btn' in str(elem)]
        if next_page_index != []:
            data['next_page'] = 'https://news.ebc.net.tw' + page_area[next_page_index[0]]['href']
        

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_ebc_content(self, content):
        
        def parse_datetime(soup):
            time_stamp = soup.select('.info .small-gray-text')[0].text.strip()
            time_stamp = ' '.join(time_stamp.split(' ')[:2])
            dat = datetime.strptime(time_stamp , '%Y/%m/%d %H:%M')
            return dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
        def parse_metadata(soup):
            metadata = {'tag':[],'category':''}
            keyword = [a.text for a in soup.select('.keyword a')]
            if len(keyword):
                metadata['tag'] =  keyword             
            metadata['category'] = soup.find('div',{'id':'web-map'}).find_all('a')[1].text
            return metadata
        
        def parse_author(soup): 
            try:
                script = json.loads(soup.select('script')[1].text)
                author = script['author']['name']
                return author
            
            except:
                return ''
                
        def parse_title(soup):
            return soup.select('h1')[0].text
        
        def parse_content(soup):
            article = soup.find('div', class_='fncnews-content')
            paragraph = []
            for p in article.find_all('p', img_=False, ):
                text = p.text.strip()
                if len(text) == 0 or text[0]=='●':
                    continue
                paragraph.append(text)
                
            content = '\n'.join(paragraph)
            content = content.split('【今日最熱門】')[0]
            return content
        

        soup = BeautifulSoup(content['html'], 'html.parser')
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        # item.set_comment(parse_comments(soup))
        item.set_metadata(parse_metadata(soup))
        return item
