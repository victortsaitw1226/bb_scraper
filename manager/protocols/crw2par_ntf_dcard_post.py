#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_DCARD_POST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_DCARD_POST, self).__init__('CRW2PAR_NTF_DCARD_POST')
        self.update({
          'content': '',
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
