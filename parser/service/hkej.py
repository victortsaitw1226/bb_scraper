#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import date, datetime, timedelta
from utils.parse_fb_comment import parse_comments

class HKEJ:
    def __init__(self, server):
        print('init hkej.py')
        self.server = server

    def on_CRW2PAR_NTF_HKEJ_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_hkej_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_HKEJ_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_hkej_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()

        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_hkej_list(self, content):

        hkej = 'https://www2.hkej.com'
        soup = BeautifulSoup(content, "lxml")
        
        try:
            next_page = hkej + '/instantnews?&page=' + str(int(soup.find('span', 'on').findNext('span').text))
        except:
            next_page = None
            
        links = [{'url':hkej+s.find('a')['href'],'metadata':[]} for s in soup.findAll('div','hkej_toc_listingAll_news2_2014')]
        
        day = soup.find('ul','hkej_online-news-menu_2014').text.strip()
        if day == '最新':
            day = date.today().strftime('%Y年%m月%d日')
        elif day == '昨日':
            day = (date.today()-timedelta(1)).strftime('%Y年%m月%d日')
        else:
            day = '2019年'+day
        time = soup.find('span','hkej_toc_top2_timeStamp_2014').text
        latest_post_date = datetime.strptime(day+' '+time, '%Y年%m月%d日 %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_hkej_content(self, content):
        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')

        url = content['url']
        dat = soup.find('span','date').text.strip()
        if '今日' in dat:
            dat = dat.replace('今日', date.today().strftime('%Y年%m月%d日'))
        elif '昨日' in dat:
            dat = dat.replace('昨日', (date.today()-timedelta(1)).strftime('%Y年%m月%d日'))
            
        try:
            dat = datetime.strptime(dat, '%Y年%m月%d日 %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            dat = datetime.strptime(dat, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        title = soup.find("h1",  id="article-title").text
        author = soup.find("meta",  property="og:site_name")['content'].strip()
        content = soup.find("div",  id="article-content").text.strip()
        
        #metadata
        category = soup.find('span','cate').text.strip()
        fb_like_count = fb_like_count_html.find('span',{'id':'u_0_3'}).text
        metadata = {'category':category, 'fb_like_count':fb_like_count}

        comment = []
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(dat)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
