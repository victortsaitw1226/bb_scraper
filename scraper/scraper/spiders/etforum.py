# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class ETforumSpider(RedisSpider):
    name = "etforum"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "etforum",
            "name": "etforum",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600 * 2,
            "url": "https://forum.ettoday.net/newslist/675/1",
            "scrapy_key": "etforum:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)

        if len(links) == 0:
            return

        for url in links:
            yield scrapy.Request(url['url'],
                    meta = meta,
                    callback=self.parse_article)

        latest_datetime = self.parse_latest_post_date(links[0]['url'])
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        split = response.url.split('/')
        current_page = int(split[-1])
        next_page_count = current_page + 1
        next_page = 'https://forum.ettoday.net/newslist/675/' + str(next_page_count)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = []
        for subsoup in soup.find_all('div', {'class': 'block_content'}):
            for news in subsoup.find_all('div', {'class': 'piece clearfix'}):
                for url in news.find_all('a', {'class': 'pic', 'ref': False}):
                    links.append({'url': url.get('href')})
        return links
    
    def parse_latest_post_date(self,first_link):
        html = requests.get(first_link).text
        soup = BeautifulSoup(html, 'html.parser')
        latest_post_date = soup.find_all('time', {'class': 'date'})[0]['datetime']
        latest_post_date = datetime.strptime( latest_post_date , '%Y-%m-%dT%H:%M:%S+08:00')
        return latest_post_date

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')

        item = NewsItem()
        item['url'] = response.url
        item['author'], item['author_url'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'etforum'
        item['proto'] =  'ETFORUM_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
        datetime = datetime[:22]+datetime[-2:] #remove ':'
        return datetime
        
    def parse_metadata(self, soup):
        metadata = { 'fb_like_count':'',
                      'category': soup.find('meta',{'property':'article:section'})['content'] }
        return metadata
        
    def parse_author(self, soup): 
        author = ''
        author_url = []
        try:
            # try Columnist
            block = soup.find_all('div', {'class': 'penname_news clearfix'})
            for ele in block:
                penname = ele.find_all('a', {'class': 'pic'})
            href = penname[0]['href']
            author = href.split('/')[-1]
            author_url.append('https://forum.ettoday.net'+href)
            return author, author_url 

        except:
            paragraph=[]
            for content in soup.find_all('div', {'itemprop': 'articleBody'}):
                for ele in content.find_all('p'):
                    text = ele.text
                    if len(text) and ('●' in list(text)):
                        paragraph.append(text)
            if(len(paragraph)):
                author_line = paragraph[0].split('●')[1]
                author_line = author_line.split('／')[0]
                author = author_line.split('，')[0]
                author = [author, None][author=='作者']
                return author, author_url

    def parse_title(self, soup):
        return soup.select('h1')[0].text
    
    def parse_content(self, soup):
        article = soup.find('div', class_='story')
        paragraph = []
        for p in article.find_all('p'):
            text = p.text.strip()
            if len(text) == 0 or text[0]=='►':
                continue
            paragraph.append(text)
            
        content = '\n'.join(paragraph)
        content = content.split('【更多鏡週刊相關報導】')[0]
        content = content.split('好文推薦')[0]
        content = content.split('熱門推薦')[0]
        content = content.split('熱門文章')[0]
        content = content.split('熱門點閱')[0]
        return content
