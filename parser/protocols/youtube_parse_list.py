#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class YOUTUBE_PARSE_LIST(Protocol):

    def __init__(self):
        super(YOUTUBE_PARSE_LIST, self).__init__('YOUTUBE_PARSE_LIST')
        self.update({
          'videos': [],
        })

    def set_videos(self, videos):
        self.set('videos', videos)

    def get_videos(self):
        return self.get('videos')
