#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re, traceback, sys
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class TWITTER:
    def __init__(self, server):
        print('init twitter.py')
        self.server = server

    def on_CRW2PAR_NTF_TWITTER_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_twitter_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TWITTER_POST(self, packet):
        print('============ on_CRW2PAR_NTF_TWITTER_POST ================')
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_twitter_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_twitter_list(self, content):

        def parse_datetime(dat):
            dat = datetime.strptime(dat,  '%a %b %d %H:%M:%S +0000 %Y')
            dat = dat + timedelta(hours=8)
            dat = dat.strftime('%Y-%m-%dT%H:%M:%S+0800') 
            return dat

        data = {
            'next_page': {'user_name': content['user_name'], 'page' : content['page']},
            'latest_post_date': parse_datetime(content['public_tweets_list'][0]['created_at']),
            'links': content['public_tweets_list']
        }

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_twitter_content(self, content):
        
        def parse_datetime(dat):
            dat = datetime.strptime(dat,  '%a %b %d %H:%M:%S +0000 %Y')
            dat = dat + timedelta(hours=8)
            dat = dat.strftime('%Y-%m-%dT%H:%M:%S+0800') 
            return dat
        
        def get_replies_content(replies):
            reply_list = []
            for reply in replies:
                reply_dict =  {
                            'comment_id': reply['user']['id_str'],
                            'datetime': parse_datetime(reply['created_at']),
                            'author': reply['user']['name'],
                            'author_url': '',
                            'content':reply['full_text'],
                            'source': 'twitter',
                            'metadata':{'like_count':reply['favorite_count'], 'retweet_count':reply['retweet_count']},
                            'comments':[] }
                    
                reply_list.append(reply_dict)
            return reply_list
                
        tweet = content['content']
        replies = content['replies']
             
        data = {
            'datetime': parse_datetime(tweet['created_at']),
            'metadata': {'like_count':tweet['favorite_count'], 'retweet_count':tweet['retweet_count']},
            'author': tweet['user']['name'],
            'author_url': [],
            'title': tweet['full_text'][:30],
            'content': tweet['full_text'],
            'comments': get_replies_content(replies),
        }

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(data['title'])
        item.set_author(data['author'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        item.set_comment(data['comments'])
        item.set_metadata(data['metadata'])
        return item
