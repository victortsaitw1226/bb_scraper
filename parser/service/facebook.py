#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib.parse
import re
import traceback, sys
from bs4 import BeautifulSoup
from protocols.facebook_parse_content import FACEBOOK_PARSE_CONTENT
from protocols.facebook_parse_comments import FACEBOOK_PARSE_COMMENTS
from protocols.par2mng_ntf_fb_post_url import PAR2MNG_NTF_FB_POST_URL
from protocols.crw2par_ntf_fb_post_info import CRW2PAR_NTF_FB_POST_INFO
from protocols.crw2par_ntf_fb_post_content import CRW2PAR_NTF_FB_POST_CONTENT
from protocols.crw2par_ntf_fb_post_urls import CRW2PAR_NTF_FB_POST_URLS
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class FACEBOOK:
    def __init__(self, server):
        print('init facebook.py')
        self.server = server

    def on_CRW2PAR_NTF_FB_POST_URLS(self, packet):
        from_notify = CRW2PAR_NTF_FB_POST_URLS()
        from_notify.parse(packet)
        urls = from_notify.get_post_urls()
        fan_page = from_notify.get_fan_page()
        for url in urls:
            to_notify = PAR2MNG_NTF_FB_POST_URL()
            to_notify.set_url(url['post_url'])
            to_notify.set_post_utime(url['post_utime'])
            to_notify.set_fan_page(fan_page)
            self.server.send(to_notify)


    def on_CRW2PAR_NTF_FB_POST_CONTENT(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_FB_POST_CONTENT()
        from_notify.parse(packet)
        url = from_notify.get_post_url()
        post_utime = from_notify.get_post_utime()
        fan_page = from_notify.get_fan_page()
        content = from_notify.get_content()
        result = self.parse_content(url, content)
        result['fan_page'] = fan_page
        result['post_utime'] = post_utime
        result['media'] = 'facebook'

        url = self.server.mongo.upsert_post('history_v2', 'data', url, result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_content(self, url, html):
        result = {
          'post': '',
          'post_date': '',
          'reactions_count': '',
          'comments_count': '',
          'shares_count': '',
          'comments': []
        }
        soup = BeautifulSoup(html, 'html.parser')

        content_wrapper = soup.find('div', class_= re.compile('userContentWrapper'))

        if not content_wrapper:
            self.server.log.error('Facebook:content_wrapper is None:{}'.format(url))

        # Parse Content
        post = content_wrapper.find('div', {'data-testid': 'post_message'})
        if not post:
            self.server.log.error('Facebook:post is None:{}'.format(url))
            result['post'] = ""
        else:
            result['post'] = post.get_text()

        post_times = content_wrapper.find('span', class_ = 'timestampContent')
        if not post_times:
            self.server.log.error('Facebook:post_times is None:{}'.format(url))

        result['post_date'] = post_times.get_text()

        reactions_count = content_wrapper.find('span', {'data-testid': 'UFI2ReactionsCount/sentenceWithSocialContext'})
        if reactions_count:
            result['reactions_count'] = reactions_count.get_text()

        comment_count = content_wrapper.find('a', {'data-testid': 'UFI2CommentsCount/root'})
        if comment_count:
            result['comments_count'] = comment_count.get_text()

        share_count = content_wrapper.find('a', {'data-testid': 'UFI2SharesCount/root'})
        if share_count:
            result['shares_count'] = share_count.get_text()

        # Parse Comments
        comments = content_wrapper.find('div', {'data-testid': 'UFI2CommentsList/root_depth_0'})
        if not comments:
            return result

        comments = comments.find('ul')
        if not comments:
            self.server.log.error('Facebook:Second Comments is None:{}'.format(url))
      
        comments = comments.find_all('li', recursive=False)
        for comment in comments:
            comment_item = {
              'author': '',
              'text': '',
              'replies': []
            }
            comment_body = comment.find('div', {'data-testid': 'UFI2Comment/body'})
            if not comment_body:
                self.server.log.error('Facebook:comment_body is None:{}'.format(url))

            comment_author = comment_body.find(class_= "_6qw4")
            if not comment_author:
                self.server.log.error('Facebook:comment_author is None:{}'.format(url))

            comment_author_name = comment_author.get_text()
            comment_author.extract()
            comment_item['author'] = comment_author_name
            comment_item['text'] = comment_body.get_text()
            comment_utime = comment.find('abbr', class_='livetimestamp').get('data-utime')
            comment_item['post_utime'] = int(comment_utime)

            comment_link_tag = comment.find('ul', {'data-testid': 'UFI2CommentActionLinks/root'})
            comment_link = comment_link_tag.find('a').get('href')
            comment_item['url'] = comment_link

            #print('comment: %s' % comment.find('div', {'data-testid': 'UFI2Comment/body'}).get_text())
            sub_comments = comment.find_all('div', {'data-testid': 'UFI2Comment/root_depth_1'})
            for sub_comment in sub_comments:
                sub_comment_item = {
                  'author': '',
                  'text': ''
                }
                sub_comment_body = sub_comment.find('div', {'data-testid': 'UFI2Comment/body'})

                if not sub_comment_body:
                    self.server.log.error('Facebook:sub_comment_body is None:{}'.format(url))

                sub_comment_author = sub_comment_body.find(class_= "_6qw4")

                if not sub_comment_author:
                    self.server.log.error('Facebook:sub_comment_author is None:{}'.format(url))

                sub_comment_author_name = sub_comment_author.get_text()
                sub_comment_author.extract()
                sub_comment_item['author'] = sub_comment_author_name
                sub_comment_item['text'] = sub_comment_body.get_text()
                sub_comment_utime = sub_comment.find('abbr', class_='livetimestamp').get('data-utime')
                sub_comment_item['post_utime'] = int(sub_comment_utime)
                
                sub_comment_link_tag = sub_comment.find('ul', {'data-testid': 'UFI2CommentActionLinks/root'})
                sub_comment_link = sub_comment_link_tag.find('a').get('href')
                sub_comment_item['url'] = sub_comment_link
                comment_item['replies'].append(sub_comment_item)
            result['comments'].append(comment_item)
        return result
