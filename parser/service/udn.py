#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime

class UDN:
    def __init__(self, server):
        print('init udn.py')
        self.server = server

    def on_CRW2PAR_NTF_UDN_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_udn_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_UDN_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_udn_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_udn_list(self, content):
        
        def parse_latest_date(html_bs4):
            pub_date = html_bs4.find_all('dt','dt')
            link_date = []
            for date in pub_date:
                date = datetime.datetime.strptime( date.text , '%m/%d %H:%M')
                date = date.replace(year=datetime.datetime.today().year)
                link_date.append(date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            
            return latest_post_date
        
        def parse_link(html_bs4):
            links = html_bs4.find_all('h2')
            links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links] 
            return links
        
        def parse_next_page(current_page):
            url_split = re.split('/',current_page)
            url_split[-1] = str(int(url_split[-1])+1)
            next_page = '/'.join(url_split)
            return next_page

        clean_html = BeautifulSoup(content['list_html'], 'lxml')
        current_page = content['current_page']
        
        next_p = parse_next_page(current_page)
        latest_post_date = parse_latest_date(clean_html)
        list_meta = parse_link(clean_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_p)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)

        return item

    def parse_udn_content(self, content):
        
        def parse_time(clean_html):
            date_org = clean_html.find('div', class_ = 'story_bady_info_author').find('span').text   
            date = datetime.datetime.strptime(date_org, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date_org, date
        
        def parse_title(clean_html):
            return clean_html.find('h1', class_ = 'story_art_title').text
              
        def parse_content(clean_html):
            return ''.join([ent.text for ent in clean_html.find_all('p')]).replace('\n', '').replace('      500字以內，目前輸入 0 字      ', '')
        
        def parse_author(clean_html, date_org):
            '''
                author written in content include other informations with different patterns
                e.g. 【文／Ben C. Solomon；節譯／呂玉嬋】,【撰文／圖片提供．徐樂眉】
            '''
            try:
                author = clean_html.find('div', class_ = 'story_bady_info_author').find('a').text
            except AttributeError:
                author = clean_html.find('div', class_ = 'story_bady_info_author').text.replace(date_org,'')   
                author = [i  for i in author.split(' ') if '報' not in i if '新聞' not in i if '運動' not in i]
                if len(author) > 0 :
                    author = ' '.join(author)
                elif author == []:
                    author = ''
                else:
                    author = author[0]                       
            
            reg = re.compile(r'【 \D+[／：/:]\D*】', re.VERBOSE)
            tmp = reg.findall(content[:51])        
            if tmp != []:
                author = tmp[0]
                
            return author    
        
        def parse_metadata(clean_html, fb_like_count_html=None):
            metadata = {'tag':[], 'category':'','fb_like_count':''}
            try: 
                metadata['tag'] = clean_html.find('div', id = 'story_tags').text.split('﹒')    
            except AttributeError:
                pass  
            #metadata['fb_like_count'] = fb_like_count_html.find('span',{'id':'u_0_3'}).text
            metadata['category'] = clean_html.find('div',{'id':'nav','class':'only_web'}).find_all('a')[-1].text
            return metadata
        
        def udn_comment(clean_html):
            ''' get udn comment '''   
            
            udn_com = clean_html.find('dl', class_='disl')
            id_ = udn_com.find_all('b')
            author = udn_com.find_all('a')
            time = udn_com.find_all('dt')
            content = udn_com.find_all('span')   # rm \n
            
            reg = re.compile(r'(\d{4}/\d{2}/\d{2}[ ]\d{2}:?[0-5]?\d:[0-5]?\d)', re.VERBOSE)
                    
            ls = []
            for i in range(len(id_)):
                date = reg.findall(time[i].text)[0]
                ls.append({'comment_id': id_[i].text,
                    'datetime': datetime.datetime.strptime(date, '%Y/%m/%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S+0800'),
                    'author': author[i].text,
                    'author_url':'',
                    'content': content[i].text.replace('\n',''),
                    'metadata': {},
                    'source':'udn',
                    'comments':[]
                        })
            return ls
        

        # detail
        clean_html = BeautifulSoup(content['html'], 'lxml') 
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')  
    
        url = content['url']
        date_org, date = parse_time(clean_html)
        title = parse_title(clean_html)
        content = parse_content(clean_html) 
        author = parse_author(clean_html, date_org)
        #metadata = parse_metadata(clean_html, fb_like_count_html)
        metadata = parse_metadata(clean_html)

        #fb_comment = parse_comments(comment_html)
        #udn_comment = udn_comment(clean_html)
        #comment = udn_comment + fb_comment
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item
