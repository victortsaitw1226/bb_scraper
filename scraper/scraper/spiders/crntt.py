# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
import requests
import re

#class CrnttSpider(scrapy.Spider):
class CrnttSpider(RedisSpider):
    name = 'crntt'

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=46&kindid=0&searchword=#",
                "category": "臺灣時政"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=92&kindid=0&searchword=#",
                "category": "今日焦點"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14679&page=1",
                "category": "國民黨及藍營"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14950&page=1",
                "category": "中評分析"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=217&kindid=0&searchword=#",
                "category": "台灣綜合"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=142&kindid=0&searchword=#",
                "category": "綠營動態"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=3&kindid=12&page=1",
                "category": "兩岸專區"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14674&page=1",
                "category": "最新消息"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=192&kindid=0&searchword=#",
                "category": "北台灣"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=153&kindid=0&searchword=#",
                "category": "南台灣"
            },
            {
                "media": "crntt",
                "name": "crntt",
                "enabled": True,
                "days_limit": 3600 * 24,
                "interval": 3600 * 6,
                "priority": 1,
                "scrapy_key": "crntt:start_urls",
                "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=3&kindid=12&page=2",
                "category": "兩岸動態"
            },
        ]
        for request in requests:
            yield scrapy.Request(
                request['url'],
                meta=request,
                dont_filter=True
            )
                    
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body.decode('utf-8', 'ignore'), 'html.parser')

        links_table = soup.find('table',{'width':"98%",'border':"0",'cellspacing':"0",'cellpadding':"10"})

        link_date = []
        for link in links_table.find_all('li'):
            date = datetime.strptime( link.find('em').text , '(%Y-%m-%d %H:%M:%S)')
            link_date.append(date)
            #href = 'http://hk.crntt.com' + link.find('a').get('href')
            href = link.find('a').get('href')
            yield response.follow(href,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_article)

        latest_datetime = max(link_date)
       
        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(str(int(current_page) + 1)), response.url)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body.decode('utf-8','ignore'), 'html.parser')

        # detail
        title = soup.find('head').find('title').text
        title = ' '.join(title.split())
        keywords = soup.find('head').find('meta',{'name':'keywords'})['content']
        keywords = re.split(',',keywords)

        next_page = [n for n in soup.find_all('table',{'width':'100%','border':"0", 'cellspacing':"0", 'cellpadding':"0"}) if '頁' in n.text]
        if next_page == []:
            content = self.parse_content(soup)
        else:
            next_page_urls = [ 'http://hk.crntt.com' + a['href'] for a in next_page[0].find_all('a') if '頁' in a.text ]
            extra_pages_soup = [BeautifulSoup(requests.get(ep).text, 'html.parser') for ep in next_page_urls]
            extra_pages_content = ''.join(self.parse_content(s) for s in extra_pages_soup)
            content = self.parse_content(soup) + extra_pages_content

        metadata = {
            'tag':keywords,
            'category': meta['category']
        }
        
        item = NewsItem()
        item['url'] = response.url
        item['content'] = content
        item['author'] = self.parse_author(content)
        item['article_title'] = title
        item['author_url'] = []
        item['comment'] = []
        item['date'] = self.parse_date(soup) 
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = meta['media']
        item['proto'] =  'SETN_PARSE_ITEM'
        return item

    def parse_date(self, soup):     
        date = soup.find_all('td',{'align':"center"})[3]
        date.find('font').decompose()
        date = date.text.strip()
        date = datetime.strptime( date , '%Y-%m-%d %H:%M:%S')
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_content(self, soup):
        content = soup.find('td',{'style':'font-size: 16px;LINE-HEIGHT: 150%;text-align:justify;text-justify:inter-ideograph;'})
        for img in content.find_all('td',{'align':"center"}):
            img.decompose()
        content = '\n'.join(content.text.split())
        return content
    
    def parse_author(self, content):
        try:
            author = re.search(r'（記者\s(.*)）',content).group(1)
        except:
            author = ''
        return author
