#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime
from utils.parse_fb_comment import parse_comments

class STORM:
    def __init__(self, server):
        print('init storm.py')
        self.server = server

    def on_CRW2PAR_NTF_STORM_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_storm_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_STORM_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        
        item = self.parse_storm_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_storm_list(self, content):

        links = []
        datetimes = []
        domain_prefix = 'https://www.storm.mg'
        soup = BeautifulSoup(content, 'html.parser')
        next_page = soup.find('a', id="next").get('href')

        for element in soup.find_all('div', class_='card_inner_wrapper'):
            if element.find('h3'):      # find the normal-title articles
                pass
            elif element.find('h2'):    # find the pinned article
                if not element.find('div', class_="tags_wrapper"):
                    continue
            else:
                continue

            links.append({
                'url': element.find('a', class_='card_link link_title').get('href'),
                'metadata': {}
            })

            time_ = element.find('span', class_='info_time')
            dt = datetime.strptime(time_.text, '%Y-%m-%d %H:%M')
            datetimes.append(dt)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(domain_prefix + next_page)
        item.set_latest_post_date(max(
            datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800'))
        item.set_urls(links)

        return item

    def parse_storm_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find(class_='info_inner_content', id='info_time')
            date = datetime.strptime(time_.text, '%Y-%m-%d %H:%M')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author_url(soup):
            author_id = soup.find('a', class_='link_author info_inner_content')[
                'data-author_id']
            return domain_prefix + '/authors/' + author_id

        def parse_author(soup):
            author_id = soup.find('a', class_='link_author info_inner_content')
            author_id = author_id.find('span').text
            return author_id

        def parse_title(soup):
            _title = soup.find('title').text[:-4]
            _title = ''.join(_title.split())
            return _title

        def parse_content(soup):

            def has_aid_but_no_span(tag):
                return tag.has_attr('aid') and not tag.has_attr('span')

            content = []
            content = soup.find_all(has_aid_but_no_span)
            content = ''.join([x.text for x in content])
            content = ''.join(content.split())
            return content

        def parse_metadata(soup, fb_share_soup):

            def parse_category(soup):
                category = soup.find('meta',{'property':'article:section'})['content']
                return category

            def parse_tag(soup):
                tags_content = []
                _tags_content = soup.find_all(class_='tag tags_content')
                for tag in _tags_content:
                    tags_content.append(tag.text)
                return tags_content

            # 文章分類、人氣、關鍵字、分享數
            metadata = {
                'category': parse_category(soup),
                'view_count': soup.find(class_='pop_number info_pop_content').text,
                'tag': parse_tag(soup),
                'fb_share_count': fb_share_soup.find('span',{'id':'u_0_3'}).text,
            }
            return metadata
        
        domain_prefix = 'https://www.storm.mg'
        # soup = BeautifulSoup(content['default_html'], 'html.parser')
        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_share_count_html =  BeautifulSoup(content['fb_share_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        # detail
        url = content.get('url')
        title = parse_title(soup)
        author_url = parse_author_url(soup)
        author = parse_author(soup)
        date = parse_datetime(soup)
        content = parse_content(soup)
        comment = parse_comments(comment_html)
        metadata = parse_metadata(soup,fb_share_count_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author_url(author_url)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
