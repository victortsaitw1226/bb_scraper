# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class BuzzorangeSpider(RedisSpider):
    name = "buzzorange"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "buzzorange",
            "name": "buzzorange",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "https://buzzorange.com/",
            "scrapy_key": "buzzorange:start_urls",
            "priority": 1,
            'referer': 'https://buzzorange.com/',
            "ajax_url": "https://buzzorange.com/wp-admin/admin-ajax.php",
            "action": "fm_ajax_load_more",
            "page": 1
        }]
        for request in requests:
            yield scrapy.Request(url=request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta=response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        script = soup.find_all('script',{'type':'text/javascript'})
        index = [i for i, e in enumerate(script) if 'nonce' in str(e)][0]
        nonce = re.split(r'"nonce":"(.*)","url"',str(script[index]))[1]
        meta['nonce'] = nonce

        yield scrapy.FormRequest(
                url=meta['ajax_url'],
                formdata={
                  'action': meta['action'],
                  'nonce': meta['nonce'],
                  'page': str(meta['page'])
                },
                headers={
                  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                  'Referer': meta['referer'],
                },
                method='POST',
                meta=meta,
                dont_filter=True,
                callback=self.parse_ajax)

    def parse_ajax(self, response):
        meta = response.meta
        body = json.loads(response.body)['data']
        soup = BeautifulSoup(body, 'html.parser')

        posts_datetime = []
        for article in soup.find_all('article'):
            link = soup.find('h4', class_='entry-title')
            href = link.find('a')['href']
            posts_datetime.append(datetime.strptime(article.find('time')['datetime'], '%Y-%m-%dT%H:%M:%S+08:00'))
            yield scrapy.Request(href,
                    meta = meta,
                    callback=self.parse_article)

        latest_datetime = max(posts_datetime)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        meta['page'] = meta['page'] + 1
        yield scrapy.FormRequest(
                url=meta['ajax_url'],
                formdata={
                  'action': meta['action'],
                  'nonce': meta['nonce'],
                  'page': str(meta['page'])
                },
                headers={
                  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                  'Referer': meta['referer'],
                },
                method='POST',
                meta=meta,
                dont_filter=True,
                callback=self.parse_ajax)
    
    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
       
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'buzzorange'
        item['proto'] =  'BUZZORANGE_PARSE_ITEM'
        return item


    def parse_title(self, soup):
        title = soup.find('h1','entry-title').text.strip()
        return title
    
    def parse_datetime(self, soup):
        date = soup.find('div','article-post-date').find('time')['datetime']
        date = datetime.strptime( date,'%Y-%m-%dT%H:%M:%S+08:00')
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_author(self, soup):
        author =  soup.find('span','author vcard').find('a').text.strip()
        return author
    
    def parse_author_url(self, soup):
        author_url = []
        author_url.append( soup.find('span','author vcard').find('a')['href'] )
        return author_url
    
    def parse_content(self, soup):
        #cont = soup.find('div','entry-content')
        cont = soup.find('div','fb-quotable')
        
        for fig in cont.find_all('figcaption','wp-caption-text'):
            fig.clear()
        
        for video in cont.find_all('div','fb-video'):
            video.clear()
        
        cont = ' '.join( cont.text.split() )
        cont =  re.split('延伸閱讀',cont)[0]
        cont =  re.split('推薦閱讀',cont)[0]
        return cont
    
    def parse_metadata(self, soup):
        tag = soup.find('meta',{'name':'keywords'})
        tag = re.split(', ',tag['content'])
        category = soup.find('meta',{'property':'og:site_name'})['content']
        return{'tag':tag, 'category':category,'fb_like':''}
