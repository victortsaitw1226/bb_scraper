# -*- coding: utf-8 -*-
import os
# Scrapy settings for scraper project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'scraper'

SPIDER_MODULES = ['scraper.spiders']
NEWSPIDER_MODULE = 'scraper.spiders'

TELNETCONSOLE_ENABLED=False

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'scraper (+http://www.yourdomain.com)'
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Enables scheduling storing requests queue in redis.
SCHEDULER = "scrapy_redis.scheduler.Scheduler"
DUPEFILTER_CLASS = "scrapy_redis.dupefilter.RFPDupeFilter"
# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'scraper.middlewares.ScraperSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'scrapy_html_storage.HtmlStorageMiddleware': 10,
#    'scraper.middlewares.MyCustomDownloaderMiddleware': 543,
}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
#    'scraper.pipelines.ScraperPipeline': 300,
    'scraper.pipelines.TransformDataPipeline':100,
    'scraper.pipelines.EvaluationPipeline':200,
    'scraper.pipelines.SaveToElasticsearchPipeline':300,
    #'scraper.pipelines.SaveToMongoPipeline':300, # replace `SaveToFilePipeline` with this to use MongoDB
    #'scraper.pipelines.SaveToRabbitMQPipeline':200, # replace `SaveToFilePipeline` with this to use MongoDB
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# RabbitMQ Connection Info
BROKER_HOST = os.environ.get("RABBITMQ_HOST", 'localhost')
BROKER_PORT = os.environ.get("RABBITMQ_PORT", 5672)
BROKER_USERID = os.environ.get("RABBITMQ_USER", 'guest')
BROKER_PASSWORD = os.environ.get("RABBITMQ_PASSWORD", 'guest')
BROKER_VIRTUAL_HOST = '/'
RABBITMQ_QUEUE = 'evt-scrapy-SCRAPY2ELT_NTF_NEWS'
RABBITMQ_ROUTING_KEY = 'SCRAPY2ELT_NTF_NEWS'

# Redis Connection Info
REDIS_HOST = os.environ.get("REDIS_HOST", "localhost")
REDIS_PORT = os.environ.get("REDIS_PORT", 6379)
REDIS_PARAMS = {
    'password': os.environ.get("REDIS_PASSWORD", "ibdo2018"),
}

#MongoDB Connection Info
MONGODB_SERVER = os.environ.get("MONGODB_SERVER", "localhost")
MONGODB_PORT = os.environ.get('MONGODB_PORT', 27017)
MONGODB_DB = os.environ.get('MONGODB_DB', "debug")        # database name to save the crawled data
MONGODB_DATA_COLLECTION = os.environ.get('MONGODB_DATA_COLLECTION', "debug")
MONGODB_USER = os.environ.get('MONGODB_USER')
MONGODB_PASSWORD = os.environ.get('MONGODB_PASSWORD')

# Evaluation URL
EVALUATION_URL = 'http://35.236.135.46:9002/predict'

# Elasticsearch Info
ELASTICSEARCH_SERVERS = os.environ.get('ELASTICSEARCH_SERVERS', 'localhost')
ELASTICSEARCH_TIMEOUT = 60
ELASTICSEARCH_USERNAME = os.environ.get('ELASTICSEARCH_USERNAME')
ELASTICSEARCH_PASSWORD = os.environ.get('ELASTICSEARCH_PASSWORD')
ELASTICSEARCH_INDEX = os.environ.get('ELASTICSEARCH_INDEX', 'debug')
ELASTICSEARCH_TYPE = os.environ.get('ELASTICSEARCH_TYPE', 'debug')
