from flask import Flask, request, abort
from linebot import LineBotApi, WebhookHandler
from linebot.exceptions import InvalidSignatureError
from linebot.models import *
import requests

app = Flask(__name__)

# Channel Access Token
line_bot_api = LineBotApi('vP2IjnfLYAy3gt94KhAiV4I4Qy2+w2MIPZXNt5WtPITuGMmpgYl01ReRjQlR1qOalML06MkgM6e7d/c16hvkw7hL28ti39uZ6zz80Mt7CQYfky2exxscvU9pWk0/U36DFqYIntgP2Af46dLxt2FzHAdB04t89/1O/w1cDnyilFU=')
# Channel Secret
handler = WebhookHandler('79b8a0aabe4b69b328cb5ef4fde44539')


# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'

# 讓 Server 可以呼叫丟 Line 訊息給使用者
@app.route("/line/line_bot/msg", methods=['post'])
def send_msg():
    
#   body = request.get_data(as_text=True)
#   user_id = body.get("user_id")
#   msg = body.get("msg")
 
    msg = request.form.get('msg')
    user_id = request.form.get('user_id')
    
    try:
        line_bot_api.push_message(user_id, TextSendMessage(text=msg))
        return 'Success'
    except Exception as e:
        print(e)


@app.route("/line/notify/msg", methods=['post'])
def lineNotifyMessage():
    
    msg = request.form.get('msg')
    token = request.form.get('user_id')
#    token = 'IlAATxvxBFE0vhAQqfMzvIRhnAQrKO4L57p7Z3xoTYE' #傳給 ibdo.king
#    token = '5b2xhBTMdSUXOpUpbm1AI7dspWGVR0j5dzlJFIbs8D8' #傳給 test
    
    url = "https://notify-api.line.me/api/notify"
    headers = {
        "Authorization": "Bearer " + token, 
        "Content-Type" : "application/x-www-form-urlencoded"
    }
	
    payload = {'message': msg}
    
    try:
        r = requests.post(url, headers = headers, params = payload)
        return 'Success'
    except Exception as e:
        print(e)
        
	


@handler.add(FollowEvent)
def handle_follow(event):
    print("in Follow")
    buttons_template = TemplateSendMessage(
            alt_text='Buttons Template', 
            template=ButtonsTemplate(
                title='訂閱功能',
                text='請選擇是否開啟訂閱功能',
                actions=[
                    MessageTemplateAction(
                        label='是',
                        text='開啟訂閱'
                    ),
                    MessageTemplateAction(
                        label='否',
                        text='關閉訂閱'
                    )
                  ]
               )
             )
    line_bot_api.reply_message(event.reply_token, buttons_template)


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    
    if event.message.text=='開啟訂閱':
        user_id = event.source.user_id
        profile = line_bot_api.get_profile(user_id)
        user_name = profile.display_name

        # 呼叫 Server 去新增使用者
        url = "http://35.201.187.56:7777/line/add"
        response = requests.post(url,data={"user_id":user_id, "name":user_name})
        print(response.text)

        message = TextSendMessage(text='已將您的帳號開啟訂閱功能')
        line_bot_api.reply_message(event.reply_token, message)
        return 0
    
        

import os
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
