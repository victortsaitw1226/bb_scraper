#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class BUZZORANGE:
    def __init__(self, server):
        print('init buzzorange.py')
        self.server = server

    def on_CRW2PAR_NTF_BUZZORANGE_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_buzzorange_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_BUZZORANGE_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_buzzorange_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)

        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_buzzorange_list(self, content):        

        def parse_links(soup):
            links = soup.find_all('h4','entry-title')
            links = [ { 'url':l.find('a')['href'] , 'metadata':[] } for l in links]
            return links
        
        def parse_latest_post_date(soup):
            articles = soup.find_all('article')
            dates = [  datetime.strptime( a.find('time')['datetime'],'%Y-%m-%dT%H:%M:%S+08:00') for a in articles]
            latest_post_date = max(dates).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        def parse_next_page(content):
            return  {'url':content['url'], 'page':content['page']}
        
        soup = BeautifulSoup(content['html'], 'html.parser')
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(content))
        item.set_latest_post_date(parse_latest_post_date(soup))
        item.set_urls(parse_links(soup))
        return item

    def parse_buzzorange_content(self, content):

        def parse_title(soup):
            title = soup.find('h1','entry-title').text.strip()
            return title
        
        def parse_datetime(soup):
            date = soup.find('time','entry-date published')['datetime']
            date = datetime.strptime( date,'%Y-%m-%dT%H:%M:%S+08:00')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_author(soup):
            author =  soup.find('span','author vcard').find('a').text.strip()
            return author
        
        def parse_author_url(soup):
            author_url = []
            author_url.append( soup.find('span','author vcard').find('a')['href'] )
            return author_url
        
        def parse_content(soup):
            cont = soup.find('div','entry-content')
            
            for fig in cont.find_all('figcaption','wp-caption-text'):
                fig.clear()
            
            for video in cont.find_all('div','fb-video'):
                video.clear()
            
            cont = ' '.join( cont.text.split() )
            cont =  re.split('延伸閱讀',cont)[0]
            cont =  re.split('推薦閱讀',cont)[0]
            return cont
        
        def parse_metadata(soup):
            tag = soup.find('meta',{'name':'keywords'})
            tag = re.split(', ',tag['content'])
            category = soup.find('meta',{'property':'og:site_name'})['content']
            return{'tag':tag, 'category':category,'fb_like':''}

        soup = BeautifulSoup(content['html'], 'html.parser')

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_author_url(parse_author_url(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        #item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        
        return item
