# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId
# from pymongo import MongoClient
# import time

def parse(data):
    comment_body = []
        
    source_id = str(data.get('_id', ''))
    docurl = data.get('url', '')
    postdate = datetime.datetime.now()       
    if data['date'] == '':
        pdate = ''
    else:
        try:
            postdate = datetime.datetime.strptime(data['date'], '%c')
            pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
        except ValueError:
            try:
                postdate = datetime.datetime.strptime(data['date'], '%a %b %d %H：%M：%S %Y')
                pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
            except:
                pdate = ''
        except Exception as e:
            pass
                
    comment_count = data['message_count']['all']
    
    if data['author'] is not None:
        author_id = data['author'].split('(')[0]
    else:
        author_id = ''
        
    cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    title = data.get('article_title', '')
    body = dict({
        "data_id": source_id,
        "postauthor": data.get("author", ''),
        "Author_ID": author_id,
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": data.get('board', ''),
        "content": data.get("content", ''),
        "media": "Ptt",
        "SourceTypeV2": data.get("media", ''),
        "SourceType": 0,
        "docurl": docurl,
        "coverurl": '',
        "PostDate": pdate,
        "contentType": '0',
        "candidate": '',
        "mediatype": 2,
        "datafrom": 2,
        "title": title,
        "brocount": comment_count,
        "repcount": comment_count,
        "datatype": 0,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2,
        "candidates": []
    })

    if len(data['messages']) > 0:
        for comment in data['messages']:
            try:
                r_postdate = datetime.datetime.now()
                if '02/29' not in comment['push_ipdatetime']:
                    if len(comment['push_ipdatetime'].split(' ')) == 3:
                        reply_date = comment['push_ipdatetime'].split(' ')[1] + \
                                ' ' + comment['push_ipdatetime'].split(' ')[2]
                    else: 
                        reply_date = comment['push_ipdatetime']
                            
                    if pdate == '':
                        try:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d %H:%M')
                            r_postdate = r_postdate.replace(year = datetime.datetime.now().year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except ValueError:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d')
                            r_postdate = r_postdate.replace(year = datetime.datetime.now().year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except Exception as e:
                            pass
                    else:
                        try:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d %H:%M')
                            r_postdate = r_postdate.replace(year = postdate.year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except ValueError:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d')
                            r_postdate = r_postdate.replace(year = postdate.year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except Exception as e:
                            pass
                else:
                    if pdate == '':
                        rdate = str(datetime.datetime.now().year) + '-02/28T23:59:00'
                    else:
                        # pdate = datetime.datetime.strptime(pdate, '%Y-%m-%dT%H:%M:%S')
                        year = (postdate.year//4) * 4
                        try:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d %H:%M')
                            r_postdate = r_postdate.replace(year = year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except ValueError:
                            r_postdate = datetime.datetime.strptime(reply_date, '%m/%d')
                            r_postdate = r_postdate.replace(year = year)
                            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:00')
                        except Exception as e:
                            pass
            except ValueError:
                rdate = ''
            except Exception as e:
                pass

            if r_postdate < postdate:
                continue

            if datetime.datetime.now() < r_postdate:
                continue

            dummy_body = dict({
                "data_id": source_id,
                "postauthor": comment.get("push_userid", ''),
                "Author_ID": comment.get("push_userid", ''),
                "createdate": cdate,
                "docid": source_id,
                "MainID": source_id,
                "docboard": data.get('board', ''),
                "content": comment.get("push_content", ''),
                "media": "Ptt",
                "SourceType": 0,
                "SourceTypeV2": data.get("media", ''),
                "docurl": docurl,
                "coverurl": '',
                "PostDate": rdate,
                "contentType": '1',
                "candidate": '',
                "mediatype": 2,
                "datafrom": 2,
                "title": title,
                "brocount": 0,
                "repcount": 0,
                "datatype": 0,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 2,
                "candidates": []
            })
            comment_body.append(dummy_body)

    comment_body.append(body)
    return comment_body
