# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class CteeSpider(RedisSpider):
    name = "ctee"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ctee",
            "name": "ctee",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://ctee.com.tw/category/news/global",
            "scrapy_key": "ctee:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        link_blocks = soup.find_all('div','featured clearfix')
        links = [li.find('a')['href'] for li in link_blocks] 
        return links
    
    def parse_next_page(self,soup):
        page_bar = soup.find('div','pagination bs-numbered-pagination')
        try:
            next_page = page_bar.find('a','next page-numbers')['href']
        except:
            next_page = None
        return next_page
    
    def parse_latest_post_date(self,soup):
        link_blocks = soup.find_all('div','item-inner clearfix')
        link_date = []
        for li in link_blocks:
            date = datetime.strptime( li.find('div','post-meta').find('time')['datetime'] , '%Y-%m-%dT%H:%M:%S+00:00')
            link_date.append(date)
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'ctee'
        item['proto'] = 'CTEE_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = datetime.strptime( soup.find('time','post-published updated')['datetime'] , '%Y-%m-%dT%H:%M:%S+00:00')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        try:
            author = soup.find('span','post-author-name').text
        except:
            author = ''
        return author
    
    def parse_title(self, soup):
        title = soup.find('span','post-title').text
        title = ' '.join(title.split())
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','entry-content clearfix single-post-content')
        for js in content.find_all('script'):
            js.clear()
        content = ''.join(content.text.split())
        content = content.split('延伸閱讀')[0]
        return content

    def parse_metadata(self, soup):
        tag = [ tag['content'] for tag in soup.find_all('meta',{'property':'article:tag'})]
        category = soup.find('meta',{'property':'article:section'})['content']
        metadata = {
            'category': category,
            'tag': tag,
        }
        return metadata
    