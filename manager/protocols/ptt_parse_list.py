#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PARSE_PTT_LIST(Protocol):

    def __init__(self):
        super(PARSE_PTT_ITEM, self).__init__('PARSE_PTT_ITEM')
        self.update({
          'urls': []
        })

    def set_urls(self, urls):
        self.set('url', url)

    def get_urls(self):
        return self.get('urls')
