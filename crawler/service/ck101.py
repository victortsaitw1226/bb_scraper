#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback
import sys


class CK101:

    def __init__(self, server):
        self.server = server

    def detect_error(self, html):
        return 'hd-box hd-logo' in html

    def on_MNG2CRW_NTF_CK101_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        try:
            html = self.server.new_requests(url, self.detect_error)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            # traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_CK101_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            sel_text = self.server.new_requests(url, self.detect_error)
    
            clean_html = BeautifulSoup(sel_text, 'lxml')    
            url_next = 1
            html_next_ls = []
            html_next_ls.append(sel_text)
            while url_next:
                try:
                    url_next = clean_html.find('div', class_ = 'pg').find(class_ = 'nxt').attrs['href']
                    print('url_next:',url_next)
                    html_next = self.server.new_requests(url_next, self.detect_error)
                    html_next_ls.append(html_next)
                    clean_html = BeautifulSoup(html_next, 'lxml')
                except:
                    url_next = None 

            html = {'detail_html':html_next_ls,'url':url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
