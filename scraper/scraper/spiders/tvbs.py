# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class TvbsSpider(RedisSpider):
    name = "tvbs"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "tvbs",
            "name": "tvbs",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://news.tvbs.com.tw/travel",
            "scrapy_key": "tvbs:start_urls",
            "cate_id": '260',
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        
        startID = soup.select('.content_center_contxt_box_news a')[0]['data-news-id']
        startID = startID.replace("'", '')

        # send XHR to get latest 100 news of each board
        board = response.url.split('/')[-1]
        cateID = {
            'politics': '7', 
            'local': '1', 
            'entertainment': '5',
            'life': '2',
            'world':'3',
            'focus':'41',
            'travel':'260',
            'health':'262',
            'fun':'50',
            'tech':'12',
            }
        list_response  = requests.post(
            url= ('&').join([
                'https://news.tvbs.com.tw/news/LoadMoreOverview?limit=100&offset=11', 
                'cateid=' + meta['cate_id'], #cateID.get(board),
                'cate=' + board,
                'newsid='+str(startID)
            ]))
        list_response = json.loads(list_response.text)
        urls = [startID]
        news_id_list = list_response['news_id_list'].split(',')
        news_id_list = [i.replace("'", '') for i in news_id_list]
        urls.extend(news_id_list)
        urls.remove('')
        for url in urls:
            yield response.follow(
                ('/').join([board, url]),
                meta= {
                    'board':board
                },
                callback=self.parse_article)
        

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,meta)
        item['content_type'] = 0
        item['media'] = 'tvbs'
        item['proto'] = 'TVBS_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        time_ = soup.find("div","icon_time time leftBox2").text
        dt = datetime.strptime(time_, '%Y/%m/%d %H:%M')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup):
        try:
            writer = soup.find("h4",'font_color5 leftBox1').a.text
        except:
            writer = ''
        return writer

    def parse_title(self,soup):
        return soup.h1.text.strip().replace(u'\u3000',
                                            u' ').replace(u'\xa0', u' ')

    def parse_content(self,soup):
        content = soup.find(id='news_detail_div').text
        content = content.strip().replace(u'\u3000', u' ')
        content = content.replace(u'\xa0', u' ')
        content = content.replace("最HOT話題在這！想跟上時事，快點我加入TVBS新聞LINE好友！","")
        return content

    def parse_metadata(self,soup,meta):
        try :
            updated_time=soup.find('div','icon_time time margin_b5').text
            updated_time=re.search("[0-9]{4,}/[0-9]{2,}/[0-9]{2,} [0-9]{2,}:[0-9]{2,}",updated_time).group()
            dt = datetime.strptime(updated_time, '%Y/%m/%d %H:%M')
            updated_time=dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            updated_time=''
            
        try:
            keys=soup.find("div","newsdetail_content_adWords_box margin_b45").text
            keys =re.findall(r"[\u4e00-\u9fa5]+" ,keys)
        except:
            keys =''
        
        return {
            'key': keys,
            "updated_time" :updated_time,
            'category':meta['board']
        }
