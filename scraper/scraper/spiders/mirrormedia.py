# -*- coding: utf-8 -*-
import scrapy
import re
import json
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from scraper.items import NewsItem
from dateutil.parser import parse as date_parser

#class MirrormediaSpider(scrapy.Spider):
class MirrormediaSpider(RedisSpider):
    name = 'mirrormedia'

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "mirrormedia",
            "name": "mirrormedia",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600,
            "url": "https://www.mirrormedia.mg/api/getlist?where=%7B%22categories%22%3A%7B%22%24in%22%3A%5B%2257e1e153ee85930e00cad4eb%22%5D%7D%7D&max_results=12&page=1&sort=-publishedDate",
            "scrapy_key": "mirrormedia:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        res, max_page = self.parse_json(response.body.decode('utf-8', 'ignore'))
        
        items = res.get("_items", [])
        links = []
        datetimes = []
        html_base = "https://www.mirrormedia.mg/story/"
        for item in items:
            links.append(html_base + item["slug"])
            dt = datetime.strptime(item["publishedDate"],
                                '%a, %d %b %Y %H:%M:%S %Z')
            datetimes.append(dt)


        for link in links:
            yield scrapy.Request(link,
                    #dont_filter=True,
                    meta=meta,
                    callback=self.parse_article)

        if len(datetimes) == 0:
            return

        latest_datetime = max(datetimes)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return

        next_page = self.handle_next_page(response.url, max_page)
        print(next_page)
        if next_page is None:
            return

        yield scrapy.Request(next_page,
                meta=meta,
                dont_filter=True,
                callback=self.parse)
        

    def parse_json(self, html):
        res = json.loads(html)
        try:
            url = res["_links"]["last"]["href"]
            re_list = re.search(r'page=[0-9]+', url).group().split("=")
            max_page = int(re_list[1])
        except:
            max_page = 1
        return res, max_page

    def handle_next_page(self, url, max_page):
        re_list = re.search(r'page=(\d+)', url).group().split("=")
        cur_page_num = int(re_list[1])
        if cur_page_num < max_page:
            re_list[1] = str(cur_page_num + 1)
            next = "=".join(re_list)
            next_page = url.replace(
                re.search('page=(\d+)', url).group(), next)
        else:
            next_page = None
        return next_page

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body.decode('utf-8', 'ignore'), 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = meta['media']
        item['proto'] =  'SETN_PARSE_ITEM'
        yield item

    def parse_datetime(self, soup):
        time_ = soup.find("div", "date").text
        post_datetime = date_parser(time_)
        return post_datetime + timedelta(hours=8)

    def parse_author(self, soup):
        try:
            author = soup.select(".article__credit")[0].text.strip().replace(
                u'\u3000', u' ').replace(u'\xa0', u' ')
            writer = re.search(".[\u4e00-\u9fa5]+", author).group()[1:]
        except:
            writer = ''
        return writer

    def parse_title(self, soup):
        return soup.h1.text.strip().replace(u'\u3000',
                                            u' ').replace(u'\xa0', u' ')

    def parse_content(self, soup):
        content = ""
        for txt in soup.findAll("div", class_="plate-vue-lazy-item-wrapper"):
            if not txt.find("section", "relateds-in-content"):
                content += txt.text
        return content.strip().replace(u'\u3000', u' ').replace(u'\xa0', u' ')

    def parse_metadata(self, soup):
        key = []
        category = soup.find('meta',{'property':'article:section'})['content']
        try:
            for world in soup.find("div", "tags"):
                key.append(world.text)
        except:
            pass
        
        return {
            'brief': soup.find("div", "brief fb-quotable").text,
            'tag': key,
            'category':category
        }

