#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class SINA:
    def __init__(self, server):
        print('init sina.py')
        self.server = server

    def on_CRW2PAR_NTF_SINA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_sina_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_SINA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_sina_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_sina_list(self, content):
        sina = 'https://news.sina.com.tw'
        soup = BeautifulSoup(content, "lxml")
        
        links = [{'url':sina +s['href'],'metadata':[]} for s in soup.find('ul','realtime').findAll('a')]
        # print('LINKS: ', links)
        link_date = [datetime.strptime(s.text.split('】')[1], '%Y-%m-%d %H:%M') for s in soup.find('ul','realtime').findAll('cite')]
        # print('LINK_DATE: ', link_date)
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        try:
            next_page = sina + soup.find('a',text = '下一頁')['href']
        except:
            date = (max(link_date).date()-timedelta(1)).strftime('%Y%m%d')
            next_page = 'https://news.sina.com.tw/realtime/politics/tw/'+date+'/list.html'

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_sina_content(self, content):
        # detail
        soup = BeautifulSoup(content['detail_html'], 'lxml')
        url = content['url']
        date = soup.find("meta",  property="article:published_time")['content'].replace('+08:00','+0800')
        title = soup.find("meta",  property="og:title")['content']
        author = soup.find('div','path').findAll('a')[1].text
        content = soup.find('div','pcont')   
        for div in content.findAll('div','main-photo'): 
            div.decompose()
        for div in content.findAll('div'): 
            div.decompose()
        content = content.text.strip()
        keywords = list(soup.find('div','artwords').text.strip().replace('關鍵字：',''))
        category = soup.find('meta',{'property':'article:section'})['content']
        metadata = {'tag':keywords,'category':category} 
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item
