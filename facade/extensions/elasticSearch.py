#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import string
import random
import datetime
from nameko.extensions import DependencyProvider
from elasticsearch import Elasticsearch

class ElasticSearch(DependencyProvider):
    class __Elasticsearch:
        def __init__(self, host, port, user, password):

            if user and password:
                self.es = Elasticsearch(
                    [{'host': host, 'port': int(port)}],
                    http_auth=(user, password)
                )
            else:
                self.es = Elasticsearch([{'host': host, 'port': int(port)}])

            if self.es.ping():
                print('Elasticsearch connected')
            else:
                raise Exception('Elasticsearch disconnected')

        def insert(self, index, doc_type, data):
            self.es.index(index=index, doc_type=doc_type, body=data, routing=1)

        def upsert(self, index, doc_type, data_id, data):
            doc = dict({
                'doc_as_upsert': True,
                'doc': data
            })
            return self.es.update(index=index, doc_type=doc_type, id=data_id, body=doc)

        def update_by_id(self, index, doc_type, data_id, data):
            return self.es.update(
                index=index, doc_type=doc_type, id=data_id, body=data)

        def find_last_one(self, index):
            exists_index = self.es.indices.exists(index=index)
            if not exists_index:
                return {'_id': ''}

            d = self.es.search(index=index, body={
                "size": 1,
                "sort": [{"data_id.keyword": {"order": "desc"}}]
            })
            data = d['hits']['hits']
            if len(data) == 0:
                return {'_id': ''}
            return {'_id': data[0]['_source']["data_id"]}

        def update_content_cover_by_url(self, index, doc_type, content, cover, url):
            exists_index = self.es.indices.exists(index=index)
            if not exists_index:
                return None, None

            d = self.es.search(index=index, doc_type=doc_type, body={
                "query": {
                    "term": {
                        "docurl.keyword": url
                    }
                }
            })
            data = d['hits']['hits']
            if len(data) == 0:
                return None, None
            doc = data[0]['_source']
            doc_id = data[0]['_id']
            doc['content'] = content
            doc['coverurl'] = cover
            r = self.upsert(index, doc_type, doc_id, doc)
            return doc_id, r

        def find_data_by_keyword_in_one_day(self, index, keywords):
            keywords_size = len(keywords)
            str_keywords = '"' + keywords.pop() + '"'
            for keyword in keywords:
                str_keywords = str_keywords + ' OR "' + keyword + '"'
            d = self.es.search(index=index, size=10000, body={
                "query": {
                    "bool": {
                        "must": [
                            {
                                "query_string": {
                                    "default_field": "SourceContent",
                                    "query": str_keywords
                                }
                            },
                            {
                                "range": {
                                    "PostDate": {
                                        "gte": "now-30d",
                                        "lte": "now"
                                    }
                                }
                            }
                        ]
                    }
                }
            })
            datas = d['hits']['hits']
            return datas


        def find_data_in_one_day(self, begin, end):
            d = self.es.search(index=index, size=10000, body={
                "query": {
                  "range": {
                      "PostDate": {
                        "gte": begin,
                        "lte": end
                      }
                  }
                }
            })
            datas = d['hits']['hits']
            return datas


        def find_news_by_latest_hours(self, index, diff_hours=2):
            current = datetime.datetime.now() + datetime.timedelta(hours=8)
            two_hours_ago = current - datetime.timedelta(hours=diff_hours)
            two_hours_ago = two_hours_ago.strftime('%Y-%m-%dT%H:00:00.000Z')
            current = current.strftime('%Y-%m-%dT%H:00:00.000Z') 
            data = {
              "size": 10000,
              "query": {
                "bool": {
                  "must": [
                    {
                      "range": {
                        "PostDate": {
                          "gte": two_hours_ago,
                          "lte": current
                        }
                      }
                    },
                    {
                      "terms": {
                        "media.keyword": [
                          "鏡週刊", "新頭殼", "自由時報", "中央通訊社", "上報", "聯合新聞網",
                          "ETtoday新聞雲", "三立新聞", "蘋果日報", "Yahoo!奇摩", "中時電子報",
                          "風傳媒", "TVBS", "關鍵評論網路", "NOWnews"
                        ]
                      }
                    }
                  ]
                }
              }
            }
            esdata = self.es.search(index=index, body=data)
            return esdata['hits']['hits']

        def media_bucket(self, index, from_date, to_date):
            d = self.es.search(index=index, body={
                "query": {
                    "range": {
                        "PostDate": {
                            "gte": from_date,
                            "lt": to_date
                        }
                    }
                },
                "aggs": {
                    "perMediaAggregation": {
                        "terms": {
                            "field": "media.keyword",
                            "size": 10000
                        }
                    },
                    "count":{
                        "cardinality": {
                            "field": "media.keyword"
                        }
                    }
                }
            })
            return d['aggregations']['perMediaAggregation']['buckets']

        

    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def setup(self):
        self.host = self.container.config['ES_HOST']
        self.user = self.container.config.get('ES_USER', '')
        self.password = self.container.config.get('ES_PASSWORD', '')
        self.port = 9200

    def start(self):
        self.instance = ElasticSearch.__Elasticsearch(
            self.host, self.port, self.user, self.password)

    def stop(self):
        pass

    def kill(self):
        pass

    def get_dependency(self, worker_ctx):
        return self.instance
