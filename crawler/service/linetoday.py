#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
import requests
import time


class LINETODAY:

    def __init__(self, server):        
        print('init LINETODAY')
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def on_MNG2CRW_NTF_LINETODAY_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        # print(str(from_notify))
        if not url:
            return
        
        try:
            html = self.goto(url)
        except:
            traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_LINETODAY_GET_POST(self, packet):

        def click_show_more_button(driver,pause = 1):
            try:
                element = driver.find_element_by_xpath("//*[@id='comment_see_all']/a")
                driver._click(element)
                time.sleep(pause)
                self.server.log.debug('comment_see_all TRY')
            except:
                self.server.log.debug('comment_see_all EXCEPT')
                pass

            while True:
                try:
                    element = driver.find_element_by_xpath("//*[@id='comment_read_more']/a")
                    driver._click(element)
                    time.sleep(pause)
                    self.server.log.debug('comment_read_more TRY')
                except:
                    self.server.log.debug('comment_read_more EXCEPT')
                    break
            
            button = driver.find_elements_by_xpath('//*[@class="replylist _replyCount"]')
            button2 = driver.find_elements_by_xpath('//*[@class="news-comment-more re-comments-more"]')
            for bt, bt2 in zip(button, button2):
                try:
                    bt._click()
                    time.sleep(pause)
                    bt2._click()
                    time.sleep(pause)
                    self.server.log.debug('comment_button_button2 TRY')
                except:
                    self.server.log.debug('comment_button_button2 EXCEPT')
                    pass
            
            button3 = driver.find_elements_by_xpath('//*[@class="bl-comm _tempBlinded"]')
            for bt3 in button3:
                try:
                    bt3._click()
                    time.sleep(pause)
                    self.server.log.debug('comment_button3 TRY')
                except:
                    self.server.log.debug('comment_button3 EXCEPT')
                    pass




        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        print('URL: ', url)

        try:
            self.server.browser.get(url)
            ### 18+ button ###
            try:
                element_18 = self.server.browser.find_element_by_xpath(
                    '//*[@id="btnOk"]')
                self.server.browser._click(element_18)
                self.server.log.debug('click 18+ TRY')
            except:
                self.server.log.debug('click 18+ EXCEPT')
                pass
            
            time.sleep(1)

            #detail
            detail_html = self.server.browser.html()
            self.server.log.debug('detail_html [OK]')
            
            #comment
            click_show_more_button(self.server.browser, pause=1)
            self.server.log.debug('click show more button')
            line_comment_html = self.server.browser.html()
            # line_comment_html = None

            html = {'html': detail_html, 'comment_html':line_comment_html, 'url': url}
        except:
            self.server.log.debug('final EXCEPT')
            traceback.print_exc(file=sys.stdout)

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
