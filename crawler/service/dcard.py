#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
import time
import traceback
import sys
from six.moves import urllib
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
from protocols.mng2crw_ntf_dcard_get_forum_list import MNG2CRW_NTF_DCARD_GET_FORUM_LIST
from protocols.mng2crw_ntf_dcard_get_post_list import MNG2CRW_NTF_DCARD_GET_POST_LIST
from protocols.mng2crw_ntf_dcard_get_post import MNG2CRW_NTF_DCARD_GET_POST
from protocols.crw2par_ntf_dcard_forum_list import CRW2PAR_NTF_DCARD_FORUM_LIST
from protocols.crw2par_ntf_dcard_post_list import CRW2PAR_NTF_DCARD_POST_LIST
from protocols.crw2par_ntf_dcard_post import CRW2PAR_NTF_DCARD_POST

class DCARD:
    proxy = None
    def __init__(self, server):
        self.server = server

    def goto_and_validate_url(self, url, max_retry=5):
        html = ''
        if DCARD.proxy is None:
            DCARD.proxy = self.server.redis.random_proxy()

        for i in range(max_retry):
            ua = UserAgent(verify_ssl=False, use_cache_server=False)
            fake_ua = ua.random
            headers = { 'user-agent': fake_ua }
            proxies = { 
                'http': 'http://{}'.format(DCARD.proxy),
                'https': 'https://{}'.format(DCARD.proxy)
            }
            #self.check_ip(headers, proxies)
            try:
                response = requests.get(url, headers=headers, proxies=proxies, timeout=(5, 5))
                response.encoding = 'utf-8'
                js = json.loads(response.text)
                html = response.text
                break
            except Exception as e:
                DCARD.proxy = self.server.redis.random_proxy()

        return html

    def check_ip(self, headers, proxies):
        url = 'http://lumtest.com/myip.json'
        response = requests.get(url, headers=headers, proxies=proxies, timeout=(5, 5))
        response.encoding = 'utf-8'
   
    def on_MNG2CRW_NTF_DCARD_GET_FORUM_LIST(self, packet):
        from_notify = MNG2CRW_NTF_DCARD_GET_FORUM_LIST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()

        html = self.goto_and_validate_url(url, 10)

        to_notify = CRW2PAR_NTF_DCARD_FORUM_LIST()
        to_notify.set_content(html)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_DCARD_GET_POST_LIST(self, packet):
        from_notify = MNG2CRW_NTF_DCARD_GET_POST_LIST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()

        start_time = time.time()

        html = self.goto_and_validate_url(url)

        elapsed_time = time.time() - start_time
        self.server.log.debug("--- on_MNG2CRW_NTF_DCARD_GET_POST_LIST: {} seconds:{} ---".format(elapsed_time, url))

        to_notify = CRW2PAR_NTF_DCARD_POST_LIST()
        to_notify.set_content(html)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_DCARD_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_DCARD_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        comment_url = from_notify.get_comment_url()

        start_time = time.time()

        html = self.goto_and_validate_url(url)
        comments = self.goto_and_validate_url(comment_url)

        elapsed_time = time.time() - start_time
        self.server.log.debug("--- on_MNG2CRW_NTF_DCARD_GET_POST: {} seconds:{} ---".format(elapsed_time, url))

        to_notify = CRW2PAR_NTF_DCARD_POST()
        to_notify.set_content(html)
        to_notify.set_comments(comments)
        self.server.send(to_notify)
