#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime


class FOUNTMEDIA:
    def __init__(self, server):
        print('init fountmedia.py')
        self.server = server

    def on_CRW2PAR_NTF_FOUNTMEDIA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_fountmedia_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_FOUNTMEDIA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_fountmedia_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_fountmedia_list(self, content):

        def parse_next_page(html_bs4):
            current =  'https:' + html_bs4.find('div',class_ = 'current').find('a').attrs['href']
            p = html_bs4.find('div',class_ = 'page-next').find('a').attrs['href']
            return current + p
        
        def parse_latest_date(html_bs4):
            post_entries = html_bs4.find_all('article', class_ = 'cell')
            time_list = [ x.find(class_='time').text for x in post_entries]
            time_list = [ datetime.datetime.strptime(x, '%Y.%m.%d') for x in time_list]
            latest_post_date = max(time_list).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        def parse_link(html_bs4):
            
            def parse_list_meta(post):
                ''' parse url and metadata of news list '''
                
                tmp = post.find('a').attrs['href']
                if tmp[:7] == 'article':
                    tmp = 'https://www.fountmedia.io/' + tmp
                
                meta = {
                    'url': tmp,
                    'metadata': {}
                }
                return meta
            
            post_entries = html_bs4.find_all('article', class_ = 'cell')
        
            list_meta = []
            for post in post_entries:
                    list_meta.append(parse_list_meta(post))
            
            return list_meta
                
        clean_html = BeautifulSoup(content, 'lxml')
        news_body =  clean_html.find('div',class_ = 'left cell large-8')
        
        next_p = parse_next_page(clean_html)
        latest_post_date = parse_latest_date(news_body)
        list_meta = parse_link(news_body)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_p)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)
        return item

    def parse_fountmedia_content(self, content):

        def parse_time(clean_html): 
            date_org = clean_html.find(class_ = 'cell small-4').text.split()[0] + ' ' + \
                            clean_html.find(class_ = 'cell auto time').text        
            date = datetime.datetime.strptime(date_org, '%Y.%m.%d %H:%M%p').strftime('%Y-%m-%dT%H:%M:%S+0800')          
            return date
        
        def parse_title(clean_html):
            return clean_html.find('div', class_ = 'cell auto text-justify title').text
        
        def parse_author(clean_html):
            author = clean_html.find(class_ = 'cell large-5 text-left author').text.split()[-1]
            return author
        
        def parse_content(clean_html):
            
            intro = clean_html.find(class_ = 'introduction').text
            content = clean_html.find(class_ = 'content').text
            content = ''.join([ent for ent in content.split()])
            
            reg = re.compile(r'[【(（]\D*圖\D*[】)）]', re.VERBOSE)
            tmp = reg.findall(content)  
            try:
                content = content.replace(tmp[0], '')        
            except:
                content = content
            return intro + ' ' + content

            
        def parse_metadata(clean_html):
            
            try: 
                category = clean_html.find(class_ = 'cell large-3 cat').text.split()
                category = category[-1]
            except AttributeError:
                category = ''

            try: 
                keywords = clean_html.find(class_ = 'tag').text.split()
            except AttributeError:
                keywords = []
            
            
            return {'category': category,
                    'tag': keywords} 


        # detail
        clean_html = BeautifulSoup(content['detail_html'], 'lxml')   
        url = content['url']
    
        date = parse_time(clean_html)
        title = parse_title(clean_html)
        author = parse_author(clean_html)
        content = parse_content(clean_html)
        metadata = parse_metadata(clean_html)
        comment = []
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
