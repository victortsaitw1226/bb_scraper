from browser_with_wire import Browser
from bs4 import BeautifulSoup
import time
import json
import csv
import datetime
import random, traceback, sys
import redis
import re
from mongodb import MongoDB
from proxy import Proxy
from util import binary2json, random_sec

def parse_detail(chrome):
    data = []
    for request in chrome.requests():
        if not request.path.startswith('https://www.facebook.com/ads/library/async/insights/?ad_archive_id='):
            continue
        print(request.path)
        try:
            detail = binary2json(request.response.body)
            if 'error' in detail:
                raise Exception('datail get error', detail['errorSummary'])
        except:
            raise
        detail['url']=request.path
        return detail

def parse_detail_from_other_version(chrome, data=[]):
    soup = chrome.soup()
    chrome.screenshot('next.png')
    next_node = soup.find('span', text=re.compile("Next"))
    there_is_next_version = True
    # There's only one version
    if next_node is None:
        there_is_next_version = False
    else:
        next_btn = next_node.parent.parent
        print(next_btn)
        disable = next_btn.get('aria-disabled')
        if disable == 'true':
             there_is_next_version = False
    print('ther_is_next_version', there_is_next_version)
    detail = parse_detail(chrome)
    data.append(detail)
    if there_is_next_version:
        element_xpath = chrome.beautifulsoup_to_xpath(next_btn)
        btn = chrome.find_element_by_xpath(element_xpath)
        chrome.delete_requests()
        chrome.click(btn)
        time.sleep(random_sec())
        return parse_detail_from_other_version(chrome, data)
    return data

def parse_summary(chrome):
    summary = []
    for request in chrome.requests():
        if not request.path.startswith('https://www.facebook.com/ads/library/async/search_ads'):
            continue
        print('summary requests:', request.path)
        try:
            data = binary2json(request.response.body)
        except:
            raise
        data['url'] = request.path
        summary.append(data)
    return summary

def parse_data(chrome, url):
    #chrome.recreate_driver()
    data = {
      'summary': '',
      'detail': []
    }
    chrome.goto(url)
    chrome.scrollDown()
    btns = chrome.soup().find_all('a', {'data-testid': 'snapshot_footer_link'})
    print('btns:{}'.format(len(btns)))
    if len(btns) == 0:
        chrome.screenshot()
        raise Exception('btns is 0')

    data['summary'] = parse_summary(chrome)
    chrome.delete_requests()
    for comment_btn in btns:
        time.sleep(random_sec())
        element_xpath = chrome.beautifulsoup_to_xpath(comment_btn)
        btn = chrome.find_element_by_xpath(element_xpath)
        chrome.click(btn)
        data['detail'].append(parse_detail_from_other_version(chrome, []))
        chrome.delete_requests()
    return data


def main(redis, mongo, proxy):
    chrome = Browser(proxy_address=proxy.random_proxy())
    while True:
        url = mongo.random_url()  
        print(url)
        if url is None:
            break
        try:
            data = parse_data(chrome, url['url'])
            data.update({
                'created_dt': datetime.datetime.now()
            })
            res = mongo.insert_one('data_sun', data)
            mongo.update_one('urls', { '_id': url['_id']}, {'$set': {'updated': True}})
        except:
            # Retry
            traceback.print_exc(file=sys.stdout)
            chrome.recreate_driver(proxy.random_proxy())

if __name__ == '__main__':
    #m = MongoDB()
    #r = redis.Redis(
    #    host='210.202.47.189', password='ibdo2018'
    #)
    #p = Proxy(m, r)
    #main(r, m, p)a
    chrome = Browser(proxy_address="34.84.153.193:3128")
    #chrome = Browser(proxy_address="35.237.220.171:54378")
    chrome.check_ip()
