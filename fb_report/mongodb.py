import pymongo
import random

class MongoDB(object):

    def __init__(self):
        self.client = pymongo.MongoClient('mongodb://ibdo:ibdo2018@210.202.47.189')
        self.db = self.client['fb_report']

    def find(self, collection, filters={}):
        for data in self.db[collection].find(filters):
            yield data

    def insert_one(self, collection, data):
        return self.db[collection].insert_one(data)

    def update_one(self, collection, filters, data):
        return self.db[collection].update_one(filters, data)

    def random_url(self):
        urls = list(self.find('urls', {'updated': False}))
        if len(urls) == 0:
            return None
         
        return random.choice(urls)
    
    def reset_url(self):
        self.db['urls'].update_many({}, {'$set': {'updated': False}})


if __name__ == '__main__':
    m = MongoDB()
    m.reset_url()
