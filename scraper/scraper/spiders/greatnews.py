# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class GreatnewsSpider(RedisSpider):
    name = "greatnews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "greatnews",
            "name": "greatnews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://greatnews.com.tw/home/news_page.php?iType=1001&page=1",
            "scrapy_key": "greatnews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        soup = soup.find('div',id = 'news_are')
        links = self.parse_links(soup)

        if links==[]:
            return

        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return


        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_latest_post_date(self, soup):
        latest_post_date = soup.find('div', class_ = 'news_list_in_date').text
        latest_post_date = datetime.strptime(latest_post_date, '%Y/%m/%d')       
        return latest_post_date

    def parse_links(self, soup):
        post_entries = soup.find_all('div', class_ = 'news_list_in_title')
        list_meta = []
        for post in post_entries:
            tmp = post.find('a').attrs['href']
            if tmp[:4] == 'news':
                tmp = 'http://greatnews.com.tw/home/' + tmp
            list_meta.append(tmp)
        return list_meta
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        content, author = self.parse_content_author(soup)
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = content
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'greatnews'
        item['proto'] = 'GREATNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        date_org = soup.find('div', class_ = 'newsin_date').text
        date = datetime.strptime(date_org, '%Y/%m/%d ').strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_title(self,soup):
        return ''.join(soup.find('div', class_ = 'newsin_title').text.split())
    
    def parse_content_author(self,soup):
        date_org = soup.find('div', class_ = 'newsin_date').text
        content = ''.join([ent for ent in soup.find(class_ = 'newsin_text').text.replace(date_org, '').split()])
        
        reg = re.compile(r'[【(（]\D+[／/╱]?\D*[】)）]', re.VERBOSE)
        tmp = reg.findall(content[:51])   
        
        content = content.replace(tmp[0], '')
        
        ind_auth = tmp[0].find('記者')
        if ind_auth != -1:
            author = list(filter(None, re.split('╱|/|／', tmp[0][(ind_auth+2):-5])))
            author = ','.join([a for a in author])
        else:
            tmp = re.sub(r'[【（(]', '', tmp[0])
            author = re.sub(r'[】)）]', '', tmp)
        return content, author   

    def parse_metadata(self,soup):
        metadata = {'category':''}
        try: 
            metadata['category'] = soup.find(class_ = 'bn_titleA').text   
        except AttributeError:
            pass 
        return metadata
