#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime

class HEALTHNEWS:
    def __init__(self, server):
        print('init healthnews.py')
        self.server = server

    def on_CRW2PAR_NTF_HEALTHNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        item = self.parse_healthnews_list(content)
        print('item:',item)
        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_days_limit(from_notify.get_days_limit())
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_HEALTHNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_healthnews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_healthnews_list(self, content):     
        soup = BeautifulSoup(content, 'lxml') 
        next_page, latest_post_date, links = None, None, []

        try:
            next_page = soup.find('ul', {'class':'pagination'}).find('a', {'rel':'next'})['href']
        except: 
            pass
    
        try:
            latest_post_date = soup.find('div', {'class':'blogicons'}).text
            latest_post_date = latest_post_date.strip().split(' ')[1]
            latest_post_date = datetime.datetime.strptime(latest_post_date, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            pass
        
        try:              
            p = soup.find_all('div', {'class':'caption col-lg-8 col-md-8 col-xs-12 col-sm-6'})
            for i in range(len(p)):
                link = p[i].find('a')['href']
                links.append({
                            'url': link,
                            'metadata': {}
                            })
        except:
            pass
    
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_healthnews_content(self, content):

        def parse_datetime(soup):
            return datetime.datetime.strptime(soup.find_all('span', {'class':'mr10'})[0].text.strip(), '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            try:
                au = soup.find_all('span', {'class':'mr10'})[1].text.split('／')[1].replace('記者','').replace('報導','').strip()
            except:
                au = ''
            return au
        
        def parse_title(soup):
            return soup.find('span', {'class':'maintext'}).text.replace('\n', '').replace('\u3000', '').strip()
        
        def parse_content(soup):
            return soup.find('div', {'id':'newsContent'}).text.replace('\n', '').replace('\u3000', '').strip()
        
        def parse_metadata(soup):
            try:
                tags = soup.find_all('div', {'class':'keywordLink'})[1].find_all('a')
                tags = [t.text for t in tags]
            except:
                tags = []
            cat = soup.find('ul', {'class':'breadcrumb'}).find_all('li')[2].text
            
            return {'category': cat,
                    'tag': tags,
                    }

        soup = BeautifulSoup(content['html'], 'lxml')
        [s.extract() for s in soup.find_all('script')]

        url = content.get('url')
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        # item.set_comment(parse_comments(comment_html))
        item.set_metadata(parse_metadata(soup))
        
        return item
