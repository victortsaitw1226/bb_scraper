#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class FACEBOOK_PARSE_EMOS(Protocol):

    def __init__(self):
        super(FACEBOOK_PARSE_EMOS, self).__init__('FACEBOOK_PARSE_EMOS')
        self.update({
          'post_url': '',
          'emos_url': '',
          'likes': [],
          'loves': [],
          'hahas': [],
          'angrys': [],
          'wows': [],
          'sads':[]
        })

    def set_post_url(self, url):
        self.set('post_url', url)

    def get_post_url(self):
        return self.get('post_url')

    def set_emos_url(self, url):
        self.set('emos_url', url)

    def get_emos_url(self):
        return self.get('emos_url')

    def set_likes(self, likes):
        self.set('likes', likes)

    def get_likes(self):
        return self.get('likes')

    def set_loves(self, loves):
        self.set('loves', loves)

    def get_loves(self):
        return self.get('loves')

    def set_hahas(self, hahas):
        self.set('hahas', hahas)

    def get_hahas(self):
        return self.get('hahas')

    def set_angrys(self, angrys):
        self.set('angrys', angrys)

    def get_angrys(self):
        return self.get('angrys')

    def set_wows(self, wows):
        self.set('wows', wows)

    def get_wows(self):
        return self.get('wows')

    def set_sads(self, sads):
        self.set('sads', sads)

    def get_sads(self):
        return self.get('sads')
