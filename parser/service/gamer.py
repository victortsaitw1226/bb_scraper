#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class GAMER:
    def __init__(self, server):
        print('init gamer.py')
        self.server = server

    def on_CRW2PAR_NTF_GAMER_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_gamer_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_GAMER_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_gamer_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_gamer_list(self, content):

        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
    
        soup = BeautifulSoup(content, 'html.parser')
        
        # Get Articles url
        post_list = soup.find_all('div','GN-lbox2B')
        
        for i in range(len(post_list)):
            news={'url': None, 'metadata': {}}
            news['url'] = 'https:' + post_list[i].find('a')['href']
            data['links'].append(news)
            
        # Get latest_post_date
        time_stamp = soup.find_all('p','GN-lbox2A')[0]
        dat = datetime.strptime(time_stamp.text ,'%m 月 %d 日')
        dat = dat.replace(year=datetime.today().year )
        data['latest_post_date'] = dat.strftime('%Y-%m-%dT%H:%M:%S+0800')


        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_gamer_content(self, content):

        def parse_datetime(soup):
            time_stamp = soup.find('span',re.compile(r'GN-lbox3C')).text
            time_stamp = time_stamp.split('）')[-1].strip()
            time_stamp = ' '.join(time_stamp.split(' ')[:2])
            dat = datetime.strptime(time_stamp , '%Y-%m-%d %H:%M:%S')
            return dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
        def parse_metadata(soup):
            metadata = {'tag':'','category':'GNN新聞'}
            keyword_section = soup.find('ul','platform-tag')
            keyword = [a.text for a in keyword_section.find_all('li')]
            if len(keyword):
                metadata['tag'] = keyword
            return metadata
                
        
        def parse_author(soup): 
            author = soup.find('span',re.compile(r'GN-lbox3C')).text
            author = author.replace('（','')
            author = author.replace('）','')
            if '記者' in author:
                author = author.split(' ')[2]
            elif '報導' in author:
                author = author.split(' ')[0]
            else:
                author = ''
            return author

        def parse_title(soup):
            return soup.find('h1').text
        
        def parse_content(soup):
            article = soup.find('div', class_='GN-lbox3B')
            article.find('ul','platform-tag').clear()
            content = ' '.join(article.text.split())
            return content
        
        def parse_comments(soup):
            
            def pare_one_commt(cb):
                author_url = 'https:' + cb.find('p').find('a')['href']
                author = cb.find('p').find('a').text
                reply_content = cb.find('span','comment-text').text
                reply_time = datetime.strptime(cb.find_all('span')[1].text ,'%m-%d %H:%M:%S')
                reply_time = reply_time.replace(year=datetime.today().year )
                reply_time = reply_time.strftime('%Y-%m-%dT%H:%M:%S+0800')
                one_commt_detail = {'comment_id':'', 'author_url':author_url, 'author':author,
                                    'content':reply_content ,'datetime':reply_time, 
                                    'source':'gamer','metadata':{},'comment':[]}
                return one_commt_detail
                
            commt_list = []
            commt_blocks = soup.find_all('div','GN-lbox6A')
            for cb in commt_blocks:
                commt_list.append(pare_one_commt(cb))     
                
            return commt_list
       

        soup = BeautifulSoup(content['html'], 'html.parser')
        comments_soup = BeautifulSoup(content['comment_html'], 'html.parser')
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_metadata(parse_metadata(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(comments_soup))
       
        return item
