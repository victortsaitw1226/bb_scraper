from bs4 import BeautifulSoup
from datetime import datetime

def parse_comments(soup):
    comments = []
    def parse_one_comment(one_commt):
        auth = one_commt.find(class_ = 'UFICommentActorName') 
        if auth.has_attr('href'):
            auth_url = auth['href'] 
        else:
            auth_url = ''
        auth_id = auth.text

        cont = one_commt.find(class_ = '_30o4')
        dat = one_commt.find('abbr','UFISutroCommentTimestamp livetimestamp')
        dat = datetime.fromtimestamp(int(dat['data-utime'])).strftime('%Y-%m-%dT%H:%M:%S+0000')
        
        # fblike
        try:
            fblike_num = one_commt.find('i','_3-8_ _4iy4 img sp_TME_gPvfJdR sx_400fbd').parent
            fblike_num = fblike_num.text
        except:
            fblike_num = '0'
            
        one_commt_detail = {'comment_id':'', 'datetime':dat, 'author':auth_id, 'author_url':auth_url, 
                                'content':cont.text ,'metadata':{'fb_like_count':fblike_num}}
        return one_commt_detail
        
    block = soup.find_all('div','_3-8y _5nz1 clearfix')
    for b in block:
        block_comments = b.find_all('div','UFIImageBlockContent _42ef clearfix')
        block_main_commt = block_comments[0]
        [s.clear() for s in block_main_commt.find_all('div','_3-8y clearfix')] 
        block_comments_detail = parse_one_comment(block_main_commt)
        block_comments_detail['source'] = 'fb'
        
        block_commt_commt = []  
        for bb in range(1,len(block_comments)):
            block_commt_commt.append(  parse_one_comment(block_comments[bb]))
        
        block_comments_detail['comments'] = block_commt_commt
        comments.append(block_comments_detail)
    
    return comments