# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class CnyesBlogSpider(RedisSpider):
#class CnyesBlogSpider(scrapy.Spider):
    name = "cnyesblog"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [
        {
            "media": "cnyesblog",
            "name": "cnyesblog",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            #"url": "http://blog.cnyes.com/SearchBlog.aspx?Purpose=blog&ClassifyId=3",
            "url": "http://blog.cnyes.com/SearchArticle.aspx?KindId=23&PageIndex=1",
            "scrapy_key": "cnyesblog:start_urls",
            "priority": 1,
            "category": "台股"
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        link_blocks = soup.find('div','bd clear noframeline').find_all('tr') 
        if len(link_blocks) == 0:
            return

        posts_date = []

        now = datetime.now() + timedelta(hours=8)
        for post in link_blocks:
            date_tag = post.find('span', class_='date')
            date = date_parser(date_tag.get_text())
            if date > now:
                date = date - timedelta(days=365)
            posts_date.append(date)
            url_tag = date_tag.find_next_sibling('a')
            href = url_tag.get('href')
            views = post.find('img', {'src': 'images/icon_pageview.gif'}).next
            views = str(views)
            yield response.follow(href,
                    meta={
                        'category': meta['category'],
                        'views': views,
                        'date': date
                    },
                    callback=self.parse_article)

        latest_datetime = max(posts_date)
        past = now - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("PageIndex=(\d+)", response.url).group(1)
        next_page = re.sub("PageIndex=(\d+)", "PageIndex={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = meta['date']
        item['metadata'] = {
            'category': meta['category'],
            'view_count': meta['views'],
        }
        item['content_type'] = 0
        item['media'] = 'cnyes'
        item['proto'] = 'CNYES_BLOG_PARSE_ITEM'
        yield item

        #comments
        for reply in soup.find_all('div','font10gray question'):
            # datetime
            date = reply.find('div',{'align':'right','style':'clear: both;'}).find_all('span')[-1].text
            date = ''.join(date.split()).replace('下午','PM').replace('上午','AM')
            date = datetime.strptime( date , '於%Y/%m/%d%p%I:%M:%S')
            # author
            try:
                author = reply.find('div',{'align':'right','style':'clear: both;'}).find('a','link').text
            except:
                author = ''
            item = NewsItem()
            item['url'] = response.url
            item['metadata'] = {
                'category': meta['category'],
            }
            item['article_title'] = self.parse_title(soup)
            item['author'] = author
            item['author_url'] = []
            item['date'] = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = reply.find('div','wordwrap').text
            item['content_type'] = 1
            item['media'] = 'cnyes'
            item['proto'] = 'CNYES_BLOG_PARSE_ITEM'
            item['comment'] = []
            yield item

    def parse_datetime(self, soup):
        date = datetime.strptime( soup.find('ul','noUL').find('li','font9red').text , '%Y/%m/%d')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        try:
            author = soup.find('ul', 'font10gray noUL').find('li','profile_tit')
            author.find('strong').clear()
            author = author.text.strip()
        except:
            author = ''
        return author
    
    def parse_title(self, soup):
        title = soup.find('head').find('title').text.strip()
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','article').text
        content = ''.join(content.split())
        return content
