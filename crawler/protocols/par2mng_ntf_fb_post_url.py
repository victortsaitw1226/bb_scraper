#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_FB_POST_URL(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_FB_POST_URL, self).__init__('PAR2MNG_NTF_FB_POST_URL')
        self.update({
          'url': '',
          'fan_page': ''
        })

    def set_fan_page(self, fan_page):
        self.set('fan_page', fan_page)

    def get_fan_page(self):
        return self.get('fan_page')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')
