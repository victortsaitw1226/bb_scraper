#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import time
import os
import traceback
import sys
import platform
import json
import datetime
import re
import random
from seleniumwire import webdriver
#from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.proxy import Proxy, ProxyType
from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from fake_useragent import UserAgent
from common import transform_xpath

class Browser(object):
    def __init__(self, **kwargs):
        self.driver = self.create_driver(**kwargs)

    def create_driver(self, proxy_address=''):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--lang=en-US')
        ua = UserAgent()
        fake_ua = ua.random
        print('proxy:{}, ua:{}'.format(proxy_address, fake_ua))
        #fake_ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
        chrome_options.add_argument('--user-agent={}'.format(fake_ua))
        if proxy_address:
            chrome_options.add_argument('--proxy-server=%s' % proxy_address)
        ## close ad
        prefs = {  
            'profile.default_content_setting_values' :  {  
                'notifications' : 2  
            }  
        }  
        chrome_options.add_experimental_option('prefs', prefs)
        desired_caps = chrome_options.to_capabilities()
        seleniumwire_options= {}
        if proxy_address:
            prox = Proxy()
            prox.proxy_type = ProxyType.MANUAL
            prox.http_proxy = 'http://{}'.format(proxy_address)
            prox.ssl_proxy = 'https://{}'.format(proxy_address)
            prox.no_proxy = ''
            prox.add_to_capabilities(desired_caps)
            seleniumwire_options= {
                'proxy': {
                    'http': 'http://{}'.format(proxy_address),
                    'https': 'https://{}'.format(proxy_address),
                    'no_proxy': ''
                },
                'connection_timeout': None
            }
        driver = webdriver.Chrome(
            chrome_options=chrome_options,
            #desired_capabilities=desired_caps
            seleniumwire_options=seleniumwire_options
        )
        driver.implicitly_wait(10) # seconds
        return driver

    def recreate_driver(self, proxy=''):
        try:
            self.driver.quit()
        except:
            pass
        self.driver = self.create_driver(proxy_address=proxy)
        self.check_ip()

    def delete_requests(self):
        del self.driver.requests

    def goto(self, url, wait=2, tag=''):
        if not url:
            raise Exception('Browser.goto:url is None')
        return self._goto(url, wait, tag)

    def _goto(self, url, wait, tag):
        start_time = time.time()
        self.driver.get(url)
        #self.driver.wait_for_request('https://www.facebook.com/ads/library/async/insights/', timeout=30)
        #elapsed_time = time.time() - start_time
        time.sleep(1)
        #element = WebDriverWait(self.driver, 10).until(
        #    EC.presence_of_element_located((By.TAG_NAME, 'a'))
        #)
        #return self.driver.page_source
        return self.driver.requests

    def requests(self):
        for request in self.driver.requests:
            yield request

    def scrollDown(self, scroll=-1, pause=1):
        return self._scrollDown(scroll, pause)
        try:
            url = self.url()
            return self._scrollDown(scroll, pause)
        except WebDriverException:
            traceback.print_exc(file=sys.stdout)
            self.recreate_driver()
            self.goto(url)
            return self.scrollDown(scroll, pause)

    def _scrollDown(self, scroll=-1, pause=10):
        """
        Function to scroll down till end of page.
        """
        import time
        lastHeight = self.driver.execute_script("return document.documentElement.scrollHeight")
        print('Browser Last Height: %d' % lastHeight)
        i = 0
        while True:
            self.driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
            time.sleep(pause)
            newHeight = self.driver.execute_script("return document.documentElement.scrollHeight")
            print('[aaa]Browser New Height: %d' % newHeight)
            if newHeight == lastHeight:
                break
            if scroll > 0 and i >= scroll:
                break
            lastHeight = newHeight
            i = i + 1
        return lastHeight

    def switch_to(self, url):
        self.driver.switch_to.frame(url)

    def output(self, filename):
        html = self.driver.page_source
        with open(filename, 'w') as f:
            f.write(html)

    def screenshot(self, filename='facebook.png'):
        self.driver.save_screenshot(filename)


    def click(self, element):
        self.driver.execute_script("arguments[0].click();", element)
        time.sleep(2)
        return self.driver.page_source

    def _click(self, element):
        element.click()

    def html(self):
        return self.driver.page_source

    def url(self):
        return self.driver.current_url

    def soup(self):
        return BeautifulSoup(self.driver.page_source, 'html.parser')

    def quit(self):
        self.driver.quit()

    def beautifulsoup_to_xpath(self, element):
        return transform_xpath(element)

    def find_element_by_xpath(self, path):
        return self.driver.find_element_by_xpath(path)

    def find_elements_by_xpath(self, path):
        return self.driver.find_elements_by_xpath(path)

    def find_element_by_tag(self, tag):
        return self.driver.find_element_by_tag_name(tag)

    def get_attribute(self, element, attribute):
        return element.get_attribute(attribute)

    def move_to_element(self, element):
        #actions = ActionChains(self.driver)
        #actions.move_to_element(element).perform()
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        return self.driver.page_source
    
    def get_window_screen_height(self):
        return self.driver.execute_script("return window.screen.availHeight") 
    
    def get_dicumentelement_scrolltop(self):
        return self.driver.execute_script("return document.documentElement.scrollTop")  
    
    def scroll_customized_height(self,height):
        return self.driver.execute_script("window.scrollTo(0," + str(height)+ ");")

    def check_ip(self):
        url = 'http://lumtest.com/myip.json'
        self.driver.get(url)
        html = self.driver.page_source
        print('my ip: {}'.format(html))
