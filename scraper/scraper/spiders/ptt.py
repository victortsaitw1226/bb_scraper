# -*- coding: utf-8 -*-
import scrapy
from .redis_spiders import RedisSpider
import time
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import urllib
import re
from scraper.items import NewsItem
from dateutil.parser import parse as date_parser

class PttSpider(RedisSpider):
#class PttSpider(scrapy.Spider):
    name = "ptt"

    def start_requests(self ):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ptt",
            "name": "ptt",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": 'https://www.ptt.cc/bbs/Actuary/index.html',
            #"url": 'https://www.ptt.cc/bbs/Gossiping/M.1577921953.A.6F4.html',
            "scrapy_key": "ptt:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(
                request['url'],
                meta=request,
                cookies={'over18': 1},
                dont_filter=True,
                callback=self.parse,
                #callback=self.parse_article,
            )


    def parse(self, response):
        meta = response.meta
        content = response.body
        links = []
        soup = BeautifulSoup(content, 'html.parser')
        page = soup.find('div', class_= 'btn-group btn-group-paging')
        next_page = page.findAll('a')[1]
        next_page_url = next_page.get('href')
        year = datetime.now().year
        #latest_datetime = datetime.now()
        latest_datetime = None
        board = ''
        for div in soup.findAll('div',class_='r-ent'):
            title_node = div.find('div', class_='title')
            a = title_node.find('a')
            text = title_node.get_text()

            if not a:
                continue

            if '公告' in text[5:]:
                continue

            #date = div.find('div', class_='date').get_text()
            #date = str(year) + '/' + date.strip()
            #date = datetime.strptime(date, '%Y/%m/%d')

            href = a.get('href')

            post_timestamp = re.search(r'M.(\d+)', href).group(1)
            date = datetime.fromtimestamp(int(post_timestamp))

            if latest_datetime is None:
                latest_datetime = date

            elif date > latest_datetime:
                latest_datetime = date

            links.append(urllib.parse.urljoin('https://www.ptt.cc/', href))
            board = href.split('/')[2]

        for link in links:
            yield scrapy.http.Request(
                    link,
                    callback=self.parse_article,
                    dont_filter=True,
                    cookies={'over18': 1})

        interval = meta['days_limit']
        last_year = datetime.now() - timedelta(seconds=interval)

        if last_year > latest_datetime:
            return

        yield scrapy.http.Request(
                urllib.parse.urljoin('https://www.ptt.cc/', next_page_url),
                callback=self.parse,
                cookies={'over18': 1},
                dont_filter=True,
                meta=meta)

    def parse_article(self, response):
        content = response.body

        soup = BeautifulSoup(content, 'html.parser')
        link = soup.find('head').find('link', {'rel': 'canonical'}).get('href')
        board = soup.find('a', class_='board')
        board.find('span').extract()
        board = board.get_text()
        main_content = soup.find(id="main-content")
        metas = main_content.select('div.article-metaline')
        author = ''
        title = ''
        date = ''
        if metas:
            author = metas[0].select('span.article-meta-value')[0].string if metas[0].select('span.article-meta-value')[0] else author
            title = metas[1].select('span.article-meta-value')[0].string if metas[1].select('span.article-meta-value')[0] else title
            date = metas[2].select('span.article-meta-value')[0].string if metas[2].select('span.article-meta-value')[0] else date

            # remove meta nodes
            for meta in metas:
                meta.extract()
            for meta in main_content.select('div.article-metaline-right'):
                meta.extract()
        # remove and keep push nodes
        pushes = main_content.find_all('div', class_='push')
        for push in pushes:
            push.extract()
        try:
            ip = main_content.find(text=re.compile(u'※ 發信站:'))
            ip = re.search('[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*', ip).group()
        except:
            ip = None
        # 移除 '※ 發信站:' (starts with u'\u203b'), '◆ From:' (starts with u'\u25c6'), 空行及多餘空白
        # 保留英數字, 中文及中文標點, 網址, 部分特殊符號
        filtered = [ v for v in main_content.stripped_strings if v[0] not in [u'※', u'◆'] and v[:2] not in [u'--'] ]
        expr = re.compile(r'[^\u4e00-\u9fa5\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\s\w:/-_.?~%()]')
        for i in range(len(filtered)):
            filtered[i] = re.sub(expr, '', filtered[i])

        filtered = [_f for _f in filtered if _f]  # remove empty strings
        filtered = [x for x in filtered if link not in x]  # remove last line containing the url of the article
        content = ' '.join(filtered)
        content = re.sub(r'(\s)+', ' ', content)
        # print 'content', content
        # push messages
        p, b, n = 0, 0, 0
        messages = []
        for push in pushes:
            if not push.find('span', 'push-tag'):
                continue

            push_tag = push.find('span', 'push-tag').string.strip(' \t\n\r')
            push_userid = push.find('span', 'push-userid').string.strip(' \t\n\r')
            # if find is None: find().strings -> list -> ' '.join; else the current way
            push_content = push.find('span', 'push-content').strings
            push_content = ' '.join(push_content)[1:].strip(' \t\n\r')  # remove ':'
            push_ipdatetime = push.find('span', 'push-ipdatetime').string.strip(' \t\n\r')
            messages.append( {'push_tag': push_tag, 'push_userid': push_userid, 'push_content': push_content, 'push_ipdatetime': push_ipdatetime} )
            if push_tag == u'推':
                p += 1
            elif push_tag == u'噓':
                b += 1
            else:
                n += 1
            
        # count: 推噓文相抵後的數量; all: 推文總數
        message_count = {'all': p+b+n, 'count': p-b, 'push': p, 'boo': b, "neutral": n}

        item = NewsItem()
        item['url'] = link
        item['metadata'] = { 'category': board}
        item['article_title'] = title
        item['author'] = author
        if ip:
            item['author_url'] = [ip]
        else:
            item['author_url'] = []
        pdate = self.parse_post_datetime(date)
        item['date'] = str(pdate)
        item['content'] = content
        item['content_type'] = 0
        item['media'] = 'ptt'
        item['proto'] = 'PTT_PARSE_ITEM'
        item['comment'] = []
        item['metadata']['reply_count'] = message_count['all']
        yield item

        for message in messages:
            item = NewsItem()
            item['url'] = link
            item['metadata'] = {'category': board}
            item['article_title'] = title
            item['author'] = message['push_userid']
            ip = self.parse_ip(message['push_ipdatetime'])
            if ip:
                item['author_url'] = [ip]
            else:
                item['author_url'] = []
            item['date'] = str(self.parse_push_datetime(pdate, message['push_ipdatetime']))
            item['content'] = message['push_content']
            item['content_type'] = 1
            item['media'] = 'ptt'
            item['proto'] = 'PTT_PARSE_ITEM'
            item['comment'] = []
            yield item

    def parse_post_datetime(self, pdate):
        try:
            postdate = datetime.strptime(pdate, '%c')
            return postdate
        except ValueError:
            try:
                postdate = datetime.strptime(pdate, '%a %b %d %H：%M：%S %Y')
                return postdate
            except:
                return datetime.now()

    def parse_push_datetime(self, postdate, push_ipdatetime):
        if '02/29' in push_ipdatetime:
            return str(datetime.now().year) + '-02/28T23:59:00'

        if len(push_ipdatetime.split(' ')) == 3:
            reply_date = push_ipdatetime.split(' ')[1] + \
                   ' ' + push_ipdatetime.split(' ')[2]
        else: 
            reply_date = push_ipdatetime
                            
        try:
            r_postdate = datetime.strptime(reply_date, '%m/%d %H:%M')
            r_postdate = r_postdate.replace(year = postdate.year)
            return r_postdate
        except ValueError:
            r_postdate = datetime.strptime(reply_date, '%m/%d')
            r_postdate = r_postdate.replace(year = postdate.year)
            return r_postdate

    def parse_ip(self, ip_str):
        try:
            return re.search('[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*', ip_str).group()
        except:
            return None
