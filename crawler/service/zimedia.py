#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
from datetime import datetime
import traceback, sys
import requests


class ZIMEDIA:
    def __init__(self, server):        
        self.server = server
        self.session = requests.session()

    def detect_error(self, html):
        return 'wf_topnav-logo' in html
    
    def parse_zimedia_list(self, content):     
           
        def parse_latest_post_date(first_link):
            res = self.server.new_requests(first_link, self.detect_error)
            #res = self.server.browser.goto(first_link)
            soup = BeautifulSoup(res, 'html.parser')
            info = soup.find('div', class_='gl_mc-outer1000 zi_ae2-article-outer')
            time_ = info.get('data-published-time')
            dt = datetime.strptime(time_.split('.')[0], '%Y-%m-%dT%H:%M:%S')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        link_prefix = 'https://zi.media'
        links = []
        
        soup = BeautifulSoup(content, 'html.parser')
        
        for element in soup.find_all('div', class_='zi_fz18'):
            link = element.find('a').get('href')
            
            links.append({
                'url': link_prefix + link,
                'metadata': {}
            })
        if soup.find('ul', class_='zi_gl-pagination') is not None:
            np = soup.find('ul', class_='zi_gl-pagination')
            next_page = np.find_all('li')[-1]
            if (next_page.get('class') == None):
                next_page_link = next_page.find('a').get('href')
                data['next_page'] = link_prefix + next_page_link
                    
        data['links'] = links
        data['latest_post_date'] = parse_latest_post_date(links[0]['url'])
        
        return data
    


    def on_MNG2CRW_NTF_ZIMEDIA_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = self.server.new_requests(url, self.detect_error)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        #html = self.server.browser.goto(url)
        content = self.parse_zimedia_list(html)
        #except:
        #    traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(content)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_ZIMEDIA_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            html = {'html': self.server.new_requests(url, self.detect_error), 'url': url}
            #html = {'html': self.server.browser.goto(url), 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
