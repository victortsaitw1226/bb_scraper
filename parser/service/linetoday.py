#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class LINETODAY:
    def __init__(self, server):
        self.server = server

    def on_CRW2PAR_NTF_LINETODAY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_linetoday_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_LINETODAY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        item = self.parse_linetoday_content(content)
        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_linetoday_list(self, content):
       
        soup = BeautifulSoup(content,"html.parser")
        group = soup.find('li',{'class':'on _link _category'}).find('a')['href'].split('/')[-1]
        
        links = [s['href'] for s in soup.find('div', {'class': 'left-area'}).findAll('a')]
        date = [int(s['data-pbt']) for s in soup.find('div', {'class': 'left-area'}).findAll('a')]
        try:
            links.extend([s['href'] for s in soup.find('ol', {'class': 'list-type-rk _side_popular_'+group}).findAll('a')])
            date.extend([int(s['data-pbt']) for s in soup.find('ol', {'class': 'list-type-rk _side_popular_'+group}).findAll('a')])
        except:
            pass
        
        links = list(set(links))
        links = [{'url':i,'metadata':[]} for i in links]
        latest_post_date = datetime.fromtimestamp(max(date)/1000).strftime('%Y-%m-%dT%H:%M:%S+0000')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(None)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_linetoday_content(self, content):
        
        def parse_content(soup):
            try:      
                content = soup.find('div',{'id':'container'}).find('article')
                for span_tag in content.findAll('a'):
                    span_tag.replace_with('')
                content = content.find_all('p')    
                content = '\n'.join([x.text for x in content ])
                content = content.replace('\xa0','').replace('【延伸閱讀】','').replace('<延伸閱讀>','').replace('延伸閱讀：','')
            except:
                content = soup.find('head').find('meta',{'name':'description'})['content']
            return content
        
        def  parse_post_date(soup):
            date = soup.find('dd',{'itemprop':'datePublished'}).text
            date = re.split(' ',date)[1]
            date = datetime.strptime(date , '%Y年%m月%d日%H:%M')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date

        def parse_author(soup):
            try:
                author = soup.find('dd','name').text.strip()
                if '|' in author:
                    author = author.split('|')[1]
            except:
                author = soup.find('meta', {'name':'author'})['content']
            return author

        def parse_publisher(soup):
            publisher = soup.find('meta', {'name':'author'})['content']
            return publisher

        def parse_category(soup):
            site_name = soup.find('meta',{'property':'og:site_name'})['content']
            if site_name=="LINE TV":
                category = 'LINE TV'
            else:
                news_keywords = soup.find('meta',{'name':'news_keywords'})['content']
                category = news_keywords.split()[-1]
            return category

        def  parse_update_date(soup):
            try:
                date = soup.find_all('dd','date')[1].text
                date = re.split(' ',date)[1]
                date = datetime.strptime(date , '%Y年%m月%d日%H:%M')
                date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                date = ''
            return date
        
        def parse_comments(soup):
        
            def parse_date(reply_time):
                reply_time = reply_time.text
                if reply_time.find('秒鐘')!=-1:
                    dat = datetime.today()-timedelta(seconds=int(re.split('秒鐘前',reply_time)[0])) 
                elif reply_time.find('分鐘')!=-1:
                    dat = datetime.today()-timedelta(minutes=int(re.split('分鐘前',reply_time)[0])) 
                elif reply_time.find('小時')!=-1:
                    dat = datetime.today()-timedelta(hours=int(re.split('小時前',reply_time)[0])) 
                elif reply_time.find('昨天')!=-1:
                    dat = datetime.today()-timedelta(days=1)
                    h = re.search(r'昨天(.*)\:',reply_time).group(1)
                    m = re.split(':',reply_time)[1]
                    dat = dat.replace(hour=int(h), minute=int(m))
                elif reply_time.find('2天前')!=-1:
                    dat = datetime.today()-timedelta(days=2)
                    h = re.search(r'2天前(.*)\:',reply_time).group(1)
                    m = re.split(':',reply_time)[1]
                    dat = dat.replace(hour=int(h), minute=int(m))
                elif  reply_time.find('剛剛')!=-1:
                    dat = datetime.today() 
                elif reply_time.find('月')!=-1: #3天以上
                    try:
                        dat = datetime.strptime(reply_time , '%m月%d日 %H:%M')
                    except:
                        dat = datetime.strptime(reply_time , '%m月%d日%H:%M')
                    dat = dat.replace(year=datetime.today().year )
                else: #無解的時間
                    dat = datetime.today() 
                dat = dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
                return dat
            
            ct_ls = soup.find('ul', class_ = 'news-comments').find_all('li')
            result = []
            next_index = 0
            for i in range(len(ct_ls)):
                if i == next_index:
                    # print(i)
                    author2 = ct_ls[i].find('span', class_ = 'writer').text
                    # print('author2: ', author2)c
                    main_comment = ct_ls[i].find('p', class_ = 'comm').text.strip()
                    # print('main_comment: ', main_comment)
                    up = int(ct_ls[i].find('button', 
                        class_ = '_up yes').find('span', class_ = "_vote_count").text)
                    # print('up: ', up)
                    down = int(ct_ls[i].find('button', 
                        class_ = '_down no').find('span', class_ = "_vote_count").text)
                    # print('down: ', down)
                    raw_date = ct_ls[i].find('span', class_ = 'time')
                    date = parse_date(raw_date)
                    
                    replies = []

                    reply_count = int(ct_ls[i].find('span', class_ = 'num').text)

                    if reply_count > 0:
                        reply_comment_block = ct_ls[i].find('ul', class_ = 're-comments').find_all('li')
                        for reply in reply_comment_block:
                            author = reply.find('span', class_ = 'writer ').text
                            raw_date2 = reply.find('span', class_ = 'time')
                            date2 = parse_date(raw_date2)
                            reply_content = reply.find('p', class_ = 'comm').text.strip()

                            reply = {
                                    'comment_id': author,
                                    'datetime': date2,
                                    'author': author,
                                    'content': reply_content,
                                    'metadata': {
                                    }
                                }
                            
                            replies.append(reply)
                    
                    next_index = i + reply_count + 1

                    comments = {
                        'comment_id': author2, 
                        'date': date, 
                        'author': author2, 
                        'source': 'linetoday',
                        'content': main_comment,
                        'metadata':{},
                        'comments':replies,                       
                    }
                    # print(comments)
                    result.append(comments)

            return result

        
        # print('parse content begin...')
        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        # print('detail_html: ', detail_html)
        
        url = content['url']
        # print('url: ', url)
        title = detail_html.find('head').find('title').text
        title = ' '.join(title.split())
        # print('title:', title)
        author = parse_author(detail_html)
        # print('source: ', source)
        date = parse_post_date(detail_html)
        # print('date: ', date)
        content = parse_content(detail_html)
        # print('content: ', content)
        like_num = detail_html.find('span','count _like_count').text
        # print('like_num: ', like_num)
        comment_num = detail_html.find('span','count _comment_count').text
        # print('comment_num: ', comment_num)
        update_date = parse_update_date(detail_html)
        # print('update_date:', update_date)
        publisher = parse_publisher(detail_html)

        category = parse_category(detail_html)

        metadata = {
            'like_count': like_num ,
            'reply_count': comment_num,
            'update_time': update_date,
            'news_source': publisher,
            'category': category
        }
        comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
