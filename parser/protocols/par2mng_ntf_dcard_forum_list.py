#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_DCARD_FORUM_LIST(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_DCARD_FORUM_LIST, self).__init__('PAR2MNG_NTF_DCARD_FORUM_LIST')
        self.update({
          'urls': [],
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')
