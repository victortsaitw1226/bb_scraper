#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime
import time


class PRNASIA:
    def __init__(self, server):
        print('init prnasia.py')
        self.server = server

    def on_CRW2PAR_NTF_PRNASIA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_prnasia_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_PRNASIA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        
        item = self.parse_prnasia_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_prnasia_list(self, content):
        
        soup = BeautifulSoup(content, 'html.parser')
        # Get Latest Post Date
        timestamp = soup.select('.presscolumn span')[0].text
        datetime = time.strptime(timestamp, '%Y-%m-%d %H:%M')
        latest_post_date =  time.strftime('%Y-%m-%dT%H:%M:%S+0800', datetime)
        
        # Get Articles url
        links = []
        for a in soup.select('.presscolumn h3 a'):
            links.append({
                'url': 'https://hk.prnasia.com/' + a['href'],
                'metadata': {}
            })
        
        # Get Next Page
        current_page = int(soup.select('.pagination strong')[0].text)   
        last_page = int(soup.select('.pagination a')[-1].text)
        if current_page != last_page:
            next_page = 'https://hk.prnasia.com/story/industry/n-2-' + str(current_page*50) + '.shtml'

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_prnasia_content(self, content):
        
        def parse_datetime(soup):
            timestamp = soup.select('#header-message span')[0].text
            datetime = time.strptime(timestamp, '%Y-%m-%d %H:%M')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800', datetime)
        
        # def parse_author(soup): 
        #     return None

        def parse_title(soup):
            return soup.select('#contenttitle')[0].text

        def parse_content(soup):
            paragraph = []
            for p in  soup.select('#dvContent p'):
                paragraph.append(p.text) 
            content = '\n'.join(paragraph)
            return content

        def parse_metadata(soup):
            source = soup.select('#dvSource')[0].text.split(' ')[1]
                                
            keywords = []                    
            for a in soup.select('.sub-block.highlight-block a'):
                word = re.sub(r'[^\w]', ' ', a.text)
                word = word.split(' ')
                keywords.extend([i for i in word if len(i)])
            
            category = soup.find('div',{'style':'padding:20px 0 0 0;'}).find_all('a')[-1].text

            return {'original_source': source, 'tag': keywords,'category':category}
        
        # domain_prefix = 'https://hk.prnasia.com/story/industry/n-2-0.shtml'
        # soup = BeautifulSoup(content['default_html'], 'html.parser')
        soup = BeautifulSoup(content.get('html'), 'html.parser')

        # detail
        url = content.get('url')
        title = parse_title(soup)
        #author = None
        date = parse_datetime(soup)
        content = parse_content(soup)
        comment = []
        metadata = parse_metadata(soup)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        #item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
