#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import requests
import traceback, sys
from bs4 import BeautifulSoup
from datetime import datetime

class LIFE:

    def __init__(self, server):        
        print('init life')
        self.server = server
        self.session = requests.session()
    
    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def parse_life_list(self, content):

        def parse_next_page(soup):
            try:
                next_page_postfix = soup.find('a', class_='next-page')['href']
            except:
                return None
            return domain_prefix + '/' + next_page_postfix

        def parse_links(soup):
            links = []
            for element in soup.find_all('dt'):
                if not element.find(target='_blank'):
                    continue
                links.append({
                    'url': domain_prefix + element.a['href'],
                    'metadata': {}
                })
            return links
        
        def parse_latest_post_date(latest_link):
            first_post = BeautifulSoup(requests.get(latest_link).text, 'html.parser')
            post_time = first_post.find('span', class_='d-num')
            post_time = ''.join(post_time.text.split())
            date = datetime.strptime(post_time, '%Y-%m-%d')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

        domain_prefix = 'https://life.tw'
        soup = BeautifulSoup(content, 'html.parser')

        next_page = parse_next_page(soup)
        links = parse_links(soup)
        latest_post_date = parse_latest_post_date(links[0]['url'])

        item = {'next_page':next_page, 'links':links, 'latest_post_date':latest_post_date}

        return item

    def on_MNG2CRW_NTF_LIFE_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        # try:
        html = self.server.browser.goto(url)
        content = self.parse_life_list(html)
        # except:
        #     traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(content)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_LIFE_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            detail_html = self.server.browser.goto(url)
            
            # fb_like_count_element = self.server.browser.find_element_by_xpath('//*[@title="fb:like Facebook Social Plugin"]')
            # fb_like_count_src = self.server.browser.get_attribute( fb_like_count_element, 'src')
            
            # fb_share_count_element = self.server.browser.find_element_by_xpath('//*[@title="fb:share_button Facebook Social Plugin"]')
            # fb_share_count_src = self.server.browser.get_attribute( fb_share_count_element, 'src')

            # fb_comment_element = self.server.browser.find_element_by_xpath('//*[@title="fb:comments Facebook Social Plugin"]')
            # fb_comment_src = self.server.browser.get_attribute( fb_comment_element, 'src')

            self.server.browser.switch_to(self.server.browser.find_element_by_xpath('//iframe[contains(@cellspacing,"0")]'))
            article_html = self.server.browser.html()

            # fb_like_count_html = self.server.browser.goto(fb_like_count_src)
            # fb_share_count_html = self.server.browser.goto(fb_share_count_src)
            
            # self.server.browser.get(fb_comment_src)
            # self.server.browser.fb_scrollDown_click_button()
            # fb_comment_html = self.server.browser.html()

            # html = {'html': detail_html, 'fb_like_count':fb_like_count_html, 'fb_share_count':fb_share_count_html, 
            #      'comment_html':fb_comment_html, 'article_html':article_html, 'url': url}
            html = {'html': detail_html, 'article_html':article_html, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
