#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import time
from utils.parse_fb_comment import parse_comments


class ITHOME:
    def __init__(self, server):
        print('init ithome.py')
        self.server = server

    def on_CRW2PAR_NTF_ITHOME_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ithome_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ITHOME_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ithome_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ithome_list(self, content):
        soup = BeautifulSoup(content, 'html.parser')

        # Get Next Page
        for page in soup.select('.pagination.pagination-centered li a'):
            if page['href']=='#':
                current_page = page.text
        if(len(soup.find_all('li', class_='active last'))):
            last_page = soup.find('li', class_='active last').text
        else:
            last_page = None
        
        if (last_page == None) or (current_page != last_page):
            next_page = 'https://www.ithome.com.tw' + soup.select('.next.last a')[0]['href']

        # Get Latest Post Date
        date = soup.find_all('p',class_='post-at')
        date = time.strptime(date[-1].text, '%Y-%m-%d ')
        latest_post_date = time.strftime('%Y-%m-%dT%H:%M:%S+0800', date)

        # Get Articles url
        links = []
        ## get head line news if exists
        if(len(soup.select('.channel-headline'))):
            url = 'https://www.ithome.com.tw' + soup.select('.channel-headline .title a')[0]['href']
            links = [{'url': url, 'metadata':{'category': None}}]

        for item in soup.select('.item'):
            category =''
            for a in item.select('.category'):
                category+=(a.text)
            category = category.replace(' ', '')
            category = list(category.split('|'))
            url = 'https://www.ithome.com.tw' + item.select('.title a')[0]['href']
            links.append({'url': url, 'metadata':{'category': category}})

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_ithome_content(self, content):
        
        def parse_datetime(soup):
            date = soup.find('span', class_='created').text
            date = time.strptime(date, '%Y-%m-%d')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800', date)

        def parse_author(soup): 
            return soup.find('span', class_='author').text
        
        def parse_title(soup):
            return soup.select('h1')[0].text
        
        def parse_content(soup):
            paragraph = [soup.select('.content-summary')[0].text]
            for p in soup.find_all('div', class_='content'):
                for ele in p.find_all('p', class_=None):
                    paragraph.append(ele.text)
            content = '\n'.join(paragraph)
            return content

        def parse_metadata(soup ,fb_like_soup=None):
            metadata = {'category':'','fb_like_count': ''}
            # fb_like_count = fb_like_soup.find('span',{'id':'u_0_3'}).text
            try:
                metadata['category'] = soup.find('div','category-label').find('a').text
            except:
                pass
            return metadata

        soup = BeautifulSoup(content['html'], 'html.parser')
        # fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        metadata = parse_metadata(soup)
        # comment = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        # item.set_comment(comment)
        item.set_metadata(metadata)
        return item
