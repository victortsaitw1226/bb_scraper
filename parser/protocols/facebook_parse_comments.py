#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class FACEBOOK_PARSE_COMMENTS(Protocol):

    def __init__(self):
        super(FACEBOOK_PARSE_COMMENTS, self).__init__('FACEBOOK_PARSE_COMMENTS')
        self.update({
          'post_url': '',
          'comments_url': '',
          'comments': [],
        })

    def set_post_url(self, url):
        self.set('post_url', url)

    def get_post_url(self):
        return self.get('post_url')

    def set_comments_url(self, url):
        self.set('comments_url', url)

    def get_comments_url(self):
        return self.get('comments_url')

    def set_comments(self, comments):
        self.set('comments', comments)

    def get_comments(self):
        return self.get('comments')

