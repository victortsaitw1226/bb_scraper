#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_FB_POST_INFO(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_FB_POST_INFO, self).__init__('CRW2PAR_NTF_FB_POST_INFO')
        self.update({
          'post_url': '',
          'comments': '',
          'shares': '',
          'post_date': '',
        })

    def set_post_url(self, post_url):
        self.set('post_url', post_url)

    def get_post_url(self):
        return self.get('post_url')

    def set_comments(self, comments):
        self.set('comments', comments)

    def get_comments(self):
        return self.get('comments')

    def set_shares(self, shares):
        self.set('shares', shares)

    def get_shares(self):
        return self.get('shares')

    def set_post_date(self, post_date):
        self.set('post_date', post_date)

    def get_post_date(self):
        return self.get('post_date')
