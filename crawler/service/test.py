#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
from six.moves import urllib

class DCard:


    def __init__(self):
        self.session = requests.Session()

    def get(self, url):
        req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urllib.request.urlopen(req).read().decode('utf-8')
        return response

    def goto(self, sitemap, url):
        sites = sitemap.split('/')
        # Special Case
        if 'post' == sites[1] and 'content' == sites[2]:
            return self.get(url[0]), self.get(url[1])

        return self.get(url)

if __name__ == '__main__':
    d = DCard()
    post = d.goto('dcard/forum/list', 'https://www.dcard.tw/_api/forums')
    print(post)
