#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback
import requests
import json
import sys
import re


class PIXNET:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        raw.encoding = "utf-8"
        return raw.text

    def on_MNG2CRW_NTF_PIXNET_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_PIXNET_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # main content
            content_html = []
            content_html.append(self.goto(url))
            soup = BeautifulSoup(content_html[-1], 'html.parser')
            
            if soup.find('div','page')!=None:
                while soup.find('div','page').find('a','next')!=None:
                    next_url = soup.find('div','page').find('a','next')['href']
                    content_html.append(self.goto(next_url))
                    soup = BeautifulSoup(content_html[-1], 'html.parser')

            # comments
            comment_url = re.split('-',url)[0] +'/comments'
            comment_html = self.goto(comment_url)
            comment_json = json.loads(comment_html)['list']
            
            html = {'html': content_html, 'comment_json':comment_json, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
