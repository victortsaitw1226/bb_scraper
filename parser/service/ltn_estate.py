#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime


class LTN_ESTATE:
    def __init__(self, server):
        print('init ltn_estate.py')
        self.server = server

    def on_CRW2PAR_NTF_LTN_ESTATE_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ltn_estate_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_LTN_ESTATE_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ltn_estate_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ltn_estate_list(self, content):
       
        soup = BeautifulSoup(content, 'lxml')
    
        dic = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        try:          
            next_ = soup.find('div', {'class':'page boxTitle boxText'}).find('a',{'data-desc':"下一頁"})['href']
            if next_[0] == '/':
                dic['next_page'] = 'https:'+next_
            else:
                dic['next_page'] = 'https://'+next_
        except: 
            pass
    
        try:              
            p = soup.find('div', {'class':'container page-name'}).find_all('div')
            for i in range(len(p)):
                try:
                    link = p[i].find('a', {'data-desc':'標'})['href']
                    dic['links'].append({
                            'url': 'https://estate.ltn.com.tw/'+link,
                            'metadata': {}
                    })
                except:
                    pass
        except:
            pass
            raise TypeError("Can't getlist")

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(dic['next_page'])
        item.set_latest_post_date(dic['latest_post_date'])
        item.set_urls(dic['links'])
        return item

    def parse_ltn_estate_content(self, content):
        
        def parse_datetime(soup):
            try:
                return datetime.datetime.strptime(soup.find('span', {'class':'time'}).text, '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                return datetime.datetime.strptime(soup.find('span', {'class':'time'}).text, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
    
        def parse_author(soup):
            author = soup.find('p', {'class':'author'}).text.split('/')[-1].replace('記者', '').replace('\n', '').strip()
            try:
                int(author[0])
                author = ''
            except:
                pass
            return author
        
        def parse_title(soup):
            return soup.find('div', {'class':'container page-name boxTitle'}).find('h1').text    
        
        def parse_content(soup):
            article = soup.find('div', {'itemprop':'articleBody'})
            [a.extract() for a in article.find_all('span')]
            article = article.find_all('p')
            article = ''.join(x.text for x in article)
            return max(article.split('\n'), key=len)
        
        def parse_metadata(soup):
            try:
                return soup.find('div', {'class':'breadcrumb boxTitle'}).find_all('a')[1].text
            except:
                return soup.find('div', {'class':'breadcrumb boxTitle'}).find('a').text

        # detail
        url = content['url']
        
        soup = BeautifulSoup(content['html'], 'lxml')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        [s.extract() for s in soup.find_all('script')]
        
        dic = {
            'datetime': '',
            'author': '',
            'title': '',
            'content': '',
            'metadata': {},
            'comments': [],
        }
        dic['datetime'] = parse_datetime(soup)
        dic['author'] = parse_author(soup)
        dic['title'] = parse_title(soup)
        dic['content'] = parse_content(soup)
        dic['metadata']['category'] = parse_metadata(soup)
        #dic['metadata']['fb_like_count'] = fb_like_count_html.find('span',{'id':'u_0_3'}).text
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(dic['title'])
        item.set_author(dic['author'])
        item.set_date(dic['datetime'])
        item.set_content(dic['content'])
        #item.set_comment(dic['comments'])
        item.set_metadata(dic['metadata'])
        
        return item
