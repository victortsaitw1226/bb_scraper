#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_RESULT(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_RESULT, self).__init__('CRW2PAR_NTF_RESULT')
        self.update({
          'content': '',
          'site': ''
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_site(self, site):
        self.set('site', site)

    def get_site(self):
        return self.get('site')
