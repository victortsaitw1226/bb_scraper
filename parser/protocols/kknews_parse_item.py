#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class KKNEWS_PARSE_ITEM(Protocol):

    def __init__(self):
        super(KKNEWS_PARSE_ITEM, self).__init__('KKNEWS_PARSE_ITEM')
        self.update({
            'url': '',
            'article_title': '',
            'author': '',
            'date': '',
            'content': '',
            'metadata':'',
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_title(self ,title):
        self.set('article_title', title)

    def get_title(self):
        return self.get('article_title')

    def set_author(self, author):
        self.set('author', author)

    def get_author(self):
        return self.get('author')

    def set_date(self, date):
        self.set('date', date)

    def get_date(self):
        return self.get('date')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_metadata(self, metadata):
        self.set('metadata', metadata)

    def get_metadata(self):
        return self.get('metadata')
