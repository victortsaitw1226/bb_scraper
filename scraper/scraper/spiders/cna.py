# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class CNASpider(RedisSpider):
    name = "cna"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "cna",
            "name": "cna",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.cna.com.tw/cna2018api/api/simplelist/categorycode/aipl/pageidx/1",
            "scrapy_key": "cna:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        list_json = json.loads(response.body)
        links = self.parse_links(list_json)
        next_page = self.parse_next_page(list_json,response.url)
        latest_datetime = self.parse_latest_post_date(list_json)

        for url in links:
            yield response.follow(url,
                    meta=meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,list_json):
        links = ['/news/aopl/' + h['Id'] + '.aspx' for h in list_json['result']['SimpleItems']]
        return links
    
    def parse_next_page(self,list_json,current_page):
        if list_json['result']['NextPageIdx'] != '':
            next_page =  '/'.join(re.split('/', current_page)[:-1]) + '/' + str(list_json['result']['NextPageIdx'])
        else:
            next_page = None
        return next_page
    
    def parse_latest_post_date(self,list_json):
        link_date = [datetime.strptime(h['CreateTime'], '%Y/%m/%d %H:%M') for h in list_json['result']['SimpleItems']]
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        content, author = self.parse_content_author(soup)
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = content
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'cna'
        item['proto'] = 'CNA_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = datetime.strptime(soup.find("meta", itemprop="dateModified")['content'], '%Y/%m/%d %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        
        return author
    
    def parse_title(self, soup):
        title = soup.find('article')['data-title']
        return title
    
    def parse_content_author(self, soup):
        content = soup.find('div','centralContent').find('div','paragraph')
        for div in content.findAll('div', 'paragraph moreArticle'): 
            div.decompose()
        content = content.text.strip()

        try:
            author_detail = re.findall(r'（(.*?)）', content)[0]
            author = re.search(r'記者(.*?)\d', author_detail).group(1)
            if any([len(l)>3 for l in author.split('、')]):
                author = re.search(r'記者(.*?)\d', author_detail).group(1)[:-2]
        except:
            author = '' 
        return content, author

    def parse_metadata(self, soup):
        title = soup.find('article')['data-title']
        keywords = soup.find("meta", {'name':"keywords"})['content'].split(',')
        keywords = [k.strip() for k in keywords if k!=title]
        keywords = [k.strip() for k in keywords if k not in ['News', '新聞', '即時新聞', '']]
        category = soup.find("meta", property="og:title")['content'].split(' | ')[1:-1]    
        category = category[0]
        metadata = {'tag': keywords, 'category': category, 'fblike':''}
        return metadata
