# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class FountmediaSpider(RedisSpider):
    name = "fountmedia"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "fountmedia",
            "name": "fountmedia",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.fountmedia.io/news?pageNum=0",
            "scrapy_key": "fountmedia:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        
        news_body =  soup.find('div',class_ = 'left cell large-8')
        post_entries = news_body.find_all('article', class_ = 'cell')
        posts_date = []
        for post in post_entries:
            url = post.find('a').get('href')
            if 'article' not in url:
                continue
            post_date = post.find('div', class_='time').get_text()
            posts_date.append(datetime.strptime(post_date, '%Y.%m.%d'))
            yield response.follow(url,
                meta=meta,
                callback=self.parse_article)

        if not posts_date:
            return

        latest_datetime = max(posts_date)
        print(latest_datetime)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("pageNum=(\d+)", response.url).group(1)
        next_page = re.sub("pageNum=(\d+)", "pageNum={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    
    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'fountmedia'
        item['proto'] = 'FOUNTMEDIA_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date_org = soup.find(class_ = 'cell small-4').text.split()[0] + ' ' + \
                        soup.find(class_ = 'cell auto time').text        
        date = datetime.strptime(date_org, '%Y.%m.%d %H:%M%p').strftime('%Y-%m-%dT%H:%M:%S+0800')          
        return date
    
    def parse_title(self,soup):
        return soup.find('div', class_ = 'cell auto text-justify title').text
    
    def parse_author(self,soup):
        author = soup.find(class_ = 'cell large-5 text-left author').text.split()[-1]
        return author
    
    def parse_content(self, soup):
        intro = soup.find(class_ = 'introduction').text
        content = soup.find(class_ = 'content').text
        content = ''.join([ent for ent in content.split()])
        
        reg = re.compile(r'[【(（]\D*圖\D*[】)）]', re.VERBOSE)
        tmp = reg.findall(content)  
        try:
            content = content.replace(tmp[0], '')        
        except:
            content = content
        return intro + ' ' + content
    
    def parse_metadata(self, soup):
        try: 
            category = soup.find(class_ = 'cell large-3 cat').text.split()
            category = category[-1]
        except AttributeError:
            category = ''

        try: 
            keywords = soup.find(class_ = 'tag').text.split()
        except AttributeError:
            keywords = []
        
        return {
            'category': category,
            'tag': keywords
        }
