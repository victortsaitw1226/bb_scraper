# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class HousefunSpider(RedisSpider):
    name = "housefun"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "housefun",
            "name": "housefun",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600 * 3,
            "url": "https://news.housefun.com.tw/News/GetNewsList?page=1&pagesize=20",
            "scrapy_key": "housefun:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        article_list = json.loads(response.body.decode('utf-8', 'ignore'))
        post_datetimes = []
        for item in article_list.get('response', []):
            url = item['link']
            post_datetimes.append(datetime.strptime(item['publicdate'], '%Y/%m/%d %H:%M'))
            yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        if len(post_datetimes) == 0:
            return

        latest_datetime = max(post_datetimes)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)",
            "page={}".format(str(int(current_page) + 1)), response.url)
        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'housefun'
        item['proto'] =  'HOUSEFUN_PARSE_ITEM'
        return item

    
    def parse_datetime(self, soup):
        date = datetime.strptime(soup.find("meta", {'name':"pubdate"})['content'], '%Y/%m/%d %H:%M:%S')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        auth = ''.join(soup.find('div','section-body').find('div').text.split())
        auth = auth.replace('╱','／') 
        if auth.find('記者')!=-1:
            author = re.search(r'記者(.*?)／', auth).group(1)
        elif auth.find('編輯')!=-1:
            author = re.search(r'編輯(.*?)／', auth).group(1)
        else:
            try:
                author = re.search(r'(.*?)／', auth).group(1)
            except:
                author = auth
        return author
    
    def parse_title(self, soup):
        title = soup.find("meta",  property="og:title")['content'].strip().replace(u'\u3000',u' ')
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','section-body')
        for div in content.findAll('a', target='_blank'): 
            div.decompose()
        for div in content.findAll('strong'): 
            div.decompose()
        content = ' '.join(content.text.split()).replace('\t','').replace('╱','／')
        return content

    def parse_metadata(self, soup):
        keywords = soup.find("meta", {'name':"news_keywords"})['content'].split(',')
        category = soup.find('meta',{'property':'article:section'})['content']
        view = soup.find('span','helpnum num').text
        share = soup.find('span','count').text
        try:
            source = soup.find('div','submitted').find('img')['alt']
        except:
            source = None
        metadata = {'tag': keywords, 'category':category,'view_count': view, 'share_count': share, 
                    'news_source':source, 'fb_like_count':''} 
        return metadata
