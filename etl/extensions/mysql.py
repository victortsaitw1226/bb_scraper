#!/usr/bin/env python
# -*- coding: utf-8 -*-
import redis
import string
import random
import pymysql
from nameko.extensions import DependencyProvider
from bson.objectid import ObjectId

class MySQL(DependencyProvider):
    class __MySQLAgent:
        def __init__(self, host, database, user, password):
            print('mysql host:{}, db: {}, user: {}, password: {}'.format(host, database, user, password))
            self.db = pymysql.connect(host, db=database, user=user, passwd=password, charset='utf8mb4')
            results = self.select_one("SELECT VERSION()")
            print(results)

        def insert(self, query):
            try:
                with self.db.cursor() as cursor:
                    cursor.execute(query)
                self.db.commit()
            except:
                self.db.rollback()
                raise

        def update(self, query):
            try:
                with self.db.cursor() as cursor:
                    cursor.execute(query)
                self.db.commit()
            except:
                self.db.rollback()
                raise

        def select_all(self, query):
            try:
                results = []
                with self.db.cursor(pymysql.cursors.DictCursor) as cursor:
                    cursor.execute(query)
                    results = cursor.fetchall()
                self.db.commit()
                return results
            except:
                raise

        def select_one(self, query):
            try:
                results = {}
                with self.db.cursor(pymysql.cursors.DictCursor) as cursor:
                    cursor.execute(query)
                    result = cursor.fetchone()
                self.db.commit()
                return result
            except:
                raise


    def __init__(self, **kwargs):
        self.kwargs=kwargs

    def setup(self):
        self.host = self.container.config['MYSQL_HOST']
        self.db = self.container.config['MYSQL_DB']
        self.user = self.container.config.get('MYSQL_USER')
        self.password = self.container.config.get('MYSQL_PASSWORD')
        self.instance = MySQL.__MySQLAgent(self.host, self.db, self.user, self.password)


    def stop(self):
        pass

    def kill(self):
        pass

    def get_dependency(self, worker_ctx):
        return self.instance
