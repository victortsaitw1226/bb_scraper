#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_NEWS_GET_POST(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_NEWS_GET_POST, self).__init__('MNG2CRW_NTF_NEWS_GET_POST')
        self.update({
          'url': '',
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_media(self, media):
        self.set('media', media)

    def get_media(self):
        return self.get('media')