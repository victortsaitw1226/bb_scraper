# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class FTSpider(RedisSpider):
#class FTSpider(scrapy.Spider):
    name = "ft"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ft",
            "name": "ft",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://big5.ftchinese.com/channel/world.html?page=1",
            "scrapy_key": "ft:start_urls",
            "page": 1,
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        
        # the end of news page
        links = self.parse_links(soup)
        if not links:
            return

        latest_datetime = self.parse_latest_post_date(links[0])

        for url in links:
            full_content_url = url.split('?')[0]+'?adchannelID=&full=y&archive'
            yield response.follow(full_content_url,
                    meta={
                        'url': url
                    },
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        if re.search("p=(\d+)", response.url):
            current_page = re.search("p=(\d+)", response.url).group(1)
            next_page = re.sub("p=(\d+)", "p={}".format(int(current_page) + 1), response.url)
        else:
            current_page = re.search("page=(\d+)", response.url).group(1)
            next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)
    

    def parse_links(self,soup):
        # find exact match 'item-headline-link' instead of 'item-headline-link locked'
        # to filter premium only (locked) articles
        article_links = soup.find_all('a','item-headline-link')
        # article_links = [ li for li in article_links if 'locked' not in str(li)]
        links = [li['href'] for li in article_links]
        return links


    def parse_latest_post_date(self,first_link):
        first_link = 'http://big5.ftchinese.com' + first_link + '&archive'
        html = requests.get(first_link).text
        soup = BeautifulSoup(html, 'html.parser')
        time = soup.find('span', class_='story-time')
        date = datetime.strptime(time.text, '更新於%Y年%m月%d日 %H:%M')
        return date

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        # main content
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,meta['url'])
        item['content_type'] = 0
        item['media'] = 'ft'
        item['proto'] = 'FT_PARSE_ITEM'
        yield item

        # comments
        all_comments = soup.find_all('div', class_='commentcontainer')

        for element in all_comments:
            dt = ''.join(element.span.text.split())
            if '今天' in dt:
                dt = datetime.strptime(dt, '今天%H:%M')
                dt = datetime.combine(datetime.today(), dt.time())
            elif '昨天' in dt:
                dt = datetime.strptime(dt, '昨天%H:%M')
                dt = datetime.combine(
                    datetime.today() - timedelta(days=1), dt.time())
            elif '前天' in dt:
                dt = datetime.strptime(dt, '前天%H:%M')
                dt = datetime.combine(
                    datetime.today() - timedelta(days=2), dt.time())
            else:
                dt = datetime.strptime(dt, '%m-%d%H:%M')
                dt = dt.replace(year=datetime.today().year)
                
            main_metadata = self.parse_metadata(soup)
            reply_metadata = {'category':main_metadata['category'],
                'tag':main_metadata['tag'],
                'from_area':''.join(element.font.text.split('来自')[1])}

            item = NewsItem()
            item['url'] = response.url
            item['metadata'] = reply_metadata
            item['article_title'] = self.parse_title(soup)
            item['author'] = ''.join(element.b.text.split())
            item['author_url'] = []
            item['date'] = dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = ''.join(element.dd.text.split()).replace('null', '')
            item['content_type'] = 1
            item['media'] = 'ft'
            item['proto'] = 'FT_PARSE_ITEM'
            item['comment'] = []
            yield item
            

    def parse_datetime(self,soup):
        time = soup.find('span', class_='story-time')
        date = datetime.strptime(time.text, '更新於%Y年%m月%d日 %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup):
        authors = []
        result = soup.find('span', class_='story-author').find_all('a')
        if result != []:
            for auth in result:
                authors.append(''.join(auth.text.split()))
                author = authors[0]
        else:
            author = ''
        return author

    def parse_title(self,soup):
        return soup.find('h1', class_='story-headline').text

    def parse_content(self,soup):
        content = []
        content = soup.find('div', id='story-body-container')
        content = ''.join(content.text.split())
        return content

    def parse_metadata(self,soup,url):
        # tags
        tags = []
        tag_content = soup.find_all(class_='story-theme')
        for tag in tag_content[1:]:
            tags.append(''.join(tag.a.text.split()))
        # category   
        category = url.split('=')[-1]
        metadata = {
            'tag': tags,
            'category':category
        }
        return metadata

