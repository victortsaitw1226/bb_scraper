#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class DCARD_PARSE_LIST(Protocol):

    def __init__(self):
        super(DCARD_PARSE_LIST, self).__init__('DCARD_PARSE_LIST')
        self.update({
          'urls': [],
          'comment_urls': [],
          'next_page_url': '',
          'latest_post_date': '',
          'forum': ''
        })

    def set_forum(self, forum):
        self.set('forum', forum)

    def get_forum(self):
        return self.get('forum')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')

    def set_comment_urls(self, urls):
        self.set('comment_urls', urls)

    def get_comment_urls(self):
        return self.get('comment_urls')

    def set_next_page_url(self, url):
        self.set('next_page_url', url)

    def get_next_page_url(self):
        return self.get('next_page_url')

    def set_latest_post_date(self, date):
        self.set('latest_post_date', date)

    def get_latest_post_date(self):
        return self.get('latest_post_date')
