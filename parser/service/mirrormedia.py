#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import json


class MIRRORMEDIA:
    def __init__(self, server):
        print('init mirrormedia.py')
        self.server = server

    def on_CRW2PAR_NTF_MIRRORMEDIA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_mirrormedia_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_MIRRORMEDIA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_mirrormedia_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_mirrormedia_list(self, content):

        def parse_json(html):
            res = json.loads(html)
            try:
                url = res["_links"]["last"]["href"]
                re_list = re.search(r'page=[0-9]+', url).group().split("=")
                max_page = int(re_list[1])
            except:
                max_page = 1
            return res, max_page

        def handle_next_page(url, max_page):
            re_list = re.search(r'page=[0-9]+', url).group().split("=")
            cur_page_num = int(re_list[1])
            if cur_page_num < max_page:
                re_list[1] = str(cur_page_num + 1)
                next = "=".join(re_list)
                next_page = url.replace(
                    re.search('page=[0-9]', url).group(), next)
            else:
                next_page = None
            return next_page

        res, max_page = parse_json(content['html'])
        
        items = res["_items"]
        links = []
        datetimes = []
        html_base = "https://www.mirrormedia.mg/story/"
        for item in items:
            links.append({'url': html_base + item["slug"], 'metadata': {}})
            dt = datetime.strptime(item["publishedDate"],
                                '%a, %d %b %Y %H:%M:%S %Z')
            datetimes.append(dt)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(handle_next_page(content['url'], max_page))
        item.set_latest_post_date(max(datetimes).strftime(
            '%Y-%m-%dT%H:%M:%S+0800'))
        item.set_urls(links)

        return item

    def parse_mirrormedia_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find("div", "date").text
            dt = datetime.strptime(time_, '%Y.%m.%d %H:%M')
            dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            try:
                author = soup.select(".article__credit")[0].text.strip().replace(
                    u'\u3000', u' ').replace(u'\xa0', u' ')
                writer = re.search(".[\u4e00-\u9fa5]+", author).group()[1:]
            except:
                writer = ''
            return writer

        def parse_title(soup):
            return soup.h1.text.strip().replace(u'\u3000',
                                                u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            content = ""
            for txt in soup.findAll("div",
                                    {"class": "plate-vue-lazy-item-wrapper"}):
                if not txt.find("section", "relateds-in-content"):
                    content += txt.text
            return content.strip().replace(u'\u3000', u' ').replace(u'\xa0', u' ')

        def parse_metadata(soup):
            key = []
            updated_time = soup.find('p', 'updated-time').text
            updated_time = re.search(r"\d+\.\d+\.\d+ \d+.\d+",
                                    updated_time).group()
            dt = datetime.strptime(updated_time, '%Y.%m.%d %H:%M')
            updated_time = dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            category = soup.find('meta',{'property':'article:section'})['content']
            try:
                for world in soup.find("div", "tags"):
                    key.append(world.text)
            except:
                pass
            
            return {
                'brief': soup.find("div", "brief fb-quotable").text,
                'tag': key,
                "update_time": updated_time,
                'category':category}


        soup = BeautifulSoup(content['html'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        metadata = parse_metadata(soup)

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        #item.set_comment(parse_comments(comment_html))
        item.set_metadata(metadata)
        return item
