#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class TSSD:
    proxy = None

    def __init__(self, server):
        print('init tssd.py')
        self.server = server


    def on_CRW2PAR_NTF_TSSD_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_tssd_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TSSD_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_tssd_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_tssd_list(self, content):

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(content['next_page'])
        item.set_latest_post_date(content['latest_post_date'])
        item.set_urls(content['links'])

        return item

    def parse_tssd_content(self, content):
        
        def parse_datetime(soup):
            reg_time = re.compile(r'\d{4}/\d{2}/\d{2}', re.VERBOSE)
            post_time = soup.find('div', id='news_author').text
            time_ = reg_time.findall(post_time)
            try:
                dt = datetime.strptime(time_[0], '%Y/%m/%d')
                return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                return None

        def parse_author(soup):
            author_block = soup.find('div', id='news_author').text
            if len(author_block) > 20:
                authors = author_block.split('／')[0]
                authors2 = author_block.split('/')[0]
                if len(authors) > len(authors2):
                    authors = authors2[3:]
                else:
                    authors = authors[3:]
            else:
                authors = ''
            return authors

        def parse_title(soup):
            board = soup.find('div', id='news_title')
            return board.get_text(strip=True)

        def parse_content(soup):            
            for script in soup.find_all('script'):
                script.extract()
            contents = soup.find('div', class_='idx3').get_text(strip=True)
            return contents

        def parse_metadata(soup):
            metadata = {
                'category': ''
            }
            entry_crumb = soup.find('div', id='navbar').find_all('a')
            entry = [x.get_text() for x in entry_crumb]
            entry = list(dict.fromkeys(entry))
            metadata['category'] = entry[-1]
            return metadata

        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        return item
