#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2ETL_NTF_NEW_DATA(Protocol):

    def __init__(self):
        super(PAR2ETL_NTF_NEW_DATA, self).__init__('PAR2ETL_NTF_NEW_DATA')
        self.update({
          'url': ''
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')
