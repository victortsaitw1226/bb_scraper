# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class UrcosmeSpider(RedisSpider):
#class UrcosmeSpider(scrapy.Spider):
    name = "urcosme"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "urcosme",
            "name": "urcosme",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.urcosme.com/beautynews?page=1",
            "scrapy_key": "urcosme:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        links = self.parse_links(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        if 0 == len(links):
            return

        for url in links:
            yield response.follow(url['url'],
                    meta=url['metadata'],
                    callback=self.parse_article)


        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return


        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)
        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = soup.findAll('div', 'uc-news-item')
        links_view = soup.findAll('div', 'uc-news-time')
        links = [{
            'url': i.find('a')['href'],
            'metadata':{
                'views': j.text.split(' / 人氣')[1]
            }
        } for i, j in zip(links, links_view)]
        return links
    
    def parse_latest_post_date(self,soup):
        link_date = [datetime.strptime(s.text.split(' / ')[0], '%Y.%m.%d') for s in soup.findAll('div','uc-news-time')]
        latest_post_date = max(link_date)
        return latest_post_date
    
    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,meta)
        item['content_type'] = 0
        item['media'] = 'urcosme'
        item['proto'] = 'URCOSME_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = datetime.strptime(soup.find("meta",  {'name':'title'})['content'].split(' | ')[-1], '%Y年%m月%d日')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find("meta",  {'name':'title'})['content'].split(' | ')[1]
        return author
    
    def parse_title(self, soup):
        title = soup.find("meta",  property="og:title")['content']
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','content-desc')
        for div in content.findAll('em'): 
            div.decompose()
        for div in content.findAll('div','buttons'): 
            div.decompose()
        content = content.text.replace('\uf059','')
        content = ''.join(content.split())
        return content

    def parse_metadata(self, soup, meta):
        keywords = [s.text for s in soup.find('div','uc-news-tag').findAll('a')]
        try:
            keywords.extend(soup.find('div','uc-news-tag-list').text[1:].split('#'))
        except:
            pass           
        # brand = soup.find("meta",  {'name':'title'})['content'].split(' | ')[2].replace('的美妝報導','')
        category = soup.find('div','uc-crumbs').find_all('span',{'itemprop':'name'})[1].text
        metadata = {
            'tag': keywords,
            'category': category,
            'view_count': ''.join(meta['views'].split())
        }
        return metadata
    
