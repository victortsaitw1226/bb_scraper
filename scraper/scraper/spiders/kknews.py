# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class KknewsSpider(RedisSpider):
#class KknewsSpider(scrapy.Spider):
    name = "kknews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "kknews",
            "name": "kknews",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 7200,
            "url": "https://kknews.cc/astrology/?page=1",
            "scrapy_key": "kknews:start_urls",
            "priority": 1,
            #'handle_httpstatus_list': [302]
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        posts_date = []
        for article in soup.find_all('article', 'post-254693 post type-post status-publish format-standard has-post-thumbnail hentry category-6'):
           url = article.find('a').get('href')
           pub_date = article.find('p', class_='meta').get_text()

           posts_date.append(
               datetime.strptime(re.split(' | ',pub_date)[-1], '%Y-%m-%d')
           )
           yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        if len(posts_date) == 0:
            return

        latest_datetime = max(posts_date)

        print(latest_datetime)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield response.follow(next_page,
                meta=meta,
                dont_filter=True,
                callback=self.parse)


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'kknews'
        item['proto'] =  'KKNEWS_PARSE_ITEM'
        return item

    def parse_title(self, soup):
        return soup.find('h1','entry-title p-name').text

    def parse_author(self, soup):
        author = soup.find('p','meta post-meta').find('em').text
        author = author.split()[1]
        return author

    def parse_datetime(self, soup):
        date = soup.find('p','meta post-meta').find('a','updated').text
        date = datetime.strptime( date , '%Y-%m-%d')
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_content(self, soup):
        content = soup.find('div','basic')
        for img in content.find_all('figure'):
            img.decompose()
        for ad in content.find_all('div','axslot lrct_inject gemini'):
            ad.decompose()
        for ad in content.find_all('div','axslot lrct_inject axsense'):
            ad.decompose()   
        content = content.text.strip()
        content = ' '.join(content.split())
        return content

    def parse_metadata(self, soup):
        metadata = {'category':''}
        try:
            metadata['category'] = soup.find('a',{'rel':'category tag'}).text
        except:
            pass
        return metadata
