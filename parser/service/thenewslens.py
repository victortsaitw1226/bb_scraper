#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime
from utils.parse_fb_comment import parse_comments

class THENEWSLENS:
    def __init__(self, server):
        print('init thenewslens.py')
        self.server = server

    def on_CRW2PAR_NTF_THENEWSLENS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_thenewslens_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_THENEWSLENS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_thenewslens_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_thenewslens_list(self, content):

        soup = BeautifulSoup(content, 'lxml')   
        dic = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        try:          
            dic['next_page'] = soup.find('ul', {'class':'pagination'}).find('a',{'rel':"next"})['href']
        except: 
            pass
    
        try:              
            p = soup.find('article', {'itemprop':'articleBody'}).find_all('div', {'class':'list-container'})
            for i in range(len(p)):
                try:
                    link = p[i].find('div', {'class':'img-box'}).find('a')['href']
                    if 'article' in link:
                        dic['links'].append({
                                'url': link,
                                'metadata': {}
                        })
                except:
                    pass
        except:
            pass
        
        try:
            dic['latest_post_date'] = datetime.datetime.strptime(soup.find('span', {'class':'time'}).text.replace('|', '').strip(), '%Y/%m/%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            pass

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(dic['next_page'])
        item.set_latest_post_date(dic['latest_post_date'])
        item.set_urls(dic['links'])
        return item

    def parse_thenewslens_content(self, content):
        
        def parse_datetime(soup):
            return datetime.datetime.strptime(soup.find('div', {'class':'article-info'}).text.split(',')[0].replace('SPONSORED', '').strip(), '%Y/%m/%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            au = soup.find('div', {'class':'article-author'}).find_all('a')[1].text.strip()
            if not au:
                au = 'SPONSORED'
            return au
        
        def parse_title(soup):
            return soup.find('h1', {'class':'article-title'}).text.strip()    
        
        def parse_content(soup):
            return soup.find('article', {'itemprop':'articleBody'}).text.replace('\n', '').strip()
        
        def parse_metadata(soup):
            cat = soup.find('div', {'class':'article-info'}).text.split(',')[1].strip()
            tag = soup.find('ul', {'class':'tags'}).text.strip().split(' ')
            try:
                sh = soup.find('li', {'class':'share-count'}).text.strip()
            except:
                sh = ''
            return {'category': cat,
                    'tag': tag,
                    'share_count': sh
                    }
   

        # detail
        url = content['url']              
        soup = BeautifulSoup(content['html'], 'lxml')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        [s.extract() for s in soup.find_all('script')]
        
        dic = {
            'datetime': '',
            'author': '',
            'title': '',
            'content': '',
            'metadata': {},
            'comments': [],
        }
        dic['datetime'] = parse_datetime(soup)
        dic['author'] = parse_author(soup)
        dic['title'] = parse_title(soup)
        dic['content'] = parse_content(soup)
        dic['metadata'] = parse_metadata(soup)
        dic['comments'] = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(dic['title'])
        item.set_author(dic['author'])
        item.set_date(dic['datetime'])
        item.set_content(dic['content'])
        item.set_comment(dic['comments'])
        item.set_metadata(dic['metadata'])
        
        return item
