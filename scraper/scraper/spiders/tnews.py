# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

#class TnewsSpider(scrapy.Spider):
class TnewsSpider(RedisSpider):
    name = "tnews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "tnews",
            "name": "tnews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://tnews.cc/02/OtherArea.asp?MainArea=judge",
            "scrapy_key": "tnews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.text, 'lxml')
        subdomain = {
                '基隆新聞網': '024',
                '台北市新聞網': '02',
                '新北市新聞': '022',
                '桃園新聞網': '03',
                '機場新聞網': 'cks',
                '新竹新聞網': '035',
                '苗栗新聞網': '037',
                '台中新聞網': '04',
                '彰化新聞網': '047',
                '南投新聞網': '049',
                '雲林新聞網': '055',
                '嘉義新聞網': '05',
                '台南新聞網': '06',
                '澎湖新聞網': '069',
                '高雄新聞網': '07',
                '屏東新聞網': '08',
                '宜蘭新聞網': '039',
                '花蓮新聞網': '038',
                '台東新聞網': '089',
                '金門新聞網': '0823',
                '馬祖新聞網': '0836'
        }
        domain_name = soup.find('head').find_all('title')[2].text.split('-')[0].replace('(北市新聞網)', '')
        domain = subdomain[domain_name]

        links = self.parse_links(soup)
        if links ==[]:
            return

        for url in links:
            yield response.follow(url['url'],
                    meta = {
                        'domain': domain_name
                    },
                    callback=self.parse_article)

        next_page = self.parse_next_page(soup,response.text)
        if next_page==None:
            return

        latest_datetime = self.parse_latest_post_date(links[0]['url'],domain)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = []
        try:              
            p = soup.find_all('table', {'width':'445'})[1].find_all('a', {'target':'_top'})
            for i in range(len(p)):
                link = p[i]['href']
                links.append({
                            'url': link,
                            'metadata': {}
                            })
        except:
            pass
        return links
    
    def parse_next_page(self,soup,html):
        try:         
            next_page = soup.find('div', {'align':'right'}).find_all('a', {'class':'menulist-m'})[-1]['href']
        except: 
            next_page = None
        return next_page
    
    def parse_latest_post_date(self,first_link,domain):
        first_link = 'https://tnews.cc/'+ domain + '/' + first_link
        html = requests.get(first_link).text
        soup = BeautifulSoup(html, 'html.parser')
        date = soup.find('font',{'color':'dimgray'}).text
        date = date.split('\u3000')[0].split('發稿日：')[-1]
        date = date.replace('下午','PM').replace('上午','AM')
        date = datetime.strptime(date,'%Y/%m/%d %p %I:%M:%S')
        return date
    
    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.text, 'html.parser')
        if '此篇文章僅限「會員」閱讀' in soup.text:
            return
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,meta)
        item['content_type'] = 0
        item['media'] = 'tnews'
        item['proto'] = 'TNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        date = soup.find('font',{'color':'dimgray'}).text
        date = date.split('\u3000')[0].split('發稿日：')[-1]
        date = date.replace('下午','PM').replace('上午','AM')
        date = datetime.strptime(date,'%Y/%m/%d %p %I:%M:%S')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup):
        au = soup.find_all('td', {'valign':'top'})[2].text.replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')
        try:
            ind = au[0:20].index('記者')
            au = au[ind+2:ind+5]
        except:
            menulist = soup.find('font', {'color':'dimgray','class':'menulist-m'})
            au = menulist.find('a','menulist-m')['href'].split('from=')[-1]
        return au

    def parse_title(self,soup):
        title = soup.find('font', {'color':'black'}).text
        title = ' '.join(title.split())
        return title

    def parse_content(self,soup):
        for tags in soup.find_all('script',{'type':'text/javascript'}):
            tags.clear()
        content = soup.find_all('td', {'valign':'top'})[2].text
        content = ''.join(content.split())
        return content

    def parse_metadata(self,soup,meta):  
        tmp = soup.find('font', {'color':'dimgray'}).text.split('\u3000')
        post = tmp[5].split('：')[1]
        
        try:
            value = tmp[11].split('：')[1].split('\xa0')[0]
        except:
            value = ''

        category = meta['domain']

        return {'post': post,
                'value': value,
                'category':category
                }
    
