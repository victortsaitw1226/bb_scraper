#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
import requests

class UDN:

    def __init__(self, server):        
        print('init udn')
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def on_MNG2CRW_NTF_UDN_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = {'list_html':self.goto(url), 'current_page':url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_UDN_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # detail_html = UDN.browser.goto(url)
            
            # fb_like_count_element = UDN.browser.find_element_by_xpath('//*[@title="fb:like Facebook Social Plugin"]')
            # fb_like_count_src = UDN.browser.get_attribute( fb_like_count_element, 'src')
            
            # fb_comment_element = UDN.browser.find_element_by_xpath('//*[@title="fb:comments Facebook Social Plugin"]')
            # fb_comment_src = UDN.browser.get_attribute( fb_comment_element, 'src')

            # fb_like_count_html = UDN.browser.goto(fb_like_count_src)
            
            # UDN.browser.get(fb_comment_src)
            # UDN.browser.fb_scrollDown_click_button()
            # fb_comment_html = UDN.browser.html()

            # html = {'html': detail_html, 'fb_like_count':fb_like_count_html,'comment_html':fb_comment_html,
            #          'url': url}
            html = {'html': self.goto(url), 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
