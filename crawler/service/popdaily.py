#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback
import requests
import sys
import re
from bs4 import BeautifulSoup


class POPDAILY:

    def __init__(self, server):
        print('init POPDAILY')
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def send_xhr(self, url_dic):
        url = url_dic['url']
        page = url_dic['page']
        score = url_dic['score']

        board = url.split('/')[-1]
        res = requests.session().post(
                url = 'https://www.popdaily.com.tw/api/list/post',
                data = {
                    'clas': board,
                    'type':'explore',
                    'token':'guest.5564632.8e38305c-37c3-4461-9c8c-7ca247159e0d', #get from cookie
                    'page': page,
                    'score':score,
                },
                headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15',
                    'Referer': 'https://www.popdaily.com.tw/explore/life',
                }
                )
        return {'js':res.text,'url':url}

    def on_MNG2CRW_NTF_POPDAILY_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        # print('url')
        try:
            html = self.send_xhr(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
        
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_POPDAILY_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # detail_html = self.server.browser.goto(url)
            detail_html = requests.get(url).text

            html = {'html': detail_html, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
