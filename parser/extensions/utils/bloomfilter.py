#!/usr/bin/env python
# -*- coding: utf-8 -*-

class BloomFilter(object):
    class __HashMap(object):
        def __init__(self, m, seed):
            self.m = m
            self.seed = seed

        def hash(self, value):
            ret = 0
            for i in range(len(value)):
                ret = ret + self.seed * ret + ord(value[i])
            return (self.m - 1) & ret

    def __init__(self, redis, key='bloomfilters', bit = 30, hash_number=6):
        self.m = 1 << bit
        self.seeds = range(hash_number)
        self.key = key
        self.redis = redis
        self.maps = [BloomFilter.__HashMap(self.m, seed) for seed in self.seeds]

    def exists(self, value):
        if not value:
            return False
        exist = True
        for map in self.maps:
            offset = map.hash(value)
            exist = exist & self.redis.getbit(self.key, offset)
        return exist

    def insert(self, value):
        for f in self.maps:
            offset = f.hash(value)
            self.redis.setbit(self.key, offset, 1)

if __name__ == '__main__':
    import redis
    _redis = redis.Redis(host="10.61.106.193")
    b = BloomFilter(_redis)
    url = 'https://www.facebook.com/story.php?story_fbid=10157180690805631&id=309973010630'
    result = b.exists(url)
    print(result)
    b.insert(url)
