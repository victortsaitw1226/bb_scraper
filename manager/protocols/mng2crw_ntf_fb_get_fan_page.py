#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_FB_GET_FAN_PAGE(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_FB_GET_FAN_PAGE, self).__init__('MNG2CRW_NTF_FB_GET_FAN_PAGE')
        self.update({
          'url': '',
          'fan_page': '',
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_fan_page(self, fan_page):
        self.set('fan_page', fan_page)

    def get_fan_page(self):
        return self.get('fan_page')
