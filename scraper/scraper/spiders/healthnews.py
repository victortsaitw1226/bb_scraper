# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class HealthnewsSpider(RedisSpider):
#class HealthnewsSpider(scrapy.Spider):
    name = "healthnews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "healthnews",
            "name": "healthnews",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "https://www.healthnews.com.tw/news/listing/10",
            "scrapy_key": "healthnews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)


        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        if next_page is None:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self, soup):
        links = []
        for post in soup.find_all('div', class_='caption col-lg-8 col-md-8 col-xs-12 col-sm-6'):
            links.append(post.find('a')['href'])
        return links
    
    def parse_next_page(self, soup):
        try:
            next_page = soup.find('ul', class_='pagination')
            next_page = next_page.find('a', {'rel':'next'})['href']
        except: 
            next_page = None
        return next_page
    
    def parse_latest_post_date(self, soup):
        latest_post_date = soup.find('div', class_='blogicons').text
        latest_post_date = latest_post_date.strip().split(' ')[1]
        latest_post_date = datetime.strptime(latest_post_date, '%Y-%m-%d')
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'healthnews'
        item['proto'] = 'HEALTHNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        _str_date = soup.find_all('span', class_='mr10')[0].text.strip()
        return datetime.strptime(_str_date, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self,soup):
        try:
            au = soup.find_all('span', class_='mr10')[1]
            au = au.text.split('／')[1]
            au = au.replace('記者','').replace('報導','').strip()
        except:
            au = ''
        return au
    
    def parse_title(self,soup):
        return soup.find('span', class_='maintext').text.replace('\n', '').replace('\u3000', '').strip()
    
    def parse_content(self,soup):
        return soup.find('div', {'id':'newsContent'}).text.replace('\n', '').replace('\u3000', '').strip()
    
    def parse_metadata(self,soup):
        try:
            tags = soup.find_all('div', class_='keywordLink')[1].find_all('a')
            tags = [t.text for t in tags]
        except:
            tags = []

        cat = soup.find('ul', class_='breadcrumb').find_all('li')[2].text
        
        return {
            'category': cat,
            'tag': tags,
        }
    
