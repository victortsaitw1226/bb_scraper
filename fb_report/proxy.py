import random

class Proxy(object):

    def __init__(self, mongo, redis):
        self.mongo = mongo
        self.redis = redis

    def random_proxy_from_mongo(self):
        proxies = []
        for proxy in self.mongo.find('proxy'):
            proxies.append(proxy['ip'])
        return random.choice(proxies)

    def random_proxy_from_redis(self):
        proxies = self.redis.smembers('eng_proxies')

        if not proxies:
            return ""

        proxies = list(proxies)
        proxy_index = random.randint(0, len(proxies) - 1)
        ip_port = proxies[proxy_index]
        return str(ip_port, 'utf-8')

    def random_proxy(self):
        return ''
        proxy = self.random_proxy_from_redis()
        if proxy:
            return proxy
        return self.random_proxy_from_mongo()
