#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import requests
from collections import defaultdict
import json, traceback, sys

class App:
    ES_INDEX = 'crawl_new'
    ES_DATA_INDEX = 'data'
    ES_TIMELINE_INDEX = 'youtube_timeline'
    MONGO_DB = 'history_v2'
    MONGO_DATA_COLLECTION = 'data'
    MONGO_LOGS_COLLECTION = 'logs'
    MONGO_ASR_DATA_COLLECTION = 'youtube_data'
    MONGO_URLS_COLLECTION = 'urls'
    MONGO_ASR_TIMELINE_COLLECTION = 'youtube_timeline'
    MONGO_OCR_DATA_COLLECTION = 'paper_data'

    def __init__(self, server):
        self.server = server


    def list_urls(self, media):
        r = {}
        query = {}
        if 'all' != media:
            query = {'media': media}
        cursor = self.server.mongo.find(
           App.MONGO_DB, 
           App.MONGO_URLS_COLLECTION,
           query
        )
        for item in cursor:
            r[item['url']] = {
                'name': item['name'],
                'enabled': item['enabled']
            }
        return r

    def list_locks(self, media):
        r = []
        query = {}
        if 'all' != media:
            query = {'media': media}
        cursor = self.server.mongo.find(
           App.MONGO_DB, 
           App.MONGO_URLS_COLLECTION,
           query
        )
        for item in cursor:
            r.append('routine_{}_lock'.format(item['_id']))
        return r

    def unlock_media(self, media):
        query = {}
        locks = self.list_locks(media)
        for lock in locks:
            self.server.redis.force_unlock(lock)
        return locks

    def reset_data(self):
        r = self.server.mongo.update_many(
            App.MONGO_DB, 
            App.MONGO_LOGS_COLLECTION, 
        {
            'status': 'done'
        }, {
            'status': 'pending'
        })
        r = self.server.mongo.delete_field(
            App.MONGO_DB,
            App.MONGO_OCR_DATA_COLLECTION, 
            {}, 'processed')
        r = self.server.mongo.delete_field(
                App.MONGO_DB, 
                App.MONGO_ASR_DATA_COLLECTION, 
                {}, 'processed')
        return {'result' : 'success'}

    def disable_urls_by_media(self, media):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'media': media},
            {'enabled': False}
        )

    def disable_urls_by_url(self, url):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'url': url},
            {'enabled': False}
        )

    def disable_all_urls(self):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {},
            {'enabled': False}
        )

    def enable_urls_by_media(self, media):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'media': media},
            {'enabled': True}
        )

    def enable_urls_by_url(self, url):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'url': url},
            {'enabled': True}
        )

    def enable_all_urls(self):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {},
            {'enabled': True}
        )

    def change_all_days_limit(self, days_limit):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {},
            {'schedule.$[].days_limit': 86400 * days_limit}
        )

    def change_days_limit_by_media(self, media, days_limit):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'media': media},
            {'schedule.$[].days_limit': 86400 * days_limit}
        )

    def change_days_limit_by_url(self, url, days_limit):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'url': url},
            {'schedule.$[].days_limit': 86400 * days_limit}
        )

    def change_all_interval(self, interval):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {},
            {'schedule.$[].interval': interval}
        )

    def change_interval_by_media(self, media, interval):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'media': media},
            {'schedule.$[].interval': interval}
        )

    def change_interval_by_url(self, url, interval):
        return self.server.mongo.update_many(
            App.MONGO_DB,
            App.MONGO_URLS_COLLECTION,
            {'url': url},
            {'schedule.$[].interval': interval}
        )

    def add_url(self, data):
        return {'result': 'success'}

    def media_bucket_list(self):
        now = datetime.datetime.now()
        media_bucket = defaultdict(list)
        for i in range(7):
            to_check_day = now - datetime.timedelta(days=i)
            from_check_day = to_check_day - datetime.timedelta(days=1)
            str_to_check_day = to_check_day.strftime('%Y-%m-%d')
            str_from_check_day = from_check_day.strftime('%Y-%m-%d')
            bucket = self.server.elasticsearch.media_bucket(
                    App.ES_INDEX, str_from_check_day, str_to_check_day)
            for data in bucket:
                media = data['key']
                count = data['doc_count']
                media_bucket[media].append({
                   'date': str_from_check_day,
                   'count': count
                })
        return media_bucket

    def update_fb_count(self):
        hits = self.server.elasticsearch.find_news_by_latest_hours(App.ES_INDEX, 2)
        for hit in hits:
            url = hit['_source']['docurl']
            try:
                count = self.server.browser.count_fb(url)
                _id = hit['_id']
                data = {
                  "script": {
                    "source": "ctx._source.brocount={}".format(count),
                    "lang": "painless"
                  }
                }
                res = self.server.elasticsearch.update_by_id(App.ES_INDEX, 'data', _id, data)
                self.server.log.info('update fb browser count:{}:{}'.format(_id, count))
            except Exception as e:
                self.server.log.exception(e)
                #traceback.print_exc(file=sys.stdout)

    def data_evaluation(self, date):
        begin = '{}T00:00:00.000Z'.format(date)
        end = '{}T23:59:59.000Z'.format(date)
        hits = self.server.elasticsearch.find_data_in_one_day(begin, end)
            
    def line_add_user(self, user_id, name):
        query = "INSERT INTO ob_line(userid, name, verify, role) VALUES('%s', '%s', 1, 'message')" % \
                  (user_id, name)
        self.server.mysql.insert(query)

    def send_line_msg(self):
        query = "SELECT a.id, a.msg_content, b.userid, b.role " + \
                "FROM peace.ob_push_msg AS a JOIN peace.ob_line AS b " + \
                "ON a.line_userid=b.id AND a.push_status=0 AND b.verify=1"
        datas = self.server.mysql.select_all(query)
        for data in datas:
            _id = data['id']
            user_id = data['userid']
            msg = data['msg_content']
            role = data['role']

            url = 'https://keywordbot.herokuapp.com/line/line_bot/msg'
            if 'notify' == role:
                url = 'https://keywordbot.herokuapp.com/line/notify/msg'
            try:
                requests.post(url, data={
                  'user_id': user_id,
                  'msg': msg
                })
                query = "UPDATE ob_push_msg SET push_status = 1 WHERE id = '%s'" % _id
                self.server.mysql.update(query)
            except:
                pass 
