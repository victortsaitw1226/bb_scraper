# -*- coding: utf-8 -*-

# Define here the models for your scraped items
from scrapy import Item, Field

class NewsItem(Item):
    url = Field()
    article_title = Field()
    author = Field()
    author_url = Field()
    comment = Field()
    date = Field()
    content = Field()
    metadata = Field()
    media = Field()
    content_type = Field()
    proto = Field()
    proto_id = Field()

class PttItem(Item):
    url = Field()
    board = Field()
    title = Field()
    author = Field()
    date = Field()
    content = Field()
    ip = Field()
    message_count = Field()
    messages = Field()

