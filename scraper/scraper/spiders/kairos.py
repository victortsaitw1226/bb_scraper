# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from .redis_spiders import RedisSpider
from scraper.items import NewsItem

class KairosSpider(RedisSpider):
    name = 'kairos'

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/headlines",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        },
        {
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/dare-to-change",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        },
        {
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/entertainment",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        },
        {
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/lifestyle",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        },
        {
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/health",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        },
        {
            "media": "kairos",
            "name": "kairos",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600 * 4,
            "url": "https://kairos.news/world",
            "scrapy_key": "kairos:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, "html.parser")
        pub_date = soup.find_all('span','date meta-item fa-before')

        link_date = []
        for date in pub_date:
            date = datetime.strptime(date.text, '%Y-%m-%d')
            link_date.append(date)
        latest_datetime = max(link_date)


        links = soup.find_all('h3','post-title')
        for li in links:
            url = li.find('a')['href']
            yield scrapy.Request(url,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_article)

        try:
            next_page = soup.find('ul','pages-numbers').find('li','the-next-page')
            next_page = next_page.find('a')['href']
        except:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
            meta=meta,
            dont_filter=True,
            callback=self.parse)


    def parse_article(self, response):
        # detail
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        # share_count = detail_html.find('span','a2a_count').text
        # metadata = {'tag':keywords, 'share_count':share_count}
        metadata = {
            'tag': self.parse_keywords(soup),
            'category': self.parse_category(soup),
            'share_count':''
        }
        #comment = parse_comments(comment_html)

        title = soup.find('head').find('title').text
        title = ' '.join(title.split())

        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = title
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_date(soup) 
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = meta['media']
        item['proto'] =  'SETN_PARSE_ITEM'
        return item

    def parse_date(self, soup):     
        date = soup.find('head').find('meta',{'property':'article:published_time'})['content']
        date = datetime.strptime( date , '%Y-%m-%dT%H:%M:%S+00:00')
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0000')
        return date
    
    def parse_content(self, soup):
        content = soup.find('div','entry-content entry clearfix')
        for ad in content.find_all('div','snippet'):
            ad.decompose()
                
        content = '\n'.join(x.text for x in content.find_all('p'))        
        content = '\n'.join(content.split())
        content = content.replace('\n則留言','')
        return content
    
    def parse_author(self, soup):
        author = soup.find('span','meta-author')
        author = author.find('a','author-name').text.strip()
        return author
    
    def parse_category(self, soup):
        menubar = soup.find('div','main-menu header-menu')
        li = menubar.find_all('li')
        cat = [ l.find('a').text for l in li if 'tie-current-menu' in str(l['class'])]
        return cat[0]
    
    def parse_keywords(self, soup):
        try:
            tag_bar = soup.find('span','post-cat-wrap')
            tags = [t.text for t in tag_bar.find_all('a')]
        except:
            tags = []
        return tags
