#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re, traceback, sys
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class YAHOO:
    def __init__(self, server):
        self.server = server

    def on_CRW2PAR_NTF_YAHOO_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_yahoo_list(content)
        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        # print(str(to_notify))
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_YAHOO_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_yahoo_content(content)
        
        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_yahoo_list(self, content):
        
        html = content['list_html']
        url = content['current_page']
        category = content['category']

        link_date = []
        for i in range(len(html)):
            link_date.append( datetime.fromtimestamp(html[i]['published_at']) )  
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0000')

        links = []
        for i in range(len(html)):
            links.append( {'url':html[i]['url'], 'metadata':{'category':category}} ) 
        
        url = re.split(r'start=(\d*)',url)
        next_page = url[0] + 'start=' + str(int(url[1])+10) + url[2] 
                
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_yahoo_content(self, content):

        def parse_title(soup):
            title = soup.find('meta',{'property':'og:title'})['content']
            return title 
        
        def parse_date(soup):
            date = soup.find('time')['datetime']
            date = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.000Z')
            date = date + timedelta(hours=8)
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_author(soup):
            if soup.find('div','caas-attr-item')!=None:
                if soup.find('div','caas-attr-item').find('img')!=None:
                    source = soup.find('div','caas-attr-item').find('img')['alt']
                else:
                    source = soup.find('div','caas-attr-item').text

            else:
                source = ''
            return source
        
        def parse_publisher(soup):            
            if soup.find('div','caas-attr-meta').find('a').text!='':
                publisher = soup.find('div','caas-attr-meta').find('a').text
            elif soup.find('div','caas-attr-meta').find('a').find('img')!=None:
                publisher = soup.find('div','caas-attr-meta').find('a').find('img')['alt']
            else:
                publisher = ''
            return publisher
        
        def parse_content(soup):
            content = soup.find('div','caas-body')
            try:
                for span_tag in content.findAll('a'):
                    span_tag.replace_with('')
                
                content = content.find_all('p')
                content = ' '.join([x.text for x in content ])
                content = ' '.join(content.split())
                content = re.split(r'更多(.*)新聞',content)[0]
                content = re.split(r'更多(.*)報導',content)[0]
            except:
                content = content.text
            return content
        
        def parse_comments(replies):
            
            def parse_reply_reply(reply_replies):
                reply_replies_list = []
                for rr in reply_replies:
                    rrd = {
                            'comment_id': rr['replyId'],
                            'datetime': datetime.fromtimestamp(rr['meta']['createdAt']).strftime('%Y-%m-%dT%H:%M:%S+0800'),
                            'author':  rr['meta']['author']['nickname'],
                            'author_url': '',
                            'content': rr['details']['userText'],
                            'metadata':{'like_count':rr['reactionStats']['upVoteCount'],
                                        'dislike_count':rr['reactionStats']['downVoteCount']},
                            }
                    reply_replies_list.append(rrd)
                return reply_replies_list
            
            replies_list = []
            for r in replies:
                reply = {
                        'comment_id': r['messageId'],
                        'datetime': datetime.fromtimestamp(r['meta']['createdAt']).strftime('%Y-%m-%dT%H:%M:%S+0800'),
                        'author':  r['meta']['author']['nickname'],
                        'author_url': '',
                        'content': r['details']['userText'],
                        'source': 'yahoo',
                        'metadata':{'like_count':r['reactionStats']['upVoteCount'],
                                    'dislike_count':r['reactionStats']['downVoteCount']},
                        'comments':parse_reply_reply(r['replies_list']) }
                replies_list.append(reply)
            return replies_list


        detail_html = BeautifulSoup(content['html'], 'html.parser')
        comments_json = content['comments_json']
        
        medata = {'news_source':parse_publisher(detail_html),
                  'category':content['url']['metadata']['category']}

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url']['url'])
        item.set_title(parse_title(detail_html))
        item.set_author(parse_author(detail_html))
        item.set_date(parse_date(detail_html))
        item.set_content(parse_content(detail_html))
        item.set_comment(parse_comments(comments_json))
        item.set_metadata(medata)
        return item
