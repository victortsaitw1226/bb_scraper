#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime
from utils.parse_fb_comment import parse_comments

class EJINSIGHT:
    def __init__(self, server):
        print('init ejinsight.py')
        self.server = server

    def on_CRW2PAR_NTF_EJINSIGHT_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ejinsight_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_EJINSIGHT_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ejinsight_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ejinsight_list(self, content):

        soup = BeautifulSoup(content, "lxml")
    
        try:
            next_page = soup.find('a','next page-numbers')['href']
        except:
            next_page = None
            
        links = [{'url':s.find('a')['href'],'metadata':[]} for s in soup.findAll('div','newest-posts-thumb')]
        
        links_date = [datetime.strptime(s.text,'%b %d, %Y %H:%M%p') for s in soup.findAll('span','date')]
        latest_post_date = max(links_date).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_ejinsight_content(self, content):
        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        date = soup.find("meta",  property="article:modified_time")['content'].strip().replace('+00:00','+0000')
        title = soup.find("meta",  property="og:title")['content'].strip()
        
        try:
            author = soup.find('a', rel='author').text
        except:
            author = soup.find("meta",  property="og:site_name")['content'].strip()
        
        content = soup.find('div', {'id':'post-content'})
        for div in content.findAll('div'): 
            div.decompose()
        content = content.text.strip()

        #metadata
        category = [s.text for s in soup.find('div', id='breadcrumbs').findAll('a')]
        keywords = category[2:]
        category = category[1]
        fblike = fb_like_count_html.find('span',id='u_0_4').text
        try:
            fblike = re.search(r'(.*?) pe', fblike).group(1)
            if fblike == 'One':
                fblike = '1'
        except:
            fblike = '0'
        metadata = {'category': category, 'tag': keywords, 'fb_like_count': fblike} 

        #fb comment
        comment = parse_comments(comment_html)
             
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
