#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class NEWS_GET_INDEX(Protocol):

    def __init__(self):
        super(NEWS_GET_INDEX, self).__init__('NEWS_GET_INDEX')
        self.update({
          'board': '',
        })

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')
