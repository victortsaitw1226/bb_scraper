#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments

class HINET:
    def __init__(self, server):
        print('init hinet.py')
        self.server = server

    def on_CRW2PAR_NTF_HINET_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_hinet_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_HINET_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_hinet_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_hinet_list(self, content):
        
        def calculate_time(s):
            t = {"天前":" days ago", "小時前":" hours ago", "分鐘前":" minutes ago", "秒前":" seconds ago"}
            for key in t:
                if key in s:
                    s = s.replace(key, t[key])
            parsed_s = [s.split()[:2]]
            time_dict = dict((fmt,float(amount)) for amount,fmt in parsed_s)
            dt = timedelta(**time_dict)
            past_time = (datetime.now() - dt).strftime("%Y-%m-%dT%H:%M:%S+0800")
            return past_time
        
        soup = BeautifulSoup(content, 'lxml')
        
        dic = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        try:          
            pN = int(soup.find('input', {'id':'pN'})['value'])+1
            pA = soup.find('input', {'id':'pA'})['value']
            dic['next_page'] = 'https://times.hinet.net/realtime?pN='+str(pN)+'&pA='+pA

            if soup.find('span','recentlyTitle').text == '即時' and pN==101:
                dic['next_page'] = None
                
        except: 
            pass
    
        try:              
            [s.extract() for s in soup.find_all('li', {'class':'yap-loaded'})]
            [s.extract() for s in soup.find_all('li', {'class':'issueBox'})]
            p = soup.find('div', {'class':'recentlyBox'}).find_all('li')
            for i in range(len(p)):
                try:
                    link = p[i].find('a')['href']
                    if link[0] == '/':
                        dic['links'].append({
                                'url': 'https://times.hinet.net'+link,
                                'metadata': {}
                        })
                except:
                    pass
        except:
            pass
        
        # print('try:')
        try:
            time = soup.find('span', {'class':'cp'}).text.split('\xa0')[-1]
            # print('time:', time)
            dic['latest_post_date'] = calculate_time(time)
            # print('latest_post_date:', dic['latest_post_date'])
        except:
            pass

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(dic['next_page'])
        item.set_latest_post_date(dic['latest_post_date'])
        item.set_urls(dic['links'])

        return item

    def parse_hinet_content(self, content):

        def parse_datetime(soup):
            return datetime.strptime(soup.find('span', {'class':'cp'}).text.split('\xa0')[1][0:16], '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            first = soup.find('div', {'itemprop':'articleBody'}).text[0:20]
            if '記者' in first:
                author = first.split('記者')[1][0:3].replace('／', '')
            elif '台灣英文新聞/' in first:
                author = first.split('台灣英文新聞/')[1][0:3]
            else:
                author = ''
            return author
        
        def parse_title(soup):
            return soup.find('div', {'class':'inContent'}).find('h2').text
        
        def parse_content(soup):
            return soup.find('div', {'itemprop':'articleBody'}).text.replace('\n', '')
        
        def parse_metadata(soup):
            source = soup.find('span', {'class':'cp'}).find('a').text
            cat = soup.find('div', {'class': 'route'}).find_all('a')[1].text
            
            # try:
            #     link = soup.find('div', {'itemprop':'articleBody'}).find_all('a')
            #     for i in link:
            #         if i.text == '查看原始文章':
            #             link = i['href']
            #     if link == []: link = None
            # except:
            #     pass

            return {'category': cat,
                    'original_source': source,
                    #'original_source_url':link
                    }

        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        [s.extract() for s in soup.find_all('script')]
        
        dic = {
            'datetime': '',
            'author': '',
            'title': '',
            'content': '',
            'metadata': {},
            'comments': [],
        }
        url = content['url']
        dic['datetime'] = parse_datetime(soup)
        dic['author'] = parse_author(soup)
        dic['title'] = parse_title(soup)
        dic['content'] = parse_content(soup)
        dic['metadata'] = parse_metadata(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(dic['title'])
        item.set_author(dic['author'])
        item.set_date(dic['datetime'])
        item.set_content(dic['content'])
        item.set_comment(dic['comments'])
        item.set_metadata(dic['metadata'])
        return item
