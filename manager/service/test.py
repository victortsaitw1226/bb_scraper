from pymongo import MongoClient
import datetime
client = MongoClient('mongodb://ibdo:ibdo2018@210.202.47.189:27017/')
db = client['history_v2']
collection = db['urls']

'''
with open('./ptt_board_list', 'r') as f:
    for page in f.readlines():
        page = page.rstrip('\n')
        collection.insert({
            'media': 'ptt',
            'name': page,
            'url': "https://www.ptt.cc/bbs/" + page + "/index.html",
            'enabled': False,
            'updated': datetime.datetime.now()
        })

with open('./facebook_page_list', 'r') as f:
    for page in f.readlines():
        page = page.rstrip("\n")
        info = page.split(' ')
        collection.insert({
          'media': 'facebook',
          'name': info[0].strip(),
          'url': info[1].strip(),
          'enabled': False,
          'updated': datetime.datetime.now()
        })

with open('./youtube_channel_list', 'r') as f:
    for page in f.readlines():
        page = page.rstrip("\n")
        info = page.split('\t')
        collection.insert({
          'media': 'youtube',
          'name': info[0].strip(),
          'url': info[1].strip(),
          'enabled': False,
          'updated': datetime.datetime.now()
        })
'''


docs = {
  'ltn_estate': 'ltn_estate_url_list',
  'ltn_news': 'ltn_news_url_list',
  'nextmag': 'nextmag_url_list',
  'linetoday' : 'linetoday_url_list',
  'appledaily': 'appledaily_url_list',
  'tvbs': 'tvbs_url_list',
  'ettoday': 'ettoday_url_list',
  'etforum': 'etforum_url_list'


}
for k, v in docs.items():

    with open('./' + v, 'r') as f:
        for page in f.readlines():
            page = page.rstrip('\n')
            collection.insert({
                'media': k,
                'name': k,
                'url': page,
                'enabled': True,
                'updated': datetime.datetime.now()
            })
