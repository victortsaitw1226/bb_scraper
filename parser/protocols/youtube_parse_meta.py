#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class YOUTUBE_PARSE_META(Protocol):

    def __init__(self):
        super(YOUTUBE_PARSE_META, self).__init__('YOUTUBE_PARSE_META')
        self.update({
          'title': '',
          'href': '',
          'count': 0,
          'date': '',
          'img': ''
        })

    def set_title(self, title):
        self.set('title', title)

    def get_title(self):
        return self.get('title')

    def set_href(self, href):
        self.set('href', href)

    def get_href(self):
        return self.get('href')

    def set_count(self, count):
        self.set('count', count)

    def get_count(self):
        return self.get('count')

    def set_date(self, date):
        self.set('date', date)

    def get_date(self):
        return self.get('date')

    def set_img(self, img):
        self.set('img', img)

    def get_img(self):
        return self.get('img')
