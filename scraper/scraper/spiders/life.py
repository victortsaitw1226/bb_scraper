# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class LifeSpider(RedisSpider):
#class LifeSpider(scrapy.Spider):
    name = "life"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "life",
            "name": "life",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://life.tw/?app=category&act=categorylist&no=1",
            "scrapy_key": "life:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)

        if len(links) == 0:
            return

        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(links[0]['url'])

        for url in links:
            yield response.follow(url['url'],
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self, soup):
        try:
            next_page_postfix = soup.find('a', class_='next-page')['href']
        except:
            return None
        return next_page_postfix

    def parse_links(self, soup):
        links = []
        for element in soup.find_all('dt'):
            if not element.find(target='_blank'):
                continue
            links.append({
                'url': element.a['href'],
                'metadata': {}
            })
        return links
    
    def parse_latest_post_date(self, latest_link):
        latest_link = 'https://life.tw' + latest_link
        first_post = BeautifulSoup(requests.get(latest_link).text, 'html.parser')
        post_time = first_post.find('span', class_='d-num')
        post_time = ''.join(post_time.text.split())
        date = datetime.strptime(post_time, '%Y-%m-%d')
        return date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'life'
        item['proto'] = 'LIFE_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        post_time = soup.find('span', class_='d-num')
        post_time = ''.join(post_time.text.split())
        date = datetime.strptime(post_time, '%Y-%m-%d')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self, soup):
        author_name = soup.find('a', class_='corange tdu').text
        return author_name

    def parse_author_url(self, soup):
        author_postfix = soup.find('a', class_='corange tdu')['href']
        return 'https://life.tw' + author_postfix

    def parse_title(self, soup):
        title = soup.find('div', class_='aricle-detail-top').find('h1').text
        return ''.join(title.split())
    
    def parse_content(self, soup):
        content = soup.find('div',{'id':'mainContent'})

        for tags in content.find_all('script'):
            tags.clear()

        for tags in content.find_all('ul','detail-articlelist greenbg cb'):
            tags.clear()
        
        for tags in content.find_all('div','recommended mb20 radius5 shadow'):
            tags.clear()

        content = ''.join(content.text.split())
        content = re.split('延伸閱讀',content)[0]
        content = re.split('【延伸貼文】',content)[0]
        return content

    def parse_metadata(self, soup):
        category = soup.find(
            'div', class_='path shadow radius5 mb20 cb').find_all('strong')[1]
        return  {'category': ''.join(category.text.split())}
    

