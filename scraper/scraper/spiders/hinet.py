# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
from pytz import timezone
import json
import re

class HinetSpider(RedisSpider):
#class HinetSpider(scrapy.Spider):
    name = "hinet"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "hinet",
            "name": "hinet",
            "enabled": True,
            "days_limit": 3600 * 24 * 6,
            "interval": 3600,
            "url": "https://times.hinet.net/category/400",
            "scrapy_key": "hinet:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup,response.url)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now(timezone('Asia/Taipei')) - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return
        
        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = []
        [s.extract() for s in soup.find_all('li', {'class':'yap-loaded'})]
        [s.extract() for s in soup.find_all('li', {'class':'issueBox'})]
        p = soup.find('div', {'class':'recentlyBox'}).find_all('li')
        for i in range(len(p)):
            try:
                link = p[i].find('a')['href']
                if link[0] == '/':
                    links.append('https://times.hinet.net'+link)
            except:
                pass
        return links
    
    def parse_next_page(self,soup,pre_url):
        pN = int(soup.find('input', {'id':'pN'})['value'])+1
        pA = soup.find('input', {'id':'pA'})['value']
        next_page = pre_url.split('?')[0] + '?pN='+str(pN)+'&pA='+pA

        if soup.find('span','recentlyTitle').text == '即時' and pN==101:
            next_page = None
        return next_page
    
    def calculate_time(self,s):
        t = {
            "天前":" days ago",
            "小時前":" hours ago",
            "分鐘前":" minutes ago",
            "秒前":" seconds ago"
        }
        for key in t:
            if key in s:
                s = s.replace(key, t[key])
        parsed_s = [s.split()[:2]]
        time_dict = dict((fmt,float(amount)) for amount,fmt in parsed_s)
        dt = timedelta(**time_dict)
        past_time = (datetime.now(timezone('Asia/Taipei')) - dt)
        return past_time

    def parse_latest_post_date(self,soup):
        time = soup.find('span', {'class':'cp'}).text.split('\xa0')[-1]
        latest_post_date = self.calculate_time(time)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        [s.extract() for s in soup.find_all('script')]
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'hinet'
        item['proto'] = 'HINET_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        return datetime.strptime(soup.find('span', {'class':'cp'}).text.split('\xa0')[1][0:16], '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        first = soup.find('div', {'itemprop':'articleBody'}).text[0:20]
        if '記者' in first:
            author = first.split('記者')[1][0:3].replace('／', '')
        elif '台灣英文新聞/' in first:
            author = first.split('台灣英文新聞/')[1][0:3]
        else:
            author = ''
        return author
    
    def parse_title(self, soup):
        return soup.find('div', {'class': 'inContent'}).find('h2').text
    
    def parse_content(self, soup):
        content = soup.find('div', {'itemprop':'articleBody'}).text.replace('\n', '')
        content = content.split('【往下看更多】')[0]
        content = content.split('【今日最熱門】')[0]
        content = ''.join(content.split())
        return content
    
    def parse_metadata(self, soup):
        source = soup.find('span', {'class':'cp'}).find('a').text
        cat = soup.find('div', {'class': 'route'}).find_all('a')[1].text
        try:
            news_source = soup.find('span', {'class':'cp'}).find('a').text
        except:
            news_source = ''

        return {
            'category': cat,
            'original_source': source,
            'news_source': news_source
        }
    
