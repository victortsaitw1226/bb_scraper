# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
import traceback, sys
import datetime
import re
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
import json
from .redis_spiders import RedisSpider

# class LtnSpider(scrapy.Spider):
class LtnSpider(RedisSpider):
    name = "ltn"

    def start_requests(self):

        if isinstance(self, RedisSpider):
            return

        request = {
            "url": "https://www.myip.com/",
            "priority": 3,
            "search": False,
            "url_pattern": "https://news.ltn.com.tw/ajax/breakingnews/all/{}",
            "interval": 3600,
            "days_limit": 3600 * 24
        }
        yield scrapy.Request(request['url'],
                meta = request)
                
    def parse(self, response):
        meta = response.meta
        meta['page'] = 1
        url = meta['url_pattern'].format(1)
        yield scrapy.http.Request(url,
            dont_filter=True,
            callback=self.parse_list,
            meta=meta
        )

    def parse_list(self, response):
        content = response.body
        meta = response.meta
        page = meta['page']
        search_page = meta['search']
        if not search_page:
            data = json.loads(response.body)
            if isinstance(data['data'], list):
                _iteration = data['data']
            elif isinstance(data['data'], dict):
                _iteration = data['data'].values()

            if len(_iteration) == 0:
                return
                #raise scrapy.exceptions.CloseSpider('Response is Empty')

            for article in _iteration:
                url = article['url']
                new_meta = article.copy()
                new_meta.update(meta)
                yield scrapy.Request(url, 
                        callback=self.parse_article, 
                        meta=new_meta)
        else:
            soup = BeautifulSoup(content, 'html.parser')
            for link in soup.find_all('a', class_='tit'):
                url = link.get('href')
                yield scrapy.Request(url,
                        callback=self.parse_article,
                        dont_filter=True)

        page = page + 1
        meta.update({'page': page})
        url = meta['url_pattern'].format(page)
        yield scrapy.Request(url,
                callback=self.parse_list,
                dont_filter=True,
                meta=meta)

    def parse_article(self, response):
        meta = response.meta
        def parse_datetime(soup):
            date = soup.find('meta', {'name':'pubdate'})
            if date:
                return date['content'].replace('Z', '+0800')
            date = soup.find('span', {'class':'time'})

            if date:
                return date_parser(date.text).strftime('%Y-%m-%dT%H:%M:%S+0800')

            date = soup.find('div', {'class':'article_header'})
            if date:
                return datetime.datetime.strptime(date.find_all('span')[1].text, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
            date = soup.find('div', {'class':'writer'})
            if date:
                return datetime.datetime.strptime(date.find_all('span')[1].text, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_title_metadata(soup):        
            title = soup.find('title').text.replace(' - 自由時報電子報', '').replace(' 自由電子報', '')
            title_ = title.split('-')
            if not title_[-1]:
                del title_[-1]
            if len(title_) > 2:
                cat = title_[-1]
                del title_[-1]
                ti = ''.join(x for x in title_)
                return ti.strip(), cat.strip()
            elif len(title_) == 2:
                cat = title_[1]
                ti = title_[0]
                return ti.strip(), cat.strip()
            elif '3C科技' in title_[0]:
                cat = '3C科技'
                ti = title_[0].replace('3C科技', '')
                return ti.strip(), cat
            elif '玩咖Playing' in title_[0]:                
                cat = '玩咖Playing'
                ti = title_[0].replace('玩咖Playing', '')            
                return ti.strip(), cat
            else:
                cat = ''
                ti = title_[0]
                return ti.strip(), cat
                
        
        def find_author(list_text):
            list_text = [x for x in list_text if x!='文']
            tmp = [x for x in list_text if '記者' in x]
            if tmp:
                au = tmp[0].replace('記者', '').replace('攝影', '').strip()                
            else:
                au = min(list_text, key=len)
            return au
            

        def parse_content_author(soup):
            
            # content
            # content = soup.find('div','text boxTitle boxText')
            content = soup.find('div',{'itemprop':'articleBody'})
            for appE1121 in content.find_all('p','appE1121'):
                appE1121.clear()
            for photo in content.find_all('div','photo boxTitle'):
                photo.clear()
            for photo in content.find_all('span','ph_b ph_d1'):
                photo.clear()
            content = ''.join(x.text for x in content.find_all('p') if x.text!='爆')

            #author
            au = ''
            reg = re.compile(r'(［\D*／\D*］|〔\D*／\D*〕)', re.VERBOSE)
            author_reg = reg.findall(content)
            if author_reg:
                au = author_reg[0].split('／')[0].replace('〔', '').replace('［', '').replace('記者', '').replace('編譯', '')

            if not au:
                author = soup.find('div', {'class':'writer boxTitle'})
                if author: 
                    au = author.find('a')['data-desc']
            if not au:
                author = soup.find('p', {'class':'auther'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:        
                author = soup.find('p', {'class':'writer'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:        
                author = soup.find('div', {'class':'writer'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au: 
                author = soup.find('div', {'class':'conbox_tit boxTitle'})
                if author:
                    au = author.find('a')['data-desc']
            if not au:
                author = soup.find('div', {'class':'article_header'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('p')
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
                    if author.find('strong'):
                        au = author.find('strong').text.split('◎')[1].replace('記者', '').replace('攝影', '').strip()
                    if '■' in author.text:
                        au = author.text.replace('■', '')
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('span', {'class':'writer'})
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('h4')
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
                    elif '◎' in author.text:
                        au = author.text.replace('◎', '')
            return content, au

        # detail
        url = response.url
        
        soup = BeautifulSoup(response.body, 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        #[s.extract() for s in soup.find_all('script')]
        
        dic = {
            'datetime': '',
            'author': '',
            'title': '',
            'content': '',
            'metadata': {},
            'comments': [],
        }
        
        dic['datetime'] = parse_datetime(soup)
        dic['content'], dic['author'] = parse_content_author(soup)
        if 'title' in meta and 'tagText' in meta:
            dic['title'] = meta['title']
            dic['metadata']['category'] = meta.get('tagText', "")
        else:
            dic['title'], dic['metadata']['category'] = parse_title_metadata(soup)
        #dic['comments'] = parse_comments(comment_html)
        item = NewsItem()
        item['url'] = url
        item['article_title'] = dic['title']
        item['author'] = dic['author']
        item['author_url'] = []
        item['comment'] = []
        item['date'] = dic['datetime']
        item['content'] = dic['content']
        #item.set_comment(dic['comments'])
        item['metadata'] = dic['metadata']
        item['content_type'] = 0
        item['media'] = 'ltn_news'
        item['proto'] =  'PTT_PARSE_ITEM'
        return item
