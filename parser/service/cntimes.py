#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class CNTIMES:
    def __init__(self, server):
        print('init cntimes.py')
        self.server = server

    def on_CRW2PAR_NTF_CNTIMES_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_cntimes_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CNTIMES_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_cntimes_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_cntimes_list(self, content):        

        cntimes = 'http://www.cntimes.info/'
        soup = BeautifulSoup(content, "lxml")    
        table = soup.findAll('h3')
        links = [{'url':cntimes + s.find('a', {'target':'_blank'})['href'],'metadata':[]} for s in table]
        [s.find('a').decompose() for s in table]
        link_date = [datetime.strptime(s.text, '（%Y-%m-%d）') for s in table]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
        try:
            next_page = cntimes + 'coluOutline.jsp?coluid=' + soup.find('input', {'name':'coluid'})['value'] + '&page=' + re.findall(r'\d+', soup.find('a',text = '下一頁')['href'])[0]
        except:
            next_page = None

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_cntimes_content(self, content):
        # detail
        soup = BeautifulSoup(content['detail_html'], 'lxml')
        url = content['url']
    
        date = datetime.strptime(soup.find('h6').text.split('\u3000')[1], '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find('h2').text.replace('\u3000',' ')
        author = soup.find('title').text.split('-')[0]
        
        content = soup.find('div','content_body')
        for div in content.findAll('div'): 
            div.decompose()
        for div in content.findAll('table'): 
            div.decompose()        
        content = content.text.strip().replace('\u3000','').replace('\r','\n').replace('本報訊／','').replace('本報訊/','')
        
        #metadata
        try:
            source = re.findall(r'來源(.*)', content)[0].replace('：','').replace(')','')
        except:
            source = ''
        category = soup.find('div','nav_left').findAll('a')[1].text
        metadata = {'original_source':source, 'category':category}

        comment = []
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
