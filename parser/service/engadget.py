#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta, date


class ENGADGET:
    def __init__(self, server):
        print('init engadget.py')
        self.server = server

    def on_CRW2PAR_NTF_ENGADGET_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_engadget_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ENGADGET_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_engadget_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_engadget_list(self, content):
        def parse_links(soup):
            link_sections = soup.find_all('div','grid@m+')
            links = [{'url':engadget + s.find('a')['href'],'metadata':[]} for s in link_sections ]
            return links
        
        def parse_next_page(soup):
            try:
                next_page = soup.find('a','o-btn o-btn--small w-100@m- w-130@tp+ th-btn')['href']
                next_page = engadget + next_page
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            link_date_sec = soup.find_all('div','relative inline-block ')
            print('LINK_DATE_SEC: ', link_date_sec)
            link_date = []
            for li in link_date_sec:
                dat = li.find_all('span')[-1].text.strip()
                dat = dat.replace('下午', 'PM').replace('傍晚', 'PM').replace('晚上', 'PM')
                dat = dat.replace('凌晨', 'AM').replace('早上', 'AM').replace('中午', 'AM')
                if dat.find('AM')!=-1:
                    link_date.append(datetime.strptime( dat , '%Y 年 %m 月 %d 日, %p %H:%M'))
                elif dat.find('PM')!=-1:
                    link_date.append( datetime.strptime( dat , '%Y 年 %m 月 %d 日, %p %I:%M'))
                elif dat.find('秒鐘')!=-1:
                    link_date.append( datetime.today()-timedelta(seconds=int(re.split(' ',dat)[0])) )
                elif dat.find('分鐘')!=-1:
                    link_date.append( datetime.today()-timedelta(minutes=int(re.split(' ',dat)[0])) )
                elif dat.find('小時')!=-1:
                    link_date.append( datetime.today()-timedelta(hours=int(re.split(' ',dat)[0])) )
                else:
                    link_date.append( datetime.today() )
            print('LINK_DAET: ', link_date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date



        print('URL: ', content['url'])
        list_html = BeautifulSoup(content['html'], 'html.parser')
        engadget = 'https://chinese.engadget.com'
    
        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_engadget_content(self, content):

        def parse_date(soup):
            dat = soup.find('div','t-meta-small@s t-meta@m+').find('div','th-meta').text.strip()
            dat = dat.replace('下午', 'PM').replace('傍晚', 'PM').replace('晚上', 'PM')
            dat = dat.replace('凌晨', 'AM').replace('早上', 'AM').replace('中午', 'AM')
            if dat.find('AM')!=-1:
                dat = datetime.strptime( dat , '%Y 年 %m 月 %d 日, %p %H:%M')
            elif dat.find('PM')!=-1:
                dat = datetime.strptime( dat , '%Y 年 %m 月 %d 日, %p %I:%M')
            elif dat.find('秒鐘')!=-1:
                dat = datetime.today()-timedelta(seconds=int(re.split(' ',dat)[0])) 
            elif dat.find('分鐘')!=-1:
                dat = datetime.today()-timedelta(minutes=int(re.split(' ',dat)[0])) 
            elif dat.find('小時')!=-1:
                dat = datetime.today()-timedelta(hours=int(re.split(' ',dat)[0])) 
            else:
                dat = datetime.today() 
            dat = dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return dat

        def parse_comments(soup):
            comments = []
            
            def parse_one_comment(one_commt):
                auth = one_commt.find(class_ = 'sppre_username')
                cont = one_commt.find(class_ = 'sppre_entity sppre_text-entity')
                try:
                    like = one_commt.find(class_ = 'param-0 param-upVotesCount').text
                except:
                    like = '0'
                
                try:
                    dat = one_commt.find(class_ = 'sppre_posted-at').text.replace('\xa0','日').replace(' 月','月2019年')
                    dat = datetime.strptime(dat, '%d日%m月%Y年').strftime('%Y-%m-%dT%H:%M:%S+0800')
                except:
                    dat = one_commt.find(class_ = 'sppre_posted-at').text
                    if '日' in dat:
                        dat = (date.today()-timedelta(days=int(dat.replace('日','')))).strftime('%Y-%m-%dT%H:%M:%S+0800')
                    if '時' in dat:
                        dat = (datetime.now()-timedelta(hours=int(dat.replace('時','')))).strftime('%Y-%m-%dT%H:%M:%S+0800')
                  
                one_commt_detail = {'comment_id':'', 'author_url':'', 'author':auth.text ,'content':cont.text , 
                                    'datetime':dat, 'metadata':{'like_count':like}}
                return one_commt_detail
                
            block = soup.find_all('li','sppre_list-item')
            for b in block:
                try:
                    block_comments = b.find_all('li',{'aria-label':'Comment reply'})
                    block_commt_commt = []
                    for bb in block_comments:
                        block_commt_commt.append(  parse_one_comment(bb))
                except:
                    block_comments = []
                
                block_comments_detail = parse_one_comment(b.find('div','sppre_message-stack'))
                block_comments_detail['source'] = 'engadget'
                
                block_comments_detail['comments'] = block_commt_commt
                comments.append(block_comments_detail)
            
            return comments
        
        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        title = detail_html.find('h1','t-h4@m- t-h1-b@tp t-h1@tl+ mt-20 mt-15@tp mt-0@m-').text.strip()
        author = detail_html.find('div','t-meta-small@s t-meta@m+').find('a').text
        dat = parse_date(detail_html)
        content = detail_html.find('div','article-text c-gray-1').text.strip()
        keywords = detail_html.findAll('span','th-meta')[-1].text.strip().replace(' ','').split(',')
        source = detail_html.findAll('span','th-meta')[-2].text

        metadata = {'tag':keywords, 'original_source':source}
        comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(dat)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
