# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class NownewsSpider(RedisSpider):
    name = "nownews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "nownews",
            "name": "nownews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.nownews.com/cat/politics/constitutionalforum/",
            "scrapy_key": "nownews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        next_page = self.parse_next_page(soup)
        latest_datetime, links = self.parse_latest_datetime_and_links(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        if next_page is None:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self, soup):
        next_page = soup.find('div', class_='page-nav td-pb-padding-side')
        if next_page is None:
            return

        if next_page.find('i', class_='td-icon-menu-right') is not None:
            next_page_link = next_page.find('i', class_='td-icon-menu-right').previous.get('href')
        else:
            next_page_link = None

        return  next_page_link

    def parse_latest_datetime_and_links(self, soup):
        links = set()
        datetimes = []
        #dummy = []
        
        main_content = soup.find('div', class_='td-main-content-wrap td-container-wrap')
        for element in main_content.find_all('div', class_ = 'td-module-thumb'):
            
            link = element.a.get('href')
            links.add(link)
            #if link not in dummy:
            #    links.append({
            #        'url': link,
            #        'metadata': {}
            #    })
            
            #dummy.append(link)
            
            time_ = element.findNext('div').find('time', class_ = 'entry-date updated td-module-date')
            if not time_:
                continue
    
            dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+00:00')
            datetimes.append(dt)
        
        latest_datetime = min(datetimes)
        return latest_datetime, links


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
       
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'nownews'
        item['proto'] =  'NOWNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        time_ = soup.find('time')
        dt = datetime.strptime(time_.text, '%Y-%m-%d %H:%M:%S')
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_metadata(self, soup):
        
        metadata = {
            'category': '',
            'tag': [],
            'fb_like_count':''
        }
        
        metadata['category'] = soup.find('span','td-bred-no-url-last').text

        if  soup.find('ul', class_='td-tags td-post-small-box clearfix') is not None:
            keywords = soup.find('ul', class_='td-tags td-post-small-box clearfix').find_all('li')
            kword = [x.text for x in keywords]
            metadata['tag'] = kword[1:]
                    
        return metadata

    def parse_title(self, soup):
        board = soup.find('header', class_='td-post-title').find('h1')
        return board.text.strip()

    def parse_author(self, soup):
        author_tag = soup.find('div', class_='td-post-author-name')
        if author_tag is None:
            return ''

        author = author_tag.get_text(strip = True).replace(" ","")

        if '記者' in author:
            authors = re.search(u"[\u4e00-\u9fa5]+", author).group()
            return authors[2:]

        return author

    def parse_content(self, soup):
        
        for script in soup.find_all('script'):
            script.extract()
        
        paragraph = []
        paragraphs = soup.find('div', class_='td-post-content').find_all('p')
        
        for pp in paragraphs:
            if any(text not in pp.text for text in ('Decrease font size', 'Increase font size', 'Reset font size')):
                paragraph.append(pp.get_text(strip=True))
        
        content = '\n'.join(paragraph)
                
        return content
    

