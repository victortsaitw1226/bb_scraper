#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import traceback, sys
import datetime
from dateutil.parser import parse as date_parser

class LTN_NEWS:
    def __init__(self, server):
        print('init ltn_news.py')
        self.server = server

    def on_CRW2PAR_NTF_LTN_NEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ltn_news_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_LTN_NEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ltn_news_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_ltn_news_list(self, content):
   
        soup = BeautifulSoup(content, 'lxml')
    
        dic = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        try:          
            next_ = soup.find('div', {'class':'pagination boxTitle'}).find('a',{'data-desc':"下一頁"})['href']
            if next_[0] == '/':
                dic['next_page'] = 'https:'+next_
            else:
                dic['next_page'] = 'https://'+next_
        except: 
            pass
    
        try:              
            p = soup.find('div', {'class':'whitecon boxTitle'}).find_all('li')
            time_ = p[0].find('span').text.split(':')[0]
            print(time_)
            try: 
                #int(time_)
                if len(time_) < 3:
                    date = datetime.datetime.now().strftime("%Y-%m-%d")+' '+p[0].find('span').text
                    dic['latest_post_date'] = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
                else:
                    dic['latest_post_date'] = datetime.datetime.strptime(p[0].find('span').text, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                date = soup.find('div', {'class':'date'})
                print(date)
                if date:
                    dic['latest_post_date'] = datetime.datetime.strptime(date['title'], '%Y%m%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
                else:
                    date = soup.find('p', {'class':'date'}).text.split('‧')[0].strip()
                    dic['latest_post_date'] = datetime.datetime.strptime(date, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')

            for i in range(len(p)):
                try:
                    link = p[i].find('a')['href']
                    if link[0] == '/':
                        dic['links'].append({
                            'url': 'https:'+link,
                            'metadata': {}
                        })
                    elif link[0] != 'h': 
                        dic['links'].append({
                            'url': 'https://news.ltn.com.tw/'+link,
                            'metadata': {}
                        })
                    else:
                        dic['links'].append({
                            'url': link,
                            'metadata': {}
                        })
                except:
                    pass
        except:
            traceback.print_exc(file=sys.stdout)


        item = NEWS_PARSE_LIST()
        item.set_next_page_url(dic['next_page'])
        item.set_latest_post_date(dic['latest_post_date'])
        item.set_urls(dic['links'])
        print(str(item))
        return item

    def parse_ltn_news_content(self, content):
        
        def parse_datetime(soup):
            date = soup.find('meta', {'name':'pubdate'})
            if date:
                return date['content'].replace('Z', '+0800')
            date = soup.find('span', {'class':'time'})
            if date:
                #return datetime.datetime.strptime(date.text, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
                return date_parser(date.text).strftime('%Y-%m-%dT%H:%M:%S+0800')

            date = soup.find('div', {'class':'article_header'})
            if date:
                return datetime.datetime.strptime(date.find_all('span')[1].text, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
            date = soup.find('div', {'class':'writer'})
            if date:
                return datetime.datetime.strptime(date.find_all('span')[1].text, '%Y-%m-%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_title_metadata(soup):        
            title = soup.find('title').text.replace(' - 自由時報電子報', '').replace(' 自由電子報', '')
            title_ = title.split('-')
            if not title_[-1]:
                del title_[-1]
            if len(title_) > 2:
                cat = title_[-1]
                del title_[-1]
                ti = ''.join(x for x in title_)
                return ti.strip(), cat.strip()
            elif len(title_) == 2:
                cat = title_[1]
                ti = title_[0]
                return ti.strip(), cat.strip()
            elif '3C科技' in title_[0]:
                cat = '3C科技'
                ti = title_[0].replace('3C科技', '')
                return ti.strip(), cat
            elif '玩咖Playing' in title_[0]:                
                cat = '玩咖Playing'
                ti = title_[0].replace('玩咖Playing', '')            
                return ti.strip(), cat
            else:
                cat = ''
                ti = title_[0]
                return ti.strip(), cat
                
        
        def find_author(list_text):
            list_text = [x for x in list_text if x!='文']
            tmp = [x for x in list_text if '記者' in x]
            if tmp:
                au = tmp[0].replace('記者', '').replace('攝影', '').strip()                
            else:
                au = min(list_text, key=len)
            return au
            
        def parse_content_author(soup):
            content = ''
            au = ''
            
            reg = re.compile(r'(［\D*／\D*］|〔\D*／\D*〕)', re.VERBOSE)
            article = soup.find_all('p')
            article = ''.join(x.text for x in article if x.text!='爆')
            for item in article.split('\n'):
                tmp = reg.findall(item)
                if tmp:
                    content = item.replace('為達最佳瀏覽效果，建議使用 Chrome、Firefox 或 Internet Explorer 10(含) 以上版本的瀏覽器。', '').replace('自由時報APP看新聞又抽獎', '')
                    au = tmp[0].split('／')[0].replace('〔', '').replace('［', '').replace('記者', '').replace('編譯', '')
                    return content, au
            
            
            if not au:
                content = max(article.split('\n'), key=len).replace('為達最佳瀏覽效果，建議使用 Chrome、Firefox 或 Internet Explorer 10(含) 以上版本的瀏覽器。', '').replace('自由時報APP看新聞又抽獎', '')
                author = soup.find('div', {'class':'writer boxTitle'})
                if author: 
                    au = author.find('a')['data-desc']
            if not au:
                author = soup.find('p', {'class':'auther'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:        
                author = soup.find('p', {'class':'writer'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:        
                author = soup.find('div', {'class':'writer'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au: 
                author = soup.find('div', {'class':'conbox_tit boxTitle'})
                if author:
                    au = author.find('a')['data-desc']
            if not au:
                author = soup.find('div', {'class':'article_header'})
                if author:
                    tmp = author.find('span').text.split('／')
                    au = find_author(tmp)
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('p')
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
                    if author.find('strong'):
                        au = author.find('strong').text.split('◎')[1].replace('記者', '').replace('攝影', '').strip()
                    if '■' in author.text:
                        au = author.text.replace('■', '')
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('span', {'class':'writer'})
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
            if not au:
                try:
                    author = soup.find('div', {'itemprop':'articleBody'}).find('h4')
                except:
                    author = ''
                if author:
                    if '／' in author.text:
                        tmp = author.text.split('／')
                        au = find_author(tmp)
                    elif '◎' in author.text:
                        au = author.text.replace('◎', '')
            return content, au

        # detail
        url = content['url']
        
        soup = BeautifulSoup(content['html'], 'lxml')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        [s.extract() for s in soup.find_all('script')]
        
        dic = {
            'datetime': '',
            'author': '',
            'title': '',
            'content': '',
            'metadata': {},
            'comments': [],
        }
        
        dic['datetime'] = parse_datetime(soup)
        dic['content'], dic['author'] = parse_content_author(soup)        
        dic['title'], dic['metadata']['category'] = parse_title_metadata(soup)
        #dic['comments'] = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(dic['title'])
        item.set_author(dic['author'])
        item.set_date(dic['datetime'])
        item.set_content(dic['content'])
        #item.set_comment(dic['comments'])
        item.set_metadata(dic['metadata'])
        
        return item
