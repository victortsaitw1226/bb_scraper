# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class KsnewsSpider(RedisSpider):
    name = "ksnews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ksnews",
            "name": "ksnews",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600 * 2,
            "url": "http://www.ksnews.com.tw/index.php/news/news_subList/16/125",
            "scrapy_key": "ksnews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        next_page = self.parse_next_page(soup)

        posts_date = []
        posts = soup.find_all('div', class_='ip_news_list')
        for post in posts:
            post_date = post.find('span', class_='date').text
            posts_date.append(datetime.strptime(post_date, '%Y年%m月%d日'))

            url = post.find('a').get('href')
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)


        latest_datetime = max(posts_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        if next_page is None:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self,soup):
        try:         
            next_page = soup.find('a', {'class':'next'})['href']
        except:
            next_page = None
        return next_page
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'ksnews'
        item['proto'] = 'KSNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        return datetime.strptime(soup.find('span', {'class':'date'}).text, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self,soup):
        try:
            au = soup.find('section').text[0:200].replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')
            ind = au.index('記者')
            au = au[ind+2:ind+5]
        except:
            au = ''
        return au

    def parse_title(self,soup):
        return soup.find('h1', {'class':'title_'}).text

    def parse_content(self,soup):
        content = soup.find('section').text
        content = content.replace('\n', '')
        content = content.replace('\t', '')
        content = content.replace('\r', '')
        content = content.replace('\xa0', '')
        return content

    def parse_metadata(self,soup):
        try:
            category = soup.find('ol', class_='bread_crumbs').text.split('\n')[3]
        except:
            category = ''

        return {
            'category': category
        }
    
