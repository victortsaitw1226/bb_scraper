#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class UDNBLOG:
    def __init__(self, server):
        self.server = server

    def on_CRW2PAR_NTF_UDNBLOG_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_udnblog_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_UDNBLOG_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_udnblog_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_udnblog_list(self, content):

        def parse_links(soup):
            link = soup.find_all('td',{'colspan':'2'})
            links = []
            for li in link:
                try:
                    if li.find('a').text != '':
                        links.append({'url': li.find('a')['href'],'metadata':[]})                  
                except:
                    pass
            return links
        
        def parse_next_page(soup):
            try:
                next_page = soup.find('a',text = '下一頁')['href']
            except:
                next_page = None
            return next_page
         
        def parse_latest_post_date(soup):
            sub_text = soup.find_all('td',{'align':"right" , 'class':'t11'})
            link_date = []
            for d in sub_text:
                d = re.split('｜',d.text)[0]
                link_date.append(datetime.strptime( d , '%Y/%m/%d %H:%M'))
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
                
        list_html = BeautifulSoup(content, "html.parser")
        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_udnblog_content(self, content):
        
        def parse_date(soup):
            date = soup.find('tr','font-size12').text.strip()
            date = re.split(r'瀏覽',date)[0]
            date = datetime.strptime( date , '%Y/%m/%d %H:%M:%S')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_content(soup):
            content = soup.find('td',{'valign':"top", 'class':"main-text"})
            content = ' '.join(content.text.split())
            content = re.split(r'相關閱讀',content)[0]
            return content
        
        def parse_browse_count(soup):
            browse = soup.find('tr','font-size12').text.strip()
            browse = re.split(r'瀏覽',browse)[1]
            browse = re.split('｜',browse)[0]
            return browse
        
        def parse_response_count(soup):
            response = soup.find('tr','font-size12').text.strip()
            response = re.split('｜',response)[1]
            response = re.split(r'回應',response)[1]
            return response
        
        def parse_recommend_count(soup):
            recommend = soup.find('tr','font-size12').text.strip()
            recommend = re.split('｜',recommend)[2]
            recommend = re.split(r'推薦',recommend)[1]
            return recommend
        
        def parse_category(soup):
            content_body = soup.find('table',{'width':"535", 'border':"0", 'cellpadding':"0",
                                            'cellspacing':"0", 'class':"font-size15"})
            category = content_body.find_all('td',{'align':'right','class':'main-text'})[-1]
            category = category.find('a').text
            return category

        def parse_comments(soup):
            comments = []
            try:
                comment_section = soup.find('table',{'width':"730", 'border':"0", 'cellpadding':"0",
                                        'cellspacing':"4", 'class':"font-size12"})
                comment_author_sec = comment_section.find_all('td',{'align':"center"})
                comment_author_url = [x.find('a','nav')['href'] for x in comment_author_sec]
                comment_author = [x.find('a','panel-text').text for x in comment_author_sec]
                comment_datetime = comment_section.find_all('td',{'align':"right", 'class':'main-title'})
                comment_datetime = [datetime.strptime( x.text , '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800') for x in comment_datetime]
                comment_content = comment_section.find_all('td',{'colspan':'2','class':'main-text'})
                comment_content = [x.text.strip() for x in comment_content]
                
                # comments_comments
                comments_comments = []
                content_section = comment_section.find_all('table','panel-bg')
                for cont_sec in content_section:
                    try:
                        commt_commt = cont_sec.find_all('span','gbook-bg')
                        
                        commt_commt_all = []
                        for ct_ct in commt_commt:
                        
                            commt_commt_title = ct_ct.find('td','gbook-title').text.strip()
                            commt_commt_author = re.split(r' 於 ',commt_commt_title)[0]
                            commt_commt_date = re.split(r' 於 ',commt_commt_title)[1]
                            commt_commt_date = datetime.strptime( commt_commt_date , '%Y-%m-%d %H:%M 回覆：').strftime('%Y-%m-%dT%H:%M:%S+0800')
                            commt_commt_cont = ct_ct.find('td','font-size15 gbook-content')
                            commt_commt_cont = ' '.join(commt_commt_cont.text.split())
                            commt_commt_all.append({'comment_id':'','author':commt_commt_author,'author_url':'' ,'content':commt_commt_cont , 
                                            'datetime':commt_commt_date,'metadata':{}})
                            
                        comments_comments.append( commt_commt_all )
                    except:
                        comments_comments.append([])
                
                for cont,auth,auth_url,date,cm_cm in zip(comment_content,comment_author,comment_author_url,comment_datetime , comments_comments):
                    comments.append({'comment_id':'','author':auth,'author_url':auth_url ,'content':cont , 'datetime':date , 
                                    'source': 'udnblog','metadata':{},'comments':cm_cm })
                
            except:
                pass
            
            return comments
        
   
        item = NEWS_PARSE_ITEM()
        detail_html = BeautifulSoup(content['detail_html'], 'html.parser')
        
        # detail
        url = content['url']
        title = detail_html.find('span',{'id':'maintopic'}).text
        date = parse_date(detail_html)
        author = detail_html.find('table',{'width':"160",'border':"0",'cellpadding':"0",'cellspacing':"0",
                                  'class':"font-size12"}).find('a','panel-text').text
        author_url = detail_html.find('table',{'width':"160",'border':"0",'cellpadding':"0",'cellspacing':"0",
                                    'class':"font-size12"}).find('a','nav')['href']
        content = parse_content(detail_html)
        browse_count = parse_browse_count(detail_html)
        response_count = parse_response_count(detail_html)
        recommend_count = parse_recommend_count(detail_html)
        comment = parse_comments(detail_html)
        category = parse_category(detail_html)
        metadata = {'view_count': browse_count , 
                    'reply_count': response_count,
                    'like_count':recommend_count,
                    'category':category}

        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_author_url(author_url)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
