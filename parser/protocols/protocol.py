#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import uuid

class Protocol:
    name = 'protocol'

    def __init__(self, name):
        self.data = {
            'proto': name,
            'proto_id': uuid.uuid4().hex
        }

    def get_proto_name(self):
        return self.data['proto']

    def get_proto_id(self):
        return self.data['proto_id']
           
    def set_proto_id(self, _id):
        self.data['proto_id'] = _id   
        return self

    def set(self, attr, value):
        self.data[attr] = value

    def get(self, attr):
        return self.data.get(attr, None)

    def to_dict(self):
        return self.data.copy()

    def __str__(self):
        return json.dumps(self.data, ensure_ascii=False)

    def parse(self, j):
        protocol = json.loads(j)
        if 'proto' not in protocol:
            err_msg = 'proto not fould'
            raise Exception(err_msg)
            
        if protocol['proto'] != self.data['proto']:
            err_msg = 'Protocol name is not %s' % self.data['proto']
            raise Exception(err_msg)

        self.data.update(protocol)
        return self

    def update(self, data):
        self.data.update(data)
        return self


