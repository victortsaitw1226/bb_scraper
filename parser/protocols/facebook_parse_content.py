#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class FACEBOOK_PARSE_CONTENT(Protocol):

    def __init__(self):
        super(FACEBOOK_PARSE_CONTENT, self).__init__('FACEBOOK_PARSE_CONTENT')
        self.update({
          'post_url': '',
          'content': ''
        })

    def set_post_url(self, url):
        self.set('post_url', url)

    def get_post_url(self):
        return self.get('post_url')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
