# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class BbcSpider(RedisSpider):
    name = "bbc"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "bbc",
            "name": "bbc",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.bbc.com/ukchina/trad/horizon",
            "scrapy_key": "bbc:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        links = self.parse_links(soup)
        print('length of links:',len(links))
        for url in links:
            yield scrapy.Request(url['url'],
                    callback=self.parse_article)

    def parse_links(self,soup):
        news_body =  soup.find('div',class_ = 'column--primary')
        post_entries = set([p.attrs['href'] for p in news_body.find_all('a')])
        links = []
        for post in post_entries:
            if post[0] == '/':
                post = 'https://www.bbc.com' + post
            meta = {'url': post,'metadata': {}}
            links.append(meta)
        return links
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'bbc'
        item['proto'] = 'BBC_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):         
        s = soup.find(class_ = 'mini-info-list__item').text   
        try:
            latest_post_date = datetime.strptime(s, '%Y年 %m月 %d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            t = {"天前":" days ago", "小時前":" hours ago", "分鐘前":" minutes ago", "秒前":" seconds ago"}
            for key in t:
                if key in s:
                    s = s.replace(key, t[key])
            parsed_s = [s.split()[:2]]
            time_dict = dict((fmt,float(amount)) for amount,fmt in parsed_s)
            dt = timedelta(**time_dict)
            latest_post_date = (datetime.now() - dt).strftime("%Y-%m-%dT%H:%M:%S+0800")
        return latest_post_date
    
    def parse_title(self,soup):
        return soup.find('h1', class_ = 'story-body__h1').text
    
    def parse_author(self,soup):
        try:
            author = soup.find('span','byline__name').text
        except:
            author = ''
        return author
    
    def parse_content(self,soup):
        for script in soup.find_all('span', src=False):
            script.decompose()
        content = ' '.join([c.text for c in soup.find(class_ = 'story-body__inner').find_all(['p', 'h2'])])
        return content

    def parse_metadata(self,soup):
        try: 
            category = soup.find(property = 'og:site_name').attrs['content'] + '-' + \
                        soup.find(property = 'article:section').attrs['content'] 
        except AttributeError:
            category = ''

        try: 
            keywords = soup.find(class_ = 'tags-list').text.split()
        except AttributeError:
            keywords = []
        
        return {'category': category,
                'tag': keywords}
    