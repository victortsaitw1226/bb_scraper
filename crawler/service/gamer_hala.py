#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
import traceback
import requests
import json
import sys


class GAMER_HALA:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def on_MNG2CRW_NTF_GAMER_HALA_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_GAMER_HALA_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:

            html = self.goto(url)
            content = html
            
            # coombine pages
            soup = BeautifulSoup(html, 'html.parser')
            nxt = soup.find('div',{'id':'BH-pagebtn'}).find('a','next')
            
            while 'href' in str(nxt):
                html = self.goto('https://forum.gamer.com.tw/C.php' + nxt['href'] )
                content = content + html
                soup = BeautifulSoup( html, 'html.parser')
                nxt = soup.find('div',{'id':'BH-pagebtn'}).find('a','next')
            
            # more comment snB
            more_comment_api_json = []
            soup = BeautifulSoup(content, 'html.parser')
            comment_sec = soup.find_all('div','c-section__side')
            comment_sec = [x.parent['id']  for x in comment_sec]
            comment_sec_id = [x.replace('post_','') for x in comment_sec]
            for x in comment_sec_id:
                more_commt_url = 'https://forum.gamer.com.tw/ajax/moreCommend.php?bsn=60076&snB=' + x
                more_comment_api_json.append( json.loads(self.goto(more_commt_url)) )

            html = {'html': content, 'more_comment': more_comment_api_json, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
