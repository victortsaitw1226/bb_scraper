#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
import re
import sys
import time
from  datetime import datetime, timedelta

from six.moves import urllib
from bs4 import BeautifulSoup
sys.path.insert(0,'..')
from protocols.crw2par_ntf_fb_post_content import CRW2PAR_NTF_FB_POST_CONTENT
from protocols.crw2par_ntf_fb_post_urls import CRW2PAR_NTF_FB_POST_URLS
from protocols.mng2crw_ntf_fb_get_fan_page import MNG2CRW_NTF_FB_GET_FAN_PAGE
from protocols.mng2crw_ntf_fb_get_post import MNG2CRW_NTF_FB_GET_POST

class FACEBOOK:

    def __init__(self, server):
        self.server = server
   
    '''
    post content and comments
    '''
    def crawl_post_urls(self, url, days_limit):
        #self.check_ip()
        post_urls = []
        self.goto_and_validate_url(url)
        self.server.browser.scrollDown(15)
        soup = BeautifulSoup(self.server.browser.html(), 'html.parser')

        if soup is None:
            self.server.log.info('crawl_post_urls:html is empty:{}'.format(url))
            return post_urls

        profile_id = soup.find('meta', {'property': 'al:ios:url'}).get('content')
        profile_id = profile_id.split('fb://page/?id=')[1]
        
        #posts = soup.findAll('div', {'role': 'article'})
        posts = soup.findAll('div', class_= re.compile('userContentWrapper'))

        # Loop All Posts
        for post in posts:
            #content_wrapper = post.find('div', class_= re.compile('userContentWrapper'))
            #if not content_wrapper:
            #    self.server.log.error('[FACEBOOK.crawl_post_urls]content_wrapper is None:{}'.format(url))
            #    continue

            timestamp_content = post.find('span', class_='timestampContent')

            if timestamp_content is None:
                self.server.log.error('[FACEBOOK.crawl_post_urls]timstampContent is None:{}'.format(url))
                continue

            timestamp_content = timestamp_content.parent

            if timestamp_content is None:
                self.server.log.error('[FACEBOOK.crawl_post_urls]timstampContent.parent is None:{}'.format(url))
                continue

            post_utime = timestamp_content.get('data-utime')
            post_utime = int(post_utime)
            post_datetime = datetime.fromtimestamp(post_utime)
            if post_datetime < datetime.now() - timedelta(seconds=days_limit):
                continue
        
            comment_item = post.find('form', class_='commentable_item')
            post_tag = comment_item.find('input', {'name': 'ft_ent_identifier'})
            if post_tag is None:
                self.server.log.error('[FACEBOOK.crawl_post_urls]post_tag is None:{}'.format(url))
                continue

            post_id = post_tag.get('value')
            post_url = 'https://www.facebook.com/{profile}/posts/{post}'.format(profile=profile_id, post=post_id)
            post_urls.append({
                'post_url': post_url, 
                'post_utime': post_utime,
                'display_time': post_datetime.isoformat()
            })

        print(post_urls)
        return post_urls


    def check_ip(self):
        ip = self.server.browser.check_my_ip()


    def crawl_content(self, url):
        # self.check_ip()
        self.goto_and_validate_url(url)
        soup = BeautifulSoup(self.server.browser.html(), 'html.parser')

        if soup is None:
            raise Exception('[FACEBOOK.crawl_content]:html is empty:{}'.format(url))

        comment_table = soup.find('form', class_ = 'commentable_item')

        if comment_table is None:
            return self.server.browser.html()
            #raise Exception('crawl_content:comment_table is None:{}'.format(url))

        comment_btn = comment_table.find('a', {'data-testid': 'UFI2CommentsCount/root'})
        if comment_btn:
            element_xpath = self.server.browser.beautifulsoup_to_xpath(comment_btn)
            btn = self.server.browser.find_element_by_xpath(element_xpath)
            self.server.browser.click(btn)

        for i in range(10):
            self.server.browser.scrollDown()
            soup = BeautifulSoup(self.server.browser.html(), 'html.parser')

            if soup is None:
                self.server.log.error('[FACEBOOK.crawl_content]:scroll_down:html is empty:{}'.format(url))
                continue

            comment_table = soup.find('form', class_ = re.compile('commentable_item'))

            sub_comments = comment_table.find_all('a', {'data-testid': 'UFI2CommentsPagerRenderer/pager_depth_1'})
            sub_comments = sub_comments[:10]
            for sub_comment in sub_comments:
                element_xpath = self.server.browser.beautifulsoup_to_xpath(sub_comment)
                btn = self.server.browser.find_element_by_xpath(element_xpath)
                self.server.browser.click(btn)

            next_page = comment_table.find('a', {'data-testid': 'UFI2CommentsPagerRenderer/pager_depth_0'})
            if not next_page:
                break

            element_xpath = self.server.browser.beautifulsoup_to_xpath(next_page)
            btn = self.server.browser.find_element_by_xpath(element_xpath)
            self.server.browser.click(btn)

        return self.server.browser.html()


    def goto_and_validate_url(self, url, max_retry=3):
        for i in range(max_retry):
            ip = self.server.browser.getProxyAddress()
            html = self.server.browser.goto(url, 10, 'div')

            soup = BeautifulSoup(html, 'html.parser')
            div = soup.find('div')
            if div is None:
                self.server.browser.recreate_driver()
                continue

            captcha = soup.find('div', {'id': "captcha"})
            if captcha:
                self.server.browser.recreate_driver()
                continue

            return html
    

    def on_MNG2CRW_NTF_FB_GET_FAN_PAGE(self, packet):
        from_notify = MNG2CRW_NTF_FB_GET_FAN_PAGE()
        from_notify.parse(packet)
        url = from_notify.get_url()
        fan_page = from_notify.get_fan_page()
        days_limit = from_notify.get_days_limit()

        post_urls = self.crawl_post_urls(url, days_limit)

        to_notify = CRW2PAR_NTF_FB_POST_URLS()
        to_notify.set_fan_page(fan_page)
        to_notify.set_post_urls(post_urls)
        self.server.send(to_notify)


    def on_MNG2CRW_NTF_FB_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_FB_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        post_utime = from_notify.get_post_utime()
        fan_page = from_notify.get_fan_page()

        html = self.crawl_content(url)

        to_notify = CRW2PAR_NTF_FB_POST_CONTENT()
        to_notify.set_post_url(url)
        to_notify.set_fan_page(fan_page)
        to_notify.set_post_utime(post_utime)
        to_notify.set_content(html)
        self.server.send(to_notify)
    

if __name__ == '__main__':
    f = Facebook()
    try:
        f.goto('facebook/all', 'https://www.facebook.com/TerryGou1018/posts', None)
    except:
        traceback.print_exc(file=sys.stdout)
    
