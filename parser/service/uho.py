#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class UHO:
    def __init__(self, server):
        print('init uho.py')
        self.server = server

    def on_CRW2PAR_NTF_UHO_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_uho_list(content)
        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_UHO_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_uho_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_uho_list(self, content):

        def parse_next_page(soup):
            if '檢視火線新聞' in soup.text:
                domain_prefix = 'http://www.uho.com.tw/hotnewslist.asp'
            elif '檢視所有文章' in soup.text:
                domain_prefix = 'http://www.uho.com.tw/article.asp'
            postfix = soup.find(class_='PageLink').find_all('a')[-1]['href']
            return domain_prefix + postfix

        def parse_links_dates(soup):
            times = []
            links = []

            if '檢視火線新聞' in soup.text:
                domain_prefix = 'http://www.uho.com.tw'
                for ele in soup.find_all(class_='cont_title'):
                    try:
                        date = ''.join(ele.parent.b.text.split())
                        times.append(datetime.strptime(date, '%Y.%m.%d'))
                    except:
                        times.append(datetime.min)

                    links.append({
                        'url': domain_prefix + ele['href'],
                        'metadata': {}
                    })
            elif '檢視所有文章' in soup.text:
                domain_prefix = 'http://www.uho.com.tw/'
                for ele in soup.find_all(class_='cont_title'):
                    regex = re.search(r'>日期：(.*)</div>-->',
                                    str(ele.parent.parent), re.M | re.S)
                    times.append(datetime.strptime(regex.group(1), '%Y.%m.%d'))

                    links.append({
                        'url': domain_prefix + ele['href'],
                        'metadata': {}
                    })
            return {'time': max(
                times).strftime('%Y-%m-%dT%H:%M:%S+0800'), 'links': links, }

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_uho_content(self, content):
        
        def parse_datetime(soup):
            time = soup.find('div', class_='tilr')
            time = ''.join(time.text.split())
            time = datetime.strptime(time, '日期：%Y.%m.%d')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            author = soup.find('div', class_='cont_dr')
            author = ''.join(author.text.split())
            author = author.replace('◎', '').replace('記者','')
            try:
                author = author.split('／')[0]
            except:
                pass
            try:
                author = author.split('/')[0]
            except:
                pass
            return author

        def parse_title(soup):
            title = soup.find('div', class_='till')
            title = ''.join(title.text.split())
            title = title.replace('》', '')
            return title

        def parse_content(soup):
            return ''.join(soup.find('div', class_='cont_all').text.split())

        def parse_metadata(soup, fb_like_soup=None):
            category = soup.find('h1','addr').find_all('a','addr_f')[-1].text
            return {
                # 'fb_like_count': content['extra_content']['like_num'],
                # 'you_may_also_want_to_know': content['extra_content']['keyword'],
                # 'you_may_also_want_to_know': None,
                'category':category,
                # 'fb_like_count': fb_like_soup.find('span',{'id':'u_0_2'}).text,
            }

        def parse_comments(soup):
            return []

        soup = BeautifulSoup(content['html'], 'html.parser')
        # fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(soup))
        item.set_metadata(parse_metadata(soup))
        return item
