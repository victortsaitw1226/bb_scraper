# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class WownewsSpider(RedisSpider):
#class WownewsSpider(scrapy.Spider):
    name = "wownews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "wownews",
            "name": "wownews",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "http://api.wownews.tw/f/pages/site/558fd617913b0c11001d003d?category=5590a737f0a8bf110060914e&children=true&limit=48&page=10",
            "scrapy_key": "wownews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        res = json.loads(response.body) 
        max_page = max(res["pages"])  

        next_page = self.parse_next_page(response.url ,max_page)
        latest_datetime, links = self.parse_latest_datetime_and_links(res["results"])

        for url in links:
            url_head = 'http://api.wownews.tw/f/pages/site/558fd617913b0c11001d003d/slug/'
            yield scrapy.Request(url_head + url.split('/')[-1],
                    meta = {'website_url':url},
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return
            
        if next_page is None:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_latest_datetime_and_links(self, items):
        dt_list = []
        links = []
        html_base = "http://www.wownews.tw/newsdetail/"
        
        for item in items:
            links.append(html_base +item["category"][1]["desc"]+"/"+ item["slug"])
            dt = datetime.strptime(item["publish_start"][:-1],"%Y-%m-%dT%H:%M:%S.%f")#"2019-06-11T20:18:15.114Z"
            dt_list.append(dt+ timedelta(hours = 8))

        latest_datetime = max(dt_list)
        return latest_datetime, links

    def parse_next_page(self, url, max_page):
        re_list = re.search(r'page=(\d+)', url).group().split("=")
        cur_page_num = int(re_list[1])
        if cur_page_num < max_page:
            re_list[1] = str(cur_page_num + 1)
            next = "=".join(re_list)
            return url.replace(re.search('page=(\d+)', url).group(), next)
        else:
            return None
    

    def parse_article(self, response):
        raw = json.loads(response.body)

        item = NewsItem()
        item['url'] = response.meta['website_url']
        item['author'] = raw['author']
        item['article_title'] = raw['title']
        item['author_url'] = []
        item['content'] = self.parse_content(raw)
        item['comment'] = []
        item['date'] = self.parse_datetime(raw)
        item['metadata'] = self.parse_metadata(raw)
        item['content_type'] = 0
        item['media'] = 'wownews'
        item['proto'] =  'WOWNEWS_PARSE_ITEM'
        return item

    def parse_datetime(self, raw):
        time_ = raw['createdAt']
        dt = datetime.strptime(time_.split('.')[0], "%Y-%m-%dT%H:%M:%S")
        dt = dt + timedelta(hours = 8)
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_content(self, raw):
        soup = BeautifulSoup(raw['content'], 'lxml')
        content = soup.text
        content = ' '.join(content.split())
        return content

    def parse_metadata(self, raw):
        category = raw['category'][1]['name']
        return {'category': category}

