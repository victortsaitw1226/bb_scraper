# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
import datetime
from .redis_spiders import RedisSpider

#class Mobile01Spider(scrapy.Spider):
class Mobile01Spider(RedisSpider):
    name = 'mobile01'
    allowed_domains = ['https://www.mobile01.com']
     
    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            'url': 'https://m.mobile01.com/forumtopic.php?c=16&sort=topictime',
            'category': '手機',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=17&sort=topictime',
            'category': '電腦',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=18&sort=topictime',
            'category': '旅遊美食',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=19&sort=topictime',
            'category': '筆電',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=20&sort=topictime',
            'category': '相機',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=21&sort=topictime',
            'category': '汽車',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=23&sort=topictime',
            'category': '遊戲',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=24&sort=topictime',
            'category': '單車',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=25&sort=topictime',
            'category': '生活',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=26&sort=topictime',
            'category': '居家',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=27&sort=topictime',
            'category': '女性',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=28&sort=topictime',
            'category': '影音',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=29&sort=topictime',
            'category': '機車',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=30&sort=topictime',
            'category': '蘋果',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=31&sort=topictime',
            'category': '時尚',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 *5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=33&sort=topictime',
            'category': '運動',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        }, 
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=35&sort=topictime',
            'category': '閒聊',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        },
        {
            'url': 'https://m.mobile01.com/forumtopic.php?c=36&sort=topictime',
            'category': '時事',
            'interval': 3600 * 4,
            'media': 'mobile01',
            'name': 'mobile01',
            'enabled': True,
            'days_limit': 3600 * 24 * 5,
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        interval = response.meta['days_limit']
        current_page = 1
        total_page = 1
        soup = BeautifulSoup(response.body, 'html.parser')
        current_page_node = soup.find('input', {"id": 'jump_page'})
        if current_page_node:
            current_page = int(current_page_node.get('value'))
        total_page_node = soup.find('span', class_='c-pagination__pager')
        if total_page_node:
            total_page = int(total_page_node.get_text())

        for div in soup.find_all('div', class_='c-articleItem'):
            title = div.find('div', class_='c-articleItem__title')
            url = title.find('a').get('href')
            date_node = div.find_all('div', class_='c-articleItemRemark__wAuto')[1]
            try:
                date = date_parser(date_node.get_text())
            except:
                continue
            announcement_node = div.find('span', class_='o-hashtag o-hashtag--mini o-hashtag--default')
            if announcement_node:
                continue
            highlight_node = div.find('span', class_='o-hashtag o-hashtag--mini o-hashtag--heightLight')
            if highlight_node:
                continue
            if datetime.datetime.now() > (date + datetime.timedelta(seconds=interval)):
                return
            yield response.follow(url,
                    dont_filter=True,
                    callback=self.parse_article,
                    meta=response.meta)

        if current_page >= total_page:
            return

        _next = '&p={}'.format(current_page + 1)
        url = ''.join([response.url.split('&p=')[0], _next])
        yield scrapy.Request(url,
            callback=self.parse,
            dont_filter=True,
            meta=meta)

    def parse_article(self, response):
        meta = response.meta
        current_page = 1
        total_page = 1
        soup = BeautifulSoup(response.body, 'html.parser')

        title_node = soup.find('div', class_='l-heading__title')
        title = title_node.get_text().strip()
        current_page_node = soup.find('input', {"id": 'jump_page'})
        if current_page_node:
            current_page = int(current_page_node.get('value'))
        total_page_node = soup.find('span', class_='c-pagination__pager')
        if total_page_node:
            total_page = int(total_page_node.get_text())

        for sub_article in soup.find_all('div', class_='l-mainArticle'):
            item = NewsItem()
            item['metadata']= { 'category': meta['category'] }
            item['content_type'] = 1
            item['article_title'] = title
            item['url'] = response.url
            item['author_url'] = []
            author_node = sub_article.find('a')
            item['author'] = author_node.get_text().strip()
            article_node = sub_article.find('article', class_='l-publishArea')
            quote_node = article_node.find('blockquote')
            if quote_node:
                quote_node.extract()
            item['content'] = article_node.get_text()
            article_tag = sub_article.find('div', class_='l-articleAuthor__tag')
            date_node = article_tag.find('div', class_='u-gapTop--sm')
            item['date'] = str(date_parser(date_node.get_text()))
            toolbar_node = sub_article.find('div', class_='l-mainArticle__tool')
            view_node = toolbar_node.find('div', class_='c-icon c-icon--fsm c-icon c-icon--view')
            item['metadata']['view_count'] = 0
            if view_node:
                item['metadata']['view_count'] = int(view_node.get_text())
                item['content_type'] = 0
            like_node = toolbar_node.find('label', class_='c-tool__check toolclap')
            item['metadata']['like_count'] = int(like_node.get_text())
            item['media'] = 'mobile01'
            item['proto'] = 'PTT_PARSE_ITEM'
            yield item

        if current_page >= total_page:
            return

        _next = '&p={}'.format(current_page + 1)
        url = ''.join([response.url.split('&p=')[0], _next])
        yield scrapy.Request(url,
            callback=self.parse_article,
            dont_filter=True,
            meta=meta)
