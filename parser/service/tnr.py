#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime
from utils.parse_fb_comment import parse_comments

class TNR:
    def __init__(self, server):
        print('init tnr.py')
        self.server = server

    def on_CRW2PAR_NTF_TNR_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_tnr_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TNR_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_tnr_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_tnr_list(self, content):

        tnr = 'http://tnr.com.tw'
        soup = BeautifulSoup(content, "lxml")
        
        try:
            next_page = tnr + soup.find('a', text='»')['href']
        except:
            next_page = None
            
        table = soup.find('ul', 'other')
        for div in table.findAll('li',{'class':'liAD'}): 
            div.decompose()
        links = [{'url':tnr+'/'+s.find('a')['href'],'metadata':[]} for s in table.findAll('li')]

        link_date = [datetime.strptime(s.text.split(' ')[-1], '%Y/%m/%d') for s in table.findAll('span','date')]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_tnr_content(self, content):

        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        date = datetime.strptime(soup.find('div','date').text.strip().split('\n')[-1], '%Y/%m/%d').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find("meta",  property="og:title")['content'].split(' - ')[0].replace('\xa0','')
        author = soup.find('span','who').text
        if len(author.split('/')[-1])>4:
            author = author.split('/')[-1].split(' ')[-1]
        else:
            author = author.split('/')[-1].replace(' ','')
        
        content = soup.find('div','txt')
        for s in content.findAll('p'):
            if '圖：' in s.text:
                s.decompose()
        content = content.text.replace('\xa0','')
        
        #metadata
        view = soup.find('div','left').find('span','read').text
        like = soup.find('div','left').find('span','like').text
        category = soup.find('span','mk').text
        metadata = {'like_count':like, 'view_count':view, 'category':category}

        comment = parse_comments(comment_html)
            
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
