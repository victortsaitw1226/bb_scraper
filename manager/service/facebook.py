#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from protocols.par2mng_ntf_fb_post_url import PAR2MNG_NTF_FB_POST_URL
from protocols.mng2crw_ntf_fb_get_fan_page import MNG2CRW_NTF_FB_GET_FAN_PAGE
from protocols.mng2crw_ntf_fb_get_post import MNG2CRW_NTF_FB_GET_POST

class FACEBOOK:
        
    url_set = set()
    delay_q = []

    def __init__(self, server):
        self.server = server


    def send_index(self, **kwargs):
        url = kwargs['url']
        name = kwargs['name']
        days_limit = kwargs['days_limit']

        init_ntf = MNG2CRW_NTF_FB_GET_FAN_PAGE()
        init_ntf.set_url(url)
        init_ntf.set_fan_page(name)
        init_ntf.set_days_limit(days_limit)
        self.server.send(init_ntf)

    def on_PAR2MNG_NTF_FB_POST_URL(self, packet):
        notify = PAR2MNG_NTF_FB_POST_URL()
        notify.parse(packet)
        fan_page = notify.get_fan_page()
        url = notify.get_url()
        post_utime = notify.get_post_utime() 

        request = MNG2CRW_NTF_FB_GET_POST()
        request.set_fan_page(fan_page)
        request.set_url(url)
        request.set_post_utime(post_utime)
        self.server.send(request)
