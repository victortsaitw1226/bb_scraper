#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime


class GREATNEWS:
    def __init__(self, server):
        print('init greatnews.py')
        self.server = server

    def on_CRW2PAR_NTF_GREATNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_greatnews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_GREATNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_greatnews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_greatnews_list(self, content):

        def parse_next_page(html_bs4):
            now_page = html_bs4.find('a', class_ = 'look') 
            next_page = now_page.attrs['href'][:-len(now_page.text)] + str(int(now_page.text)+1)
            return 'http://greatnews.com.tw/home/news_page.php' + next_page
        
        def parse_latest_date(html_bs4):
            
            latest_post_date = html_bs4.find('div', class_ = 'news_list_in_date').text
            latest_post_date = datetime.datetime.strptime(latest_post_date, '%Y/%m/%d').strftime('%Y-%m-%dT%H:%M:%S+0800')        
            return latest_post_date
        
        def parse_link(html_bs4):
            
            def parse_list_meta(post):
                ''' parse url and metadata of news list '''
                
                tmp = post.find('a').attrs['href']
                if tmp[:4] == 'news':
                    tmp = 'http://greatnews.com.tw/home/' + tmp
                
                meta = {
                    'url': tmp,
                    'metadata': {}
                }
                return meta
            
            post_entries = html_bs4.find_all('div', class_ = 'news_list_in_title')
        
            list_meta = []
            for post in post_entries:
                    list_meta.append(parse_list_meta(post))
            
            return list_meta

        clean_html = BeautifulSoup(content, 'lxml')
        news_body =  clean_html.find('div',id = 'news_are')
        
        next_p = parse_next_page(news_body)
        latest_post_date = parse_latest_date(news_body)
        list_meta = parse_link(news_body)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_p)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)
        return item

    def parse_greatnews_content(self, content):
        
        def parse_time(clean_html):
            date_org = clean_html.find('div', class_ = 'newsin_date').text
            date = datetime.datetime.strptime(date_org, '%Y/%m/%d ').strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date_org, date
        
        def parse_title(clean_html):
            return ''.join(clean_html.find('div', class_ = 'newsin_title').text.split())
        
        def parse_content_author(clean_html, date_org):
            content = ''.join([ent for ent in clean_html.find(class_ = 'newsin_text').text.replace(date_org, '').split()])
            
            reg = re.compile(r'[【(（]\D+[／/╱]?\D*[】)）]', re.VERBOSE)
            tmp = reg.findall(content[:51])   
            
            content = content.replace(tmp[0], '')
            
            ind_auth = tmp[0].find('記者')
            if ind_auth != -1:
                author = list(filter(None, re.split('╱|/|／', tmp[0][(ind_auth+2):-5])))
                author = ','.join([a for a in author])
            else:
                tmp = re.sub(r'[【（(]', '', tmp[0])
                author = re.sub(r'[】)）]', '', tmp)
            return content, author   
  
        def parse_metadata(clean_html):
            metadata = {'category':''}
            try: 
                metadata['category'] = clean_html.find(class_ = 'bn_titleA').text   
            except AttributeError:
                pass 
            return metadata
   

        # detail
        clean_html = BeautifulSoup(content['detail_html'], 'lxml')   
        url = content['url']
    
        date_org, date = parse_time(clean_html)
        title = parse_title(clean_html)
        content, author = parse_content_author(clean_html, date_org) 
        metadata = parse_metadata(clean_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
