#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_REQ_RESULT(Protocol):

    def __init__(self):
        super(CRW2PAR_REQ_RESULT, self).__init__('CRW2PAR_REQ_RESULT')
        self.update({
          'content': '',
        })

    def set_content(self, content):
        self.set('content', content)
