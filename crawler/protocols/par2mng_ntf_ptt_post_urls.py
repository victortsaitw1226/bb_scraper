#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_PTT_POST_URLS(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_PTT_POST_URLS, self).__init__('PAR2MNG_NTF_PTT_POST_URLS')
        self.update({
          'urls': [],
        })

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')
