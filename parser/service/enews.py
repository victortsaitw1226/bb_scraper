#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments

class ENEWS:
    def __init__(self, server):
        print('init enews.py')
        self.server = server

    def on_CRW2PAR_NTF_ENEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_enews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ENEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_enews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_enews_list(self, content):
        def parse_links(soup):
            links = soup.find_all('div','articleTitle')
            links = [ {'url':enews + li.find('a')['href'],'metadata':[]} for li in links]  
            return links
        
        def parse_next_page(soup):
            page_bar = soup.find('div',{'id':'pageBox'})
            try:
                next_page = enews + page_bar.find('div','next').find('a')['href']
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find_all('div','ArticleInfo')
            link_date = []
            for date in pub_date:
                date = ''.join(date.text.split())
                date = re.split('\|',date)[1]
                date = datetime.strptime( date , '%Y-%m-%d')
                link_date.append(date)
            # latest_post_date
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        list_html = BeautifulSoup(content, 'html.parser')
        enews = 'https://enews.tw'
        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_enews_content(self, content):
        
        def parse_title(soup):
            title = soup.find('head').find('meta',{'property':"og:title"})['content']
            title = ' '.join(title.split())
            return title
        
        def parse_date(soup):     
            date = soup.find('div',{'id':'contentInfo'}).find_all('span')[1].text
            date = datetime.strptime( date , '%Y-%m-%d')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_author(soup):
            author = soup.find('div',{'id':'contentInfo'}).find_all('a')[0].text
            author = ' '.join(author.split())
            return author
        
        def parse_content(soup):
            content = soup.find('div',{'id':'contentWrap'})
            content = ' '.join(x.text for x in content.find_all('p'))
            content = '\n'.join(content.split())
            return content
        
        def parse_browse_num(soup):
            browse_num = soup.find('div',{'id':'contentInfo'}).find('span',{'id':'view_count'}).text
            return browse_num
        
        

        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        fb_share_count_html =  BeautifulSoup(content['fb_share_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        title = parse_title(detail_html)
        date = parse_date(detail_html) 
        author = parse_author(detail_html)
        content = parse_content(detail_html)
        browse_num = parse_browse_num(detail_html)
        fb_like_count = fb_like_count_html.find('span',{'id':'u_0_1'}).text
        fb_share_count = fb_share_count_html.find('span',{'id':'u_0_1'}).text
        metadata = {'view_count':browse_num ,'fb_like_count':fb_like_count,'fb_share_count':fb_share_count}
        comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
