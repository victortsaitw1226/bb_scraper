# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class PeopoSpider(RedisSpider):
    name = "peopo"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "peopo",
            "name": "peopo",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600 * 2,
            "url": "https://www.peopo.org/list/post/all/all/all?page=0",
            "scrapy_key": "peopo:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        datetimes = []
        links = soup.findAll('h3', 'view-list-title')
        for article in soup.find_all('div', class_=re.compile('views-row')):
            link = article.find('h3', 'view-list-title')
            href = link.find('a')['href']

            post_date = article.find('div', class_='view-list-date')
            datetimes.append(datetime.strptime(post_date.get_text(), '%Y-%m-%d %H:%M'))

            yield response.follow(href,
                    meta = meta,
                    callback=self.parse_article)


        latest_datetime = max(datetimes)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return


        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page_href = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield response.follow(next_page_href,
                dont_filter=True,
                meta=meta,
                callback=self.parse)
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')

        date = datetime.strptime(soup.find('div','submitted').text, '%Y.%m.%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find("meta",  property="og:title")['content'].strip().replace(u'\u3000',u' ')
        author = soup.find('div','user-infos floatleft').text.split('\n')[1]
        content = ''.join([s.text for s in soup.find('div','post_text_s').findAll('p')]).replace('\xa0','').replace('\n\t','')

        item = NewsItem()
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = title
        item['author_url'] = []
        item['content'] = content
        item['comment'] = self.peopo_comments(soup)
        item['date'] = date
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'peopo'
        item['proto'] =  'PEOPO_PARSE_ITEM'
        return item


    def parse_metadata(self, soup):
        keywords = ''.join([s.text for s in soup.find(id='node-terms').findAll('ul', {'class','inline'})])[:-1].split('\n')
        category = soup.find(id='node-terms').findAll('div', {'class','field-items'})[1].findAll('a')[0].text
        view = soup.find('div','amount amount-view').text[:-2]
        share = soup.find('div','amount amount-comment').text[:-2]
        peopo_vote_count = soup.find('span','vote-amount').text
        peopo_comment_count = soup.find('div','dialog-number').text
        try:
            video_url = re.split('src:  "|" }',str(soup.find(id='h5p').find('script')))[1]
        except:
            video_url = ''
    
        return {
            'tag': keywords,
            'category':category,
            'view_count': view,
            'share_count': share,
            'like_count':peopo_vote_count,
            'reply_count':peopo_comment_count,
            'video_url': video_url, 
            'fb_like_count':''
        }
    

    def peopo_comments(self, soup):
        return []
        comments = []
        comment_author = [s.find('a')['title'] for s in soup.findAll('div','user-picture')]
        comment_author_url = ['https://www.peopo.org'+s.find('a')['href'] for s in soup.findAll('div','user-picture')]
        comment_content = [s.find('p').text for s in soup.findAll('div','comment-inner')]
        comment_datetime = [s.text.split(' 於 ')[1] for s in soup.findAll('div','s-lt floatleft')]
        comment_datetime = [datetime.strptime(s, '%Y/%m/%d - %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800') for s in comment_datetime]
        for cont, auth, auth_url, date in zip(comment_content,comment_author,comment_author_url,comment_datetime):
            comments.append({
                'comment_id':'',
                'datetime':date,
                'author':auth,
                'author_url':auth_url ,
                'content':cont , 
                'metadata':{},
                'source':'peopo',
                'comments':[]
            })
        return comments
