#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
# from utils.parse_fb_comment import parse_comments

class CHINATIMES:
    def __init__(self, server):
        print('init chinatimes.py')
        self.server = server

    def on_CRW2PAR_NTF_CHINATIMES_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_chinatimes_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CHINATIMES_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_chinatimes_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_chinatimes_list(self, content):

        chinatimes = 'https://www.chinatimes.com'
        soup = BeautifulSoup(content, "html.parser")
        
        try:
            next_page = chinatimes + soup.find('ul','pagination').find('a',text = '下一頁')['href']
        except:
            next_page = None
        
        links = [{'url':chinatimes + s.find('a')['href'],'metadata':[]} for s in soup.findAll("h3", {"class":"title"})]
        link_date = [datetime.strptime(s['datetime'], '%Y-%m-%d %H:%M') for s in soup.findAll("time")]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
       
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_chinatimes_content(self, content):
        
        def parse_date(soup):
            date = soup.find('div','meta-info').find('time')['datetime']
            date = datetime.strptime( date , '%Y-%m-%d %H:%M')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        author = re.findall('\S',detail_html.find('div','author').text)
        author = ''.join([x for x in author ]) 
        date = parse_date(detail_html)
        title = detail_html.find('h1','article-title').text
        title = ' '.join(title.split())
        content = detail_html.find('head').find('meta',{'name':'description'})['content']
        keywords = detail_html.find('div','article-hash-tag').find_all('span','hash-tag')
        keywords = [x.text.replace('#','') for x in keywords]
        category = detail_html.find('meta',{'property':'article:section'})['content']
        metadata = {'tag': keywords, 'category':category}
        # comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        # item.set_comment(comment)
        item.set_metadata(metadata)
        return item
