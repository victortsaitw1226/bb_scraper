#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, time
import json
import time



class POPDAILY:
    def __init__(self, server):
        print('init popdaily.py')
        self.server = server

    def on_CRW2PAR_NTF_POPDAILY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_popdaily_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_POPDAILY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_popdaily_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_popdaily_list(self, content):        

        js = content['js']
        url = content['url'] 
        
        data = {
            'next_page': None,
            'latest_post_date': None,
            'links': []
        }
        
        prefix = 'https://www.popdaily.com.tw'
        
        js = json.loads(js)
        article_list = []
        for i in range(len(js['list'])):
            postID = js['list'][i]['postID'].split('.')
            postID.insert(0, prefix)
            article_list.append({
                'url': ('/').join(postID),
                'newScore': js['list'][i]['newScore'],
                't': js['list'][i]['t'],
            })
        # Get Latest Post Date
        sec = article_list[0]['t']
        data['latest_post_date'] = datetime.fromtimestamp(int(sec/1000)).strftime("%Y-%m-%dT%H:%M:%S+0800")

        # Get Articles url
        for i in article_list:
            data['links'].append({
                'url': i['url'],
                'metadata': {}
            })

        # Get Next Page
        data['next_page'] = {'page': article_list[-1]['t'], 'score': article_list[0]['newScore'],
                            'url':url }
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'] )
        item.set_urls(data['links'])
        return item

    def parse_popdaily_content(self, content):

        def parse_datetime(js):
            timestamp = js['datePublished'].split('.')[0]
            datetime = time.strptime(timestamp, '%Y-%m-%dT%H:%M:%S')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800', datetime)

        def parse_metadata(js):
            return js['keywords']

        def parse_author_url(html):
            prefix = 'https://www.popdaily.com.tw/user/'
            ind = html.find('%22%2C%22name')
            return [prefix + html[(ind-6):ind]]
        
        def parse_author(js):
            return ','.join(js['author']) 
            
        def parse_title(js):
            return js['headline'].split('｜')[0]

        def parse_content(js):
            return js['articleBody']

        soup = BeautifulSoup(content['html'], 'lxml')

        url = content['url']
        js = soup.findAll('script', type='application/ld+json')[-1]
        js = json.loads(js.text)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(js))
        item.set_author(parse_author(js))
        item.set_author_url(parse_author_url(content['html']))
        item.set_date(parse_datetime(js))
        item.set_content(parse_content(js))
        #item.set_comment([])
        item.set_metadata({'tag': parse_metadata(js), 'category':url.split('/')[-2]})
        
        return item
