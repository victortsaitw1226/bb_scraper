#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class SETN:
    def __init__(self, server):
        print('init setn.py')
        self.server = server

    def on_CRW2PAR_NTF_SETN_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_setn_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_SETN_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_setn_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_setn_list(self, content):

        def parse_next_page(soup):
            prefix_html = 'https://www.setn.com/'
            suffix_url = soup.find("li", "active").find("a")["href"]
            current_page = int(re.search("p=(\d+)", suffix_url).group(1))
            suffix_url = re.sub("p=(\d)", "p={}".format(current_page + 1), suffix_url)
            next_page = prefix_html + suffix_url
            return next_page

        def parse_links_dates(soup):
            prefix_html = 'https://www.setn.com/'
            data = {'next_page': None, 'latest_post_date': None, 'links': []}
            links = []
            dt_list = []

            for ind in soup.find_all("div", "row NewsList"):
                for time_ in ind.find_all("time", {"style": "color: #a2a2a2;"}):
                    year = datetime.today().year
                    dt = datetime.strptime(str(year) + time_.text, '%Y%m/%d %H:%M')
                    dt_list.append(dt)
                for url_ in ind.find_all("a", "gt"):
                    suffix = url_["href"]
                    if re.search("https",suffix):
                        link = suffix
                    else:
                        link = prefix_html + suffix
                    links.append({"url": link, "matadata": {}})

            data["links"] = links
            data["time"] = max(dt_list).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return data

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_setn_content(self, content):
        
        def parse_datetime(soup):
            published_time = soup.find(
                "meta", {"property": "article:published_time"})["content"]
            published_time = published_time + "+0800"
            return published_time

        def parse_author(soup):
            try:
                p1 = soup.find("div", {"id": "Content1"}).find("p").text
                writer = re.search("記者([\u4E00-\u9FFF]+)", p1).group(1)
            except:
                writer = ''
            return writer

        def parse_title(soup):
            return soup.h1.text.strip().replace(u'\u3000',
                                    u' ').replace(u'\xa0', u' ')

        def parse_content(soup):
            result = ""
            content = soup.find("div",{"itemprop":"articleBody"}).find_all("p")
            for ind in range(1, len(content), 1):
                if not re.search("▲", content[ind].text):
                    result += content[ind].text

            return result

        def parse_metadata(soup ,fb_like_soup=None):
            keys = []
            try:
                updated_time = soup.find(
                    "meta", {"property": "article:modified_time"})["content"]
                # print(updated_time)
                updated_time = updated_time + "+0800"

            except:
                updated_time = ''
            try:
                for tag in soup.find_all("strong"):
                    keys.append(tag.text)
            except:
                pass
            #fb_like_count = fb_like_soup.find('span',{'id':'u_0_3'}).text
            category = soup.find('meta',{'property':'article:section'})['content']
            return {'tag': keys, "update_time": updated_time, 'category':category,
                    'fb_like_count': ''}


        soup = BeautifulSoup(content['html'], 'html.parser')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        #metadata = parse_metadata(soup, fb_like_count_html)
        metadata = parse_metadata(soup)

        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        #item.set_author_url(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        #item.set_comment(parse_comments(comment_html))
        item.set_metadata(metadata)
        return item
