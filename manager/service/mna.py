#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys, traceback
from datetime import datetime, timedelta
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.news_get_index import NEWS_GET_INDEX

class MNA:
        
    url_set = set()
    delay_q = []

    def __init__(self, server):
        self.server = server

    def send_index(self, **kwargs):
        url = kwargs['url']
        name = kwargs['name']
        days_limit = kwargs['days_limit']

        init_ntf = MNG2CRW_NTF_NEWS_GET_INDEX()
        init_ntf.set_url(url)
        init_ntf.set_days_limit(days_limit)
        init_ntf.set_name(name)
        init_ntf.set_media('mna') ###
        self.server.send(init_ntf)

    def on_PAR2MNG_NTF_MNA_POST_URLS(self, packet):
        notify = PAR2MNG_NTF_NEWS_POST_URLS()
        notify.parse(packet)
        days_limit = notify.get_days_limit()
        media = notify.get_media()

        for element in notify.get_urls():
            request = MNG2CRW_NTF_NEWS_GET_POST()
            request.set_url(element['url'])
            request.set_media(notify.get_media())
            self.server.send(request)

        # Check the latest datetime is less than 3 days
        latest_post_date = notify.get_latest_post_date()
        latest_post_date = datetime.strptime(latest_post_date, "%Y-%m-%dT%H:%M:%S+0800")
        
        #print('media:{}, days_limit:{}'.format(media, days_limit))
        now = datetime.now()
        if latest_post_date < (now - timedelta(days=days_limit)):
            return

        # Fetch the next page
        init_ntf = MNG2CRW_NTF_NEWS_GET_INDEX()
        init_ntf.set_url(notify.get_next_page_url())
        init_ntf.set_media(notify.get_media())
        init_ntf.set_days_limit(days_limit)
        self.server.send(init_ntf)
