#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol
import datetime

class CRW2PAR_NTF_YOUTUBE_COMMENTS(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_YOUTUBE_COMMENTS, self).__init__('CRW2PAR_NTF_YOUTUBE_COMMENTS')
        self.update({
          'channel_url': '',
          'channel': '',
          'video_url': '',
          'video_img': '',
          'content': '',
          'crawl_date': datetime.datetime.now().isoformat()
        })

    def set_channel_url(self, channel_url):
        self.set('channel_url', channel_url)

    def get_channel_url(self):
        return self.get('channel_url')

    def set_channel(self, channel):
        self.set('channel', channel)

    def get_channel(self):
        return self.get('channel')

    def set_video_url(self, video_url):
        self.set('video_url', video_url)

    def get_video_url(self):
        return self.get('video_url')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_video_img(self, video_img):
        self.set('video_img', video_img)

    def get_video_img(self):
        return self.get('video_img')

