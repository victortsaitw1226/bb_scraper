#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import time, sys


class MOBILE01:
    def __init__(self, server):
        print('init mobile01.py')
        self.server = server

    def on_CRW2PAR_NTF_MOBILE01_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_mobile01_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_MOBILE01_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        media = from_notify.get_media()

        item = self.parse_mobile01_content(content)

        result = item.to_dict()
        result['media'] = media
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_mobile01_list(self, content):

        def parse_next_page(soup):
            next_url = content['url'].split('&p=')[0]
            current_page = int(soup.select('.l-content .l-tabulate__action .l-pagination__page.is-active')[0].text)
            last_page = int(soup.select('.l-content .l-tabulate__action .l-pagination__page')[-1].text)
            if current_page < last_page:
                next_page = ('&p=').join([next_url, str(current_page+1)])
            return next_page

        def parse_links_dates(soup):
            data = {
                'latest_post_date': None, 
                'links': []}
            links = []
            dt_list = []
            prefix = 'https://www.mobile01.com/'
            result = soup.select('.l-listTable__td.l-listTable__td--time .o-fNotes')
            for d in result:
                d = time.strptime(d.text, '%Y-%m-%d %H:%M')
                dt_list.append(d)
            
            result = soup.select('.c-listTableTd__title a')
            for l in result:
                href = l.get('href')
                if(len(href.split('&p='))==1):
                    links.append({'url': prefix+href})
            data["links"] = links
            data["time"] = time.strftime('%Y-%m-%dT%H:%M:%S+0800', max(dt_list))
            return data

        soup = BeautifulSoup(content['html'], 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_mobile01_content(self, content):
        
        def parse_datetime(soup):
            date_list = []
            result = soup.select('.l-articlePage .l-navigation .o-fNotes.o-fSubMini')
            for r in result:
                try:
                    time_ = datetime.strptime(r.text, "%Y-%m-%d %H:%M").strftime('%Y-%m-%dT%H:%M:%S+0800')
                    date_list.append(time_)
                except:
                    pass
            return date_list

        def parse_author_url(soup):
            authors_url = []
            try:
                prefix = 'https://www.mobile01.com'
                result = soup.select('.c-authorInfo__id a')
                for a in result:
                    authors_url.append(prefix + a.get('href'))
            except:
                authors_url = []
            return authors_url

        
        def parse_author(soup):
            authors_list = []
            try:
                result = soup.select('.c-authorInfo__id a')
                for a in result:
                    authors_list.append(a.text.split()[0])
            except:
                authors_list = []
            return authors_list


        def parse_title_views(soup):
            title = soup.select('.l-articlePage__publish .l-heading__title .t2')[0].text
            views = soup.select('.c-iconLink .o-fNotes.o-fSubMini')[0].text
            return {'title': title, 'views': views}

        def parse_content(soup):
            content = soup.select('.l-publishArea.topic_article')[0]
            return content.text.strip()

        def parse_comments(soup):
            comment_list = []
            try:
                floor = soup.find_all('div', 'u-gapBottom--max c-articleLimit')
                for i in range(len(floor)):
                    quote = ''
                    if(floor[i].select('blockquote')):
                        quote = floor[i].select('blockquote')[0].text
                    content = floor[i].text.replace(quote, '') # remove quote
                    content = content.strip()
                    comment_list.append(content)
                return comment_list
            except:
                return []

        def parse_metadata(soup):
            likes_list = []
            try:
                result = soup.select('.c-tool .o-fSubMini')
                for r in result:
                    if(r.text.isdigit()):
                        likes_list.append(r.text)
            except:
                return []
            return likes_list

        # main_html = content['html'][0]
        # soup = BeautifulSoup(main_html, 'html.parser')
        # title = parse_title(soup)
        # main_date = parse_datetime(soup)[0]
        # content = parse_content(soup)[0]

        def handle_comments(authors_url_list, authors_list, article_list, date_list, likes_list):
            comments = []
            if(len(authors_list)):
                for i in range(len(authors_list)):
                    comments.append({
                        "comment_id": i + 1,
                        'author':authors_list[i],
                        'author_url': authors_url_list[i],
                        "content": article_list[i],
                        "datetime": date_list[i],
                        "source":'mobile01',
                        "matadata": {'like_count': likes_list[i]},
                        "comments":[],
                    })
            return comments

        # soup = BeautifulSoup(content['html'][0], 'html.parser')
        # urls = get_all_url(soup)
        prefix = 'https://www.mobile01.com'
        date_list = []
        authors_list = []
        authors_url_list = []
        title = ''
        views = None
        article = ''
        likes_list = []
        comment_list = []
        print('len of content:{}'.format(len(content['html'])))
        for i, html in enumerate(content['html']):
            print(i)
            soup = BeautifulSoup(html, 'html.parser')
            if(i==0):
                likes_list += parse_metadata(soup)
                title = parse_title_views(soup)['title']
                views = parse_title_views(soup)['views']
                category = soup.find('meta',{'property':'article:section'})['content']
                article = parse_content(soup)
                date_list += parse_datetime(soup)
            else:
                result = parse_metadata(soup)
                likes_list += result[1:len(result)]
                result = parse_datetime(soup)
                date_list  += result[1:len(result)]
            authors_list += parse_author(soup)
            authors_url_list += parse_author_url(soup)
            comment_list += parse_comments(soup)
            print('{} bytes'.format(sys.getsizeof(comment_list)))
        data = {
            'datetime': date_list.pop(0),
            'author': authors_list.pop(0),
            'author_url': authors_url_list.pop(0),
            'title': title,
            'content': article,
            'metadata': {
                'category': category,
                'view_count': views,
                'like_count': likes_list.pop(0)},
            'comments': handle_comments(authors_url_list, authors_list, comment_list, date_list, likes_list),
        }
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(data['title'])
        item.set_author(data['author'])
        item.set_author_url(data['author_url'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        item.set_comment(data['comments'])
        item.set_metadata(data['metadata'])
        return item
