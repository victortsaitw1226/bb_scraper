#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re, traceback, sys
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class TWITTER_S:
    def __init__(self, server):
        print('init twitter_s.py')
        self.server = server

    def on_CRW2PAR_NTF_TWITTER_S_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_twitter_s_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TWITTER_S_POST(self, packet):
        print('============ on_CRW2PAR_NTF_TWITTER_POST ================')
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        try:
            item = self.parse_twitter_s_content(content)
            # self.server.log.debug(str(item))
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
            # self.server.log.exception(e)
            # self.server.log.error(str(from_notify))
        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_twitter_s_list(self, content):

        data = {
        'next_page': None,
        'latest_post_date': None,
        'links': content['public_tweets_list']
        }

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_twitter_s_content(self, content):
        
        def parse_reply(reply_json):
       
            def parse_datetime(t):
                splitStr = t.split()
                abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
                dt = splitStr[5] + '/' + str(abbr_to_num[splitStr[1]]) + '/' + splitStr[2] + ' ' + splitStr[3] 
                dt = datetime.strptime( dt, '%Y/%m/%d %H:%M:%S')   
                dt = dt + timedelta(hours=8)
                return  dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
            reply_list = []
            
            for reply in reply_json:
                reply_dict =  {
                        'comment_id': reply['id_str'],
                        'datetime': parse_datetime(reply['created_at']),
                        'author': reply['user_id_str'],
                        'author_url': '',
                        'content':''.join(reply['full_text'].split()),
                        'source': 'twitter',
                        'metadata':{'like_count':reply['favorite_count'], 'retweet_count':reply['retweet_count']},
                        'comments':[] }
                    
                reply_list.append(reply_dict)
                
            return reply_list


        data = content['content']
        replies = content['replies']
        data['comment'] = parse_reply(replies)


        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(data['title'])
        item.set_author(data['author'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        item.set_comment(data['comments'])
        item.set_metadata(data['metadata'])
        return item
