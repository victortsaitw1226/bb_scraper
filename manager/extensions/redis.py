#!/usr/bin/env python
# -*- coding: utf-8 -*-
import traceback
import sys
from nameko.extensions import DependencyProvider
from .utils.redis_agent import RedisAgent

class Redis(DependencyProvider):

    def setup(self):
        print('redis setup')
        try:
            self.url = self.container.config['REDIS_URL']
            self.port = self.container.config['REDIS_PORT']
            self.db = self.container.config['REDIS_DB']
            self.password = self.container.config['REDIS_PASSWORD']
            self.instance = RedisAgent(
                self.url, self.port, self.db, self.password
            )
        except:
            traceback.print_exc(file=sys.stdout)

    def start(self):
        print('redis start')

    def stop(self):
        print('redis stop')
        self.instance.stop()
        self.instance = None

    def kill(self):
        print('redis kill')
        self.instance.stop()
        self.instance = None

    def get_dependency(self, worker_ctx):
        return self.instance
