#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib.parse
import re, traceback, sys
from bs4 import BeautifulSoup
from protocols.youtube_parse_list import YOUTUBE_PARSE_LIST
from protocols.youtube_parse_meta import YOUTUBE_PARSE_META
from protocols.youtube_parse_info import YOUTUBE_PARSE_INFO
from protocols.youtube_parse_comment import YOUTUBE_PARSE_COMMENT
from protocols.youtube_parse_reply import YOUTUBE_PARSE_REPLY
from protocols.crw2par_ntf_youtube_video_list import CRW2PAR_NTF_YOUTUBE_VIDEO_LIST
from protocols.crw2par_ntf_youtube_comments import CRW2PAR_NTF_YOUTUBE_COMMENTS
from protocols.crw2par_ntf_youtube_info import CRW2PAR_NTF_YOUTUBE_INFO
from protocols.par2mng_ntf_youtube_video_url import PAR2MNG_NTF_YOUTUBE_VIDEO_URL
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class YOUTUBE:
    def __init__(self, server):
        self.server = server


    def on_CRW2PAR_NTF_YOUTUBE_VIDEO_LIST(self, packet):
        from_notify = CRW2PAR_NTF_YOUTUBE_VIDEO_LIST()
        from_notify.parse(packet)
        channel_url = from_notify.get_url()
        channel = from_notify.get_channel()
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()

        if channel_url.endswith('videos'):
            video_urls = self.parse_list_by_video_list(content)
        else:
            video_urls = self.parse_list_by_playlist(content)

        to_notify = PAR2MNG_NTF_YOUTUBE_VIDEO_URL()
        to_notify.set_video_urls(video_urls.get_videos())
        to_notify.set_channel(channel)
        to_notify.set_channel_url(channel_url)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)


    def on_CRW2PAR_NTF_YOUTUBE_COMMENTS(self, packet):
        from_notify = CRW2PAR_NTF_YOUTUBE_COMMENTS()
        from_notify.parse(packet)
        channel_url = from_notify.get_channel_url()
        video_url = from_notify.get_video_url()
        channel = from_notify.get_channel()
        content = from_notify.get_content()
        video_img = from_notify.get_video_img()

        info = self.parse_info(content)

        info.set_video_url(video_url)
        info.set_channel_url(channel_url)

        data = info.to_dict()
        data['media'] = 'youtube'
        data['channel'] = channel
        data['img'] = video_img
        comment_list = []
        try:
            comments = self.parse_comments(content)
            for comment in comments:
                comment.set_video_url(video_url)
                comment.set_channel_url(channel_url)
                comment_list.append(comment.to_dict())
        except:
            pass

        data['comments'] = comment_list

        url = self.server.mongo.upsert_post('history_v2', 'data', video_url, data)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_info(self, content):
        item = YOUTUBE_PARSE_INFO()
        soup = BeautifulSoup(content, 'html.parser')

        head = soup.find('head')
        info_title = head.find('title')
        #info_title = soup.find('h1', {'class': 'title style-scope ytd-video-primary-info-renderer'})
        if info_title is None:
            raise Exception('info_title is None')
        item.set_title(info_title.get_text())

        emotions = soup.find_all('yt-formatted-string', {'class': 'style-scope ytd-toggle-button-renderer style-text'})
        if len(emotions) >= 2:
            item.set_p(emotions[0].get_text())
            item.set_n(emotions[1].get_text())
        else:
            like_button = soup.find('span', class_="like-button-renderer")
            item.set_p(like_button.find('button', {"title": "I like this"}).get_text())
            item.set_n(like_button.find('button', {"title": "I dislike this"}).get_text())

        info_date = soup.find('span', {'slot': 'date'})
        if info_date:
            item.set_date(info_date.get_text())
        else:
            action_panel = soup.find('div', {'id': 'action-panel-details'})
            publish_date = action_panel.find('strong', class_='watch-time-text')
            item.set_date(publish_date.get_text())


        views = soup.find('span', {'class': 'view-count style-scope yt-view-count-renderer'})
        if views:
            item.set_views(views.get_text().strip('views').strip())
        else:
            action_view_count = soup.find('div', class_="watch-view-count")
            item.set_views(action_view_count.get_text().strip('views').strip())
        return item


    def parse_comments(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        comment_thread = soup.find_all('ytd-comment-thread-renderer')
        if comment_thread:
            return self.parse_other_comments(soup)
        return self.parse_eng_comments(soup)


    def parse_other_comments(self, soup):
        comment_thread = soup.find_all('ytd-comment-thread-renderer')
    
        results = []
        for comment in comment_thread:
            item = YOUTUBE_PARSE_COMMENT()
            post = comment.find('ytd-comment-renderer', {'id':'comment'})
            if post is None:
                raise Exception("post is None")

            author = post.find('a', {'id': 'author-text'})
            if author is None:
                raise Exception('post_author is None')
            item.set_author(author.get_text())

            content = post.find('div', {'id': 'content'})
            if content is None:
                raise Exception('post_content is None')
            item.set_content(content.get_text())

            published_time = post.find('yt-formatted-string', {'class': 'published-time-text'})
            if published_time is None:
                raise Exception('post_published time is None')
            item.set_date(published_time.get_text())

            vote_count = post.find('span', {'id': 'vote-count-middle'})
            if vote_count is None:
                raise Exceptin('post_vote count is None')
            item.set_p(vote_count.get_text())

            replies = comment.find('div', {'id': 'replies'})
            if not replies:
                results.append(item)
                continue

            replies = replies.find_all('ytd-comment-renderer')
            reply_results = []
            for reply in replies:
                reply_item = dict()

                reply_author = reply.find('a', {'id': 'author-text'})
                if reply_author is None:
                    raise Exception('reply_author is None')
                reply_item['author'] = reply_author.get_text().strip()

                reply_content = reply.find('yt-formatted-string', {'id': 'content-text'})
                if reply_content is None:
                    raise Exception('reply_content is None')
                reply_item['content'] = reply_content.get_text()

                reply_date = reply.find('yt-formatted-string', {'class': 'published-time-text'})
                if reply_date is None:
                    raise Exception('reply_date is None')
                reply_item['date'] = reply_date.get_text().strip()

                reply_p = reply.find('span', {'id': 'vote-count-middle'})
                if reply_p is None:
                    raise Exception('reply_p is None')
                reply_item['p'] = reply_p.get_text().strip()

                reply_results.append(reply_item)
            item.set_replies(reply_results)
            results.append(item)
        return results


    def parse_eng_comments(self, soup):
        results = []
        comment_section = soup.find('div', {'id': 'comment-section-renderer-items'})
        if not comment_section:
            return results

        comment_thread = comment_section.find_all('section',  recursive=False)
        for comment in comment_thread:
            item = YOUTUBE_PARSE_COMMENT()
            post = comment.find('div', class_=re.compile('comment-renderer-content'))
            if post is None:
                raise Exception("post is None")

            author = post.find('a', class_=re.compile('comment-author-text'))
            if author is None:
                raise Exception('post_author is None')
            item.set_author(author.get_text())

            content = post.find('div', class_=re.compile('comment-renderer-text-content'))
            if content is None:
                raise Exception('post_content is None')
            item.set_content(content.get_text())

            published_time = post.find('span', class_=re.compile('comment-renderer-time'))
            if published_time is None:
                raise Exception('post_published time is None')
            item.set_date(published_time.get_text())

            vote_count = post.find('span', class_=re.compile('comment-renderer-like-count'))
            if vote_count is None:
                raise Exceptin('post_vote count is None')
            item.set_p(vote_count.get_text())

            results.append(item)
        return results
    

    def parse_list_by_video_list(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        videos = soup.find_all('ytd-grid-video-renderer')
        if videos:
            return self.parse_other_list(soup)
        return self.parse_eng_list(soup)


    def parse_other_list(self, soup):
        item = YOUTUBE_PARSE_LIST()
        videos = soup.find_all('ytd-grid-video-renderer')
        results = []
        for video in videos:
            video_info = YOUTUBE_PARSE_META()
            title = video.find('a', {'id': 'video-title'})
            video_info.set_title(title.get('title'))
            video_info.set_href('https://www.youtube.com' + title.get('href'))
            img = video.find('img', {'id': 'img'}).get('src')
            video_info.set_img(img)
            metadata = video.find('div', {'id': 'metadata-line'})
            meta = metadata.find_all('span')
            try:
                video_info.set_count(meta[0].get_text())
                video_info.set_date(meta[1].get_text())
            except:
                pass
            results.append(video_info.to_dict())
        item.set_videos(results)
        return item

    def parse_eng_list(self, soup):
        item = YOUTUBE_PARSE_LIST()
        videos = soup.find_all('li', class_='channels-content-item yt-shelf-grid-item')
        results = []
        for video in videos:
            video_info = YOUTUBE_PARSE_META()
            title = video.find('h3', class_='yt-lockup-title').find('a')
            video_info.set_title(title.get('title'))
            video_info.set_href('https://www.youtube.com' + title.get('href'))
            img = video.find('img').get('src')
            video_info.set_img(img)
            metadata = video.find('ul', class_='yt-lockup-meta-info')
            meta = metadata.find_all('li')
            try:
                video_info.set_count(meta[0].get_text())
                video_info.set_date(meta[1].get_text())
            except:
                pass
            results.append(video_info.to_dict())
        item.set_videos(results)
        return item
    
    def parse_list_by_playlist(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        item = YOUTUBE_PARSE_LIST()
        results = []
        videos = soup.find_all('ytd-playlist-video-renderer')
        if videos:
            for video in videos:
                video_info = YOUTUBE_PARSE_META()
                title = video.find('span', id_='video-title').get('title')
                url = video.find('a').get('href')
                video_info.set_title(title)
                video_info.set_href('https://www.youtube.com' + url)
                img = video.find('img', {'id': 'img'}).get('src')
                video_info.set_img(img)
                video_info.set_count("")
                video_info.set_date("")
                results.append(video_info.to_dict())
            item.set_videos(results)
            return item

        videos = soup.find_all('td', class_='pl-video-title')
        for video in videos:
            video_info = YOUTUBE_PARSE_META()
            title = video.find('a')
            video_info.set_title(title.get_text().strip())
            video_info.set_href('https://www.youtube.com' + title.get('href'))
            img = video.find('img', {'id': 'img'}).get('src')
            video_info.set_img(img)
            video_info.set_count("")
            video_info.set_date("")
            results.append(video_info.to_dict())
        item.set_videos(results)
        return item
