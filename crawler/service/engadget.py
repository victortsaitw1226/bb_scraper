#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
import time

class ENGADGET:

    def __init__(self, server):        
        print('init engadget')
        self.server = server

    def on_MNG2CRW_NTF_ENGADGET_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            html = {'html': self.server.browser.goto(url), 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def scrollDown(self, driver, pause=1):
        """
        Function to scroll down till end of page.
        """
        lastHeight = driver.execute_script("return document.documentElement.scrollHeight")
        i = 0
        while True:
            driver.execute_script("window.scrollTo(0, document.documentElement.scrollHeight);")
            
            try:
                driver.find_element_by_class_name('sppre_container').click()
            except:
                pass
            
            try:
                driver.find_element_by_class_name('sppre_show-more-replies-button').click()
            except:
                pass
            
            time.sleep(pause)
            newHeight = driver.execute_script("return document.documentElement.scrollHeight")
            if newHeight == lastHeight:
                break
            lastHeight = newHeight
            i = i + 1

    def on_MNG2CRW_NTF_ENGADGET_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            detail_html = self.server.browser.goto(url)
            
            # comment html
            driver = self.server.browser.get_driver()
            driver.execute_script("window.scrollTo(0, "+str(driver.find_element_by_id('comments').location['y'])+");")
            time.sleep(1)
            comments = driver.find_element_by_id('comments')
            soup = self.server.browser.soup(comments.get_attribute("outerHTML"), 'html.parser')
            src = soup.find('iframe')['src']

            driver.get(src)
            self.scrollDown(driver)
            comment_html = driver.page_source

            html = {'html': detail_html, 'comment_html': comment_html, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
