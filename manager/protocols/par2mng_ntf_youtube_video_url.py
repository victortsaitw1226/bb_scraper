#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_YOUTUBE_VIDEO_URL(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_YOUTUBE_VIDEO_URL, self).__init__('PAR2MNG_NTF_YOUTUBE_VIDEO_URL')
        self.update({
          'video_urls': [],
          'channel': '',
          'channel_url': '',
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_video_urls(self, video_urls):
        self.set('video_urls', video_urls)

    def get_video_urls(self):
        return self.get('video_urls')

    def set_channel(self, channel):
        self.set('channel', channel)

    def get_channel(self):
        return self.get('channel')

    def set_channel_url(self, channel_url):
        self.set('channel_url', channel_url)

    def get_channel_url(self):
        return self.get('channel_url')
