# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class PrnasiaSpider(RedisSpider):
    name = "prnasia"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "prnasia",
            "name": "prnasia",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600 * 2,
            "url": "https://hk.prnasia.com/story/industry/n-2-0.shtml",
            "scrapy_key": "prnasia:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield response.follow(url,
                    meta=meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = []
        for a in soup.select('.presscolumn h3 a'):
            links.append(a['href'])
        return links
    
    def parse_next_page(self,soup):
        current_page = int(soup.select('.pagination strong')[0].text)   
        last_page = int(soup.select('.pagination a')[-1].text)
        if current_page != last_page:
            return 'https://hk.prnasia.com/story/industry/n-2-{}.shtml'.format(str(current_page * 50))
        else:
            return None
    
    def parse_latest_post_date(self,soup):
        timestamp = soup.select('.presscolumn span')[0].text
        latest_post_date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M')
        return latest_post_date

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = ''
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'prnasia'
        item['proto'] = 'PRNASIA_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        timestamp = soup.select('#header-message span')[0].text
        date = datetime.strptime(timestamp, '%Y-%m-%d %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_title(self,soup):
        return soup.select('#contenttitle')[0].text

    def parse_content(self,soup):
        paragraph = []
        for p in  soup.select('#dvContent p'):
            paragraph.append(p.text) 
        content = '\n'.join(paragraph)
        content = ''.join(content.split())
        return content

    def parse_metadata(self,soup):
        source = soup.select('#dvSource')[0].text.split(' ')[1]
        keywords = []                    
        for a in soup.select('.sub-block.highlight-block a'):
            word = re.sub(r'[^\w]', ' ', a.text)
            word = word.split(' ')
            keywords.extend([i for i in word if len(i)])
        
        category = soup.find('div',{'style':'padding:20px 0 0 0;'}).find_all('a')[-1].text
        return {'original_source': source, 'tag': keywords,'category':category}
