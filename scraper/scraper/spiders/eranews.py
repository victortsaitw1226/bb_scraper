# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class EranewsSpider(RedisSpider):
    name = "eranews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "eranews",
            "name": "eranews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://www.eracom.com.tw/EraNews/Home/political/",
            "scrapy_key": "eranews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url['url'],
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self,soup):
        links = soup.find_all('li','pr')
        links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links]  
        return links
    
    def parse_next_page(self,soup):
        page_bar = soup.find('div','page_list')
        try:
            page_bar_links = page_bar.find_all('a')
            next_page = [a['href'] for a in page_bar_links if '下一页' in str(a)][0]
        except:
            next_page = None
        return next_page
    
    def parse_latest_post_date(self,soup):
        pub_date = soup.find_all('p','date pa')
        link_date = []
        for date in pub_date:
            date = datetime.strptime( date.text , '%Y-%m-%d %H:%M:%S')
            link_date.append(date)
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
       
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'eranews'
        item['proto'] =  'ERANEWS_PARSE_ITEM'
        print(item)
        return item

    def parse_datetime(self, soup):
        if soup.find('html')['lang']=='che':
            post_time = soup.find('span', class_='article-date')
            date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M:%S')
        else:
            post_time = soup.find('span', class_='time')
            date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        try:
            author = soup.find('span', class_='author').text
        except:
            author = ''
        return author
    
    def parse_title(self, soup):
        title = soup.find('head').find('title').text.split('_')[0]
        title = ' '.join(title.split())
        return title
    
    def parse_content(self, soup):
        if soup.find('html')['lang']=='che':
            content = soup.find('div','cell_6660_ brief').text
        else:
            content = soup.find('div', class_='article-main').text
        content = ''.join(content.split())
        return content

    def parse_metadata(self, soup):
        tag = soup.find('meta',{'name':'keywords'})['content'].split(',')
        category = soup.find('div',class_ = re.compile('dqwz')).find_all('a')[-1].text
        metadata = {
            'category': category,
            'tag': tag,
        }
        return metadata
    

