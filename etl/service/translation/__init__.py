from . import youtube, ptt, dcard, facebook, news, paper

class Parser(object):
    def __init__(self):
        self.parser_list = dict()
        self.register()

    def register(self):
        self.parser_list['youtube'] = youtube
        self.parser_list['ptt'] = ptt
        self.parser_list['dcard'] = dcard
        self.parser_list['facebook'] = facebook
        self.parser_list['paper'] = paper

    def parse(self, parser_name, data):
        m = self.parser_list.get(parser_name, news)
        return m.parse(data)
