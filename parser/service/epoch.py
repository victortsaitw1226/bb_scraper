#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments

class EPOCH:
    def __init__(self, server):
        print('init epoch.py')
        self.server = server

    def on_CRW2PAR_NTF_EPOCH_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_epoch_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_EPOCH_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_epoch_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_epoch_list(self, content):

        def parse_next_page(soup):
            if soup.find('span', class_= "current"):
                current_page = int(soup.find('span', class_= "current").get_text())
                last_page = [int(s) for s in soup.find('span', class_='pages').text.split() if s.isdigit()][1]
                
                if current_page < last_page:
                    next_page_prefix = soup.find('div', class_= "page-nav td-pb-padding-side").find('a', class_='page').get('href')
                    next_page = next_page_prefix[0:len(next_page_prefix)-1] + str(current_page +1)  
                else:
                    next_page = None
            
            return next_page

        def parse_links_dates(soup):
            data = {
                'time': [],
                'links': []
            }      

            links = []
            datetimes = []
            for element in soup.find_all('div', class_ = 'item-details'):
                time_ = element.find('time')
                if not time_:
                    continue

                link = domain_prefix + element.a.get('href')
                links.append({
                    'url': link,
                    'metadata': {}
                })
            
                dt = datetime.strptime(time_.text, '%Y年%m月%d日')
                datetimes.append(dt)

            data['time'] = datetimes
            data['links'] = links
            return data

        domain_prefix = 'https://www.epochtimes.com.tw'
        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(max(parse_links_dates(soup)['time']).strftime('%Y-%m-%dT%H:%M:%S+0800'))
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_epoch_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find('time')
            dt = datetime.strptime(time_.text, '%Y年%m月%d日')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            if (soup.find('span', class_='author_box_01') == None) & (soup.find('div', class_='author_box') == None):
                author = None
            elif soup.find('span', class_='author_box_01') == None:
                author = soup.find('div', class_='author_box').text[2:]        
            else:
                author = soup.find('span', class_='author_box_01').text
            
            if '記者' in author:
                authors = re.search(u"[\u4e00-\u9fa5]+", author).group()
                authors = authors[2:]
            else:
                authors = author
            return authors

        def parse_title(soup):
            board = soup.find('div', class_='td-post-header td-pb-padding-side').find('h1')
            return board.text.strip()

        def parse_content(soup):
            for script in soup.find_all('script'):
                script.extract()
            paragraph = []
            paragraphs = soup.find('div', class_='td-paragraph-padding-1').find_all('p')
            for pp in paragraphs:
                paragraph.append(pp.get_text(strip=True))
            content = '\n'.join(paragraph)
            if len(paragraph) == 0:
                content = soup.find('div', class_='td-paragraph-padding-1').get_text(strip=True)
            return content

        def parse_metadata(soup ,fb_like_soup):
            
            entry_crumb = soup.find('div', class_='entry-crumbs').find_all('span')
            entry = [x.text for x in entry_crumb]
            entry = list(dict.fromkeys(entry))
            
            keywords = soup.find('ul', class_='td-tags td-post-small-box clearfix').find_all('li')
            kword = [x.text for x in keywords]
            
            return {
                'category': entry[-1],
                'tag': kword[1:],
                'fb_like_count': fb_like_soup.find('span',{'id':'u_0_3'}).text,
            }

        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_metadata(parse_metadata(soup, fb_like_count_html))
        item.set_comment(parse_comments(comment_html))
        return item
