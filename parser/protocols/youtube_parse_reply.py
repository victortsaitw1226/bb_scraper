#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class YOUTUBE_PARSE_REPLY(Protocol):

    def __init__(self):
        super(YOUTUBE_PARSE_REPLY, self).__init__('YOUTUBE_PARSE_REPLY')
        self.update({
          'author': '',
          'date': '',
          'content': '',
        })

    def set_author(self, author):
        self.set('author', author.strip())

    def get_author(self):
        return self.get('author')

    def set_date(self, date):
        self.set('date', date.strip())

    def get_date(self):
        return self.get('date')

    def set_content(self, content):
        self.set('content', content.strip())

    def get_content(self):
        return self.get('content')
