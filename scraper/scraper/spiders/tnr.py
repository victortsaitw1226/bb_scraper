# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class TnrSpider(RedisSpider):
    name = "tnr"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "tnr",
            "name": "tnr",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600 * 6,
            "url": "http://tnr.com.tw/coverstory.aspx?kind=8&page=1",
            "scrapy_key": "tnr:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        links = self.parse_links(soup)
 
        if len(links) == 0:
            return

        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self, soup):
        table = soup.find('ul', 'other')
        for div in table.findAll('li',{'class':'liAD'}): 
            div.decompose()

        links = []
        for s in table.findAll('li'):
            links.append(s.find('a')['href'])
        return links
    
    
    def parse_latest_post_date(self,soup):
        table = soup.find('ul', 'other')
        link_date = [datetime.strptime(s.text.split(' ')[-1], '%Y/%m/%d') for s in table.findAll('span','date')]
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'tnr'
        item['proto'] =  'TNR_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = datetime.strptime(soup.find('div','date').text.strip().split('\n')[-1], '%Y/%m/%d')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('span','who').text
        if len(author.split('/')[-1]) > 4:
            author = author.split('/')[-1].split(' ')[-1]
        else:
            author = author.split('/')[-1].replace(' ','')
        return author
    
    def parse_title(self, soup):
        title = soup.find("meta",  property="og:title")['content'].split(' - ')[0].replace('\xa0','')
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','txt')
        for s in content.findAll('p'):
            if '圖：' in s.text:
                s.decompose()
        content = content.text.replace('\xa0','')
        return content

    def parse_metadata(self, soup):
        view = soup.find('div','left').find('span','read').text
        like = soup.find('div','left').find('span','like').text
        category = soup.find('span','mk').text
        metadata = {'like_count':like, 'view_count':view, 'category':category}
        return metadata
    
