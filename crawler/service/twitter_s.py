#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
from datetime import datetime
import traceback
import requests
import time
import json
import sys


def get_guest_token(driver):
    driver.get('https://mobile.twitter.com')
    cookies = driver.get_cookies()
    print('cookies:',cookies)
    gt_dict = [ c for c in cookies if c['name']=='gt']
    print('gt:',gt_dict[0]['value'])
    return gt_dict[0]['value']

class TWITTER_S:

    def __init__(self, server):        
        self.server = server
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()
        # self.gt = get_guest_token(self.server.browser.get_driver())
        self.gt = '1201779598645592066'

    def on_MNG2CRW_NTF_TWITTER_S_GET_INDEX(self, packet):

        def crawl_Tweet_Article(direction,user_id,since,max_position):
            if max_position == 'initial':
                url = 'https://twitter.com/search?q={direction}%3A%40{id}%20since%3A{since}&src=typed_query'.format(direction = direction,id = user_id.split('@')[-1], since = since)
        
            else:
                url = 'https://twitter.com/search?q={direction}%3A%40{id}%20since%3A{since}&src=typed_query&max_position={p}'.format(
                        direction = direction, id = user_id.split('@')[-1], since = since,p = max_position)
            params = {'q':'from:%s'% user_id ,
                    'src':'typed_query'}
            headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                    'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14'}
            html = self.session.get(url,params=params,headers=headers).text
            
            soup = BeautifulSoup(html,'html.parser')
            
            try:
                min_position = soup.find('div','stream-container')['data-max-position']
                return {'html':html, 'min_position':min_position}
            except: 
                pass
                return{'html':'', 'min_position':''}


        def parse_Tweet_Article(html):
            soup = BeautifulSoup(html,'html.parser')
            items = soup.find_all('li',{'data-item-type':'tweet'})
            articles = []
            for item in items:
                
                if item.find('div','ReplyingToContextBelowAuthor')==None: # filter replies
                
                    article = {}
                    
                    article['author'] = item.find('span','username u-dir u-textTruncate').text.split('@')[-1]
                    
                    article['ID'] = item.find('div')['data-conversation-id']
                    
                    article['url'] = 'https://twitter.com/' + item.find('div')['data-permalink-path']
                    
                    date= item.find('div','stream-item-header').find('small','time').find('a').find(
                            'span')['data-time']
                    article['date'] = datetime.fromtimestamp(int(date)).strftime('%Y-%m-%dT%H:%M:%S+0800')
                    
                    article['content'] = item.find('div','js-tweet-text-container').find('p').text
                    
                    article['title'] =  article['content'][:30]
                    
                    nbr_retweet = item.find('span','ProfileTweet-action--retweet').find('span','ProfileTweet-actionCount').text.split()[0]
                    nbr_favorite = item.find('span','ProfileTweet-action--favorite').find('span','ProfileTweet-actionCount').text.split()[0]
                    nbr_reply = item.find('span','ProfileTweet-action--reply').find('span','ProfileTweet-actionCount').text.split()[0]
                    article['metadata'] = {'like_count':nbr_favorite,
                                        'reply_count':nbr_reply,
                                        'retweet_count':nbr_retweet}
                        
                    article['comment']=[]
                    
                    articles.append(article)
                else:
                    continue
            return articles


        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        

        try:
            user_id = url['user_name']
            since = url['since']
            max_position = 'initial'
            direction = 'from'
            
            articles = []
            html = crawl_Tweet_Article(direction, user_id, since, max_position)
            max_position = html['min_position']
            
            while max_position!='':
                articles.extend ( parse_Tweet_Article(html['html']) )
                html = crawl_Tweet_Article(direction, user_id,since,max_position)
                max_position = html['min_position']

            html = {'public_tweets_list':articles}
        except:
            traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)
        

    def on_MNG2CRW_NTF_TWITTER_S_GET_POST(self, packet):

        def crawl_reply(self, url,user_id):
    
            def get_cursor(raw):
                try:
                    cursor = raw['timeline']['instructions'][0]['addEntries']['entries'][-1]['content']['operation']['cursor']['value']
                except:
                    cursor = ''
                return cursor
            
            def filter_reply(tweets):
                reply_subset = []
                tweet_id = list(tweets.keys())
                for t in tweet_id:
                    if user_id in tweets[t]['full_text']:
                        reply_subset.append(tweets[t])
                return reply_subset
            
            payload = ""
            
            querystring = {"include_can_media_tag":"1",
                        "include_composer_source":"true",
                        "tweet_mode":"extended",
                        "include_entities":"true",
                        "include_user_entities":"true"}
            
            
            headers = {'Authorization': "Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA",
                    'x-guest-token': self.gt}
            
            reply = []
            cursor_list = []
            
            response = self.session.request("GET", url, data=payload, headers=headers, params=querystring).text
            
            # if forbidden, get a new gt
            if 'Forbidden' in response:
                self.gt = get_guest_token()
                headers = {'Authorization': "Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA",
                            'x-guest-token': self.gt}
                response = self.session.request("GET", url, data=payload, headers=headers, params=querystring).text
                
                
            raw = json.loads(response)
            
            reply.extend ( filter_reply(raw['globalObjects']['tweets']))
            cursor = get_cursor(raw)
            cursor_list.append(cursor)
            
            while cursor!='':
                
                querystring = {"include_can_media_tag":"1",
                        "include_composer_source":"true",
                        "tweet_mode":"extended",
                        "include_entities":"true",
                        "include_user_entities":"true",
                        "cursor":cursor}
                
                response = self.session.request("GET", url, data=payload, headers=headers, params=querystring).text
                raw = json.loads(response)
                reply.extend ( filter_reply(raw['globalObjects']['tweets']))
                cursor = get_cursor(raw)
                cursor_list.append(cursor)
            
            return reply



        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        tweet_info = from_notify.get_url()

        try:
            url = "https://api.twitter.com/2/timeline/conversation/{id}.json".format(id = tweet_info['ID'])
            user_id = '@' + tweet_info['author']
            reply_json = crawl_reply(url, user_id)

            html = {'content':tweet_info, 'replies':reply_json}
        except:
            traceback.print_exc(file=sys.stdout)

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
