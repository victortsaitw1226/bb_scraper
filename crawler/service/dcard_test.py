#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
from six.moves import urllib
import sys
sys.path.append('..')
from utils.browser import Browser
from bs4 import BeautifulSoup
from protocols.mng2crw_ntf_dcard_get_forum_list import MNG2CRW_NTF_DCARD_GET_FORUM_LIST
from protocols.mng2crw_ntf_dcard_get_post_list import MNG2CRW_NTF_DCARD_GET_POST_LIST
from protocols.mng2crw_ntf_dcard_get_post import MNG2CRW_NTF_DCARD_GET_POST
from protocols.crw2par_ntf_dcard_forum_list import CRW2PAR_NTF_DCARD_FORUM_LIST
from protocols.crw2par_ntf_dcard_post_list import CRW2PAR_NTF_DCARD_POST_LIST
from protocols.crw2par_ntf_dcard_post import CRW2PAR_NTF_DCARD_POST

class DCard:
    REQUEST_COUNT = 0

    def __init__(self, server):
        self.server = server
        self.browser = Browser()
        html = self.browser.goto('https://www.dcard.tw/signup')

    
    def get(self, url):
        #DCard.REQUEST_COUNT = DCard.REQUEST_COUNT + 1

        #if DCard.REQUEST_COUNT >= 10:
        #    self.browser.recreate_driver()
        #    DCard.REQUEST_COUNT = 0

        all_proxies = self.browser.get_all_proxies()
        print('all proxies %d' % len(all_proxies))

        self.browser.check_my_ip()

        html = self.browser.goto(url)
        print(html)
        soup = BeautifulSoup(html, 'html.parser').get_text()
        data = json.loads(soup)

        return html

if __name__ == '__main__':
    dcard = DCard(None)
    html = dcard.get('https://www.dcard.tw/_api/forums')
    print(html)
