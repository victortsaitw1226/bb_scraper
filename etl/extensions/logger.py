#!/usr/bin/env python
# -*- coding: utf-8 -*-
from nameko.extensions import DependencyProvider
import logging
import time
import os
import traceback
import sys
import platform
from cmreslogging.handlers import CMRESHandler

class Logger(DependencyProvider):
    class __Logger:
        def __init__(self, **kwargs):
            # self.val = arg
            self.host = kwargs['host']
            self.port = kwargs['port']
            self.name = kwargs['name']
            self.index = kwargs['index']
            self.env = kwargs['env']
            self.user = kwargs['user']
            self.password = kwargs['password']

            if self.host and self.user and self.password:
                self.handler = CMRESHandler(hosts=[{'host': self.host, 'port': self.port}],
                    auth_type=CMRESHandler.AuthType.BASIC_AUTH,
                    auth_details=(self.user, self.password),
                    use_ssl=False,
                    buffer_size=500,
                    flush_frequency_in_sec=0.5,
                    es_index_name=self.index,
                    raise_on_indexing_exceptions=True)
            else:
                self.handler = CMRESHandler(hosts=[{'host': self.host, 'port': self.port}],
                    auth_type=CMRESHandler.AuthType.NO_AUTH,
                    use_ssl=False,
                    buffer_size=500,
                    flush_frequency_in_sec=0.5,
                    es_index_name=self.index,
                    raise_on_indexing_exceptions=True)


            self.log = logging.getLogger(self.name)
            self.log.setLevel(logging.INFO)
            self.log.addHandler(self.handler)


        def extract_function_name(self):
            tb = sys.exc_info()[-1]
            stk = traceback.extract_tb(tb, 1)
            fname = stk[0][3]
            return fname

        def exception(self, e):
            self.log.error(
                "Function {function_name} raised {exception_class} ({exception_docstring}): {exception_message}".format(
                    function_name = self.extract_function_name(), #this is optional
                    exception_class = e.__class__,
                    exception_docstring = e.__doc__,
                    exception_message = str(e)))

        def debug(self, msg):
            if self.env == 'production':
                return
            self.log.debug(msg)

        def info(self, msg):
            if self.env == 'production':
                return
            self.log.info(msg)

        def warn(self, msg):
            self.log.warn(msg)

        def error(self, msg):
            self.log.error(msg)

        def __str__(self):
            return repr(self) + self.val

    def __init__(self, name, env):
        self.name = name
        self.env = env

    def setup(self):
        self.host = self.container.config.get('ES_HOST', '')
        self.user = self.container.config.get('ES_USER', '')
        self.password = self.container.config.get('ES_PASSWORD', '')
        self.port=9200
        self.index = 'log'

    def start(self):
        self.instance = Logger.__Logger(
            host=self.host,
            port=self.port,
            name=self.name,
            index=self.index,
            env=self.env,
            user=self.user,
            password=self.password
        )

    def get_dependency(self, worker_ctx):
        return self.instance
