#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_NEWS_INDEX(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_NEWS_INDEX, self).__init__('CRW2PAR_NTF_NEWS_INDEX')
        self.update({
          'content': '',
          'media': '',
          'name': '',
          'days_limit': 0
        })

    def set_name(self, name):
        self.set('name', name)

    def get_name(self):
        return self.get('name')

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_media(self, media):
        self.set('media', media)

    def get_media(self):
        return self.get('media')
