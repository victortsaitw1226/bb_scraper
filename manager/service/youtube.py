#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from protocols.par2mng_ntf_youtube_video_url import PAR2MNG_NTF_YOUTUBE_VIDEO_URL
from protocols.mng2crw_ntf_youtube_get_video_list import MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST
from protocols.mng2crw_ntf_youtube_get_comments import MNG2CRW_NTF_YOUTUBE_GET_COMMENTS
from protocols.mng2crw_ntf_youtube_get_info import MNG2CRW_NTF_YOUTUBE_GET_INFO

class YOUTUBE:
        
    url_set = set()
    delay_q = []

    def __init__(self, server):
        self.server = server

    def send_index(self, **kwargs):
        url = kwargs['url']
        name = kwargs['name']
        days_limit = kwargs['days_limit']

        init_ntf = MNG2CRW_NTF_YOUTUBE_GET_VIDEO_LIST()
        init_ntf.set_url(url)
        init_ntf.set_channel(name)
        init_ntf.set_days_limit(days_limit)
        self.server.send(init_ntf)


    def on_PAR2MNG_NTF_YOUTUBE_VIDEO_URL(self, packet):
        notify = PAR2MNG_NTF_YOUTUBE_VIDEO_URL()
        notify.parse(packet)
        video_urls = notify.get_video_urls()
        channel_url = notify.get_channel_url()
        channel = notify.get_channel()
        days_limit = notify.get_days_limit()

        #print('PAR2MNG_NTF_YOUTUBE_VIDEO_URL:days_limit:{}'.format(days_limit))

        for video in video_urls:
            video_url = video['href']
            video_date = video['date']
            video_img = video['img']
            #if -1 == video_date.find(r'min') and \
            #   -1 == video_date.find(r'hour') and \
            #   -1 == video_date.find(r'1 day'):
            #    print('youtube: {} , {} stop crawl'.format(video_url, video_date))
            #    continue
           
            request = MNG2CRW_NTF_YOUTUBE_GET_COMMENTS()
            request.set_channel_url(channel_url)
            request.set_channel(channel)
            request.set_video_url(video_url)
            request.set_video_img(video_img)
            self.server.send(request)
