# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class LawtwSpider(RedisSpider):
    name = "lawtw"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "lawtw",
            "name": "lawtw",
            "enabled": True,
            "days_limit": 3600 * 24 * 3,
            "interval": 3600,
            "url": "http://www.lawtw.com/article.php?template=article_category_list&article_category_id=856&area=free_browse&page=1",
            "scrapy_key": "lawtw:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.text, 'html.parser')
        latest_datetime, links = self.parse_links_dates(soup)

        print(latest_datetime)
        print(links)
        if latest_datetime is None:
            return

        for url in links:
            continue
            yield response.follow(
                    url['link'],
                    meta = {
                        'post_date': url['post_date']
                    },
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        
        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_links_dates(self,soup):
        times = []
        links = []

        for ele in soup.find_all('a', class_='HeaderBord'):
            date = ele.parent.parent.find_all(
                'span', class_='clsReportDate')[-1]
            date = ''.join(date.text.split())
            date = datetime.strptime(date, '%Y-%m-%d%H:%M:%S')
            times.append(date)

            links.append({
                'link': ele['href'],
                'post_date': date
            })

        try:
            _time = max(times)
        except:
            _time = None
        return  _time, links
    

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = meta['post_date'].strftime('%Y-%m-%dT%H:%M:%S+0800')
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'lawtw'
        item['proto'] = 'LAWTW_PARSE_ITEM'
        return item
    
    def parse_author(self,soup):
        author = soup.find('div', style='float:left')
        author = author.text.replace('文 / ', '').replace('教授', '')
        return author

    def parse_title(self,soup):
        return soup.h1.text

    def parse_content(self,soup):
        return ''.join(soup.find('span', style='TEXT-INDENT: 0px; LINE-HEIGHT: 26px; word-break: break-all;').text.split())

    def parse_metadata(self,soup):
        metadata = {
            'category': '',
        }
        cat = soup.find('div', id='article_main_content').find(
            'table').find_all('a')[-1].text
        metadata['category'] = cat
        return metadata
    
