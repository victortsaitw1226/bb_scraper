#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import time


class MNA:
    def __init__(self, server):
        print('init mna.py')
        self.server = server

    def on_CRW2PAR_NTF_MNA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_mna_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_MNA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_mna_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_mna_list(self, content):
        soup = BeautifulSoup(content, 'html.parser')

        # Get Next Page
        current_page = int(soup.select('nav li .active')[0].text)   
        last_page_href = soup.select('nav li .last')[0]['href']
        last_page = int(last_page_href.split('=')[-1])
        
        if current_page != last_page:
            next_page = 'https://mna.gpwb.gov.tw/all.php?id=1&Num='+str(current_page+1)
        
        # Get Latest Post Date
        timestamp = soup.select('.content div')[0].text
        datetime = timestamp.split('【軍聞消息】')[1]
        datetime = time.strptime(datetime, '%Y-%m-%d %H:%M:%S')
        latest_post_date =  time.strftime('%Y-%m-%dT%H:%M:%S+0800', datetime)
    
        # Get Articles url
        links = []
        for a in soup.select('.ctn--search-list h3 .link'):
            links.append({'url': 'https://mna.gpwb.gov.tw/' + a['href'], 'metadata': None})
                    
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_mna_content(self, content):
        
        def parse_datetime(soup):
            timestamp = soup.select('.ctn .cf span')[0].text
            timestamp = timestamp.split('民國')[1]
            year, mon_date = timestamp.split('年')
            date = str(int(year)+1911)+mon_date   # change Republic Era into A.D
            date = time.strptime(date, '%Y%m月%d日')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800', date)

        def parse_author(soup): 
            first_sentence = soup.select('.ctn p')[0].text.split('）')[0]
            ind_first = first_sentence.find('軍聞社記者')
            last_sentence = soup.select('.ctn p')[-1].text.split('。')[-1]
            ind_last = last_sentence.find('軍聞社')
            if ind_first == 1:
                return first_sentence[6:9]
            elif ind_last == 1:
                return last_sentence[4:7]
            elif ind_last == 3:
                return last_sentence[6:9]
            else:
                return ''

        def parse_title(soup):
            title = soup.select('.ctn h2')[0].text
            return title.replace(u'\u3000' , ',')

        def parse_content(soup):
            paragraph = []
            for p in soup.select('.ctn p'):
                paragraph.append(p.text.replace(u'\u3000', ','))
                
            content = '\n'.join(paragraph)
            return content

        def parse_metadata(soup):
            category = soup.find('ul','breadcrumbs--default').find('li','breadcrumb--current').text
            return {'category':category}

        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_metadata(parse_metadata(soup))
        return item
