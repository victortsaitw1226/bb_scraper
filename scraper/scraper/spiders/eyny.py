# -*- coding: utf-8 -*-
import scrapy
import re
from bs4 import BeautifulSoup
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from datetime import datetime, timedelta
from .redis_spiders import RedisSpider

class EynySpider(RedisSpider):
#class EynySpider(scrapy.Spider):
    name = 'eyny'
    allowed_domains = ['m.eyny.com']

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            'media': 'eyny',
            'name': 'eyny',
            'url': 'http://m.eyny.com/',
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600,
            "scrapy_key": "eyny:start_urls",
            "priority": 1
           
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        for link in soup.find_all('div', class_='fl'):
            urltag = link.find('a')
            meta['board'] = urltag.get_text()
            href = urltag.get('href')
            yield response.follow(href,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_index)

    def parse_index(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        for link in soup.find_all('div', class_='bm_c'):
            url = link.find('a')
            board = url.get_text()
            if re.search(r'下載', board) or re.search(r'影片區', board):
                continue
            #meta['board'] = board
            href = url['href'] + '&orderby=dateline&filter=dateline&dateline={}'.format(meta['days_limit'])
            yield response.follow(href,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_list)

    def parse_list(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        for link in soup.find_all('div', class_="bm_c"):
            _article = link.find('a')
            if _article is None:
                continue
            if _article.find('img', {'alt': '本版置頂'}):
                continue
            if _article.find('img', {'alt': '本區置頂'}):
                continue
            if _article.find('img', {'src': 'static/image/common/folder_lock.gif'}):
                continue
            article_url = _article.get('href')
            article_text = _article.get_text()
            article_meta = link.find('span', class_='xg1')
            if article_meta is None:
                continue
            article_meta = article_meta.get_text()
            article_date = re.search(r'[0-9]{4}-[0-9]+-[0-9]+', article_meta)
            article_date = article_date.group(0)
             
            meta['title'] = article_text.strip()

            past = datetime.now() - timedelta(seconds=meta['days_limit'])

            if date_parser(article_date) < past:
                return

            yield response.follow(article_url,
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_article)

        next_page = soup.find('a', class_='nxt')
        if next_page is None:
            return

        yield response.follow(next_page.get('href'),
                meta=meta,
                dont_filter=True,
                callback=self.parse_list)

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        main_article_info = meta.get('main_article_info')
        if not main_article_info:
            article_meta = soup.find('div', class_='bm_inf')
            view_count = article_meta.find('font').get_text()
            view_count = view_count.split('|')
            bro_count = re.search(r"(\d+)", view_count[0]).group(0)
            reply_count = re.search(r"(\d+)", view_count[1]).group(0)
            meta['main_article_info'] = {
                'bro_count': int(bro_count),
                'reply_count': int(reply_count),
                'main_article_url': response.url
            }
        for article_div in soup.find_all('div', class_='bm_c bm_c_bg'):
            _as = article_div.find_all('a')
            author_url = _as[0].get('href')
            author_text = _as[0].get_text()
            article_url = _as[1].get('href')
            _as[0].extract()
            _as[1].extract()
            article_date_node = article_div.find_all('em')[-1]
            try:
                article_date_text = article_date_node.find('span').get('title')
            except:
                article_date_text = article_date_node.get_text()
            article_date_node.extract()
            content_type = 1
            reply_count = 0
            bro_count = 0
            if '樓主' == article_div.get_text().strip():
                content_type = 0
                reply_count = meta['main_article_info']['reply_count']
                bro_count = meta['main_article_info']['bro_count']
            content = article_div.find_next_sibling("div").get_text()

            item = NewsItem()
            item['url'] = meta['main_article_info']['main_article_url']
            item['author'] = author_text
            item['article_title'] = meta['title']
            item['author_url'] = [response.urljoin(author_url)]
            item['content'] = content.strip()
            item['comment'] = []
            item['date'] = date_parser(article_date_text)
            item['metadata'] = {
              'view_count': bro_count,
              'reply_count': reply_count,
              'category': meta['board'],
              'original': response.url
            }
            item['content_type'] = content_type
            item['media'] = meta['media']
            item['proto'] =  'EYNY_PARSE_ITEM'
            yield item

        next_page = soup.find('a', class_='nxt')
        if next_page is None:
            return

        yield response.follow(next_page.get('href'),
                meta=meta,
                dont_filter=True,
                callback=self.parse_article)
