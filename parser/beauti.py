from bs4 import BeautifulSoup


def parse_comments(content):
    print('parser youtube parse_comments')
    soup = BeautifulSoup(content, 'html.parser')
    comment_thread = soup.find_all('ytd-comment-thread-renderer')
    #print(comment_thread)
    results = []
    for comment in comment_thread:
        #print(comment)
        post = comment.find('ytd-comment-renderer', {'id':'comment'})
        item = dict()
        item['author'] = post.find('a', {'id': 'author-text'}).get_text()
        item['content'] = post.find('div', {'id': 'content'}).get_text()
        item['date'] = post.find('yt-formatted-string', {'class': 'published-time-text'}).get_text()
        item['replies'] = []

        replies = comment.find('div', {'id': 'replies'})
        if not replies:
            results.append(item)
            continue

        replies = replies.find_all('ytd-comment-renderer')
        reply_results = []
        for reply in replies:
            reply_item = dict()
            reply_item['author'] = reply.find('a', {'id': 'author-text'}).get_text()
            reply_item['content'] = reply.find('div', {'id': 'content'}).get_text()
            reply_item['date'] = reply.find('yt-formatted-string', {'class': 'published-time-text'}).get_text()
            reply_results.append(reply_item)
        item['replies'] = reply_results
        results.append(item)
    return results

with open('youtube_content.html', 'r') as f:
    html = f.read()
    results = parse_comments(html)
    print(results[0])
    #for r in results:
    #    print(r)
