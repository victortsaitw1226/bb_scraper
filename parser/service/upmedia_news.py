#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments

class UPMEDIA_NEWS:
    def __init__(self, server):
        print('init upmedia_news.py')
        self.server = server

    def on_CRW2PAR_NTF_UPMEDIA_NEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_upmedia_news_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_UPMEDIA_NEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_upmedia_news_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_upmedia_news_list(self, content):

        def parse_links(soup):
            links = soup.find('div',{'id':'news-list'})
            try:
                for tags in soup.find_all('div','tag'):
                    tags.decompose()
            except:
                pass
            
            links = links.find_all('dd')
            links = [ {'url':upmedia + li.find('a')['href'],'metadata':[]} for li in links]  
            return links
        
        def parse_next_page(soup):
            upmedia_list_url = 'https://www.upmedia.mg/news_list.php'
            try:
                next_page = soup.find('a',text = '»')['href']
                next_page = upmedia_list_url + next_page
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find('div',{'id':'news-list'})
            pub_date = pub_date.find_all('dd')
            link_date = []
            # main
            for auth in pub_date[0].find('div','author').find_all('a'):
                auth.decompose()
            pub_date_main = pub_date[0].find('div','author').text
            pub_date_main = pub_date_main.replace('、','')
            pub_date_main = pub_date_main.strip()
            link_date.append( datetime.strptime(pub_date_main, '%Y年%m月%d日 %H:%M') )
            # others
            for date in pub_date[1:]:
                date = datetime.strptime( date.find('div','time').text.strip() , '%Y年%m月%d日 %H:%M')
                link_date.append(date)
            # latest_post_date
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date


        list_html = BeautifulSoup(content, 'html.parser')
        upmedia = 'https://www.upmedia.mg/'

        links = parse_links(list_html)
        print('links ', links)
        next_page = parse_next_page(list_html)
        print('next_page ', next_page)
        latest_post_date = parse_latest_post_date(list_html)
        print('latest_post_date ', latest_post_date)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_upmedia_news_content(self, content):
        
        def parse_title(soup):
            title = soup.find('h2',{'id':'ArticleTitle'}).text
            title = ' '.join(title.split())
            return title
        
        def parse_date(soup):     
            date = soup.find('head').find('meta',{'name':"pubdate"})['content']
            date = datetime.strptime( date , '%Y-%m-%dT%H:%M:%S')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_author(soup):
            try:
                author = soup.find('div','author').find_all('a')
                author = ','.join(x.text for x in author)
                if author.find('／')!=-1:
                    author = re.split('／',author)[1]
            except:
                author = ''
            return author
        
        def parse_content(soup):
            content = soup.find('div','editor')
            for rss_close in content.find_all('div','rss_close'):
                rss_close.decompose()
            for ad in content.find_all('a'):
                ad.decompose()
            content = '\n'.join(content.text.split())
            content = content.replace('（）','')
            return content
        
        def parse_keywords(soup):
            keywords = soup.find('head').find('meta',{'name':'keywords'})['content']
            keywords = re.split(',',keywords)
            return keywords
        
        
        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        url = content['url']
        title = parse_title(detail_html)
        date = parse_date(detail_html) 
        author = parse_author(detail_html)
        content = parse_content(detail_html)
        keywords = parse_keywords(detail_html)
        fb_like_count = detail_html.find('div','um_like')['caption']
        category = detail_html.find('meta',{'itemprop':'articleSection'})['content']
        metadata = {'tag':keywords, 'category':category,'fb_like_count':fb_like_count}
        comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
