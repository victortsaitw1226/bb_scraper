# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class UDNBlogSpider(RedisSpider):
    name = "udnblog"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "udnblog",
            "name": "udnblog",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://classic-blog.udn.com/category.jsp?mid=ud",
            "scrapy_key": "udnblog:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta=meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self, soup):
        link = soup.find_all('td',{'colspan':'2'})
        links = []
        for li in link:
            try:
                if li.find('a').text != '':
                    links.append(li.find('a')['href'])                  
            except:
                pass
        return links
    
    def parse_next_page(self, soup):
        try:
            next_page = soup.find('a',text = '下一頁')['href']
        except:
            next_page = None
        return next_page
        
    def parse_latest_post_date(self, soup):
        sub_text = soup.find_all('td',{'align':"right" , 'class':'t11'})
        link_date = []
        for d in sub_text:
            d = re.split('｜',d.text)[0]
            link_date.append(datetime.strptime( d , '%Y/%m/%d %H:%M'))
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
       
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'udnblog'
        item['proto'] = 'UDNBLOG_PARSE_ITEM'
        yield item

        comments = self.parse_comments(soup)
        for reply in comments:
            item = NewsItem()
            item['url'] = response.url
            item['metadata'] = {}
            item['article_title'] = self.parse_title(soup)
            item['author'] = reply['author']
            item['author_url'] = reply['author_url']
            item['date'] = reply['datetime']
            item['content'] = reply['content']
            item['content_type'] = 1
            item['media'] = 'udnblog'
            item['proto'] = 'UDNBLOG_PARSE_ITEM'
            item['comment'] = reply['comments']
            yield item

    def parse_datetime(self, soup):
        date = soup.find('tr','font-size12').text.strip()
        date = re.split(r'瀏覽',date)[0]
        date = datetime.strptime( date , '%Y/%m/%d %H:%M:%S')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('table',{'width':"160",'border':"0",'cellpadding':"0",'cellspacing':"0",
                                  'class':"font-size12"}).find('a','panel-text').text
        return author
    
    def parse_author_url(self, soup):
        author_url = []
        author_url.append(soup.find('table',{'width':"160",'border':"0",'cellpadding':"0",'cellspacing':"0",
                                    'class':"font-size12"}).find('a','nav')['href'])
        return author_url

    def parse_title(self, soup):
        title = soup.find('span',{'id':'maintopic'}).text
        return title
    
    def parse_content(self, soup):
        content = soup.find('td',{'valign':"top", 'class':"main-text"})
        content = ' '.join(content.text.split())
        content = re.split(r'相關閱讀',content)[0]
        return content

    def parse_metadata(self, soup):
        def parse_browse_count(soup):
            browse = soup.find('tr','font-size12').text.strip()
            browse = re.split(r'瀏覽',browse)[1]
            browse = re.split('｜',browse)[0]
            return browse
        
        def parse_response_count(soup):
            response = soup.find('tr','font-size12').text.strip()
            response = re.split('｜',response)[1]
            response = re.split(r'回應',response)[1]
            return response
        
        def parse_recommend_count(soup):
            recommend = soup.find('tr','font-size12').text.strip()
            recommend = re.split('｜',recommend)[2]
            recommend = re.split(r'推薦',recommend)[1]
            return recommend
        
        def parse_category(soup):
            content_body = soup.find('table',{'width':"535", 'border':"0", 'cellpadding':"0",
                                            'cellspacing':"0", 'class':"font-size15"})
            category = content_body.find_all('td',{'align':'right','class':'main-text'})[-1]
            category = category.find('a').text
            return category

        metadata = {'view_count': parse_browse_count(soup), 
                    'reply_count': parse_response_count(soup),
                    'like_count': parse_recommend_count(soup),
                    'category':parse_category(soup)}
        return metadata
    

    def parse_comments(self, soup):
        comments = []
        try:
            comment_section = soup.find('table',
                    {'width':"730", 'border':"0", 'cellpadding':"0", 'cellspacing':"4", 'class':"font-size12"})
            comment_author_sec = comment_section.find_all('td',{'align':"center"})
            comment_author_url = [x.find('a','nav')['href'] for x in comment_author_sec]
            comment_author = [x.find('a','panel-text').text for x in comment_author_sec]
            comment_datetime = comment_section.find_all('td',{'align':"right", 'class':'main-title'})
            comment_datetime = [datetime.strptime( x.text , '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800') for x in comment_datetime]
            comment_content = comment_section.find_all('td', {'colspan':'2', 'class':'main-text'})
            comment_content = [x.text.strip() for x in comment_content]
            # comments_comments
            comments_comments = []
            content_section = comment_section.find_all('table','panel-bg')
            for cont_sec in content_section:
                try:
                    commt_commt = cont_sec.find_all('span','gbook-bg')
                    commt_commt_all = []
                    for ct_ct in commt_commt:
                        commt_commt_title = ct_ct.find('td','gbook-title').text.strip()
                        commt_commt_author = re.split(r' 於 ',commt_commt_title)[0]
                        commt_commt_date = re.split(r' 於 ',commt_commt_title)[1]
                        commt_commt_date = datetime.strptime( commt_commt_date , '%Y-%m-%d %H:%M 回覆：').strftime('%Y-%m-%dT%H:%M:%S+0800')
                        commt_commt_cont = ct_ct.find('td','font-size15 gbook-content')
                        commt_commt_cont = ' '.join(commt_commt_cont.text.split())
                        commt_commt_all.append({
                            'comment_id':'',
                            'author':commt_commt_author,
                            'author_url':'' ,
                            'content':commt_commt_cont , 
                            'datetime':commt_commt_date,
                            'metadata':{}
                        })
                        
                    comments_comments.append(commt_commt_all)
                except:
                    comments_comments.append([])
            
            for cont, auth, auth_url, date, cm_cm in zip(
                    comment_content, comment_author, comment_author_url,
                    comment_datetime, comments_comments):
                comments.append({
                    'comment_id':'',
                    'author':auth,
                    'author_url':auth_url,
                    'content':cont,
                    'datetime':date, 
                    'source': 'udnblog',
                    'metadata':{},
                    'comments':cm_cm
                })
        except:
            pass
        return comments
