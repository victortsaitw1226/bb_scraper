import datetime
import traceback, sys
from bson.objectid import ObjectId
from dateutil.parser import parse as parse_time

def transfer_number(data):
    if type(data) == str:
        if not data.isdigit():
            data = '0'
        data = int(data)
    return data

def parse(data):
    comment_body = []
    
    source_id = str(data.get('_id', ''))
    docurl = data.get('url', '')
    time = data.get('date', '')
    cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    
    fb_like_count = transfer_number(data['metadata'].get('fb_like_count', ''))
    fb_share_count = transfer_number(data['metadata'].get('fb_share_count', ''))
    line_share_count = transfer_number(data['metadata'].get('line_share_count', ''))
    like_count = transfer_number(data['metadata'].get('like_count', ''))
    share_count = transfer_number(data['metadata'].get('fb_like_count', ''))
    reply_count = transfer_number(data['metadata'].get('reply_count', ''))
    thanks_count = transfer_number(data['metadata'].get('thanks_count', ''))
    agree_count = transfer_number(data['metadata'].get('agree_count', ''))
    disagree_count = transfer_number(data['metadata'].get('disagree_count', ''))

    docboard = data['metadata'].get('category','')
    news_source = data['metadata'].get('news_source','')

    all_comments = data.get('comment', [])
    if reply_count == 0:
       reply_count = len(all_comments)

    view_count = transfer_number(data['metadata'].get('view_count', ''))
    if view_count != 0:
        brocount = view_count
    else:
        brocount = fb_like_count + fb_share_count + line_share_count + like_count +  \
                share_count + reply_count + thanks_count + agree_count + disagree_count
    
    raw_media = data.get('media', '')
    media_dict = {
        'mirrormedia': '鏡週刊',
        'setn' : '三立新聞',
        'match' : 'Match生活網',
        'mobile01' : 'Mobile01',
        'wownews' : 'Wownews',
        'grinews' : '草根引響力新視野',
        'epoch' : '大紀元',
        'taro' : '芋傳媒',
        'tssd' : '台灣新生報',
        'kairos': '風向新聞',
        'newtalk' : '新頭殼',
        'udnblog' : 'udn部落格',
        'engadget' : 'engadget',
        'kknews' : 'kk新聞',
        'crntt' : '中評網',
        'upmedia_comment' : '上報',
        'upmdeia_life' : '上報',
        'upmedia_news' : '上報',
        'enews' : 'eNews',
        'peopo' : '公民新聞',
        'eranews' : '年代',
        'urcosme' : 'UrCosme',
        'sina' : 'Sina新聞',
        'cntimes' : '大華網路報',
        'housefun' : '好房網',
        'tnr' : '台灣新論',
        'twtimes' : '台灣時報',
        'hkej' : '信報財經新聞',
        'ejinsight' : 'ejinsight',
        'greatnews' : '大成報',
        'ck101' : '卡提諾論壇',
        'fountmedia' : '放言科技傳媒',
        'bbc' : 'BBC',
        'prnasia' : '美通社PR',
        'ithome' : 'iThome',
        'mna' : '國防部軍事新聞通訊社',
        'storm' : '風傳媒',
        'ttv' : '台視',
        'ft' : 'FT中文網',
        'cool3c' : 'Cool3c',
        'uho' : 'uho優活健康網',
        'lawtw' : '台灣法律網',
        'ltn_news' : '自由時報',
        'ltn_estate' : '自由時報',
        'thenewslens' : '關鍵評論網路',
        'ksnews' : '更生日報',
        'tnews' : '大台灣新聞網',
        'healthnews' : '健康醫療網',
        'businesstoday' : '今周刊',
        'yahoo' : 'Yahoo奇摩',
        'linetoday' : 'LineToday',
        'chinatimes' : '中時電子報',
        'life' : 'LIFE生活網',
        'nextmag' : '壹週刊', 
        'appledaily' : '蘋果日報',
        'cts' : '華視新聞網',
        'udn' : '聯合新聞網',
        'udn_opinion' : '聯合新聞網',
        'hinet_times' : 'Hinet',
        'hinet' : 'Hinet',
        'nownews' : 'NOWnews',
        'tvbs' : 'TVBS',
        'ettoday' : 'ETtoday新聞雲',
        'etforum' : '雲論(ETtoda論壇)',
        'zimedia': '字媒體',
        'cnyes': 'anue鉅亨',
        'cna': '中央通訊社',
        'popdaily': '波波黛莉的異想世界',
        'buzzorange': '報橘',
        'eyny': '伊莉論壇',
        'gamer_hala': '巴哈姆特',
        'pixnet': '痞客邦',
        'plurk': '噗浪',
        'twitter': '推特',
        'ptt': 'PTT',
        'dcard': 'DCard',
        'facebook': 'Facebook',
        'youtube': 'YouTube'
    }
    
    media = media_dict.get(raw_media, raw_media)

    datafrom = 0
    if raw_media == 'mobile01' or raw_media == 'ck101' or raw_media == 'udnblog' or raw_media == 'eyny':
        datafrom = 2

    if time is None:
        time = ''
    
    if time != '':
        time_obj = parse_time(time, ignoretz=True)
        timestamp = time_obj.timestamp()
        postdate = datetime.datetime.fromtimestamp(timestamp)
        pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
        #time = time[-4:]
        #hrs = int(time[:2])
        #mins = int(time[2:])
        #ppdate = datetime.datetime.strptime(data['date'][:-5], '%Y-%m-%dT%H:%M:%S')
        #postdate = ppdate + datetime.timedelta(hours = hrs, minutes = mins)
        #pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
    else:
        pdate = cdate
    
    #repcount = data['metadata'].get('reply_count', 0)
    #all_comments = data.get('comment', [])
    #if repcount == 0:
    #    repcount = len(all_comments)

    title = data.get('article_title', '')
    body = dict({
        "data_id": source_id,
        "postauthor": data.get('author', ''),
        "Author_ID": data.get('author_url', ''),
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": docboard,
        "news_source": news_source,
        "content": data['content'],
        "media": media,
        "SourceTypeV2": data.get("media", ''),
        "SourceType": 0,
        "docurl": docurl,
        "PostDate": pdate,
        "contentType": 0,
        "candidate": '',
        "mediatype": datafrom,
        "datafrom": datafrom,
        "title": title,
        "brocount": brocount,
        "repcount": int(reply_count),
        "datatype": 0,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 0,
        "candidates": []
    })

    for comment in all_comments:
        original_comment_time = comment.get('datetime', comment.get('date', ''))

        rdate = cdate
        if original_comment_time != '':
            comment_time_obj = parse_time(original_comment_time, ignoretz=True)
            comment_timestamp = comment_time_obj.timestamp()
            r_postdate = datetime.datetime.fromtimestamp(comment_timestamp)
            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:%S')

            #time2 = original_comment_time[-4:]
            #hrs2 = int(time2[:2])
            #mins2 = int(time2[2:])
            #r_pdate = datetime.datetime.strptime(original_comment_time[:-5], '%Y-%m-%dT%H:%M:%S')
            #r_postdate = r_pdate + datetime.timedelta(hours=hrs2, minutes=mins2)
            #rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:%S')

        #if comment['source'] == 'fb':
        #    datafrom = 2
            
        c_repcount = 0
        comment_reply = comment.get('comments', [])
        if len(comment_reply) > 0:
            c_repcount = len(comment_reply)
            
        c_fb_like_count = comment['metadata'].get('fb_like_count', 0)
        c_like_count = comment['metadata'].get('like_count', 0)
        c_dislike_count = comment['metadata'].get('dislike_count', 0)
        c_agree_count = comment['metadata'].get('agree_count', 0)
        c_disagree_count = comment['metadata'].get('disagree_count', 0)

        c_brocount = int(c_fb_like_count) + int(c_like_count) + int(c_dislike_count) + \
            int(c_agree_count) + int(c_disagree_count) + c_repcount

        dummy_body = dict({
            "data_id": source_id ,
            "postauthor": comment.get("author", ''),
            "Author_ID": comment.get("author", ''),
            "createdate": cdate,
            "docid": source_id,
            "MainID": comment.get('comment_id', source_id),
            "docboard": docboard,
            "news_source": news_source,
            "content": comment["content"],
            "media": media,
            "SourceTypeV2": data.get("source", data.get('media', '')),
            "SourceType": 0,
            "docurl": docurl,
            "PostDate": rdate,
            "contentType": 1,
            "candidate": '',
            "mediatype": datafrom,
            "datafrom": datafrom,
            "title": title,
            "brocount": c_brocount,
            "repcount": c_repcount,
            "datatype": 0,
            "posscore": 0,
            "negscore": 0,
            "evaluation": 2,
            "candidates": []
        })
        comment_body.append(dummy_body)

        for reply in comment_reply:
            original_reply_time = reply.get('datetime', reply.get('date', ''))
            rpdate = cdate
            if original_reply_time != '':
                reply_time_obj = parse_time(original_reply_time, ignoretz=True)
                reply_timestamp = reply_time_obj.timestamp()
                rp_postdate = datetime.datetime.fromtimestamp(reply_timestamp)
                rpdate = datetime.datetime.strftime(rp_postdate, '%Y-%m-%dT%H:%M:%S')

                #time3 = original_reply_time[-4:]
                #hrs3 = int(time3[:2])
                #mins3 = int(time3[2:])
                #rp_pdate = datetime.datetime.strptime(original_reply_time[:-5], '%Y-%m-%dT%H:%M:%S')
                #rp_postdate = rp_pdate + datetime.timedelta(hours=hrs3, minutes=mins3)
                #rpdate = datetime.datetime.strftime(rp_postdate, '%Y-%m-%dT%H:%M:%S')
                    
            reply_dummy_body = dict({
                "data_id": source_id ,
                "postauthor": reply.get("author", ''),
                "Author_ID": reply.get("author", ''),
                "createdate": cdate,
                "docid": source_id,
                "MainID": reply.get('comment_id', source_id),
                "docboard": docboard,
                "news_source": news_source,
                "content": reply["content"],
                "media": media,
                "SourceTypeV2": data.get("source", data.get('media', '')),
                "SourceType": 0,
                "docurl": docurl,
                "PostDate": rpdate,
                "contentType": 1,
                "candidate": '',
                "mediatype": datafrom,
                "datafrom": datafrom,
                "title": title,
                "brocount": 0,
                "repcount": 0,
                "datatype": 0,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 0,
                "candidates": []
            })
            comment_body.append(reply_dummy_body)
    comment_body.append(body)
    return comment_body

