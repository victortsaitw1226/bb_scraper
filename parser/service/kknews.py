#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class KKNEWS:
    def __init__(self, server):
        print('init kknews.py')
        self.server = server

    def on_CRW2PAR_NTF_KKNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_kknews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())        
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_KKNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        if not content['html']:
            print('POST content[\'html\'] is None, ignoring...')
            return

        item = self.parse_kknews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

 
    def parse_kknews_list(self, content):        
        def parse_next_page(soup):
            try:
                next_page = soup.find('ul','pagination').find('li','next page-numbers').find('a')['href']
                next_page = kknews + next_page
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find_all('p','meta')
            link_date = []
            for date in pub_date:
                date = datetime.strptime( re.split(' | ',date.text)[-1], '%Y-%m-%d')
                link_date.append(date)
            try:
                latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
                # latest_post_date = max(link_date)
            except:
                latest_post_date = datetime.now().strftime('%Y-%m-%dT%H:%M:%S+0800')
                # latest_post_date = datetime.now()
            return latest_post_date
        
        list_html = BeautifulSoup(content, "html.parser")
        kknews = 'https://kknews.cc'
        
        links = list_html.find_all('article','post-254693 post type-post status-publish format-standard has-post-thumbnail hentry category-6')
        links = [ kknews + li.find('a')['href'] for li in links]
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        
        return item

    def parse_kknews_content(self, content):

        def parse_date(soup):
            date = soup.find('p','meta post-meta').find('a','updated').text
            date = datetime.strptime( date , '%Y-%m-%d')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_content(soup):
            content = soup.find('div','basic')
            for img in content.find_all('figure'):
                img.decompose()
            for ad in content.find_all('div','axslot lrct_inject gemini'):
                ad.decompose()
            for ad in content.find_all('div','axslot lrct_inject axsense'):
                ad.decompose()   
                
            content = content.text.strip()
            content = ' '.join(content.split())
            return content
        

        def parse_metadata(soup):
            metadata = {'category':''}
            try:
                metadata['category'] = soup.find('a',{'rel':'category tag'}).text
            except:
                pass
            return metadata

        print('parse content: ', content['url'])
        item = NEWS_PARSE_ITEM()
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        print('detail_html', len(detail_html))
        # if len(detail_html) == 12:
            # print(detail_html)
        
        # detail
        url = detail_html.find('link', rel='canonical')['href']
        # print(url)
        title = detail_html.find('h1','entry-title p-name').text
        # print(title)
        author = detail_html.find('p','meta post-meta').find('em').text
        author = author.split()[1]
        # print(author)
        date = parse_date(detail_html) 
        # print(date)
        content = parse_content(detail_html)
        # print(content)
        metadata = parse_metadata(detail_html)

        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_metadata(metadata)
        return item

