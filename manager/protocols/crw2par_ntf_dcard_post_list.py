#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_DCARD_POST_LIST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_DCARD_POST_LIST, self).__init__('CRW2PAR_NTF_DCARD_POST_LIST')
        self.update({
          'content': '',
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
