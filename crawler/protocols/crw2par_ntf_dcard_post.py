#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol
import datetime

class CRW2PAR_NTF_DCARD_POST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_DCARD_POST, self).__init__('CRW2PAR_NTF_DCARD_POST')
        self.update({
          'content': '',
          'comments': '',
          'crawl_date': datetime.datetime.now().isoformat()
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_comments(self, comments):
        self.set('comments', comments)

    def get_comments(self):
        return self.get('comments')
