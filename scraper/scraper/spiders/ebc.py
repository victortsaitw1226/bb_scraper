# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class EbcSpider(RedisSpider):
    name = "ebc"


    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ebc",
            "name": "ebc",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interal": 3600,
            "url": "http://news.ebc.net.tw",
            "post_url": 'https://news.ebc.net.tw/News/List_Category_Realtime',
            "scrapy_key": "ebc:start_urls",
            "priority": 1,
            "cate_code": "SPORT",
            "page": 1,
        }]
        for request in requests:
            yield scrapy.Request(url=request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

 
    def parse(self, response):
        meta = response.meta
        yield scrapy.FormRequest(
            url=meta['post_url'],
            headers={
                'x-requested-with': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            formdata={
                'cate_code': meta["cate_code"],
                'exclude': '',
                'page': str(meta["page"]),
                'width': "1135",
                'layout': "web"
            },
            method='POST',
            meta=meta,
            dont_filter=True,
            callback=self.parse_list)

    def parse_list(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        
        if links==[]:
            return
        
        latest_datetime = self.parse_latest_post_date(links[0])

        for url in links:
            yield response.follow(url,
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        # crawl next_page
        meta['page'] = meta['page'] + 1
        yield scrapy.FormRequest(
            url=meta['post_url'],
            headers={
                'x-requested-with': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            formdata={
                'cate_code': meta["cate_code"],
                'exclude': '',
                'page': str(meta["page"]),
                'width': "1135",
                'layout': "web"
            },
            method='POST',
            meta=meta,
            dont_filter=True,
            callback=self.parse_list)
  
    def parse_links(self, soup):
        boxes = soup.find_all('div','style1 white-box')
        links = [b.find('a')['href'] for b in boxes] 
        return links
    
    def parse_latest_post_date(self, first_link):
        html = requests.get('https://news.ebc.net.tw' + first_link).text
        soup = BeautifulSoup(html, 'html.parser')
        time_stamp = soup.select('.info .small-gray-text')[0].text.strip()
        time_stamp = ' '.join(time_stamp.split(' ')[:2])
        latest_post_date = datetime.strptime(time_stamp , '%Y/%m/%d %H:%M')
        return latest_post_date
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'ebc'
        item['proto'] = 'EBC_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        time_stamp = soup.select('.info .small-gray-text')[0].text.strip()
        time_stamp = ' '.join(time_stamp.split(' ')[:2])
        dat = datetime.strptime(time_stamp , '%Y/%m/%d %H:%M')
        return dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
    def parse_metadata(self,soup):
        metadata = {
            'tag':[],
            'category':''
        }
        keyword = [a.text for a in soup.select('.keyword a')]
        if len(keyword):
            metadata['tag'] =  keyword             
        metadata['category'] = soup.find('div',{'id':'web-map'}).find_all('a')[1].text
        return metadata
    
    def parse_author(self,soup): 
        try:
            script = json.loads(soup.select('script')[1].text)
            author = script['author']['name']
            return author
        except:
            return ''
            
    def parse_title(self,soup):
        return soup.select('h1')[0].text
    
    def parse_content(self,soup):
        content = ''.join(soup.find('div','raw-style').find('span',{'id':re.compile(r'react_')}).text.split())    
        return content
