#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from bs4 import BeautifulSoup
from datetime import datetime
import traceback
import requests
import sys
import re
from fake_useragent import UserAgent

class TSSD:
    proxy = None

    def __init__(self, server):
        self.session = requests.session()
        #requests.packages.urllib3.disable_warnings()
        self.server = server

    def detect_error(self, html):
        return 'logo' in html
     
    def parse_tssd_list(self, content):

        def parse_next_page(soup):
            if soup.find('a', text='►') is not None:
                next_page = domain_prefix + soup.find('a', text='►').get('href')
            else:
                next_page = None
            return next_page

        def parse_links(soup):
            links = []
            
            for ad in soup.find_all('dd', class_='only_1280'):
                ad.extract()
            
            for element in soup.find('div', id='story').find_all('dd'):
                link = element.a.get('href')
                links.append({
                    'url': domain_prefix + link,
                    'metadata': {}
                })
            
            return links 

        def parse_latest_post_date(first_link):
            html = self.server.new_requests(first_link, self.detect_error)
            soup = BeautifulSoup(html, 'lxml')
            reg_time = re.compile(r'\d{4}/\d{2}/\d{2}', re.VERBOSE)
            post_time = soup.find('div', id='news_author').text
            time_ = reg_time.findall(post_time)
            dt = datetime.strptime(time_[0], '%Y/%m/%d')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        domain_prefix = 'http://www.tssdnews.com.tw'
        soup = BeautifulSoup(content, 'lxml')
        
        links = parse_links(soup)
        next_page = parse_next_page(soup)
        latest_post_date = parse_latest_post_date(links[0]['url'])

        item = {'next_page':next_page, 'links':links, 'latest_post_date':latest_post_date}

        return item

    
    def on_MNG2CRW_NTF_TSSD_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        #html = self.goto(url)
        # html = self.server.browser.goto(url)
        try:
            html = self.server.new_requests(url, self.detect_error)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        content = self.parse_tssd_list(html)

        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(content)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_TSSD_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # html = {'html': self.goto(url), 'url': url}
            html = {'html': self.server.new_requests(url, self.detect_error), 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
