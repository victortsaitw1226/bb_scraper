#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class CNA:
    def __init__(self, server):
        print('init cna.py')
        self.server = server

    def on_CRW2PAR_NTF_CNA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_cna_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())        
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CNA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        if not content['html']:
            print('POST content[\'html\'] is None, ignoring...')
            return

        item = self.parse_cna_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

 
    def parse_cna_list(self, content):        
        
        html = content['html']
        url = content['url']
        
        cna = 'https://www.cna.com.tw'

        links = [{'url':cna+'/news/aopl/'+h['Id']+'.aspx','metadata':[]} for h in html['result']['SimpleItems']]
        
        link_date = [datetime.strptime(h['CreateTime'], '%Y/%m/%d %H:%M') for h in html['result']['SimpleItems']]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        if html['result']['NextPageIdx'] != '':
            next_page =  '/'.join(re.split('/',url)[:-1]) + '/' + str(html['result']['NextPageIdx'])
        else:
            next_page = None

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        
        return item

    def parse_cna_content(self, content):

        soup = BeautifulSoup(content['html'], 'lxml')
        
        # detail
        url = content['url']
        date = datetime.strptime(soup.find("meta", itemprop="dateModified")['content'], '%Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
        title = soup.find('article')['data-title']
        
        content = soup.find('div','centralContent').find('div','paragraph')
        for div in content.findAll('div', 'paragraph moreArticle'): 
            div.decompose()
        content = content.text.strip()

        author_detail = re.findall(r'（(.*?)）', content)[0]
        author = ''
        # place = None
        try:
            author = re.search(r'記者(.*?)\d', author_detail).group(1)
            if any([len(l)>3 for l in author.split('、')]):
                author = re.search(r'記者(.*?)\d', author_detail).group(1)[:-2]
                # place = re.search(r'記者(.*?)\d', author_detail).group(1)[-2:]
        except:
            pass
        
        #metadata
        keywords = soup.find("meta", {'name':"keywords"})['content'].split(',')
        keywords = [k.strip() for k in keywords if k!=title]
        keywords = [k.strip() for k in keywords if k not in ['News', '新聞', '即時新聞', '']]
        
        category = soup.find("meta", property="og:title")['content'].split(' | ')[1:-1]    
        category = category[0]
        
        # try:
        #     author_detail = re.findall(r'（(.*?)）', content)[-1]
        # except:
        #     author_detail = None
        
        fblike = ''
        # fblike_html = BeautifulSoup(content_html['fblike_html'], 'lxml')
        # fblike = fblike_html.find(class_='_5n6j _5n6l').text
        
        # metadata = {'keywords': keywords, 'place':place, 'category': category, 'author_detail':author_detail, 'fblike':fblike}
        metadata = {'tag': keywords, 'category': category, 'fblike':fblike}

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        # item.set_comment(comment)
        item.set_metadata(metadata)
        return item
