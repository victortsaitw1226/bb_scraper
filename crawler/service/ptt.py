#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import requests
from protocols.mng2crw_ntf_ptt_get_index import MNG2CRW_NTF_PTT_GET_INDEX
from protocols.crw2par_ntf_ptt_index import CRW2PAR_NTF_PTT_INDEX
from protocols.mng2crw_ntf_ptt_get_post import MNG2CRW_NTF_PTT_GET_POST
from protocols.crw2par_ntf_ptt_post import CRW2PAR_NTF_PTT_POST

class PTT:

    gossip_data = {
        "from":"bbs/Gossiping/index.html",
        "yes": "yes"
    }

    def __init__(self, server):
        self.session = requests.session()
        requests.packages.urllib3.disable_warnings()
        self.session.post("https://www.ptt.cc/ask/over18",
                           verify=False,
                           data=self.gossip_data)
        self.server = server

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        return raw.text

    def on_MNG2CRW_NTF_PTT_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_PTT_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()

        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))


        to_notify = CRW2PAR_NTF_PTT_INDEX()
        to_notify.set_content(html)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_PTT_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_PTT_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_PTT_POST() 
        to_notify.set_content(html)
        self.server.send(to_notify)
