#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_PTT_NEXT_PAGE(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_PTT_NEXT_PAGE, self).__init__('PAR2MNG_NTF_PTT_NEXT_PAGE')
        self.update({
          'new_page_url': '',
          'current_page_date': ''
        })

    def set_new_page_url(self, url):
        self.set('new_page_url', url)

    def get_new_page_url(self):
        return self.get('new_page_url')

    def set_current_page_date(self, date):
        self.set('current_page_date', date)

    def get_current_page_date(self):
        return self.get('current_page_date')
