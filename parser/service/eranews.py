#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class ERANEWS:
    def __init__(self, server):
        print('init eranews.py')
        self.server = server

    def on_CRW2PAR_NTF_ERANEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_eranews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ERANEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_eranews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_eranews_list(self, content):

        def parse_links(soup):
            links = soup.find_all('li','pr')
            links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links]  
            return links
        
        def parse_next_page(soup):
            page_bar = soup.find('div','page_list')
            try:
                page_bar_links = page_bar.find_all('a')
                next_page = [a['href'] for a in page_bar_links if '下一页' in str(a)][0]
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find_all('p','date pa')
            link_date = []
            for date in pub_date:
                date = datetime.strptime( date.text , '%Y-%m-%d %H:%M:%S')
                link_date.append(date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date

        list_html = BeautifulSoup(content, "html.parser")

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(list_html))
        item.set_latest_post_date(parse_latest_post_date(list_html))
        item.set_urls(parse_links(list_html))

        return item

    def parse_eranews_content(self, content):
        
        def parse_datetime(soup):
            if soup.find('html')['lang']=='che':
                post_time = soup.find('span', class_='article-date')
                date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M:%S')
            else:
                post_time = soup.find('span', class_='time')
                date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            try:
                author = soup.find('span', class_='author').text
            except:
                author = ''
            return author
        
        def parse_title(soup):
            title = soup.find('head').find('title').text.split('_')[0]
            title = ' '.join(title.split())
            return title
        
        def parse_content(soup):
            if soup.find('html')['lang']=='che':
                content = soup.find('div','cell_6660_ brief').text
            else:
                content = soup.find('div', class_='article-main').text
            content = ''.join(content.split())
            return content

        def parse_metadata(soup):
            tag = soup.find('meta',{'name':'keywords'})['content'].split(',')
            category = soup.find('div',class_ = re.compile('dqwz')).find_all('a')[-1].text
            metadata = {
                'category': category,
                'tag': tag,
            }
            return metadata

        # detail        
        soup = BeautifulSoup(content['detail_html'], 'html.parser')
        url = content['url']
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_metadata(parse_metadata(soup))
        # item.set_comment(comment)
        
        return item
