#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_NEWS_POST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_NEWS_POST, self).__init__('CRW2PAR_NTF_NEWS_POST')
        self.update({
          'content': '',
        })

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_media(self, media):
        self.set('media', media)

    def get_media(self):
        return self.get('media')