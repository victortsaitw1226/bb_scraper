#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from utils.parse_fb_comment import parse_comments

class TARO:
    def __init__(self, server):
        print('init taro.py')
        self.server = server

    def on_CRW2PAR_NTF_TARO_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_taro_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TARO_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_taro_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_taro_list(self, content):

        def parse_next_page(soup):
            if soup.find('div', class_='older').a is not None:
                next_page = soup.find('div', class_='older').a.get('href')
            return next_page

        def parse_links_dates(soup):
            data = {
                'time': None,
                'links': []
            }

            links = []
            datetimes = []

            # Lastest News (5 of them)
            # for element in soup.find_all('div', class_='col-sm-12'):
            #     for sub in element.find_all('div', class_='item-content'):
            #         link = sub.find('a').get('href')
            #         links.append({
            #             'url': link,
            #             'metadata': {}
            #         })
                
            #     time_ = element.find('time', class_='post-published updated')
            #     if not time_:
            #         continue
        
            #     dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+00:00')
            #     datetimes.append(dt)
            
            # Older ones
            for ele in soup.find_all('div', class_='col-sm-8 content-column'):
                for news in ele.find_all('div', class_='item-inner'):
                    link = news.find('a', class_='post-title post-url').get('href')
                    links.append({
                            'url': link,
                            'metadata': {}
                            })
                
                    time_ = news.find('time', class_='post-published updated')
                    if not time_:
                        continue
            
                    dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+00:00')
                    datetimes.append(dt)
            
            data['time'] = max(datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800')
            links = list({i['url']:i for i in links}.values())
            data['links'] = links
            return data

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_taro_content(self, content):
        
        def parse_datetime(soup):
            time_ = soup.find('time', class_='post-published updated')
            dt = datetime.strptime(time_.get('datetime'), '%Y-%m-%dT%H:%M:%S+00:00')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            if (soup.find('span', class_='post-author-name') == None):
                authors = ''
            else:
                authors = soup.find('span', class_='post-author-name').find('b').get_text(strip=True)
            return authors

        def parse_title(soup):
            board = soup.find('h1', class_='single-post-title')
            return board.get_text(strip=True)

        def parse_content(soup):
            for script in soup.find_all('script'):
                script.extract()
            paragraphs = soup.find('div', class_='entry-content clearfix single-post-content').find_all('p')
            contents = []
            for pp in paragraphs:
                text = pp.get_text(strip=True)
                contents.append(text)
            contents = '\n'.join(contents)
            return contents

        def parse_metadata(soup):
            metadata = {
                'category': '',
                'tag': [],
            }
            
            # entry_crumb = soup.find('ul', class_='bf-breadcrumb-items').find_all('span', itemprop = 'name')
            # entry = [x.get_text() for x in entry_crumb]
            # entry = list(dict.fromkeys(entry))
            category_tag = soup.find('div', class_='term-badges floated')
            category = [y.get_text() for y in category_tag]   
            keyword_tag = soup.find('div', class_='entry-terms post-tags clearfix')
            if  keyword_tag is not None:
                keywords = keyword_tag.find_all('a')
                kword = [x.get_text() for x in keywords]
                metadata['tag'] = kword
            # metadata['entry_crumb'] = entry[1:-1]
            metadata['category'] = category[0]
            return metadata

        

        soup = BeautifulSoup(content['html'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(comment_html))
        item.set_metadata(parse_metadata(soup))
        return item
