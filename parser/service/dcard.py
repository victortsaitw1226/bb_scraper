#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib.parse
import re
import json
import time
from bs4 import BeautifulSoup
from protocols.dcard_parse_item import DCARD_PARSE_ITEM
from protocols.dcard_parse_list import DCARD_PARSE_LIST
from protocols.crw2par_ntf_dcard_forum_list import CRW2PAR_NTF_DCARD_FORUM_LIST
from protocols.crw2par_ntf_dcard_post_list import CRW2PAR_NTF_DCARD_POST_LIST
from protocols.crw2par_ntf_dcard_post import CRW2PAR_NTF_DCARD_POST
from protocols.par2mng_ntf_dcard_forum_list import PAR2MNG_NTF_DCARD_FORUM_LIST
from protocols.par2mng_ntf_dcard_post_list import PAR2MNG_NTF_DCARD_POST_LIST
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class DCARD:
    def __init__(self, server):
        print('init dcard.py')
        self.server = server

    def on_CRW2PAR_NTF_DCARD_FORUM_LIST(self, packet):
        from_notify = CRW2PAR_NTF_DCARD_FORUM_LIST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()

        item = self.parse_forum_list(content)

        to_notify = PAR2MNG_NTF_DCARD_FORUM_LIST()
        to_notify.set_urls(item.get_urls())
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_DCARD_POST_LIST(self, packet):
        from_notify = CRW2PAR_NTF_DCARD_POST_LIST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()

        item = self.parse_post_list(content)

        to_notify = PAR2MNG_NTF_DCARD_POST_LIST()
        to_notify.set_urls(item.get_urls())
        to_notify.set_comment_urls(item.get_comment_urls())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_forum(item.get_forum())
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_DCARD_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_DCARD_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        comments = from_notify.get_comments()
        item = self.parse_post_content([content, comments])
       
        #db = self.server.mongo.getDB()
        #db['dcard'].insert(item)
        item['media'] = 'dcard'
        item['url'] = 'https://www.dcard.tw/f/{}/p/{}'.format(item['forumAlias'], item['id'])
        url = self.server.mongo.upsert_post('history_v2', 'data', item['url'], item)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_forum_list(self, content):
        item = DCARD_PARSE_LIST()
        soup = BeautifulSoup(content, 'html.parser')
        content = soup.get_text()
        forums = json.loads(content)
        links = []
        for forum in forums:
            forum_id = forum['id']
            url = 'https://www.dcard.tw/_api/forums/{forum}/posts?popular=false'.format(forum=forum_id)
            links.append(url)
        item.set_urls(links)
        return item
        
    def parse_post_list(self, content):
        item = DCARD_PARSE_LIST()
        soup = BeautifulSoup(content, 'html.parser')
        content = soup.get_text()
        metas = json.loads(content)

        # post urls and comment urls
        forum_id = ''
        post_links = []
        comment_links = []
        for post in metas:
            post_url = 'https://www.dcard.tw/_api/posts/{id}'.format(id=post['id'])
            post_links.append(post_url)
            comment_links.append(post_url + '/comments')
            forum_id = post['forumId']

        # latest datetime
        ids = [e['id'] for e in metas]
        update_dt = [e['updatedAt'] for e in metas][-1]
        latest_datetime = datetime.strptime(update_dt, "%Y-%m-%dT%H:%M:%S.%fZ")

        # next page   
        params = {
            'popular': 'false',
            'before': ids[-1],
            'forum': forum_id
        }
        next_page_url = 'https://www.dcard.tw/_api/forums/{forum}/posts?popular={popular}&before={before}'.format(**params)

        item.set_next_page_url(next_page_url)
        item.set_latest_post_date(latest_datetime.isoformat())	
        item.set_urls(post_links)
        item.set_comment_urls(comment_links)
        item.set_forum(forum_id)
        return item

    def parse_post_content(self, content):
        soup = BeautifulSoup(content[0], 'html.parser')
        post = soup.get_text()
        post = json.loads(post)

        soup = BeautifulSoup(content[1], 'html.parser')
        comments = soup.get_text()
        post['comments'] = json.loads(comments)
        return post
