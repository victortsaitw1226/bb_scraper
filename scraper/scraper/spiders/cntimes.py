# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class CNTimesSpider(RedisSpider):
    name = "cntimes"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "cntimes",
            "name": "cntimes",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://www.cntimes.info/coluOutline.jsp?page=1&coluid=108&searchword=",
            "scrapy_key": "cntimes:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')

        posts_date = []
        for post in soup.find_all('div', class_='pictxt_row'):
            title = post.find('h3')
            link = title.find('a', {'target': '_blank'})
            url = link.get('href')
            link.extract()
            posts_date.append(datetime.strptime(title.text, '（%Y-%m-%d）'))

            yield response.follow(url,
                    meta=meta,
                    callback=self.parse_article)


        latest_datetime = max(posts_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        current_page = re.search("page=(\d+)", response.url).group(1)
        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        content = self.parse_content(soup)
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = content
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,content)
        item['content_type'] = 0
        item['media'] = 'cntimes'
        item['proto'] = 'CNTIMES_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = datetime.strptime(soup.find('h6').text.split('\u3000')[1], '%Y-%m-%d %H:%M:%S')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('title').text.split('-')[0]
        return author
    
    def parse_title(self, soup):
        title = soup.find('h2').text.replace('\u3000',' ')
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','content_body')
        for div in content.findAll('div'): 
            div.decompose()
        for div in content.findAll('table'): 
            div.decompose()        
        content = content.text.strip().replace('\u3000','').replace('\r','\n').replace('本報訊／','').replace('本報訊/','')
        content = ''.join(content.split())
        return content

    def parse_metadata(self, soup, content):
        try:
            source = re.findall(r'來源：(.*)', content)[0].replace('）','').replace(')','')
        except:
            source = ''
        category = soup.find('div','nav_left').findAll('a')[1].text
        metadata = {'news_source':source, 'category':category}
        return metadata
    
