# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

#class CtsSpider(scrapy.Spider):
class CTSSpider(RedisSpider):
    name = "cts"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "cts",
            "name": "cts",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://news.cts.com.tw/real/index.html",
            "scrapy_key": "cts:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta

        soup = BeautifulSoup(response.body, 'lxml')

        # links
        list_container =  soup.find('div','newslist-container flexbox')
        if not list_container:
            return

        for url in list_container.findAll('a'):
            yield scrapy.Request(url.get('href'),
                    meta=meta,
                    callback=self.parse_article)

        # latest post date
        link_date = [datetime.strptime(s.text, '%Y/%m/%d %H:%M') for s in soup.find('div','newslist').findAll('span','newstime')]
        latest_datetime = max(link_date)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        # next_page
        date = (latest_datetime.date() - timedelta(1)).strftime('%Y/%m/%d')
        next_page = 'https://news.cts.com.tw/api/news/%s/daylist-news.json' % (date)

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse_next_page)

    def parse_next_page(self, response):
        meta = response.meta
        list_json = json.loads(response.text)
            
        # links
        for url in list_json:
            yield scrapy.Request(url['news_url'],
                    meta=meta,
                    callback=self.parse_article)

        # latest post date
        link_date = [datetime.strptime(l['newsdate'] + ' ' + l['newstime'], '%Y/%m/%d %H:%M') for l in list_json]
        latest_datetime = max(link_date)

        # next_page
        date = (latest_datetime.date() - timedelta(1)).strftime('%Y/%m/%d')
        next_page = 'https://news.cts.com.tw/api/news/%s/daylist-news.json' % (date)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield scrapy.Request(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse_next_page)

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        author, content = self.parse_author_content(soup)
        item['url'] = response.url
        item['author'] = author
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = content
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'cts'
        item['proto'] = 'CTS_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = soup.find("meta",  itemprop="dateCreated")['content'].replace('+08:00','+0800')
        return date

    def parse_title(self, soup):
        title = soup.find('h1','artical-title').text
        return title

    def parse_author_content(self, soup):
        content = soup.find('div','artical-content')
        for div in content.findAll('div'): 
            div.decompose()
        for div in content.findAll('p','news-src'): 
            div.decompose()   
        for div in content.findAll('script'): 
            div.decompose() 
        for div in content.findAll('style'): 
            div.decompose() 
        content = content.text.strip().replace('\xa0', '')

        author = content.split('/')[0].strip()
        try:
            author = re.search(r'(.*?) 綜合報導', author).group(1)
        except:
            try:
                author = re.search(r'(.*?) 報導', author).group(1)
            except:
                pass
        return author, content
    

    def parse_metadata(self, soup):
        tag = soup.find("meta", {'name':"keywords"})['content'].split(',')
        category = soup.find("meta", {'name':"section"})['content']
        try:
            video_url = soup.find('div','youtube_player').find('iframe')['src']
        except:
            try:
                video_url = soup.find('video')['src']
            except:
                video_url = ''
        metadata = {'category': category, 'tag': tag,'fb_like_count':'',
                    'line_share_count':'', 'video_url': video_url}
        return metadata
    
