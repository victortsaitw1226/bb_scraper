#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_FB_POST_CONTENT(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_FB_POST_CONTENT, self).__init__('CRW2PAR_NTF_FB_POST_CONTENT')
        self.update({
          'post_url': '',
          'fan_page': '',
          'content': '',
          'post_utime': 0,
          'crawl_date': ''
        })

    def get_post_utime(self):
        return self.get('post_utime')

    def set_post_utime(self, post_utime):
        self.set('post_utime', post_utime)

    def set_crawl_date(self, date):
        self.set('crawl_date', date)

    def get_crawl_date(self):
        return self.get('crawl_date')

    def set_fan_page(self, fan_page):
        self.set('fan_page', fan_page)

    def get_fan_page(self):
        return self.get('fan_page')

    def set_post_url(self, post_url):
        self.set('post_url', post_url)

    def get_post_url(self):
        return self.get('post_url')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
