#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class TTV:
    def __init__(self, server):
        print('init ttv.py')
        self.server = server

    def on_CRW2PAR_NTF_TTV_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ttv_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TTV_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ttv_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ttv_list(self, content):
        soup = BeautifulSoup(content, 'html.parser')
        domain_prefix = 'https://www.ttv.com.tw'

        # Parse Next Page
        next_page = domain_prefix + soup.find(
            'span', class_='glyphicon glyphicon-forward').parent['href']

        # Parse Links and Datetime
        links=[]
        datetimes=[]
        for element in soup.find_all('li', class_='ellipsis newsText large'):
            url = domain_prefix + element.find('a').get('href')
            links.append({
                'url': url,
                'metadata': {}
            })
        
        article_time = url.split('=')[1][:7]
        dt = datetime.strptime(str(int(article_time[:3])+1911)+article_time[3:], '%Y%m%d')
        datetimes.append(dt)

        latest_post_date = max(datetimes).strftime('%Y-%m-%dT%H:%M:%S+0800')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_ttv_content(self, content):
        
        def parse_datetime(soup):
            post_time = soup.find('div', class_='ReportDate middle').find('a')
            date = datetime.strptime(post_time.text, '%Y-%m-%d')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

        # def parse_author(soup):
        #     return None

        def parse_title(soup):
            return soup.find('h1', class_='title').text

        def parse_content(soup):
            content = []
            segment = soup.find('div', class_='br')
            try:    # remove ADs
                segment.style.decompose()
            except:
                pass
            content = ''.join(segment.text.split())
            return content

        def parse_metadata(soup, fb_like_soup):

            def parse_category(soup):
                return soup.find('span', 'glyphicon glyphicon-file r2p').parent.text
            
            def parse_tag(soup):
                tags_content = []
                tags = soup.find('div', class_='br4x middle')
                for element in tags.find_all('a'):
                    tags_content.append(element.text)
                return tags_content

            # 文章分類、關鍵字、按讚數
            metadata = {
                'category': parse_category(soup),
                'tag': parse_tag(soup),
                'fb_like_count': fb_like_soup.find('span',{'id':'u_0_3'}).text,
            }
            return metadata

        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')

        metadata = parse_metadata(soup, fb_like_count_html)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        #item.set_author(None)
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment([])
        item.set_metadata(metadata)
        return item
