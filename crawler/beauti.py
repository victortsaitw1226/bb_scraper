from bs4 import BeautifulSoup
import re

def get_post_info(post):
    print(post)
    post_url = post.find('a', {'aria-label': re.compile('Open story|')}).get('href')
    comments = post.find('span', text=re.compile('Comments'))
    if comments:
        comments_text = comments.get_text()
    else:
        comments_text = ''
    shares = post.find('span', text=re.compile('Share'))
    if shares:
        shares_text = shares.get_text()
    else:
        shares_text = ''
    post_date = post.find('abbr').get_text()
    return post_url, comments_text, shares_text, post_date

soup = BeautifulSoup(open('facebook.html'), 'html.parser')
posts = soup.findAll('div', {'role': 'article'})
for post in posts:
    print('+' * 50)
    print(post)
    #post_url, comments, shares, post_date = get_post_info(post)
    #results.append({
    #    "post_url": 'https://m.facebook.com' + post_url,
    #    "comments": comments,
    #    "shares": shares,
    #    "post_date": post_date
    #})

