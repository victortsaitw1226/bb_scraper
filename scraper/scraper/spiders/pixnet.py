# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import calendar
import requests
import json
import re

class PixnetBlogSpider(RedisSpider):
#class PixnetBlogSpider(scrapy.Spider):
    name = "pixnetblog"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "pixnetblog",
            "name": "pixnetblog",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.pixnet.net/blog/articles/group/1/latest",
            "scrapy_key": "pixnetblog:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links, latest_datetime = self.parse_latest_post_date(soup)

        # 有些頁面雖然可以按下一頁 但是內容都是空的
        if links is []:
            return

        for url in links:
            meta['comment_count'] = url['comment_count']
            yield scrapy.Request(url['url'],
                    meta=meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        next_page = self.parse_next_page(soup)

        # 若下一頁為 javascript 則為不可點取的url
        if 'javascript:;' in next_page:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self, soup):
        try:
            next_page = soup.find('div','page').find('a','page-next')['href']
        except:
            next_page = None
        return next_page
    
    def get_date_text(self, d):
        d = d.split()
        comment_count = re.search('(\d+)', d[-1]).group(0)
        d = d[d.index('|')-1]
        date = datetime.strptime(d, '%Y.%m.%d')
        return date, int(comment_count)

    def parse_latest_post_date(self, soup):
        
        links_date = []
        links_list = []

        first_link = soup.find('div','featured')
        if first_link != None:
            url = first_link.find('a')['href']
            post_date, comment_count = self.get_date_text(first_link.find('span', 'publish').text)
            links_list.append({'url': url, 'comment_count': comment_count})
            links_date.append(post_date)

        links = soup.find('ol', 'article-list').find_all('li')
        for link in links:
            url = link.find('a')['href']
            post_date, comment_count = self.get_date_text(link.find('span', 'meta').text)
            links_list.append({'url': url, 'comment_count': comment_count})
            links_date.append(post_date)

        return links_list, max(links_date) 
    
    #def parse_links(self, soup):
    #    links = soup.find('div','box-body').find_all('li')
    #    first_link = soup.find('div','featured')
    #    links_list = []
    #    if first_link!=None:
    #        links_list.append( { 'url':first_link.find('a')['href'], 'metadata':[] } )
    #    [links_list.append( { 'url':l.find('a')['href'] , 'metadata':[] } ) for l in links]
    #    return links_list
    

    def parse_article(self, response):
        meta = response.meta
        # get all the main content in different pages
        content_html = []
        content_html.append(response.body)
        soup = BeautifulSoup(response.body, 'html.parser')
        if soup.find('div','page')!=None:
            while soup.find('div','page').find('a','next')!=None:
                next_url = soup.find('div','page').find('a','next')['href']
                content_html.append(requests.get(next_url).text)
                soup = BeautifulSoup(content_html[-1], 'html.parser')
        
        # parse and save main content
        soup = BeautifulSoup(content_html[0], 'html.parser')

        content_detail = []
        for ch in content_html:
            content_soup = BeautifulSoup(ch, 'html.parser')
            content_detail.append(self.parse_content(content_soup))
        content_detail = '    * next-page *    '.join(content_detail)

        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = content_detail
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['metadata']['reply_count'] = meta['comment_count']
        item['content_type'] = 0
        item['media'] = 'pixnet'
        item['proto'] = 'PIXNET_PARSE_ITEM'
        yield item

        # get comments list
        comment_url = re.split('-',response.url)[0] +'/comments'
        comment_html = requests.get(comment_url).text
        comments_list_html = json.loads(comment_html)['list']

        #parse and save comments
        comment_soup = BeautifulSoup(comments_list_html, 'html.parser')
        single_post = comment_soup.find_all('ul', 'single-post')
        for sp in single_post:
            if sp.find('li', 'post-info') is None:
                continue
            item = NewsItem()
            item['url'] = response.url
            item['metadata'] = self.parse_metadata(soup)
            item['article_title'] = self.parse_title(soup)
            item['author'] = sp.find('span','user-name').text.strip()
            item['author_url'] = []
            item['date'] = datetime.strptime( sp.find('span','post-time').text, '於 %Y/%m/%d %H:%M').strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = sp.find('li','post-text').text.strip()
            item['content_type'] = 1
            item['media'] = 'pixnet'
            item['proto'] = 'PIXNET_PARSE_ITEM'
            item['comment'] = self.parse_reply_reply(sp.find('li','reply-text'))
            yield item
                

    def parse_datetime(self, soup):
        abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
        publish = soup.find('li','publish')
        year = publish.find('span','year').text.strip()
        month = str(abbr_to_num[publish.find('span','month').text.strip()])
        date = publish.find('span','date').text.strip()
        HM = publish.find('span','time').text.strip()
        date = datetime.strptime( year + '-' + month + '-' + date + ' ' + HM , '%Y-%m-%d %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('meta',{'name':'author'})['content']
        return author
    
    def parse_author_url(self, soup):
        author_url = []
        author_url.append(soup.find('div','author-profile__content').find('a')['href'])
        return author_url

    def parse_title(self, soup):
        title = ' '.join(soup.find('h2',{'itemprop':'headline'}).text.split())
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','article-content-inner')
        for s in content.find_all('script'):
            s.clear()
        for s in content.find_all('noscript'):
            s.clear()
        for s in content.find_all('style',{'type':'text/css'}):
            s.clear()
        content = ' '.join(content.text.split())
        if '延伸閱讀' in content:
            content = content.split('延伸閱讀')[0]
        return content

    def parse_metadata(self, soup):
        if soup.find('div','tag__main') != None:
            tag = soup.find('div','tag__main').find_all('a','tag')
            tag = [t['data-tag']for t in tag]
        else:
            tag = []
        category = soup.find('p', {'id':'blog-category'}).find('a').text
        return {
            'tag': tag,
            'category': category,
            'fb_like': ''
        }
    

    def parse_reply_reply(self, rr):
        replies = []
        if rr!=None:
            auth = rr.find('a').text
            rr.find_all('a')[-1].clear()
            date = datetime.strptime(rr.find('p').text.strip(), '於 %Y/%m/%d %H:%M 回覆').strftime('%Y-%m-%dT%H:%M:%S+0800')
            rr.find('p').clear()
            cont = ' '.join(rr.text.split())
            reply = {
                'comment_id': '',
                'datetime': date,
                'author': auth,
                'author_url': '',
                'content': cont,
                'metadata': {}
            }
            replies.append(reply)
        return replies
