# -*- coding: utf-8 -*-
import scrapy
from .redis_spiders import RedisSpider
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
import re

#class EpochSpider(scrapy.Spider):
class EpochSpider(RedisSpider):
    name = 'epoch'

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "epoch",
            "name": "epoch",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600,
            "url": "https://www.epochtimes.com.tw/%E6%9C%80%E6%96%B0%E6%96%87%E7%AB%A0/all/%E5%85%A8%E9%83%A8/p/1",
            "scrapy_key": "epoch:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        datetimes = []
        for element in soup.find_all('div', class_ = 'item-details'):
            time_ = element.find('time')

            if not time_:
                continue

            post_datetime = date_parser(time_.get('datetime'))
            datetimes.append(post_datetime)
            meta['post_date'] = post_datetime
            author = element.find('div', class_='td-post-author-name').a.get_text()
            meta['author'] = author

            yield response.follow(element.a.get('href'),
                    meta=meta,
                    callback=self.parse_article)
        

        latest_datetime = max(datetimes)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return

        next_page = soup.find('i', class_= "td-icon-menu-right").parent.get('href')
        next_page_count = re.search(r'p/(\d+)', next_page).group(1)
        current_page_count = re.search(r'p/(\d+)', response.url).group(1)

        if next_page_count == current_page_count:
            return

        yield scrapy.Request(next_page,
                meta=meta,
                dont_filter=True,
                callback=self.parse)

    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = meta['author']
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = meta['post_date']
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = meta['media']
        item['proto'] =  'SETN_PARSE_ITEM'
        return item


    def parse_title(self, soup):
        board = soup.find('div', class_='td-post-header td-pb-padding-side').find('h1')
        return board.text.strip()

    def parse_content(self, soup):
        for script in soup.find_all('script'):
            script.extract()
        paragraph = []
        paragraphs = soup.find('div', class_='td-paragraph-padding-1').find_all('p')
        for pp in paragraphs:
            paragraph.append(pp.get_text(strip=True))
        content = '\n'.join(paragraph)
        if len(paragraph) == 0:
            content = soup.find('div', class_='td-paragraph-padding-1').get_text(strip=True)
        return content

    def parse_metadata(self, soup):
        entry_crumb = soup.find('div', class_='entry-crumbs').find_all('span')
        entry = [x.text for x in entry_crumb]
        entry = list(dict.fromkeys(entry))
        
        keywords = soup.find('ul', class_='td-tags td-post-small-box clearfix').find_all('li')
        kword = [x.text for x in keywords]
        
        return {
            'category': entry[-1],
            'tag': kword[1:]
        }
