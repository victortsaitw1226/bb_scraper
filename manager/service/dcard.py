#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from protocols.mng2crw_ntf_dcard_get_forum_list import MNG2CRW_NTF_DCARD_GET_FORUM_LIST
from protocols.mng2crw_ntf_dcard_get_post_list import MNG2CRW_NTF_DCARD_GET_POST_LIST
from protocols.mng2crw_ntf_dcard_get_post import MNG2CRW_NTF_DCARD_GET_POST
from protocols.par2mng_ntf_dcard_forum_list import PAR2MNG_NTF_DCARD_FORUM_LIST
from protocols.par2mng_ntf_dcard_post_list import PAR2MNG_NTF_DCARD_POST_LIST
from protocols.dcard_get_post_list import DCARD_GET_POST_LIST

class DCARD:
        
    url_set = set()
    initial_flag = True
    delay_q = []

    def __init__(self, server):
        self.server = server

    def send_index(self, **kwargs):
        url = kwargs['url']
        days_limit = kwargs['days_limit']
        # Initialize
        init_ntf = MNG2CRW_NTF_DCARD_GET_FORUM_LIST()
        init_ntf.set_url(url)
        init_ntf.set_days_limit(days_limit)
        self.server.send(init_ntf)

    def on_PAR2MNG_NTF_DCARD_FORUM_LIST(self, packet):
        notify = PAR2MNG_NTF_DCARD_FORUM_LIST()
        notify.parse(packet)
        days_limit = notify.get_days_limit()
        for url in notify.get_urls():
            self.go_MNG2CRW_NTF_DCARD_GET_POST_LIST(url, days_limit)

    def go_MNG2CRW_NTF_DCARD_GET_POST_LIST(self, url, days_limit):
        notify = MNG2CRW_NTF_DCARD_GET_POST_LIST()
        notify.set_url(url)
        notify.set_days_limit(days_limit)
        self.server.send(notify)

    def on_PAR2MNG_NTF_DCARD_POST_LIST(self, packet):
        notify = PAR2MNG_NTF_DCARD_POST_LIST()
        notify.parse(packet)
        urls = notify.get_urls()
        comment_urls = notify.get_comment_urls()
        latest_post_date = notify.get_latest_post_date()
        next_page_url = notify.get_next_page_url()
        forum = notify.get_forum()
        days_limit = notify.get_days_limit()

        for index in range(len(urls)):
            url = urls[index]
            comment_url = comment_urls[index]
            self.go_MNG2CRW_NTF_DCARD_GET_POST(url, comment_url)

        # Check the latest datetime is less than 3 days
        try:
            latest_post_date = datetime.strptime(latest_post_date, "%Y-%m-%dT%H:%M:%S.%f")
        except:
            latest_post_date = datetime.strptime(latest_post_date, "%Y-%m-%dT%H:%M:%S")

        now = datetime.now()
        if latest_post_date < (now - timedelta(seconds=days_limit)):
            return

        self.go_MNG2CRW_NTF_DCARD_GET_POST_LIST(next_page_url, days_limit)


    def go_MNG2CRW_NTF_DCARD_GET_POST(self, url, comment_url):
        notify = MNG2CRW_NTF_DCARD_GET_POST()
        notify.set_url(url)
        notify.set_comment_url(comment_url)
        self.server.send(notify)
