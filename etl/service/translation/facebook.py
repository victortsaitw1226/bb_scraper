# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId
import datetime
import text2digits
from google.cloud import translate
translate_client = translate.Client()
target = 'EN'

def extract_numbers(text):
    text = text.strip()
    thousand_ls = ['k', 'K', 'thousand']

    # check the key 'translatedText' exsit or not
    if not text:
        return 0
    if any(i in text for i in thousand_ls):
        try:
            count = ''.join([i for i in text if (i.isdigit()) or (i == '.')])
            count = float(count) * 1000
            return int(count)
            
        # The situation when there is no digit in the text
        except:
            t2d = text2digits.Text2Digits()
            count = ''.join([i for i in t2d.convert(text) if i.isdigit()])
            return int(count)

    else:
        count = ''.join([i for i in text if i.isdigit()])

        # The situation when there is no digit in the text
        if count == '':
            t2d = text2digits.Text2Digits()
            count = ''.join([i for i in t2d.convert(text) if i.isdigit()])
            return int(count)
        else:
            return int(count)

def translate(text):
    test_text = text.lower().strip()
    if '' == test_text or 'share' in test_text or \
        'comment' in test_text or test_text.isdigit():
        return text
    return translate_client.translate(
      text,
      target_language=target
    )['translatedText']

def parse(data):
    source_id = str(data.get('_id', ''))
    docurl = data.get('url', '')
    pdate = datetime.datetime.fromtimestamp(data['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
    
    try:
        docboard = data['fan_page']
    except:
        docboard = ''    

    comment_body = []
    comment_count = 0
    try:
        raw_comment = data.get('comments_count', ' ')
        comment = translate(raw_comment)
        comment_count = extract_numbers(comment)
    except:
        traceback.print_exc(file=sys.stdout)

    share_count = 0
    try:
        raw_share = data.get('shares_count', ' ')
        share = translate(raw_share)
        share_count = extract_numbers(share)
    except:
        traceback.print_exc(file=sys.stdout)

    reaction_count = 0
    try:
        raw_reaction = data.get('reactions_count', ' ')
        reaction = translate(raw_reaction)
        reaction_count = extract_numbers(reaction)
    except:
        traceback.print_exc(file=sys.stdout)
    
    
    cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    content = data.get('post', '')
    title = content[:30]
    body = dict({
        "data_id": source_id,
        "postauthor": data["fan_page"],
        "Author_ID": data["fan_page"],
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": docboard,
        "content": content,
        "media": 'facebook粉絲團',
        "SourceTypeV2": data["media"],
        "SourceType": 0,
        "docurl": docurl,
        "PostDate": pdate or cdate,
        "contentType": 0,
        "candidate": '',
        "mediatype": 2,
        "datafrom": 2,
        "title": title,
        "brocount": comment_count + reaction_count + share_count,
        "repcount": comment_count,
        "datatype": 0,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2,
        "candidates": []
    })

    if len(data['comments']) > 0:
        for comment in data['comments']:
            content = comment.get('text', '')
            pdate = datetime.datetime.fromtimestamp(comment['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
            dummy_body = dict({
                "data_id": source_id,
                "postauthor": comment.get("author", ''),
                "Author_ID": comment.get("author", ''),
                "createdate": cdate,
                "docid": source_id,
                "MainID": source_id,
                "docboard": docboard,
                "content": content,
                "media": 'facebook粉絲團',
                "SourceTypeV2": data.get("media", ''),
                "SourceType": 0,
                "docurl": docurl,
                "PostDate": pdate or cdate,
                "contentType": 1,
                "candidate": '',
                "mediatype": 2,
                "datafrom": 2,
                "title": title,
                "brocount": 0,
                "repcount": len(comment['replies']),
                "datatype": 0,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 2,
                "candidates": []
            })
            comment_body.append(dummy_body)
            
            if comment['replies']:
                for reply in comment['replies']:
                    content = reply.get("text", '')
                    pdate = datetime.datetime.fromtimestamp(comment['post_utime']).strftime("%Y-%m-%dT%H:%M:%S%z")
                    dummy_reply_body = dict({
                        "data_id": source_id,
                        "postauthor": reply.get("author", ''),
                        "Author_ID": reply.get("author", ''),
                        "createdate": cdate,
                        "docid": source_id,
                        "MainID": source_id,
                        "docboard": docboard,
                        "content": content,
                        "media": 'facebook粉絲團',
                        "SourceTypeV2": data.get("media", ''),
                        "SourceType": 0,
                        "docurl": docurl,
                        "PostDate": pdate or cdate,
                        "contentType": 1,
                        "candidate": '',
                        "mediatype": 2,
                        "datafrom": 2,
                        "title": title,
                        "brocount": 0,
                        "repcount": 0,
                        "datatype": 0,
                        "posscore": 0,
                        "negscore": 0,
                        "evaluation": 2,
                        "candidates": []
                    })
                    comment_body.append(dummy_reply_body)
            
    comment_body.append(body)
    return comment_body
