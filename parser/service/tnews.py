#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime

class TNEWS:
    def __init__(self, server):
        print('init tnews.py')
        self.server = server

    def on_CRW2PAR_NTF_TNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        item = self.parse_tnews_list(content)
        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_days_limit(from_notify.get_days_limit())
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_TNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        
        item = self.parse_tnews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_tnews_list(self, content):      
        next_page, latest_post_date, links = None, None, []

        soup = BeautifulSoup(content, 'lxml')  
        
        subdomain = {'基隆新聞網': '024', '台北市新聞網': '02', '新北市新聞': '022', '桃園新聞網': '03', '機場新聞網': 'cks', '新竹新聞網': '035', '苗栗新聞網': '037', '台中新聞網': '04', '彰化新聞網': '047', '南投新聞網': '049', '雲林新聞網': '055', '嘉義新聞網': '05', '台南新聞網': '06', '澎湖新聞網': '069', '高雄新聞網': '07', '屏東新聞網': '08', '宜蘭新聞網': '039', '花蓮新聞網': '038', '台東新聞網': '089', '金門新聞網': '0823', '馬祖新聞網': '0836'}
        domain = subdomain[soup.find('head').find_all('title')[2].text.split('-')[0].replace('(北市新聞網)', '')]
        try:         
            next_page = 'https://tnews.cc/'+ domain + '/' + soup.find('div', {'align':'right'}).find_all('a', {'class':'menulist-m'})[-1]['href']
        except: 
            pass
    
        try:
            latest_post_date = str(datetime.datetime.now().year) + '-' + datetime.datetime.strptime(soup.find_all('td', {'class':'menulist-s'})[1].text.replace('\xa0', ' '), '%m/%d %H:%M').strftime('%m-%dT%H:%M:%S+0800')
        except:
            pass
        
        try:              
            p = soup.find_all('table', {'width':'445'})[1].find_all('a', {'target':'_top'})
            for i in range(len(p)):
                link = p[i]['href']
                links.append({
                            'url': 'https://tnews.cc/'+ domain + '/' + link,
                            'metadata': {}
                            })
        except:
            pass
        if links == []:
            next_page = None

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_tnews_content(self, content):
        def parse_datetime(soup):
            return datetime.datetime.strptime(soup.find('font', {'color':'dimgray'}).text.split('\u3000')[0].replace('發稿日：', '').replace(' 下午 ', ' ').replace(' 上午 ', ' '), '%Y/%m/%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            au = soup.find_all('td', {'valign':'top'})[2].text.replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')
            try:
                ind = au[0:20].index('記者')
                au = au[ind+2:ind+5]
            except:
                au = '' 
            return au

        def parse_title(soup):
            return soup.find('font', {'color':'black'}).text
    
        def parse_content(soup):
            return soup.find_all('td', {'valign':'top'})[2].text.replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')
    
        def parse_metadata(soup):  
            tmp = soup.find('font', {'color':'dimgray'}).text.split('\u3000')
            post = tmp[5].split('：')[1]
            
            try:
                value = tmp[11].split('：')[1].split('\xa0')[0]
            except:
                value = ''

            category = soup.find('input',{'style':'height:21;font-size:12px','name':'NT'})['value']

            return {'post': post,
                    'value': value,
                    'category':category
                    }

        soup = BeautifulSoup(content['html'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')

        url = content['url']
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        # item.set_comment(parse_comments(comment_html))
        item.set_metadata(parse_metadata(soup))
        
        return item
