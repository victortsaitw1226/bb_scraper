#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os
import sys
import traceback
from nameko.web.handlers import http
from nameko.events import EventDispatcher, event_handler, SINGLETON
from nameko.timer import timer
from extensions.redis import Redis
from extensions.logger import Logger
from extensions.elasticSearch import ElasticSearch
from extensions.mongo import Mongo
#from extensions.mysql import MySQL
from extensions.evaluation import Evaluation
from nameko.dependency_providers import Config
from service.job import Job
from nameko.web.handlers import HttpRequestHandler, http
from werkzeug.wrappers import Response
from protocols.protocol import Protocol
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA

class ETL:
    name = 'etl'
    dispatch = EventDispatcher()
    config = Config()
    elasticsearch = ElasticSearch()
    redis = Redis()
    mongo = Mongo()
    #mysql = MySQL()
    log = Logger(
        name=os.environ.get('POD_NAME', 'manager'),
        env=os.environ.get('ENV', 'debug')
    )
    evaluation = Evaluation()
        
    def __init__(self):
        super(ETL, self).__init__()
        self.job = Job(self)

    @timer(500)
    def routine_2(self):
        self.job.process_asr_data()

    @timer(500)
    def routin_3(self):
        self.job.process_ocr_data()

    @event_handler(
        'parser', PAR2ETL_NTF_NEW_DATA.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2ETL_NTF_NEW_DATA(self, packet):
        notify = PAR2ETL_NTF_NEW_DATA()
        notify.parse(packet)
        url = notify.get_url()
        self.job.process_internet_data(url)

    @event_handler(
        'scrapy', 'SCRAPY2ELT_NTF_NEWS',
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_SCRAPY2ELT_NTF_NEWS(self, packet):
        url = json.loads(packet)['url']
        self.job.process_internet_data(url, 'data')
