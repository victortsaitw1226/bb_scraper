#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PAR2MNG_NTF_DCARD_POST_LIST(Protocol):

    def __init__(self):
        super(PAR2MNG_NTF_DCARD_POST_LIST, self).__init__('PAR2MNG_NTF_DCARD_POST_LIST')
        self.update({
          'urls': [],
          'comment_urls': [],
          'next_page_url': '',
          'latest_post_date': '',
          'forum': '',
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_forum(self, forum):
        self.set('forum', forum)

    def get_forum(self):
        return self.get('forum')

    def set_urls(self, urls):
        self.set('urls', urls)

    def get_urls(self):
        return self.get('urls')

    def set_comment_urls(self, urls):
        self.set('comment_urls', urls)

    def get_comment_urls(self):
        return self.get('comment_urls')

    def set_next_page_url(self, url):
        self.set('next_page_url', url)

    def get_next_page_url(self):
        return self.get('next_page_url')

    def set_latest_post_date(self, date):
        self.set('latest_post_date', date)

    def get_latest_post_date(self):
        return self.get('latest_post_date')
