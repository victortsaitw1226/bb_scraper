import redis
import ujson
import time
from scutils.redis_queue import Base, RedisQueue, RedisPriorityQueue, RedisStack
#from scrapy_redis import PriorityQueue
from scrapy.http import Request

r = redis.StrictRedis()
#redis_key = "TweetScraper:start_urls"
#redis_key = "appledaily:start_urls"
#redis_key = "facebook:start_urls"
redis_key = "youtube:start_urls"
#redis_key = "ptt:start_urls"


#redis_key = "example:requests"
q = RedisPriorityQueue(r, redis_key, encoding=ujson)

data = []
#data.append({"url": "https://twitter.com/search?q=jack&src=typed_query", "priority": 3})
#query = 'https://twitter.com/i/search/timeline?l=&f=tweets&q={}&src=typed&max_position=&vertical=default'.format('iingwen')
#data.append({
#    "url": query, 
#    "priority": 3, 
#    'url_template': 'https://twitter.com/i/search/timeline?l=&f=tweets&q={}&src=typed&max_position=%s&vertical=default'.format('iingwen')
#})

#data.append({
#    "url": 'https://tw.appledaily.com/new/realtime/1', 
#    "priority": 3, 
#})

data.append({
    "url": 'https://www.youtube.com/watch?v=0N-XNoqkcLI', 
    "priority": 3, 
})

#data.append({
#    "url": 'https://www.ptt.cc/bbs/Actuary/index.html', 
#    "priority": 3, 
#    "start_url": 'https://www.ptt.cc/bbs/Actuary/index.html', 
#})

for d in data:
    q.push(d, d['priority'])

'''
import pymongo

connection = pymongo.MongoClient('10.61.106.136')
db = connection['TweetScraper']
collection = db['urls']
links = [link['url'] for link in collection.find({'media': 'ptt'})]
for url in links:
    d = {
        "url": url, 
        "priority": 3, 
        "start_url": url
    }
    q.push(d, d['priority'])
    time.sleep(1)
'''
