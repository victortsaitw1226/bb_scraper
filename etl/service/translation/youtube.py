# -*- coding: utf-8 -*-
import datetime
import traceback, sys
from bson.objectid import ObjectId

def parse(data):
    comment_body = []
            
    source_id = str(data.get('_id', ''))
    docurl = data.get('video_url', '')
    channel = data.get('channel', '')
    video_img = data.get('img', '')
    if 'Premier' in data['date']:
        try:
            if 'Premiered' in data['date']:
                postdate = datetime.datetime.strptime(data['date'], 'Premiered %b %d, %Y')
            else:
                postdate = datetime.datetime.strptime(data['date'], 'Premieres %b %d, %Y')

        except:
            for word in data['date']:
                try:
                    time = int(word)
                except:
                    pass

                if 'second' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(seconds = time)
                        
                elif 'minute' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(minutes = time)
                        
                elif 'hour' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(hours = time)
                        
                elif 'day' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(days = time)
                    
                elif 'week' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(days = time * 7)

                elif 'month' in data['date']:
                    postdate = datetime.datetime.now() - datetime.timedelta(days = time * 30)   
                    
                else:
                    postdate = datetime.datetime.now() - datetime.timedelta(days = time * 365)
                        
    if 'Published on' in data['date']:
        postdate = datetime.datetime.strptime(data['date'], 'Published on %b %d, %Y')
    if 'Streamed live' in data['date']:
        try:
            postdate = datetime.datetime.strptime(data['date'], 'Streamed live on %b %d, %Y')
        except:
            for word in data['date']:
                try:
                    time = int(word)
                except:
                    pass

            if 'second' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(seconds = time)
                    
            elif 'minute' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(minutes = time)
                    
            elif 'hour' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(hours = time)
                    
            elif 'day' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(days = time)
                
            elif 'week' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(days = time * 7)

            elif 'month' in data['date']:
                postdate = datetime.datetime.now() - datetime.timedelta(days = time * 30)   
                
            else:
                postdate = datetime.datetime.now() - datetime.timedelta(days = time * 365)

    cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
    brocount = data.get('views', '0')
    brocount = int(brocount.split(' ')[0].replace(',',''))
    title = data.get('title', '')
    body = dict({
        "data_id": source_id,
        "postauthor": channel,
        "Author_ID": channel,
        "createdate": cdate,
        "docid": source_id,
        "MainID": source_id,
        "docboard": channel,
        "content": '',
        "media": "Youtube",
        "SourceTypeV2": data.get("media", 'youtube'),
        "SourceType": 0,
        "docurl": docurl,
        "coverurl": '',
        "PostDate": pdate,
        "contentType": '0',
        "candidate": '',
        "mediatype": 4,
        "datafrom": 4,
        "title": title,
        "brocount": brocount,
        "repcount": len(data['comments']),
        "datatype": 3,
        "posscore": 0,
        "negscore": 0,
        "evaluation": 2,
        "candidates": [],
        "like": data.get("p", -1),
        "unlike": data.get("n", -1),
        "video_img": video_img
    })

    if len(data['comments']) > 0:
        for comment in data['comments']:
            if 'seconds' in comment['date']:
                time = int(comment['date'].split('seconds')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(seconds = time)
                    
            elif ('minute' in comment['date']) or ('minutes' in comment['date']):
                time = int(comment['date'].split('minute')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(minutes = time)

            elif ('hour' in comment['date']) or ('hours' in comment['date']):
                time = int(comment['date'].split('hour')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(hours = time)

            elif ('day' in comment['date']) or ('days' in comment['date']):
                time = int(comment['date'].split('day')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(days = time)
                
            elif ('week' in comment['date']) or ('weeks' in comment['date']):
                time = int(comment['date'].split('week')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 7)

            elif ('month' in comment['date']) or ('months' in comment['date']):
                time = int(comment['date'].split('month')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 30)
                
            else:
                time = int(comment['date'].split('year')[0])
                r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 365)
                
            rdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:%S')
            dummy_body = dict({
                "data_id": source_id,
                "postauthor": comment.get("author", ''),
                "Author_ID": comment.get("author", ''),
                "createdate": cdate,
                "docid": source_id,
                "MainID": source_id,
                "docboard": channel,
                "content": comment.get("content", ''),
                "media": "Youtube",
                "SourceTypeV2": data.get("media", ''),
                "SourceType": 0,
                "docurl": docurl,
                "coverurl": '',
                "PostDate": rdate,
                "contentType": '1',
                "candidate": '',
                "mediatype": 4,
                "datafrom": 4,
                "title": title,
                "brocount": 0,
                "repcount": 0,
                "datatype": 3,
                "posscore": 0,
                "negscore": 0,
                "evaluation": 2,
                "candidates": []
            })
            comment_body.append(dummy_body)
            if comment['replies']:
                for reply in comment['replies']:
                        
                    if 'seconds' in comment['date']:
                        time = int(comment['date'].split('seconds')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(seconds = time)

                    elif ('minute' in comment['date']) or ('minutes' in comment['date']):
                        time = int(comment['date'].split('minute')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(minutes = time)

                    elif ('hour' in comment['date']) or ('hours' in comment['date']):
                        time = int(comment['date'].split('hour')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(hours = time)

                    elif ('day' in comment['date']) or ('days' in comment['date']):
                        time = int(comment['date'].split('day')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(days = time)
                            
                    elif ('week' in comment['date']) or ('weeks' in comment['date']):
                        time = int(comment['date'].split('week')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 7)

                    elif ('month' in comment['date']) or ('months' in comment['date']):
                        time = int(comment['date'].split('month')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 30)

                    else:
                        time = int(comment['date'].split('year')[0])
                        r_postdate = datetime.datetime.now() - datetime.timedelta(days = time * 365)

                    rpdate = datetime.datetime.strftime(r_postdate, '%Y-%m-%dT%H:%M:%S')
                    dummy_reply_body = dict({
                        "data_id": source_id,
                        "postauthor": reply.get("author", ''),
                        "Author_ID": reply.get("author", ''),
                        "createdate": cdate,
                        "docid": source_id,
                        "MainID": source_id,
                        "docboard": channel,
                        "content": reply.get("content", ''),
                        "media": "Youtube",
                        "SourceTypeV2": data.get("media", ''),
                        "SourceType": 0,
                        "docurl": docurl,
                        "PostDate": rpdate,
                        "contentType": '1',
                        "candidate": '',
                        "mediatype": 4,
                        "datafrom": 4,
                        "title": title,
                        "brocount": 0,
                        "repcount": 0,
                        "datatype": 3,
                        "posscore": 0,
                        "negscore": 0,
                        "evaluation": 2,
                        "candidates": []
                    })
                    comment_body.append(dummy_reply_body)
                
    comment_body.append(body)
    return comment_body

