# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class SinaSpider(RedisSpider):
    name = "sina"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "sina",
            "name": "sina",
            "enabled": True,
            "days_limit": 3600 * 24 * 2,
            "interval": 3600 * 2,
            "url": "https://news.sina.com.tw",
            "url_pattern": "https://news.sina.com.tw/realtime/politics/tw/{date}/list.html",
            "scrapy_key": "sina:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 
    def parse(self, response):
        meta = response.meta
        now = datetime.now()

        past = now - timedelta(seconds=meta['days_limit'])

        while True:
            
            _url_pattern = meta['url_pattern']
            url = _url_pattern.format(date=now.strftime('%Y%m%d'))

            yield scrapy.Request(url,
                    meta=meta,
                    callback=self.parse_list)

            now = now - timedelta(seconds=3600 * 24)
            if now <= past:
                break

    def parse_list(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'lxml')
        
        post_block = soup.find('ul','realtime')
        if not post_block:
            return

        for s in post_block.findAll('a'):
            yield response.follow(s.get('href'),
                    meta=meta,
                    callback=self.parse_article)

        next_page = soup.find('a',text = '下一頁')
        if not next_page:
            return

        yield response.follow(next_page.get('href'),
            dont_filter=True,
            meta=meta,
            callback=self.parse_list)

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'lxml')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'sina'
        item['proto'] = 'SINA_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        date = soup.find("meta",  property="article:published_time")['content'].replace('+08:00','+0800')
        return date
    
    def parse_author(self, soup):
        author = soup.find('div','path').findAll('a')[1].text
        return author
    
    def parse_title(self, soup):
        title = soup.find("meta",  property="og:title")['content']
        return title
    
    def parse_content(self, soup):
        content = soup.find('div','pcont')   
        for div in content.findAll('div','main-photo'): 
            div.decompose()
        for div in content.findAll('div'): 
            div.decompose()
        content = content.text.strip()
        return content

    def parse_metadata(self, soup):
        keywords = list(soup.find('div','artwords').text.strip().replace('關鍵字：',''))
        category = soup.find('meta',{'property':'article:section'})['content']
        try:
            news_source = soup.find('div','articles').find('cite').find('a').text
        except:
            news_source = None
        metadata = {
            'tag': keywords,
            'category': category,
            'news_source': news_source
        }
        return metadata
