#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class NEWTALK:
    def __init__(self, server):
        print('init newtalk.py')
        self.server = server

    def on_CRW2PAR_NTF_NEWTALK_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_newtalk_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_NEWTALK_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_newtalk_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_newtalk_list(self, content):

        def parse_links(soup):
            # links = soup.find_all('h3','post-title')
            # links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links]
            links = soup.find_all('div', 'news_title')
            links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links if 'newtalk' in str(li)]
            return links
        
        def parse_next_page(soup):
            try:
                # next_page = soup.find('ul','pages-numbers').find('li','the-next-page')
                # next_page = next_page.find('a')['href']
                # next_page = soup.find('div','mb-pages-container').find('a','nextprev')['href']
                next_page = soup.find('div','pages-container').find('a',text = '下一頁')['href']
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            # pub_date = soup.find_all('span','date meta-item')
            pub_date = soup.find_all(class_='news_date')
            link_date = []
            for date in pub_date:
                try:
                    date = datetime.strptime(date.text , '發布%Y.%m.%d | %H:%M ')
                    link_date.append(date)
                except:
                    pass
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date

        list_html = BeautifulSoup(content, "html.parser")

        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_newtalk_content(self, content):

        def parse_date(soup):
            date = soup.find('div','content_date').text.strip()
            date = datetime.strptime( date , '發布 %Y.%m.%d | %H:%M')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_content(soup):
            content = soup.find('div',{'itemprop':'articleBody'})
            content = content.find_all('p')
            
            for cont in content:
                try:
                    cont.find('a').decompose()
                except:
                    pass
            
            content = '\n'.join(x.text for x in content)
            content = re.split(r'延伸閱讀',content)[0]
            return content
        

        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')

        url = content['url']
        title = detail_html.find('h1','content_title').text
        title = ' '.join(title.split())
        author = detail_html.find('div','content_reporter').find('a').text
        date = parse_date(detail_html)
        content = parse_content(detail_html)
        keywords = detail_html.find('head').find('meta',{'name':'keywords'})['content']
        keywords = re.split(',',keywords)
        category = detail_html.find('meta',{'property':'article:section'})['content']
        #fblike_count = fb_like_count_html.find('span','_5n6h _2pih').text
        #metadata = {'tag':keywords,'fb_like_count':fblike_count}
        metadata = {'tag':keywords, 'category':category, 'fb_like_count':''}
        #comment = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
