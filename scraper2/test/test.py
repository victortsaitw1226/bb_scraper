import redis
import pymongo
import time
from datetime import datetime
import json

m = pymongo.MongoClient("mongodb://ibdo:ibdo2018@210.202.47.189:27017/")

db = m['history_v2']
collection = db['urls']

collection.update_many({"media": "epoch"}, {"$set": {"enabled": False}})
#collection.delete_many({"media": "mirrormedia"})

'''
doc = collection.find_one({"media": "ltn_news"})
doc['_id'] = str(doc['_id'])
print(type(doc['priority']))
doc = json.dumps(doc)
'''

#requests = [
#{
#    "url": 'https://tw.appledaily.com/new/realtime/1',
#    "priority": 1,
#    "interval": 3600,
#    "days_limit": 3600 * 24,
#    'scrapy_key': 'appledaily:start_urls',
#    'media': 'appledaily',
#    'name': 'appledaily',
#    'enabled': True,
#}
#]


#requests = [{
#    "media": "setn",
#    "name": "setn",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600,
#    "url": "https://www.setn.com/ViewAll.aspx?p=1",
#    "scrapy_key": "setn:start_urls",
#    "priority": 1
#}]

#requests = [{
#    "media": "grinews",
#    "name": "grinews",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "http://grinews.com/news/category/newest/",
#    "scrapy_key": "grinews:start_urls",
#    "priority": 1
#}]


#requests = [{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/headlines",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#},
#{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/dare-to-change",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#},
#{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/entertainment",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#},
#{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/lifestyle",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#},
#{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/health",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#},
#{
#    "media": "kairos",
#    "name": "kairos",
#    "enabled": True,
#    "days_limit": 3600 * 24,
#    "interval": 3600 * 4,
#    "url": "https://kairos.news/world",
#    "scrapy_key": "kairos:start_urls",
#    "priority": 1
#}]

#requests = [
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=46&kindid=0&searchword=#",
#        "category": "臺灣時政"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=92&kindid=0&searchword=#",
#        "category": "今日焦點"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14679&page=1",
#        "category": "國民黨及藍營"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14950&page=1",
#        "category": "中評分析"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=217&kindid=0&searchword=#",
#        "category": "台灣綜合"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=142&kindid=0&searchword=#",
#        "category": "綠營動態"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=3&kindid=12&page=1",
#        "category": "兩岸專區"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=253&kindid=14674&page=1",
#        "category": "最新消息"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=192&kindid=0&searchword=#",
#        "category": "北台灣"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/msgOutline.jsp?page=1&coluid=153&kindid=0&searchword=#",
#        "category": "南台灣"
#    },
#    {
#        "media": "crntt",
#        "name": "crntt",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 6,
#        "priority": 1,
#        "scrapy_key": "crntt:start_urls",
#        "url": "http://hk.crntt.com/crn-webapp/kindOutline.jsp?coluid=3&kindid=12&page=2",
#        "category": "兩岸動態"
#    },
#]



#requests = [{
#    'media': 'eyny',
#    'name': 'eyny',
#    'url': 'http://m.eyny.com/',
#    "enabled": True,
#    "days_limit": 3600 * 24 * 2,
#    "interval": 3600 * 3,
#    "scrapy_key": "eyny:start_urls",
#    "priority": 1
#}]

#urls=[
#'https://enews.tw/ArticleCategory/1',
#'https://enews.tw/ArticleCategory/2',
#'https://enews.tw/ArticleCategory/3',
#'https://enews.tw/ArticleCategory/4',
#'https://enews.tw/ArticleCategory/5',
#'https://enews.tw/ArticleCategory/6',
#'https://enews.tw/ArticleCategory/7',
#'https://enews.tw/ArticleCategory/8',
#'https://enews.tw/ArticleCategory/39',
#'https://enews.tw/ArticleCategory/40',
#'https://enews.tw/ArticleCategory/10',
#'https://enews.tw/ArticleCategory/11',
#'https://enews.tw/ArticleCategory/12',
#'https://enews.tw/ArticleCategory/13',
#'https://enews.tw/ArticleCategory/14',
#'https://enews.tw/ArticleCategory/15',
#'https://enews.tw/ArticleCategory/16',
#'https://enews.tw/ArticleCategory/43',
#'https://enews.tw/ArticleCategory/44',
#'https://enews.tw/ArticleCategory/17',
#'https://enews.tw/ArticleCategory/18',
#'https://enews.tw/ArticleCategory/19',
#'https://enews.tw/ArticleCategory/20',
#'https://enews.tw/ArticleCategory/42',
#'https://enews.tw/ArticleCategory/21',
#'https://enews.tw/ArticleCategory/22',
#'https://enews.tw/ArticleCategory/45',
#'https://enews.tw/ArticleCategory/23',
#'https://enews.tw/ArticleCategory/24',
#'https://enews.tw/ArticleCategory/25',
#'https://enews.tw/ArticleCategory/26',
#'https://enews.tw/ArticleCategory/27',
#'https://enews.tw/ArticleCategory/28',
#'https://enews.tw/ArticleCategory/29',
#'https://enews.tw/ArticleCategory/30',
#'https://enews.tw/ArticleCategory/31',
#'https://enews.tw/ArticleCategory/32',
#'https://enews.tw/ArticleCategory/33',
#'https://enews.tw/ArticleCategory/34',
#'https://enews.tw/ArticleCategory/35',
#'https://enews.tw/ArticleCategory/36',
#'https://enews.tw/ArticleCategory/37',
#'https://enews.tw/ArticleCategory/38',
#'https://enews.tw/ArticleCategory/46',
#'https://enews.tw/ArticleCategory/47',
#'https://enews.tw/ArticleCategory/48',
#'https://enews.tw/ArticleCategory/41'
#]
#for r in urls:
#    request = {
#        "media": "enews",
#        "name": "enews",
#        "enabled": True,
#        "days_limit": 3600 * 24,
#        "interval": 3600 * 2,
#        "priority": 1,
#        "scrapy_key": "enews:start_urls",
#        "url": r,
#    }
#    collection.insert_one(request)



'''
redis_key = "ltn:start_urls"

collection.insert_one({
    "url": "https://www.myip.com/",
    "priority": 3,
    "search": False,
    "url_pattern": "https://news.ltn.com.tw/ajax/breakingnews/all/{}",
    "interval": 3600,
    "days_limit": 3600,
    "media": "ltn_news",
    "name": "ltn_news",
    "enabled": False,
    "scrapy_key": redis_key
})

collection.update_many({'media': 'facebook'}, {
    '$set': {'schedule.$[].days_limit': 86400}
})
'''
