#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime
from utils.parse_fb_comment import parse_comments

class UDN_OPINION:
    def __init__(self, server):
        print('init udn_opinion.py')
        self.server = server

    def on_CRW2PAR_NTF_UDN_OPINION_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_udn_opinion_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_UDN_OPINION_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_udn_opinion_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_udn_opinion_list(self, content):
        
        def parse_latest_date(html_bs4):
            pub_date = html_bs4.find_all('time')
            link_date = []
            for date in pub_date:
                date = datetime.datetime.strptime( date['datetime'] , '%Y-%m-%d')
                link_date.append(date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        def parse_link(html_bs4):
            links = html_bs4.find_all('h2')
            links = [ {'url':'https://opinion.udn.com'+li.find('a')['href'],'metadata':[]} for li in links] 
            return links
        
        def parse_next_page(current_page):
            url_split = re.split('/',current_page)
            url_split[-1] = str(int(url_split[-1])+1)
            next_page = '/'.join(url_split)
            return next_page

        clean_html = BeautifulSoup(content['list_html'], 'lxml')
        current_page = content['current_page']
        
        next_p = parse_next_page(current_page)        
        latest_post_date = parse_latest_date(clean_html)
        list_meta = parse_link(clean_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_p)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)

        return item

    def parse_udn_opinion_content(self, content):
        
        def parse_time(clean_html):
            date_org = clean_html.find('time').text
            date = datetime.datetime.strptime(date_org, '%d %b, %Y').strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_title(clean_html):
            return clean_html.find('h1', class_ = 'story_art_title').text
        
        
        def parse_content(clean_html):
            return ''.join([ent.text for ent in clean_html.find_all('p')[:-1]]).replace('\n', '') #.replace('      500字以內，目前輸入 0 字      ', '')

        
        def parse_author(clean_html):
            author = clean_html.find('div', class_ = 'story_bady_info_author').find('a').text
            return author    
        
        def parse_metadata(clean_html, fb_like_count_html):
            tmp = clean_html.find('div', id = 'keywords').find_all('a')
            metadata = {'category':'', 
                        'tag':[], 
                        'fb_like_count':''}
            try: 
                metadata['category'] = tmp[0].text    
            except AttributeError:
                pass
            
            try: 
                metadata['tag'] = [i.text for i in tmp[1:]]     
            except AttributeError:
                pass

            metadata['fb_like_count'] = fb_like_count_html.find('span',{'id':'u_0_3'}).text 
            
            return metadata
        
        # detail
        clean_html = BeautifulSoup(content['html'], 'lxml') 
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')  

        url = content['url']
        date = parse_time(clean_html)
        title = parse_title(clean_html)
        content = parse_content(clean_html) 
        author = parse_author(clean_html)
        metadata = parse_metadata(clean_html, fb_like_count_html)

        comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        return item
