#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback, sys
from bs4 import BeautifulSoup
import re
import requests

class MOBILE01:
    
    
    def __init__(self, server):        
        self.server = server

    def on_MNG2CRW_NTF_MOBILE01_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        headers = {'user-agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'}
        try:
            html = {'html': requests.get(url, headers=headers).text, 'url': url}
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_MOBILE01_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()
        
        headers = {'user-agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'}
        html = requests.get(url, headers=headers).text
        soup = BeautifulSoup(html, 'html.parser')
        url_comments = self.get_comments_url(soup)

        html_list = []
        i = 0
        if url_comments:
            for u in url_comments:
                if i > 9:
                    break
                html = requests.get(u, headers=headers).text
                html_list.append(html)
                i = i + 1
        else:
            html_list = [requests.get(url, headers=headers).text]
    
        try:
            html = {'html': html_list, 'url': url}
            # print(html)
        except:
            traceback.print_exc(file=sys.stdout)

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)

    def get_comments_url(self, soup):
        # prefix = 'https://www.mobile01.com'
        # result = soup.select('.u-gapTop--lg.u-gapBottom--max.u-tac .l-pagination__page a')
        # prefix = prefix + result[0].get('href').split('&p=')[0] + '&p='
        # if not result:
        #     return None
        # else:
        #     urls = []
        #     last_page = int(result[-1].get('href').split('&p=')[1])

        #     for i in range(last_page):
        #         urls.append(prefix + str(i+1))
        #     return urls
        
        urls = []
        prefix = 'https://www.mobile01.com'
        result = soup.select('.u-gapTop--lg.u-gapBottom--max.u-tac .l-pagination__page a')
        if result:
            prefix = prefix + result[0].get('href').split('&p=')[0] + '&p='
            last_page = int(result[-1].get('href').split('&p=')[1])
            for i in range(last_page):
                urls.append(prefix + str(i+1))
        return urls



