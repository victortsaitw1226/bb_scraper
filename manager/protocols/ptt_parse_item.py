#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class PARSE_PTT_ITEM(Protocol):

    def __init__(self):
        super(PARSE_PTT_ITEM, self).__init__('PARSE_PTT_ITEM')
        self.update({
          'url': '',
          'board': board,
          'article_title': title,
          'author': author,
          'date': date,
          'content': '',
          'ip': '',
          'message_count': 0,
          'messages': ''
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_board(self, board):
        self.set('board', board)

    def get_board(self):
        return self.get('board')

    def set_title(self ,title):
        self.set('article_title', title)

    def get_title(self):
        return self.get('article_title')

    def set_author(self, author):
        self.set('author', author)

    def get_author(self):
        return self.get('author')

    def set_date(self, date):
        self.set('date', date)

    def get_date(self):
        return self.get('date')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')

    def set_ip(self, ip):
        self.set('ip', ip)

    def get_ip(self):
        return self.get('ip')

    def set_message_count(self, message_count):
        self.set('message_count', message_count)

    def get_message_count(self):
        return self.get('message_count')

    def set_messages(self, messages):
        self.set('messages', messages)

    def get_messages(self):
        return self.get('messages')
