# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class ItHomeNewsSpider(RedisSpider):
    name = "ithomenews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "ithome",
            "name": "ithome",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            #"url": "https://www.ithome.com.tw/news?page=0",
            #"url": "https://www.ithome.com.tw/tech?page=0",
            #"url": "https://www.ithome.com.tw/feature?page=0",
            #"url": "https://www.ithome.com.tw/big-data?page=0",
            #"url": "https://www.ithome.com.tw/blockchain?page=0",
            #"url": "https://www.ithome.com.tw/cloud?page=0",
            #"url": "https://www.ithome.com.tw/devops?page=0",
            #"url": "https://www.ithome.com.tw/tags/gdpr?page=0",
            "url": "https://www.ithome.com.tw/security?page=0",
            "scrapy_key": "ithomenews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.text, 'html.parser')
        current_page = re.search("page=(\d+)", response.url).group(1)
        current_page = int(current_page)
        links, latest_datetime = self.parse_links(soup, current_page)

        for url in links:
            yield response.follow(url,
                    meta = meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        next_page = re.sub("page=(\d+)", "page={}".format(int(current_page) + 1), response.url)

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_links(self, soup, page):
        links = []
        link_date = []
        ## get head line news if exists
        if (page == 0) and (len(soup.select('.channel-headline'))):
            url = soup.select('.channel-headline .title a')[0]['href']
            links.append(url)

        for item in soup.find_all('div', class_='item'):
            url = item.select('.title a')[0]['href']
            links.append(url)
            date_tag = item.find('p', class_='post-at')
            if not date_tag:
                date_tag = item.find('p', class_='created')
            date = date_tag.text.strip()
            link_date.append(datetime.strptime(date, '%Y-%m-%d'))

        return links, max(link_date)
    
    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'ithome'
        item['proto'] = 'ITHOME_PARSE_ITEM'

        # Get FB Like
        meta['item'] = item
        header = soup.find('head')
        fb_app_id = header.find('meta', {'property': 'fb:app_id'}).get('content')
        fb_like = 'https://www.facebook.com/plugins/like.php?action=like&app_id={}&href={}'.format(
            fb_app_id, response.url)

        yield scrapy.Request(fb_like,
                meta=meta,
                dont_filter=True,
                callback=self.parse_fb_like)

    def parse_fb_like(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        like_count = 0
        like_msg = soup.find('span', {"id":"u_0_3"})
        if like_msg:
            like_msg = like_msg.get_text()
            like = re.search('(\d+) people like this', like_msg)
            if like:
                like_count = int(like.group(1))
        item = meta['item']
        item['metadata']['fb_like_count'] = like_count
        return item

    def parse_datetime(self,soup):
        date = soup.find('span', class_='created').text
        date = datetime.strptime(date, '%Y-%m-%d')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author(self,soup): 
        return soup.find('span', class_='author').text
    
    def parse_title(self,soup):
        return soup.select('h1')[0].text
    
    def parse_content(self,soup):
        paragraph = [soup.select('.content-summary')[0].text]
        for p in soup.find_all('div', class_='content'):
            for ele in p.find_all('p', class_=None):
                paragraph.append(ele.text)
        content = '\n'.join(paragraph)
        return content

    def parse_metadata(self,soup ,fb_like_soup=None):
        metadata = {'category':'','fb_like_count': ''}
        try:
            metadata['category'] = soup.find('div','category-label').find('a').text
        except:
            pass
        return metadata
