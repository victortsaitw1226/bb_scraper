# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class PtsSpider(RedisSpider):
#class PtsSpider(scrapy.Spider):
    name = "pts"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "pts",
            "name": "pts",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "post_url": "https://news.pts.org.tw/news_more.php",
            "url": "https://news.pts.org.tw",
            "scrapy_key": "pts:start_urls",
            "priority": 1,
            "page": -1
        }]
        for request in requests:
            yield scrapy.Request(url=request['url'],
                    meta=request)


    def parse(self, response):
        meta = response.meta
        yield scrapy.FormRequest(
            url=meta['post_url'],
            formdata={'page': str(meta['page'])},
            method='POST',
            meta=meta,
            dont_filter=True,
            callback=self.parse_list)
 

    def parse_list(self, response):
        meta = response.meta
        body = json.loads(response.body.decode('utf-8', 'ignore'))

        # the final page
        if body==[]:
            return

        posts_date = []
        now = datetime.now() + timedelta(hours=8)
        for b in body:
            timediff = b['timediff']
            time_search = re.search('(\d+)天(\d+)小時', timediff)
            post_days = int(time_search.group(1))
            post_hours = int(time_search.group(2))
            post_date = now - timedelta(hours=(post_days * 23 + post_hours))
            posts_date.append(post_date)

            url = 'https://news.pts.org.tw/article/' + b['news_id']
            yield scrapy.Request(url,
                    meta={
                        'date': post_date  
                    },
                    callback=self.parse_article)

        latest_datetime = max(posts_date)
        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        meta['page'] += 1

        # page0 and page1 are the same
        if meta['page'] == 1:
            meta['page'] += 1
            
        yield scrapy.FormRequest(
                url=meta['url'],
                formdata={'page': str(meta['page'])},
                method='POST',
                meta=meta,
                dont_filter=True,
                callback=self.parse_list)
    
    
    def parse_article(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = meta['date']
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'pts'
        item['proto'] = 'PTS_PARSE_ITEM'
        return item

    #def parse_datetime(self, soup):
    #    date_org = soup.find(class_ = 'mid-news').find('h2').text
    #    date = datetime.strptime(date_org, '%Y年%m月%d日')
    #    return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
    
    def parse_author(self, soup):
        author = soup.find('div','subtype-sort').text
        return author
    
    def parse_title(self, soup):
        title = soup.find(class_ = 'article-title').text
        title = ' '.join(title.split())
        return title
    
    def parse_content(self, soup):
        content = ' '.join(soup.find(class_ = 'article_content').text.split())
        return content

    def parse_metadata(self, soup):
        try: 
            category = soup.find(class_ = 'title hidden-xs').text.split()[0]
        except:
            category = ''
        metadata = {'category': category}
        return metadata
    
