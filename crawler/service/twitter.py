#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback
import tweepy
import time
import sys


class TWITTER:

    def __init__(self, server):        
        self.server = server

        # Key和secret資訊
        consumer_key = 'JXDsfHPXJhLZKsd8SL51sZHoE'
        consumer_secret = 'VxiWucXQLf2kVPjtBnlj9IziiWo3OngZjGbX8NJJQKLPxkU85r'
        access_token = '1168809630790610944-0FWQajJL0tLpJmYCX68YmaCSTv5I5H'
        access_token_secret = 'VGL23efJnvjbrEQboez8eqXaw1tka0axAKXc5hRfmuoEh'
        
        # 提交Key和secret
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)


    def on_MNG2CRW_NTF_TWITTER_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()
        
        try:
            user_name = url['user_name']
            page = url['page']
            public_tweets = self.api.user_timeline(user_name, page = page, tweet_mode="extended")
            public_tweets_list = [ pt._json for pt in public_tweets]
            html = {'public_tweets_list':public_tweets_list,'user_name':user_name,'page':page+1}
        except:
            traceback.print_exc(file=sys.stdout)
            
        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)
        

    def on_MNG2CRW_NTF_TWITTER_GET_POST(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        tweet_info = from_notify.get_url()

        try:
            def get_replies(tweet_id, user_name):
    
                replies = tweepy.Cursor(self.api.search, q='to:{}'.format(user_name),
                                        since_id=tweet_id, tweet_mode='extended').items()
                reply_list = []
                i = 0
                while True:
                    i = i + 1
                    try:
                        reply = replies.next()
                        if not hasattr(reply, 'in_reply_to_status_id_str'):
                            continue

                        if reply.in_reply_to_status_id_str == tweet_id:
                            reply_list.append(reply)
                
                    except tweepy.RateLimitError as e:
                        print("Twitter api rate limit reached".format(e))
                        time.sleep(60)
                        continue
                
                    except tweepy.TweepError as e:
                        print("Tweepy error occured:{}".format(e))
                        break
                
                    except StopIteration:
                        print('Twitter stop iteration')
                        break
                
                    except Exception as e:
                        print("Failed while fetching replies {}".format(e))
                        break
                
                return reply_list
            
            replies = get_replies(tweet_info['id_str'], tweet_info['user']['screen_name'])
            replies_list = [ r._json for r in replies]
            url = 'https://twitter.com/' + tweet_info['user']['screen_name'] + '/status/'+ tweet_info['id_str']
            html = {'content':tweet_info, 'replies':replies_list, 'url': url}
        except:
            traceback.print_exc(file=sys.stdout)

        to_notify = CRW2PAR_NTF_NEWS_POST() 
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
