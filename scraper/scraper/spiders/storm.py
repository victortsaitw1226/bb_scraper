# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class StormSpider(RedisSpider):
    name = "storm"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return

        requests = [{
            "media": "storm",
            "name": "storm",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://www.storm.mg/articles",
            "scrapy_key": "storm:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        next_page = self.parse_next_page(soup)
        latest_datetime, links = self.parse_latest_date_and_links(soup)

        for url in links:
            yield scrapy.Request(url,
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_latest_date_and_links(self,soup):
        links = []
        datetimes = []
        for element in soup.find_all('div', class_='card_inner_wrapper'):

            if element.find('h3'):      # find the normal-title articles
                pass

            elif element.find('h2'):    # find the pinned article
                if not element.find('div', class_="tags_wrapper"):
                    continue
            else:
                continue

            links.append(element.find('a', class_='card_link link_title').get('href'))

            time_ = element.find('span', class_='info_time')
            dt = datetime.strptime(time_.text, '%Y-%m-%d %H:%M')
            datetimes.append(dt)

        latest_post_date = max(datetimes)
        return latest_post_date, links
    
    def parse_next_page(self,soup):
        try:
            next_page = soup.find('a', id="next").get('href')
        except:
            next_page = None
        return next_page

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        article_id = response.url.split('/')[-1]
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup,article_id)
        item['content_type'] = 0
        item['media'] = 'storm'
        item['proto'] =  'STORM_PARSE_ITEM'
        return item

    def parse_datetime(self, soup):
        time_ = soup.find(class_='info_inner_content', id='info_time')
        date = datetime.strptime(time_.text, '%Y-%m-%d %H:%M')
        return date.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author_url(self, soup):
        domain_prefix = 'https://www.storm.mg'
        author_id = soup.find('a', class_='link_author info_inner_content')[
            'data-author_id']
        return domain_prefix + '/authors/' + author_id

    def parse_author(self, soup):
        author_id = soup.find('a', class_='link_author info_inner_content')
        author_id = author_id.find('span').text
        return author_id

    def parse_title(self, soup):
        _title = soup.find('title').text[:-4]
        _title = ''.join(_title.split())
        return _title

    def parse_content(self, soup):

        def has_aid_but_no_span(tag):
            return tag.has_attr('aid') and not tag.has_attr('span')

        content = []
        content = soup.find_all(has_aid_but_no_span)
        content = ''.join([x.text for x in content])
        content = ''.join(content.split())
        return content

    def parse_metadata(self, soup, article_id):

        category = soup.find('meta',{'property':'article:section'})['content']

        tags_content = []
        _tags_content = soup.find_all(class_='tag tags_content')
        for tag in _tags_content:
            tags_content.append(tag.text)

        try:
            view_count_html = requests.get('https://service-pvapi.storm.mg/pvapi/get_pv/' + article_id).text
            view_count = int(json.loads(view_count_html)['total_count'])
        except:
            view_count = 0

        # 文章分類、人氣、關鍵字、分享數
        metadata = {
            'category': category,
            'view_count': view_count,
            'tag': tags_content,
        }
        return metadata
    

