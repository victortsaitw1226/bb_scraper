#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class CRNTT:
    def __init__(self, server):
        print('init crntt.py')
        self.server = server

    def on_CRW2PAR_NTF_CRNTT_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_crntt_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CRNTT_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_crntt_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_crntt_list(self, content):

        def parse_links(soup):
            links = soup.find('table',{'width':"98%",'border':"0",'cellspacing':"0",'cellpadding':"10"})
            links = links.find_all('li')
            links = [ {'url':crntt + li.find('a')['href'],'metadata':[]} for li in links]  
            return links
        
        def parse_next_page(soup):
            crntt_app = 'http://hk.crntt.com/crn-webapp/'
            try:
                next_page = soup.find('a',text = '下一頁')['href']
                next_page = crntt_app + next_page
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find('table',{'width':"98%",'border':"0",'cellspacing':"0",'cellpadding':"10"})
            pub_date = pub_date.find_all('li')
            link_date = []
            for date in pub_date:
                date = datetime.strptime( date.find('em').text , '(%Y-%m-%d %H:%M:%S)')
                link_date.append(date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
                
        list_html = BeautifulSoup(content, "html.parser")
        crntt = 'http://hk.crntt.com'

        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_crntt_content(self, content):
        
        def parse_date(soup):     
            date = soup.find_all('td',{'align':"center"})[3]
            date.find('font').decompose()
            date = date.text.strip()
            date = datetime.strptime( date , '%Y-%m-%d %H:%M:%S')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
            return date
        
        def parse_content(soup):
            content = soup.find('td',{'style':'font-size: 16px;LINE-HEIGHT: 150%;text-align:justify;text-justify:inter-ideograph;'})
            for img in content.find_all('td',{'align':"center"}):
                img.decompose()
            content = '\n'.join(content.text.split())
            return content
        
        def parse_author(content):
            try:
                author = re.search(r'（記者\s(.*)）',content).group(1)
            except:
                author = ''
            return author
   

        # detail
        detail_html = BeautifulSoup(content['detail_html'], 'html.parser')
        url = content['url']
        title = detail_html.find('head').find('title').text
        title = ' '.join(title.split())
        date = parse_date(detail_html) 
        content = parse_content(detail_html)
        author = parse_author(content)
        keywords = detail_html.find('head').find('meta',{'name':'keywords'})['content']
        keywords = re.split(',',keywords)   
        category = detail_html.find('td',{'width':'478','height':'25','style':'line-height:150%;'}).find_all('a')[-1].text
    
        metadata = {'tag':keywords, 'category':category}
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_metadata(metadata)
        
        return item
