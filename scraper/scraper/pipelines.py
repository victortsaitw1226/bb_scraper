# -*- coding: utf-8 -*-
from scrapy.exceptions import DropItem
from scrapy.utils.project import get_project_settings
from scrapy import signals
from scrapy.utils.serialize import ScrapyJSONEncoder
from twisted.internet.threads import deferToThread
from dateutil.parser import parse as parse_time
import logging
import pymongo
import json
import os
from elasticsearch import Elasticsearch
# for mysql
import mysql.connector
from mysql.connector import errorcode
import pika
import datetime
import hashlib
import requests

from scraper.items import NewsItem

SETTINGS = get_project_settings()

logger = logging.getLogger(__name__)


class SaveToRabbitMQPipeline(object):

    def __init__(self):
        self.host = SETTINGS['BROKER_HOST']
        self.port = SETTINGS['BROKER_PORT']
        self.user = SETTINGS['BROKER_USERID']
        self.password = SETTINGS['BROKER_PASSWORD']
        self.virtual_host = SETTINGS['BROKER_VIRTUAL_HOST']
        #self.exchange=SETTINGS["RABBITMQ_EXCHANGE"]
        self.exchange = "crawler.events"
        self.routing_key = SETTINGS["RABBITMQ_ROUTING_KEY"]
        #self.routing_key = spider.name
        self.queue = SETTINGS["RABBITMQ_QUEUE"]

        credentials = pika.PlainCredentials(self.user, self.password)
        parameters = pika.ConnectionParameters(self.host,
                                               self.port,
                                               self.virtual_host,
                                               credentials)
        # Connecting to RabbitMQ
        self.connection = pika.BlockingConnection(parameters=parameters)
        self.channel = self.connection.channel()

        # Declaring RabbitMQ exchange
        self.channel.exchange_declare(exchange=self.exchange,
                                      exchange_type="topic",
                                      durable=True,
                                      auto_delete=True)
        # Decaling RabbitMQ queue
        self.channel.queue_declare(queue=self.queue,
                                   durable=True)
        # Binding exchange + routing_key = queue
        self.channel.queue_bind(exchange=self.exchange,
                                routing_key=self.routing_key,
                                queue=self.queue)
        self.encoder = ScrapyJSONEncoder()

    def process_item(self, item, spider):
        # Encoding item dict using Scrapy JSON Encoder
        data = self.encoder.encode(item)
        # Publishing item to exchange + routing_key = queue
        self.channel.basic_publish(
            exchange=self.exchange,
            routing_key=self.routing_key,
            body=data,
        )
        # Returning item to be processed
        return item

class SaveToMongoPipeline(object):

    ''' pipeline that save data to mongodb '''
    def __init__(self):
        connection = pymongo.MongoClient(SETTINGS['MONGODB_SERVER'], SETTINGS['MONGODB_PORT'])
        self.db = connection[SETTINGS['MONGODB_DB']]
        self.collection = self.db[SETTINGS['MONGODB_DATA_COLLECTION']]


    def process_item(self, item, spider):
        data = self.collection.find_one({'url': item['docurl']})
        if data is None:
            res = self.collection.insert_one(dict(item))
            _id = res.inserted_id
        else:
            _id = data['_id']
        item['proto_id'] = str(_id)
        return item


class SaveToElasticsearchPipeline(object):

    def __init__(self):
        es_timeout = SETTINGS['ELASTICSEARCH_TIMEOUT']
        es_servers = SETTINGS['ELASTICSEARCH_SERVERS']
        es_servers = es_servers if isinstance(es_servers, list) else [es_servers]
        es_settings = dict()
        es_settings['hosts'] = es_servers
        es_settings['timeout'] = es_timeout
        if SETTINGS['ELASTICSEARCH_USERNAME'] and SETTINGS['ELASTICSEARCH_PASSWORD']:
            es_settings['http_auth'] = (SETTINGS['ELASTICSEARCH_USERNAME'], SETTINGS['ELASTICSEARCH_PASSWORD'])
        self.index = SETTINGS['ELASTICSEARCH_INDEX']
        self.type = SETTINGS['ELASTICSEARCH_TYPE']
        self.es = Elasticsearch(**es_settings)

    def upsert(self, _id, data):
        doc = dict({
            'doc_as_upsert': True,
            'doc': data
        })
        return self.es.update(
            index=self.index, doc_type=self.type, id=_id, body=doc)

    def process_item(self, item, spider):
        print('-' * 100)
        print('-' * 100)
        print('-' * 100)
        print('save to ES')
        print('-' * 100)
        print('-' * 100)
        print('-' * 100)
        _id = item['data_id']
        self.upsert(_id, item)
        return item


class TransformDataPipeline(object):

    def __init__(self):
        self.media_dict = {
            'mirrormedia': '鏡週刊',
            'setn' : '三立新聞',
            'match' : 'Match生活網',
            'mobile01' : 'Mobile01',
            'wownews' : 'Wownews',
            'grinews' : '草根引響力新視野',
            'epoch' : '大紀元',
            'taro' : '芋傳媒',
            'tssd' : '台灣新生報',
            'kairos': '風向新聞',
            'newtalk' : '新頭殼',
            'udnblog' : 'udn部落格',
            'engadget' : 'engadget',
            'kknews' : 'kk新聞',
            'crntt' : '中評網',
            'upmedia_comment' : '上報',
            'upmdeia_life' : '上報',
            'upmedia_news' : '上報',
            'upmedia' : '上報',
            'enews' : 'eNews',
            'peopo' : '公民新聞',
            'eranews' : '年代',
            'urcosme' : 'UrCosme',
            'sina' : 'Sina新聞',
            'cntimes' : '大華網路報',
            'housefun' : '好房網',
            'tnr' : '台灣新論',
            'twtimes' : '台灣時報',
            'hkej' : '信報財經新聞',
            'ejinsight' : 'ejinsight',
            'greatnews' : '大成報',
            'ck101' : '卡提諾論壇',
            'fountmedia' : '放言科技傳媒',
            'bbc' : 'BBC',
            'prnasia' : '美通社PR',
            'ithome' : 'iThome',
            'mna' : '國防部軍事新聞通訊社',
            'storm' : '風傳媒',
            'ttv' : '台視',
            'ft' : 'FT中文網',
            'cool3c' : 'Cool3c',
            'uho' : 'uho優活健康網',
            'lawtw' : '台灣法律網',
            'ltn_news' : '自由時報',
            'ltn_estate' : '自由時報',
            'thenewslens' : '關鍵評論網路',
            'ksnews' : '更生日報',
            'tnews' : '大台灣新聞網',
            'healthnews' : '健康醫療網',
            'businesstoday' : '今周刊',
            'yahoo' : 'Yahoo奇摩',
            'linetoday' : 'LineToday',
            'chinatimes' : '中時電子報',
            'life' : 'LIFE生活網',
            'nextmag' : '壹週刊', 
            'appledaily' : '蘋果日報',
            'cts' : '華視新聞網',
            'udn' : '聯合新聞網',
            'udn_opinion' : '聯合新聞網',
            'hinet_times' : 'Hinet',
            'hinet' : 'Hinet',
            'nownews' : 'NOWnews',
            'tvbs' : 'TVBS',
            'ettoday' : 'ETtoday新聞雲',
            'etforum' : '雲論(ETtoda論壇)',
            'zimedia': '字媒體',
            'cnyes': 'anue鉅亨',
            'cna': '中央通訊社',
            'popdaily': '波波黛莉的異想世界',
            'buzzorange': '報橘',
            'eyny': '伊莉論壇',
            'gamer': '巴哈姆特',
            'pixnet': '痞客邦',
            'plurk': '噗浪',
            'twitter': '推特',
            'ebc': '東森新聞',
            'pts': '公視新聞',
            'ptt': 'Ptt',
            'dcard': 'DCard',
            'facebook': 'Facebook',
            'youtube': 'YouTube'
        }

    def transfer_number(self, data):
        if type(data) == str:
            if not data.isdigit():
                data = '0'
            data = int(data)
        return data

    def hash_code(self, data):
        str_data = json.dumps({
          'postauthor': data['author'],
          'url': data['url'],
          'body': data['content']
        }, sort_keys=True, indent=2)
        return hashlib.md5(str_data.encode("utf-8")).hexdigest()

    def process_item(self, item, spider):
        comment_body = []
    
        docurl = item.get('url', '')
        time = item.get('date', '')
        cdate = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%dT%H:%M:%S')
    
        metadata = item.get('metadata', {})
        fb_like_count = self.transfer_number(metadata.get('fb_like_count', ''))
        fb_share_count = self.transfer_number(metadata.get('fb_share_count', ''))
        line_share_count = self.transfer_number(metadata.get('line_share_count', ''))
        like_count = self.transfer_number(metadata.get('like_count', ''))
        share_count = self.transfer_number(metadata.get('fb_like_count', ''))
        reply_count = self.transfer_number(metadata.get('reply_count', ''))
        thanks_count = self.transfer_number(metadata.get('thanks_count', ''))
        agree_count = self.transfer_number(metadata.get('agree_count', ''))
        disagree_count = self.transfer_number(metadata.get('disagree_count', ''))
        view_count = self.transfer_number(metadata.get('view_count', ''))

        docboard = metadata.get('category','')
        news_source = metadata.get('news_source','')

        all_comments = item.get('comment', [])
        if reply_count == 0:
            reply_count = len(all_comments)

        if view_count != 0:
            brocount = view_count
        else:
            brocount = fb_like_count + fb_share_count + line_share_count + like_count +  \
                    share_count + reply_count + thanks_count + agree_count + disagree_count
    
        raw_media = item.get('media', '')
        media = self.media_dict.get(raw_media, raw_media)

        datafrom = 0
        if raw_media in ['mobile01', 'ck101', 'udnblog', 'eyny', 'ptt']:
            datafrom = 2

        if time is None:
            time = ''
    
        if isinstance(time, str) and time != '':
            time_obj = parse_time(time, ignoretz=True)
            timestamp = time_obj.timestamp()
            postdate = datetime.datetime.fromtimestamp(timestamp)
            pdate = datetime.datetime.strftime(postdate, '%Y-%m-%dT%H:%M:%S')
        elif isinstance(time, datetime.datetime):
            pdate = datetime.datetime.strftime(time, '%Y-%m-%dT%H:%M:%S')
        else:
            pdate = cdate
    

        title = item.get('article_title', '')
        
        source_id = self.hash_code(item)
        return dict({
            "data_id": source_id,
            "postauthor": item.get('author', ''),
            "Author_ID": item.get('author_url', ''),
            "createdate": cdate,
            "docid": source_id,
            "MainID": source_id,
            "docboard": docboard,
            "news_source": news_source,
            "content": item['content'],
            "media": media,
            "SourceTypeV2": item.get("media", ''),
            "SourceType": 0,
            "docurl": docurl,
            "PostDate": pdate,
            "contentType": item['content_type'],
            "candidate": '',
            "mediatype": datafrom,
            "datafrom": datafrom,
            "title": title,
            "brocount": brocount,
            "repcount": int(reply_count),
            "datatype": 0,
            "posscore": 0,
            "negscore": 0,
            "evaluation": 0,
            "candidates": []
        })

class EvaluationPipeline(object):

    def __init__(self):
        self.url = SETTINGS['EVALUATION_URL']

    def evaluate(self, body):
        value = {
            'sentences': [body]        
        }
        try:
            response = requests.post(self.url, json=value, timeout=0.5)
            j = response.json()
            return j.get('results', [0])[0]
        except:
            return -1

    def process_item(self, item, spider):
        score = self.evaluate(item['content'])
        item['evaluation'] = score
        return item
