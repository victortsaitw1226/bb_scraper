#!/usr/bin/env python
# -*- coding: utf-8 -*-
import urllib.parse
import re
import time
from bs4 import BeautifulSoup
from protocols.ptt_parse_item import PTT_PARSE_ITEM
from protocols.ptt_parse_list import PTT_PARSE_LIST
from protocols.par2mng_ntf_ptt_post_urls import PAR2MNG_NTF_PTT_POST_URLS
from protocols.crw2par_ntf_ptt_post import CRW2PAR_NTF_PTT_POST
from protocols.crw2par_ntf_ptt_index import CRW2PAR_NTF_PTT_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime

class PTT:
    def __init__(self, server):
        print('init ptt.py')
        self.server = server

    def on_CRW2PAR_NTF_PTT_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_PTT_INDEX()
        from_notify.parse(packet)
        days_limit = from_notify.get_days_limit()
        content = from_notify.get_content()

        item = self.parse_ptt_list(content)

        to_notify = PAR2MNG_NTF_PTT_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_board(item.get_board())
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_PTT_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_PTT_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ptt_content(content)

        result = item.to_dict()
        result['media'] = 'ptt'
        url = self.server.mongo.upsert_post('history_v2', 'data', result['url'], result)
        #if url is None:
        #    return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ptt_list(self, content):
        item = PTT_PARSE_LIST()
        links = []
        soup = BeautifulSoup(content, 'html.parser')
        page = soup.find('div', class_= 'btn-group btn-group-paging')
        next_page = page.findAll('a')[1]
        next_page_url = next_page.get('href')
        year = datetime.now().year
        #latest_datetime = datetime.now()
        latest_datetime = None
        board = ''
        for div in soup.findAll('div',class_='r-ent'):
            a = div.find('div', class_='title').find('a')
            if not a:
                continue

            #date = div.find('div', class_='date').get_text()
            #date = str(year) + '/' + date.strip()
            #date = datetime.strptime(date, '%Y/%m/%d')


            href = a.get('href')
            post_timestamp = re.search(r'M.(\d+)', href).group(1)
            date = datetime.fromtimestamp(int(post_timestamp))

            if latest_datetime is None:
                latest_datetime = date

            elif date > latest_datetime:
                latest_datetime = date

            links.append(urllib.parse.urljoin('https://www.ptt.cc/', href))
            board = href.split('/')[2]

        item.set_next_page_url(urllib.parse.urljoin('https://www.ptt.cc/', next_page_url))
        item.set_latest_post_date(latest_datetime.isoformat())	
        item.set_urls(links)
        item.set_board(board)
        return item

    def parse_ptt_content(self, content):
        item = PTT_PARSE_ITEM()
        soup = BeautifulSoup(content, 'html.parser')
        link = soup.find('head').find('link', {'rel': 'canonical'}).get('href')
        board = soup.find('a', class_='board')
        board.find('span').extract()
        board = board.get_text()
        main_content = soup.find(id="main-content")
        metas = main_content.select('div.article-metaline')
        author = ''
        title = ''
        date = ''
        if metas:
            author = metas[0].select('span.article-meta-value')[0].string if metas[0].select('span.article-meta-value')[0] else author
            title = metas[1].select('span.article-meta-value')[0].string if metas[1].select('span.article-meta-value')[0] else title
            date = metas[2].select('span.article-meta-value')[0].string if metas[2].select('span.article-meta-value')[0] else date

            # remove meta nodes
            for meta in metas:
                meta.extract()
            for meta in main_content.select('div.article-metaline-right'):
                meta.extract()
        # remove and keep push nodes
        pushes = main_content.find_all('div', class_='push')
        for push in pushes:
            push.extract()
        try:
            ip = main_content.find(text=re.compile(u'※ 發信站:'))
            ip = re.search('[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*', ip).group()
        except:
            ip = "None"
        # 移除 '※ 發信站:' (starts with u'\u203b'), '◆ From:' (starts with u'\u25c6'), 空行及多餘空白
        # 保留英數字, 中文及中文標點, 網址, 部分特殊符號
        filtered = [ v for v in main_content.stripped_strings if v[0] not in [u'※', u'◆'] and v[:2] not in [u'--'] ]
        expr = re.compile(r'[^\u4e00-\u9fa5\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b\s\w:/-_.?~%()]')
        for i in range(len(filtered)):
            filtered[i] = re.sub(expr, '', filtered[i])

        filtered = [_f for _f in filtered if _f]  # remove empty strings
        filtered = [x for x in filtered if link not in x]  # remove last line containing the url of the article
        content = ' '.join(filtered)
        content = re.sub(r'(\s)+', ' ', content)
        # print 'content', content
        # push messages
        p, b, n = 0, 0, 0
        messages = []
        for push in pushes:
            if not push.find('span', 'push-tag'):
                continue

            push_tag = push.find('span', 'push-tag').string.strip(' \t\n\r')
            push_userid = push.find('span', 'push-userid').string.strip(' \t\n\r')
            # if find is None: find().strings -> list -> ' '.join; else the current way
            push_content = push.find('span', 'push-content').strings
            push_content = ' '.join(push_content)[1:].strip(' \t\n\r')  # remove ':'
            push_ipdatetime = push.find('span', 'push-ipdatetime').string.strip(' \t\n\r')
            messages.append( {'push_tag': push_tag, 'push_userid': push_userid, 'push_content': push_content, 'push_ipdatetime': push_ipdatetime} )
            if push_tag == u'推':
                p += 1
            elif push_tag == u'噓':
                b += 1
            else:
                n += 1
            
        # count: 推噓文相抵後的數量; all: 推文總數
        message_count = {'all': p+b+n, 'count': p-b, 'push': p, 'boo': b, "neutral": n}
        item.set_url(link)
        item.set_board(board)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_ip(ip)
        item.set_message_count(message_count)
        item.set_messages(messages)
        return item
