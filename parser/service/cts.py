#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta

class CTS:
    def __init__(self, server):
        print('init cts.py')
        self.server = server

    def on_CRW2PAR_NTF_CTS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_cts_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CTS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_cts_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_cts_list(self, content):
             
        soup = BeautifulSoup(content,"lxml")
        
        links = [{'url':s['href'],'metadata':[]} for s in soup.find('div','newslist-container flexbox').findAll('a')]
        link_date = [datetime.strptime(s.text, '%Y/%m/%d %H:%M') for s in soup.find('div','newslist').findAll('span','newstime')]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
        next_page = None

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_cts_content(self, content):

        # detail
        soup = BeautifulSoup(content['html'], 'lxml')
        #fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        #line_share_count_html =  BeautifulSoup(content['line_share_count'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']    
        date = soup.find("meta",  itemprop="dateCreated")['content'].replace('+08:00','+0800')
        title = soup.find('h1','artical-title').text

        try:
            video_url = soup.find('div','youtube_player').find('iframe')['src']
        except:
            try:
                video_url = soup.find('video')['src']
            except:
                video_url = ''
        
        content = soup.find('div','artical-content')
        for div in content.findAll('div'): 
            div.decompose()
        for div in content.findAll('p','news-src'): 
            div.decompose()   
        for div in content.findAll('script'): 
            div.decompose() 
        for div in content.findAll('style'): 
            div.decompose() 
        content = content.text.strip().replace('\xa0', '')

        author = content.split('/')[0].strip()
        try:
            author = re.search(r'(.*?) 綜合報導', author).group(1)
        except:
            try:
                author = re.search(r'(.*?) 報導', author).group(1)
            except:
                pass

        tag = soup.find("meta", {'name':"keywords"})['content'].split(',')
        category = soup.find("meta", {'name':"section"})['content']


        # fb_like_count = fb_like_count_html.find('span',{'id':'u_0_3'}).text
        # line_share_count = line_share_count_html.find('span','num').text
        # metadata = {'category': category, 'tag': tag,'fb_like_count':fb_like_count,
        #             'line_share_count':line_share_count, 'video_url': video_url}
        metadata = {'category': category, 'tag': tag,'fb_like_count':'',
                    'line_share_count':'', 'video_url': video_url}
        # comment = parse_comments(comment_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item
