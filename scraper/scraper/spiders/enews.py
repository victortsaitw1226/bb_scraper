# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
import traceback, sys
from datetime import datetime, timedelta
import re
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
import json
from .redis_spiders import RedisSpider

class EnewsSpider(RedisSpider):
#class EnewsSpider(scrapy.Spider):
    name = "enews"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "enews",
            "name": "enews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://enews.tw/ArticleCategory/1",
            "scrapy_key": "enews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        latest_datetime = self.parse_latest_post_date(soup)

        for link in soup.find_all('div','articleTitle'):
            url = link.find('a')
            if url is None:
                continue
            yield response.follow(url['href'],
                    meta = meta,
                    callback=self.parse_article)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return
 
        next_page = self.parse_next_page(soup)
        if next_page is None:
            return 

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self,soup):
        page_bar = soup.find('div',{'id':'pageBox'})
        try:
            next_page = page_bar.find('div','next').find('a')['href']
        except:
            next_page = None
        return next_page
    
    def parse_latest_post_date(self,soup):
        pub_date = soup.find_all('div','ArticleInfo')
        link_date = []
        for date in pub_date:
            date = ''.join(date.text.split())
            date = re.split('\|',date)[1]
            date = datetime.strptime( date , '%Y-%m-%d')
            link_date.append(date)
        # latest_post_date
        latest_post_date = max(link_date)
        return latest_post_date
    

    def parse_article(self, response):
        cookie = response.headers.getlist('Set-Cookie')
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        metadata = {
            'browse_num':self.parse_browse_num(soup) , 
            'category':self.parse_category(soup),
            'fb_like_num':'',
            'fb_share_num':''
        }
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = metadata
        item['content_type'] = 0
        item['media'] = meta['media']
        item['proto'] =  'ENEWS_PARSE_ITEM'
        return item

    def parse_title(self,soup):
        title = soup.find('head').find('meta',{'property':"og:title"})['content']
        title = ' '.join(title.split())
        return title
    
    def parse_datetime(self,soup):     
        date = soup.find('div',{'id':'contentInfo'}).find_all('span')[1].text
        date = datetime.strptime( date , '%Y-%m-%d')
        date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        return date
    
    def parse_author(self,soup):
        author = soup.find('div',{'id':'contentInfo'}).find_all('a')[0].text
        author = ' '.join(author.split())
        return author
    
    def parse_content(self,soup):
        content = soup.find('div',{'id':'contentWrap'})
        content = ' '.join(x.text for x in content.find_all('p'))
        content = '\n'.join(content.split())
        return content
    
    def parse_browse_num(self,soup):
        browse_num = soup.find('div',{'id':'contentInfo'}).find('span',{'id':'view_count'}).text
        return browse_num
    
    def parse_category(self,soup):
        try:
            category = soup.find('div',{'id':'contentInfo'}).find_all('a')[-1].text
        except:
            category = ''
        return category
    

