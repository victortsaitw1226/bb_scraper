#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime


class BBC:
    def __init__(self, server):
        print('init bbc.py')
        self.server = server

    def on_CRW2PAR_NTF_BBC_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_bbc_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_BBC_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_bbc_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_bbc_list(self, content):

        def parse_next_page(html_bs4):        
            return ''
        
        def parse_latest_date(html_bs4):
            time_element = html_bs4.find('li','mini-info-list__item')
            s = ''.join(time_element.text.split())
            #s = ''.join(html_bs4.find(class_ = 'mini-info-list__item').text.split())
            try:
                latest_post_date = datetime.datetime.strptime(s, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                t = {"天前":" days ago", "小時前":" hours ago", "分鐘前":" minutes ago", "秒前":" seconds ago"}
                for key in t:
                    if key in s:
                        s = s.replace(key, t[key])
                parsed_s = [s.split()[:2]]
                time_dict = dict((fmt,float(amount)) for amount,fmt in parsed_s)
                dt = datetime.timedelta(**time_dict)
                latest_post_date = (datetime.datetime.now() - dt).strftime("%Y-%m-%dT%H:%M:%S+0800")
            
            return latest_post_date
            
        def parse_link(html_bs4):
            
            def parse_list_meta(post):
                ''' parse url and metadata of news list '''
                
                if post[0] == '/':
                    post = 'https://www.bbc.com' + post
                
                meta = {
                    'url': post,
                    'metadata': {}
                }
                return meta
            
            post_entries = set([p.attrs['href'] for p in html_bs4.find_all('a')])
        
            list_meta = []
            for post in post_entries:
                    list_meta.append(parse_list_meta(post))
            
            return list_meta            
                
        clean_html = BeautifulSoup(content, 'lxml')
        news_body =  clean_html.find('div',class_ = 'column--primary')

        list_meta = parse_link(news_body)
        latest_post_date = parse_latest_date(news_body)
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(None)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(list_meta)
        return item

    def parse_bbc_content(self, content):

        def parse_time(clean_html):         
            s = clean_html.find(class_ = 'mini-info-list__item').text   
            try:
                latest_post_date = datetime.datetime.strptime(s, '%Y年 %m月 %d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
            except:
                t = {"天前":" days ago", "小時前":" hours ago", "分鐘前":" minutes ago", "秒前":" seconds ago"}
                for key in t:
                    if key in s:
                        s = s.replace(key, t[key])
                parsed_s = [s.split()[:2]]
                time_dict = dict((fmt,float(amount)) for amount,fmt in parsed_s)
                dt = datetime.timedelta(**time_dict)
                latest_post_date = (datetime.datetime.now() - dt).strftime("%Y-%m-%dT%H:%M:%S+0800")
            return latest_post_date
        
        def parse_title(clean_html):
            return clean_html.find('h1', class_ = 'story-body__h1').text
        
        def parse_author(clean_html):
            return 'BBC'
        
        def parse_content(clean_html):
            for script in clean_html.find_all('span', src=False):
                script.decompose()
            content = ' '.join([c.text for c in clean_html.find(class_ = 'story-body__inner').find_all(['p', 'h2'])])
            return content
    
        def parse_metadata(clean_html):
            try: 
                category = clean_html.find(property = 'og:site_name').attrs['content'] + '-' + \
                            clean_html.find(property = 'article:section').attrs['content'] 
            except AttributeError:
                category = ''

            try: 
                keywords = clean_html.find(class_ = 'tags-list').text.split()
            except AttributeError:
                keywords = []
            
            return {'category': category,
                    'tag': keywords}
          
        # detail
        url = content['url']        
        clean_html = BeautifulSoup(content['detail_html'], 'lxml')   
    
        date = parse_time(clean_html)
        title = parse_title(clean_html)
        author = parse_author(clean_html)
        content = parse_content(clean_html)
        metadata = parse_metadata(clean_html)
        comment = []
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
