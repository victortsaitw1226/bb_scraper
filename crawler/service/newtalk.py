#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from protocols.mng2crw_ntf_news_get_index import MNG2CRW_NTF_NEWS_GET_INDEX
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.mng2crw_ntf_news_get_post import MNG2CRW_NTF_NEWS_GET_POST
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
import traceback
import requests
import sys


class NEWTALK:

    def __init__(self, server):
        self.session = requests.session()
        self.server = server

    def goto(self, url):
        raw  = self.session.get(url, verify=False)
        raw.encoding = raw.apparent_encoding
        return raw.text 

    def on_MNG2CRW_NTF_NEWTALK_GET_INDEX(self, packet):
        from_notify = MNG2CRW_NTF_NEWS_GET_INDEX()
        from_notify.parse(packet)
        url = from_notify.get_url()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        try:
            html = self.goto(url)
        except:
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_INDEX()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_MNG2CRW_NTF_NEWTALK_GET_POST(self, packet):
        
        from_notify = MNG2CRW_NTF_NEWS_GET_POST()
        from_notify.parse(packet)
        url = from_notify.get_url()

        try:
            # detail_html = NEWTALK.browser.goto(url)
            
            # fb_like_count_element = NEWTALK.browser.find_element_by_xpath('//*[@id="contentBottom"]/div[2]/div[2]/div[1]/span/iframe')
            # fb_like_count_src = NEWTALK.browser.get_attribute( fb_like_count_element, 'src')
            
            # fb_comment_element = NEWTALK.browser.find_element_by_xpath('//*[@id="fb_message"]/div[2]/span/iframe')
            # fb_comment_src = NEWTALK.browser.get_attribute( fb_comment_element, 'src')

            # fb_like_count_html = NEWTALK.browser.goto(fb_like_count_src)
            
            # NEWTALK.browser.get(fb_comment_src)
            # NEWTALK.browser.fb_scrollDown_click_button()
            # fb_comment_html = NEWTALK.browser.html()

            # html = {'html': detail_html, 'fb_like_count':fb_like_count_html,
            #      'comment_html':fb_comment_html, 'url': url}
            html = {'html': self.goto(url), 'url': url}

        except:
            traceback.print_exc(file=sys.stdout)
            self.server.log.warn('fail:{}'.format(url))
            return
            #traceback.print_exc(file=sys.stdout)

        self.server.log.warn('success:{}'.format(url))

        to_notify = CRW2PAR_NTF_NEWS_POST()
        to_notify.set_content(html)
        to_notify.set_media(from_notify.get_media())
        self.server.send(to_notify)
