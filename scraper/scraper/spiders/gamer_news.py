# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import json
import re

class GamerNewsSpider(RedisSpider):
#class GamerNewsSpider(scrapy.Spider):
    name = "gamer"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "gamer",
            "name": "gamer",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "https://gnn.gamer.com.tw/",
            "scrapy_key": "gamer:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)

        for url in links:
            yield scrapy.Request(url['url'],
                    meta = meta,
                    callback=self.parse_article)

    def parse_links(self,soup):
        links = []
        post_list = soup.find_all('div','GN-lbox2B')
        for i in range(len(post_list)):
            link = {'url': 'https:' + post_list[i].find('a')['href'], 'metadata': {}}
            links.append(link)
        return links
    
    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')

        # check if the article is GNNnews or not
        if soup.find('div',{'id':'BH-menu-path'}) == None:
            return

        # main content
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'gamer'
        item['proto'] = 'GAMER_NEWS_PARSE_ITEM'
        #yield item
        
        # crawl comments
        comment_url = response.url.replace('detail.php','ajax/gnn_list_comment.php')
        yield scrapy.Request(comment_url,
                dont_filter=True,
                meta={
                    'url':response.url,
                    'title':self.parse_title(soup),
                    'post': item
                },
                callback=self.parse_comment)
        
    def parse_comment(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        commt_blocks = soup.find_all('div','GN-lbox6A')
        for cb in commt_blocks:
            item = NewsItem()
            reply_time = datetime.strptime(cb.find_all('span')[1].text ,'%m-%d %H:%M:%S')
            reply_time = reply_time.replace(year=datetime.today().year)
            item['url'] = meta['url']
            item['metadata'] = {'category':'GNN新聞'}
            item['article_title'] = meta['title'] 
            item['author'] = cb.find('p').find('a').text
            item['author_url'] = ['https:' + cb.find('p').find('a')['href']]
            item['date'] = reply_time.strftime('%Y-%m-%dT%H:%M:%S+0800')
            item['content'] = cb.find('span','comment-text').text
            item['content_type'] = 1
            item['media'] = 'gamer'
            item['proto'] = 'GAMER_NEWS_PARSE_ITEM'
            item['comment'] = []
            yield item

        post = meta['post']
        post['metadata']['reply_count'] = len(commt_blocks)
        yield post

    def parse_datetime(self,soup):
        time_stamp = soup.find('span',re.compile(r'GN-lbox3C')).text
        time_stamp = time_stamp.split('）')[-1].strip()
        time_stamp = ' '.join(time_stamp.split(' ')[:2])
        dat = datetime.strptime(time_stamp , '%Y-%m-%d %H:%M:%S')
        return dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
    def parse_metadata(self,soup):
        metadata = {'tag':'','category':'GNN新聞'}
        keyword_section = soup.find('ul','platform-tag')
        keyword = [a.text for a in keyword_section.find_all('li')]
        if len(keyword):
            metadata['tag'] = keyword
        return metadata
            
    def parse_author(self,soup): 
        author = soup.find('span',re.compile(r'GN-lbox3C')).text
        author = author.replace('（','')
        author = author.replace('）','')
        if '記者' in author:
            author = author.split(' ')[2]
        elif '報導' in author:
            author = author.split(' ')[0]
        else:
            author = ''
        return author

    def parse_title(self,soup):
        return soup.find('h1').text
    
    def parse_content(self,soup):
        article = soup.find('div', class_='GN-lbox3B')
        for s in article.find_all('script',{'type':'text/template'}):
            s.clear()
        article.find('ul','platform-tag').clear()
        content = ' '.join(article.text.split())
        return content
    
