#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_YOUTUBE_COMMENTS(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_YOUTUBE_COMMENTS, self).__init__('CRW2PAR_NTF_YOUTUBE_COMMENTS')
        self.update({
          'url': '',
          'channel': '',
          'video_url': '',
          'content': ''
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_channel(self, channel):
        self.set('channel', channel)

    def get_channel(self):
        return self.get('channel')

    def set_video_url(self, video_url):
        self.set('video_url', video_url)

    def get_video_url(self):
        return self.get('video_url')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
