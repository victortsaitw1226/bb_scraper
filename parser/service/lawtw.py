#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class LAWTW:
    def __init__(self, server):
        print('init lawtw.py')
        self.server = server

    def on_CRW2PAR_NTF_LAWTW_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_lawtw_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_LAWTW_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_lawtw_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)


    def parse_lawtw_list(self, content):

        def parse_next_page(soup):
            domain_prefix = 'http://www.lawtw.com/'
            try:
                postfix = soup.find(value='下一頁')['onclick'].split('\'')[1]
            except:
                return None
            return domain_prefix + postfix

        def parse_links_dates(soup):
            times = []
            links = []

            domain_prefix = 'http://www.lawtw.com/'
            for ele in soup.find_all('a', class_='HeaderBord'):
                date = ele.parent.parent.find_all(
                    'span', class_='clsReportDate')[-1]
                date = ''.join(date.text.split())
                date = datetime.strptime(date, '%Y-%m-%d%H:%M:%S')
                date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
                times.append(date)

                links.append({
                    'url': {'link': domain_prefix + ele['href'],
                            'post_date': date,
                    },
                    'metadata': {
                        'datetime': date,
                    }
                })
            try:
                _time = max(times)
            except:
                _time = None
            return {'time': _time, 'links': links, }

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_lawtw_content(self, content):

        def parse_author(soup):
            author = soup.find('div', style='float:left')
            author = author.text.replace('文 / ', '').replace('教授', '')
            return author

        def parse_title(soup):
            return soup.h1.text

        def parse_content(soup):
            return ''.join(soup.find('span', style='TEXT-INDENT: 0px; LINE-HEIGHT: 26px; word-break: break-all;').text.split())

        def parse_metadata(soup):
            metadata = {
                'category': '',
            }
            cat = soup.find('div', id='article_main_content').find(
                'table').find_all('a')[-1].text
            metadata['category'] = cat
            return metadata

        soup = BeautifulSoup(content['html'], 'html.parser')

        # metadata = parse_metadata(soup)
        # metadata['author'] = parse_author(soup)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url']['link'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(content['url']['post_date'])
        item.set_content(parse_content(soup))
        item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        return item
