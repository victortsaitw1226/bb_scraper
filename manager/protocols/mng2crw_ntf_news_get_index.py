#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_NEWS_GET_INDEX(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_NEWS_GET_INDEX, self).__init__('MNG2CRW_NTF_NEWS_GET_INDEX')
        self.update({
          'url': '',
          'media': '',
          'name': '',
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_name(self, name):
        self.set('name', name)

    def get_name(self):
        return self.get('name')

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_media(self, media):
        self.set('media', media)

    def get_media(self):
        return self.get('media')
