#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from datetime import datetime, timedelta


class GAMER_HALA:
    def __init__(self, server):
        print('init gamer_hala.py')
        self.server = server

    def on_CRW2PAR_NTF_GAMER_HALA_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_gamer_hala_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_GAMER_HALA_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_gamer_hala_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)

    def parse_gamer_hala_list(self, content):

        def parse_links(soup):
            post_link_sec = soup.find_all('tr','b-list__row b-list-item b-imglist-item')
            post_links = [{'url': link_prefix + x.find('td','b-list__main').find('a')['href'], 'metadata': {}} for x in post_link_sec]
            return post_links
        
        def parse_next_page(soup):
            next_page_sec = soup.find('div','b-pager pager').find('a','next')
            if 'href' in str(next_page_sec):
                next_page = 'https://forum.gamer.com.tw/B.php' + next_page_sec['href']
            else:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            post_link_sec = soup.find_all('tr','b-list__row b-list-item b-imglist-item')
            post_date_list = []
            for pls in post_link_sec:
                post_time = pls.find('td','b-list__time').find('a').text
                if '今日' in post_time:
                    post_time = datetime.strptime(post_time,'今日 %H:%M')
                    post_time = post_time.replace(year=datetime.today().year, month=datetime.today().month, day=datetime.today().day)
                    post_date_list.append(post_time)
                elif '昨日' in post_time:
                    post_time = datetime.strptime(post_time,'昨日 %H:%M')
                    post_time = post_time.replace(year=datetime.today().year, month=datetime.today().month, day=datetime.today().day)
                    post_time = post_time - timedelta(days=1)
                    post_date_list.append(post_time)
                else:
                    post_time = datetime.strptime(post_time,'%m/%d %H:%M')
                    post_time = post_time.replace(year=datetime.today().year)
                    post_date_list.append(post_time)
            latest_post_date = max(post_date_list).strftime('%Y-%m-%dT%H:%M:%S+0800')   
            return latest_post_date
        
        link_prefix = 'https://forum.gamer.com.tw/'
        soup = BeautifulSoup(content, 'html.parser')
        
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_latest_post_date(soup))
        item.set_urls(parse_links(soup))

        return item

    def parse_gamer_hala_content(self, content):
        
        def parse_datetime(soup):
            time_stamp = soup.find('div','c-post__header__info').find('a','edittime tippy-post-info')['data-mtime']
            dat = datetime.strptime(time_stamp , '%Y-%m-%d %H:%M:%S')
            return dat.strftime('%Y-%m-%dT%H:%M:%S+0800')
            
        def parse_metadata(soup):
            agree_count = soup.find('div','postcount').find('span','postgp').find('span').text
            disagree_count = soup.find('div','postcount').find('span','postbp').find('span').text
            return {'agree_count':agree_count, 'disagree_count':disagree_count}
        
        def parse_author(soup): 
            author = soup.find('div','c-post__header__author').find('a','userid').text
            return author
        
        def parse_author_url(soup): 
            author_url = 'https:' + soup.find('div','c-post__header__author').find('a','userid')['href']
            return author_url

        def parse_title(soup):
            return soup.find('meta',{'property':'og:title'})['content']
        
        def parse_content(soup):
            content = soup.find('div','c-article__content')
    #        content = ' '.join( c.text for c in content.find_all('div'))
    #        content = ' '.join(content.split())
            content = ' '.join(content.text.split())
            return content
        
        def parse_comment(soup, more_comments_json):
            
            def parse_more_comment(mcj):
                whole_more_comment = []
                for i in range(len(mcj)-1):
                    one_more_comment_data = {
                        'author':mcj[str(i)]['userid'],
                        'author_url':'',
                        'comment_id':'',
                        'content':mcj[str(i)]['comment'],
                        'datetime': datetime.strptime(mcj[str(i)]['wtime'],'%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%S+0800'),
                        'metadata': {'agree_count':mcj[str(i)]['gp'], 'disagree_count':mcj[str(i)]['bp']},
                        }
                    whole_more_comment.append(one_more_comment_data)
                return whole_more_comment
            
            comment_sec = soup.find_all('div',re.compile('c-section__main c-post'))
            whole_comment = []
            for i in range(len(comment_sec)):
                cs = comment_sec[i]
                mcj = more_comments_json[i]
                one_comment_data = {
                        'author':parse_author(cs),
                        'author_url':parse_author_url(cs),
                        'comment_id':'',
                        'content':parse_content(cs),
                        'datetime':parse_datetime(cs),
                        'source':'gamer',
                        'metadata': parse_metadata(cs),
                        'comments':parse_more_comment(mcj)
                        }
                whole_comment.append(one_comment_data)
            if len(whole_comment)!=0:
                whole_comment.pop(0)    
            return whole_comment
        
        soup = BeautifulSoup(content['html'], 'html.parser')
        more_comments_json = content['more_comment']
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_author_url(parse_author_url(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comment(soup, more_comments_json))
        item.set_metadata(parse_metadata(soup))

        return item
