#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta


class COOL3C:
    def __init__(self, server):
        print('init cool3c.py')
        self.server = server

    def on_CRW2PAR_NTF_COOL3C_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_cool3c_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_COOL3C_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_cool3c_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_cool3c_list(self, content):

        def parse_next_page(soup):
            return soup.find('li', class_='page-item next').a['href']

        def parse_links_dates(soup):
            times = []
            links = []

            for elem in soup.find_all('div', class_='created'):
                # parse post time
                now = datetime.now()
                offset = ''.join(elem.text.split())
                if '就在剛剛' in offset:
                    times.append(now)
                elif '秒前' in offset:
                    times.append(
                        now - timedelta(seconds=int(offset.split('秒前')[0])))
                elif '分鐘前' in offset:
                    times.append(
                        now - timedelta(minutes=int(offset.split('分鐘前')[0])))
                elif '個小時前' in offset:
                    times.append(
                        now - timedelta(hours=int(offset.split('個小時前')[0])))
                elif '天前' in offset:
                    times.append(
                        now - timedelta(days=int(offset.split('天前')[0])))
                elif '個月前' in offset:
                    times.append(
                        now - timedelta(months=int(offset.split('個月前')[0])))
                elif '年前' in offset:
                    times.append(
                        now - timedelta(years=int(offset.split('年前')[0])))
                else:
                    continue
                # parse link
                links.append({
                    'url': elem.a['href'],
                    'metadata': {}
                })
            return {'time': max(
                times).strftime('%Y-%m-%dT%H:%M:%S+0800'), 'links': links, }

        soup = BeautifulSoup(content, 'html.parser')

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(parse_next_page(soup))
        item.set_latest_post_date(parse_links_dates(soup)['time'])
        item.set_urls(parse_links_dates(soup)['links'])

        return item

    def parse_cool3c_content(self, content):
        
        def parse_datetime(soup):
            time = soup.find('div', class_='created slacken')
            time = ''.join(time.text.split())
            time = datetime.strptime(time, '%Y.%m.%d%I:%M%p')
            return time.strftime('%Y-%m-%dT%H:%M:%S+0800')

        def parse_author(soup):
            auth = soup.find('div', class_='author').find('a')
            return auth.text

        def parse_author_url(soup):
            return soup.find('div', class_='author').a['href']

        def parse_title(soup):
            return soup.find('h1', class_='col-12').text

        def parse_content(soup):
            try:
                return ''.join(soup.find('div', class_='lucy-content').text.split())
            except:
                return ''.join(soup.find('div', class_='row content').text.split())

        def parse_metadata(soup, fb_like_soup):

            def parse_category(soup):
                cat = soup.find(
                    'li', class_='list-inline-item type-primary term-category')
                return ''.join(cat.text.split())

            def parse_tag(soup):
                tags = []
                for tag in soup.find_all('li', class_='list-inline-item type-default term-tag'):
                    tags.append(''.join(tag.text.split()))
                return tags

            metadata = {
                'category': parse_category(soup),
                'tag': parse_tag(soup),
                'fb_like_count': fb_like_soup.find('span',{'id':'u_0_3'}).text,
                'view_count': soup.find(class_='views').text.replace('\n', '')
            }
            return metadata


        def parse_comments(soup):
            all_comments = soup.find('section', class_='block comment-area')
            all_comments = all_comments.find('ul', recursive=False).find_all(
                'li', recursive=False)
            
            comments = []  # post time, author name, content
            for _element in all_comments:
                element = _element.find(
                    'div', class_='media clearfix', recursive=False)
                now = datetime.now()
                dt = ''.join(element.find('div', class_='created').text.split())
                if '就在剛剛' in dt:
                    dt = now
                elif '秒前' in dt:
                    dt = now - timedelta(seconds=int(dt.split('秒前')[0]))
                elif '分鐘前' in dt:
                    dt = now - timedelta(minutes=int(dt.split('分鐘前')[0]))
                elif '個小時前' in dt:
                    dt = now - timedelta(hours=int(dt.split('個小時前')[0]))
                elif '天前' in dt:
                    dt = now - timedelta(days=int(dt.split('天前')[0]))
                elif '個月前' in dt:
                    dt = now - timedelta(months=int(dt.split('個月前')[0]))
                else:
                    dt = datetime.strptime(dt, '%Y-%m-%d%H:%M')

                comment = {
                    'comment_id': '',
                    'datetime': dt.strftime('%Y-%m-%dT%H:%M:%S+0800'),
                    'author': ''.join(element.find(class_='author').text.split()),
                    'author_url': '',
                    'content': ''.join(element.find(class_='content clearfix').text.split()),
                    'source': 'cool3c',
                    'metadata': {},
                    'comments': []
                    }
                comments.append(comment)

                child_comments = _element.find(
                    'div', class_='media clearfix', recursive=False).div
                child_comments = child_comments.find(
                    'ul', class_='list-unstyled').find_all('li', recursive=False)

                if child_comments:
                    child_comm = []
                    for child in child_comments:
                        now = datetime.now()
                        dt = ''.join(child.find('div', class_='created').text.split())
                        if '就在剛剛' in dt:
                            dt = now
                        elif '秒前' in dt:
                            dt = now - timedelta(seconds=int(dt.split('秒前')[0]))
                        elif '分鐘前' in dt:
                            dt = now - timedelta(minutes=int(dt.split('分鐘前')[0]))
                        elif '個小時前' in dt:
                            dt = now - timedelta(hours=int(dt.split('個小時前')[0]))
                        elif '天前' in dt:
                            dt = now - timedelta(days=int(dt.split('天前')[0]))
                        elif '個月前' in dt:
                            dt = now - timedelta(months=int(dt.split('個月前')[0]))
                        else:
                            dt = datetime.strptime(dt, '%Y-%m-%d%H:%M')

                        _comment = {
                            'comment_id': '',
                            'datetime': dt.strftime('%Y-%m-%dT%H:%M:%S+0800'),
                            'author': ''.join(child.find(
                                class_='author').text.split()),
                            'author_url':'',
                            'content': ''.join(child.find(
                                class_='content clearfix').text.split()),
                            'metadata': {},
                        }
                        child_comm.append(_comment)
                    comment['comments'] = child_comm
                
            return comments

        soup = BeautifulSoup(content['html'], 'html.parser')
        fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'html.parser')
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_author_url(parse_author_url(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment(parse_comments(soup))
        item.set_metadata(parse_metadata(soup, fb_like_count_html))
        return item
