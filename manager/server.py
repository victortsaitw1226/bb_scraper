#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import traceback
import importlib
import time
from nameko.web.handlers import http
from nameko.events import EventDispatcher, event_handler, SINGLETON
from nameko.timer import timer
from datetime import datetime, timedelta
from protocols.protocol import Protocol
from protocols.par2mng_ntf_ptt_post_urls import PAR2MNG_NTF_PTT_POST_URLS
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.par2mng_ntf_dcard_forum_list import PAR2MNG_NTF_DCARD_FORUM_LIST
from protocols.par2mng_ntf_dcard_post_list import PAR2MNG_NTF_DCARD_POST_LIST
from protocols.par2mng_ntf_fb_post_url import PAR2MNG_NTF_FB_POST_URL
from protocols.par2mng_ntf_youtube_video_url import PAR2MNG_NTF_YOUTUBE_VIDEO_URL
from extensions.redis import Redis
from extensions.logger import Logger
from extensions.mongo import Mongo
from nameko.dependency_providers import Config


class Manager:
    name = 'manager'
    dispatch = EventDispatcher()
    config = Config()
    redis = Redis()
    mongo = Mongo()
    log = Logger(
        name=os.environ.get('POD_NAME', 'manager'),
        env=os.environ.get('ENV', '')
    )
    instances = {}

    def __init__(self):
        super(Manager, self).__init__()

    def get_media_instance(self, media):
        if media in Manager.instances:
            instance = Manager.instances[media]
        else:
            my_module = importlib.import_module('service.{}'.format(media))
            MyClass = getattr(my_module, media.upper())
            instance = MyClass(self)
            Manager.instances[media] = instance
        return instance

    def send(self, protocol):
        proto = protocol.get_proto_name()
        self.dispatch(proto, str(protocol))

    '''
    NEWS
    '''
    @event_handler(
        'parser', PAR2MNG_NTF_NEWS_POST_URLS.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_NEWS_POST_URLS(self, packet):
        request = PAR2MNG_NTF_NEWS_POST_URLS()
        request.parse(packet)
        try:
            site = request.get_media()
            instance = self.get_media_instance(site)
            protocol_name = ('_').join(
                ['on_PAR2MNG_NTF', site.upper(), 'POST_URLS'])
            getattr(instance, protocol_name)(packet)
        except Exception as e:
            self.log.exception(e)

    '''
    Youtube
    '''
    @event_handler(
        'parser', PAR2MNG_NTF_YOUTUBE_VIDEO_URL.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_YOUTUBE_VIDEO_URL(self, packet):
        try:
            instance = self.get_media_instance('youtube')
            getattr(instance, 'on_PAR2MNG_NTF_YOUTUBE_VIDEO_URL')(packet)
        except Exception as e:
            self.log.exception(e)

    '''
    PTT
    '''
    @event_handler(
        'parser', PAR2MNG_NTF_PTT_POST_URLS.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_PTT_POST_URLS(self, packet):
        try:
            instance = self.get_media_instance('ptt')
            getattr(instance, 'on_PAR2MNG_NTF_PTT_POST_URLS')(packet)
        except Exception as e:
            self.log.exception(e)


    '''
    DCard
    '''
    @event_handler(
        'parser', PAR2MNG_NTF_DCARD_FORUM_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_DCARD_FORUM_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_PAR2MNG_NTF_DCARD_FORUM_LIST')(packet)
        except Exception as e:
            self.log.exception(e)

    @event_handler(
        'parser', PAR2MNG_NTF_DCARD_POST_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_DCARD_POST_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_PAR2MNG_NTF_DCARD_POST_LIST')(packet)
        except Exception as e:
            self.log.exception(e)

    '''
    Facebook
    '''
    @event_handler(
        'parser', PAR2MNG_NTF_FB_POST_URL.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_PAR2MNG_NTF_FB_POST_URL(self, packet):
        try:
            instance = self.get_media_instance('facebook')
            getattr(instance, 'on_PAR2MNG_NTF_FB_POST_URL')(packet)
        except Exception as e:
            self.log.exception(e)

    # =======================================================
    # ====================== routine ========================
    # =======================================================

    '''
    update IP List
    '''
    @timer(1)
    def update_proxy_routine(self):
        environment = os.environ.get('ENV', 'debug')
        print(environment)
        if environment == 'debug':
            return

        proxy_lock = self.redis.lock('proxy_lock', 5)
        if not proxy_lock:
            return
        try:
            proxies = self.redis.generate_proxies()
        except Exception as e:
            traceback.print_exc(file=sys.stdout)

    @timer(1)
    def remove_illegal_proxy(self):
        environment = os.environ.get('ENV', 'debug')
        print(environment)
        if environment == 'debug':
            return

        lock = self.redis.lock('proxy_remove_lock', 10)
        if not lock:
            return
        try:
            self.redis.remove_illegal_proxy()
        except Exception as e:
            traceback.print_exc(file=sys.stdout)
        finally:
            self.redis.unlock('proxy_remove_lock', lock)

    def check_proxy(self):
        environment = os.environ.get('ENV', 'debug')
        if environment == 'debug':
            return True

        proxy = self.redis.random_proxy()
        if proxy is None:
            raise Exception('Proxies is empty')

    '''
    Service Routine
    '''
    @timer(10)
    def service_routine(self):

        self.check_proxy()

        now = datetime.now()
        instances = {}
        docs = self.mongo.find('history_v2', 'urls', {'enabled': True})
        for doc in docs:
            # Command crawler by scrapy
            scrapy_key = doc.get('scrapy_key')
            if scrapy_key:
                print('doc:',doc)
                interval = doc.get('interval', 0)
                priority = doc.get('priority', 0)
                routine_lock = self.redis.lock(
                    'routine_{}_lock'.format(str(doc['_id'])), interval)
                print(routine_lock)
                if not routine_lock:
                    continue
                doc['_id'] = str(doc['_id'])
                res = self.redis.put_to_priority_queue(scrapy_key, priority, doc)
                print(res)
                continue

            schedulers = doc.get('schedule', [])
            updated = doc.get('updated', '')
            # Set the default value
            interval = -1
            days_limit = -1
            _id = doc['_id']

            media = doc.get('media')
            name = doc.get('name')

            # Find the match interval and days_limit
            for scheduler in schedulers:
                start = scheduler['start']
                end = scheduler['end']
                start = now.replace(
                    hour=int(start.split(':')[0]),
                    minute=int(start.split(':')[1]),
                    second=int(start.split(':')[2])
                )
                end = now.replace(
                    hour=int(end.split(':')[0]),
                    minute=int(end.split(':')[1]),
                    second=int(end.split(':')[2])
                )
                if start < now and now < end:
                    interval = scheduler['interval']
                    days_limit = scheduler['days_limit']
                    break

            #Check the lock
            routine_lock = self.redis.lock('routine_{}_lock'.format(str(_id)), interval)
            if not routine_lock:
                continue

            doc['days_limit'] = days_limit
            try:
                instance = self.get_media_instance(media)
                instance.send_index(**doc)
            except:
                traceback.print_exc(file=sys.stdout)
                pass
