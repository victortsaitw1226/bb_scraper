#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import traceback
import sys
import os
import importlib
from nameko.web.handlers import http
from nameko.events import EventDispatcher, event_handler, SINGLETON
from nameko.timer import timer
from protocols.protocol import Protocol
from protocols.crw2par_ntf_ptt_post import CRW2PAR_NTF_PTT_POST
from protocols.crw2par_ntf_ptt_index import CRW2PAR_NTF_PTT_INDEX
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.crw2par_ntf_fb_post_info import CRW2PAR_NTF_FB_POST_INFO
from protocols.crw2par_ntf_fb_post_content import CRW2PAR_NTF_FB_POST_CONTENT
from protocols.crw2par_ntf_fb_post_urls import CRW2PAR_NTF_FB_POST_URLS
from protocols.crw2par_ntf_youtube_video_list import CRW2PAR_NTF_YOUTUBE_VIDEO_LIST
from protocols.crw2par_ntf_youtube_comments import CRW2PAR_NTF_YOUTUBE_COMMENTS
from protocols.crw2par_ntf_youtube_info import CRW2PAR_NTF_YOUTUBE_INFO
from protocols.crw2par_ntf_dcard_forum_list import CRW2PAR_NTF_DCARD_FORUM_LIST
from protocols.crw2par_ntf_dcard_post_list import CRW2PAR_NTF_DCARD_POST_LIST
from protocols.crw2par_ntf_dcard_post import CRW2PAR_NTF_DCARD_POST
from extensions.logger import Logger
from extensions.mongo import Mongo
from nameko.dependency_providers import Config
#from nameko_mongodb.database import MongoDatabase


class Parser:
    name = 'parser'
    dispatch = EventDispatcher()
    config = Config()
    #mongo = MongoDatabase()
    mongo = Mongo()
    log = Logger(
        name=os.environ.get('POD_NAME', 'parser'),
        env=os.environ.get('ENV', 'debug')
    )
    instances = {}

    def __init__(self):
        super(Parser, self).__init__()
        
    def get_media_instance(self, media):
        if media in Parser.instances:
            instance = Parser.instances[media]
        else:
            my_module = importlib.import_module('service.{}'.format(media))
            MyClass = getattr(my_module, media.upper())
            instance = MyClass(self)
            Parser.instances[media] = instance
        return instance

    def send(self, protocol):
        proto = protocol.get_proto_name()
        self.dispatch(proto, str(protocol))

    '''
    NEWS
    '''
    @event_handler(
        'crawler', CRW2PAR_NTF_NEWS_INDEX.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_NEWS_INDEX(self, packet):
        request = CRW2PAR_NTF_NEWS_INDEX()
        request.parse(packet)
        try:
            site = request.get_media()
            instance = self.get_media_instance(site)
            protocol_name = ('_').join(
                ['on_CRW2PAR_NTF', site.upper(), 'INDEX'])
            getattr(instance, protocol_name)(packet)
        except Exception as e:
            self.log.error("media: {}, proto: {}, error: {}".format(
                request.get_media(), request.get_proto_name(), traceback.format_exc()))

    @event_handler(
        'crawler', CRW2PAR_NTF_NEWS_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_NEWS_POST(self, packet):
        request = CRW2PAR_NTF_NEWS_POST()
        request.parse(packet)
        try:
            site = request.get_media()
            instance = self.get_media_instance(site)
            protocol_name = ('_').join(
                ['on_CRW2PAR_NTF', site.upper(), 'POST'])
            getattr(instance, protocol_name)(packet)
        except Exception as e:
            self.log.error("media: {}, server:{}, error: {}, packet: {}".format(
                request.get_media(), 'parser', traceback.format_exc(), packet))
            # self.log.error("media: {}, proto: {}, error: {}".format(
            #     request.get_media(), request.get_proto_name(), traceback.format_exc()))

    '''
    Youtube
    '''
    @event_handler(
        'crawler', CRW2PAR_NTF_YOUTUBE_VIDEO_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_YOUTUBE_VIDEO_LIST(self, packet):
        request = CRW2PAR_NTF_YOUTUBE_VIDEO_LIST()
        request.parse(packet)
        try:
            instance = self.get_media_instance('youtube')
            getattr(instance, 'on_CRW2PAR_NTF_YOUTUBE_VIDEO_LIST')(packet)
        except Exception as e:
            self.log.error("media: {}, server:{}, error:{}, packet:{}".format( 'Youtube', 'parser', str(e), packet))

    @event_handler(
        'crawler', CRW2PAR_NTF_YOUTUBE_COMMENTS.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_YOUTUBE_COMMENTS(self, packet):
        request = CRW2PAR_NTF_YOUTUBE_COMMENTS()
        request.parse(packet)
        try:
            instance = self.get_media_instance('youtube')
            getattr(instance, 'on_CRW2PAR_NTF_YOUTUBE_COMMENTS')(packet)
        except Exception as e:
            self.log.error("media: {}, server:{}, error:{}, packet:{}".format('Youtube', 'parser', str(e), packet))
            with open('youtube-error.html', 'w') as f:
                f.write(request.get_content())
    '''
    Facebook
    '''
    @event_handler(
        'crawler', CRW2PAR_NTF_FB_POST_URLS.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_FB_POST_URLS(self, packet):
        try:
            instance = self.get_media_instance('facebook')
            getattr(instance, 'on_CRW2PAR_NTF_FB_POST_URLS')(packet)
        except Exception as e:
            self.log.error("media: {}, server:{}, error:{}, packet:{}".format('Facebook', 'parser', str(e), packet))

    @event_handler(
        'crawler', CRW2PAR_NTF_FB_POST_CONTENT.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_FB_POST_CONTENT(self, packet):
        try:
            instance = self.get_media_instance('facebook')
            getattr(instance, 'on_CRW2PAR_NTF_FB_POST_CONTENT')(packet)
        except Exception as e:
            self.log.error("media: {}, server:{}, error:{}, packet:{}".format('Facebook', 'parser',str(e), packet))

    '''
    PTT
    '''
    @event_handler(
        'crawler', CRW2PAR_NTF_PTT_INDEX.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_PTT_INDEX(self, packet):
        try:
            instance = self.get_media_instance('ptt')
            getattr(instance, 'on_CRW2PAR_NTF_PTT_INDEX')(packet)
        except Exception as e:
            self.log.error("media:{}, server:{}, error:{}, packet:{}".format('PTT', 'parser', str(e), packet))

    @event_handler(
        'crawler', CRW2PAR_NTF_PTT_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_PTT_POST(self, packet):
        try:
            instance = self.get_media_instance('ptt')
            getattr(instance, 'on_CRW2PAR_NTF_PTT_POST')(packet)
        except Exception as e:
            self.log.error("media:{}, server:{}, error:{}, packet:{}".format('PTT', 'parser', str(e), packet))


    '''
    DCard
    '''
    @event_handler(
        'crawler', CRW2PAR_NTF_DCARD_FORUM_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_DCARD_FORUM_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_CRW2PAR_NTF_DCARD_FORUM_LIST')(packet)
        except Exception as e:
            self.log.error("media:{}, server:{}, error:{}, packet:{}".format('DCard', 'parser', str(e), packet))

    @event_handler(
        'crawler', CRW2PAR_NTF_DCARD_POST_LIST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_DCARD_POST_LIST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_CRW2PAR_NTF_DCARD_POST_LIST')(packet)
        except Exception as e:
            self.log.error("media:{}, server:{}, error:{}, packet:{}".format('DCard', 'parser', str(e), packet))

    @event_handler(
        'crawler', CRW2PAR_NTF_DCARD_POST.__name__,
        handler_type=SINGLETON, reliable_delivery=True
    )
    def on_CRW2PAR_NTF_DCARD_POST(self, packet):
        try:
            instance = self.get_media_instance('dcard')
            getattr(instance, 'on_CRW2PAR_NTF_DCARD_POST')(packet)
        except Exception as e:
            self.log.error("media:{}, server:{}, error:{}, packet:{}".format('DCard', 'parser', str(e), packet))
