#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
import numpy as np
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
from itertools import chain

class APPLEDAILY:
    def __init__(self, server):
        print('init appledaily.py')
        self.server = server

    def on_CRW2PAR_NTF_APPLEDAILY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_appledaily_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_APPLEDAILY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        # try:
        #     item = self.parse_appledaily_content(content)
        #     # self.server.log.debug(str(item))
        # except Exception as e:
        #     self.server.log.exception(e)
        #     print(e)
        #     # self.server.log.error(str(from_notify))
        item = self.parse_appledaily_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_appledaily_list(self, content):

        soup = BeautifulSoup(content,"html.parser")
    
        if 'alert' not in soup.text:
            url = soup.find("meta",  property="og:url")['content']
            next_page = url[:-1] + str(int(url[-1])+1)
            
            
            time = [s.findAll("time") for s in soup.findAll("ul", {"class": "rtddd slvl"})]
            day = [d.text for d in soup.findAll("h1", {"class": "dddd"})]
            
            for i in range(len(day)):
                time[i] = [datetime.strptime(day[i] + ' ' + t.text, '%Y / %m / %d %H:%M') for t in time[i]]
            latest_post_date = max(list(chain(*time))).strftime('%Y-%m-%dT%H:%M:%S+0800')
            
            links = list(chain(*[s.findAll("a") for s in soup.findAll("ul", {"class": "rtddd slvl"})]))
            links = [{'url':l['href'],'metadat':[]} for l in links if 'micromovie' not in l('href')]
            
        else:
            next_page = None
            latest_post_date = None
            links = None

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_appledaily_content(self, content):
        
        def parse_charged_detail(detail_html):
            
            def parse_content(soup):
                content = soup.find('div','ndArticle_contentBox').find('div','ndArticle_margin').find('p').text
                content = '\n'.join(content.split())
                return content
            
            def parse_author(content):
                content = content.replace('(','（')
                content = content.replace(')','）')
                
                end_indx = []
                for m in re.finditer('報導）', content):
                    end_indx.append(m.start())            
                
                start_indx = []
                for m in re.finditer('（', content):
                    start_indx.append(m.end())
                
                if len(end_indx)!=1 or len(start_indx)==0:
                    author = ''
                else:
                    find_close = end_indx[0] - np.array(start_indx)
                    start_indx = start_indx[ np.where( find_close == min(find_close[find_close>0]) )[0][0] ]
                    author = re.split('／',content[start_indx:end_indx[0]])[0]
                return author
            
            def parse_date(soup):
                date = soup.find('div','ndArticle_creat').text
                date = re.split('：',date)[1]  
                date = datetime.strptime(date , '%Y/%m/%d %H:%M')
                date = date.strftime('%Y-%m-%dT%H:%M:%S+0800')
                return date
            
            def parse_keywords(soup):
                try:
                    keywords = soup.find('div','ndgKeyword').find_all('h3')
                    keywords = [x.text for x in keywords]
                except:
                    keywords = ''
                return keywords
            
            title = detail_html.find('article','ndArticle_leftColumn').find('h1').text
            title = ' '.join(title.split())
            date = parse_date(detail_html)    
            content = parse_content(detail_html)  
            author = parse_author(content)    
            keywords = parse_keywords(detail_html)
            category = detail_html.find('div','ndgNav_floatList twlist').find('a','current').text
            
            data = {'datetime':date, 'author':author, 'title':title, 'content':content, 'comments':[],
                    'metadata':{'keywords': keywords,'category':category} }
            
            return data


        def parse_free_detail(detail_html):
    
            def parse_content(soup):
                raw_json = soup.find_all('script',{'type':'application/javascript'})
                raw_json = [ r for r in raw_json if 'raw_html' in str(r)]
                raw_json = str(raw_json[0])
                
                content = re.search(r'content":"(.*?)"}', raw_json).group(1)  
                content = BeautifulSoup(content, 'html.parser')
                content = ''.join(content.text.split())
                
                return content
            
            def parse_author(content):
                content = content.replace('(','（')
                content = content.replace(')','）')
                
                end_indx = []
                for m in re.finditer('報導）', content):
                    end_indx.append(m.start())            
                
                start_indx = []
                for m in re.finditer('（', content):
                    start_indx.append(m.end())
                
                if len(end_indx)!=1 or len(start_indx)==0:
                    author = ''
                else:
                    find_close = end_indx[0] - np.array(start_indx)
                    start_indx = start_indx[ np.where( find_close == min(find_close[find_close>0]) )[0][0] ]
                    author = re.split('／',content[start_indx:end_indx[0]])[0]
                return author
            
            def parse_date(soup):
                raw_json = soup.find_all('script',{'type':'application/javascript'})
                raw_json = [ r for r in raw_json if 'raw_html' in str(r)]
                raw_json = str(raw_json[0])
                date = re.search(r'created_date":"(.*?)",', raw_json).group(1)  
                date = date.split('.')[0]+'+0800'
                return date
                
            title = ' '.join(detail_html.find('meta',{'property':'twitter:title'})['content'].split())
            keywords = detail_html.find('meta',{'name':'keywords'})['content'].split(',')
            # category = detail_html.find('div','section-name-container section-name-container-underscore ').find('a').text
            category = detail_html.find('div', {'class': re.compile(r'section-name-container section-name-container-underscore')}).find('a').text
            date = parse_date(detail_html)    
            content = parse_content(detail_html)  
            author = parse_author(content)
            
            data = {'datetime':date, 'author':author, 'title':title, 'content':content, 'comments':[],
                    'metadata':{'keywords': keywords,'category':category} }
            
            return data

        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')

        charged_bool = '每月只120元* 即可成為「升級壹會員」' in str(detail_html)
        print('charged_bool:',charged_bool)
        if charged_bool==True:
            data = parse_charged_detail(detail_html)
        else:
            data = parse_free_detail(detail_html)
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_title(data['title'])
        item.set_author(data['author'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        item.set_metadata(data['metadata'])
        return item
