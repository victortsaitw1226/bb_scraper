#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class MNG2CRW_NTF_DCARD_GET_POST(Protocol):

    def __init__(self):
        super(MNG2CRW_NTF_DCARD_GET_POST, self).__init__('MNG2CRW_NTF_DCARD_GET_POST')
        self.update({
          'url': '',
          'comment_url': ''
        })

    def set_url(self, url):
        self.set('url', url)

    def get_url(self):
        return self.get('url')

    def set_comment_url(self, url):
        self.set('comment_url', url)

    def get_comment_url(self):
        return self.get('comment_url')
