#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
import calendar



class PLURK:
    def __init__(self, server):
        print('init plurk.py')
        self.server = server

    def on_CRW2PAR_NTF_PLURK_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_plurk_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_PLURK_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_plurk_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_plurk_list(self, content):

        def parse_datetime(t):
            splitStr = t.split()
            abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
            dt = splitStr[3] + '/' + str(abbr_to_num[splitStr[2]]) + '/' + splitStr[1] + ' ' + splitStr[4] 
            dt = datetime.strptime( dt, '%Y/%m/%d %H:%M:%S')   
            dt = dt + timedelta(hours=8)
            return  dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_timeoffset(t): 
            splitStr = t.split()
            abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
            timeOffset = splitStr[3] + '-' + str(abbr_to_num[splitStr[2]]) + '-' + splitStr[1] + 'T' + splitStr[4]
            return timeOffset
        
        data = {
            'next_page': {'user_id': content['user_id'], 'time_Offset' : parse_timeoffset(content['plurk_rawJson'][-1]['posted'])},
            'latest_post_date': parse_datetime(content['plurk_rawJson'][-1]['posted']),
            'links': content['plurk_rawJson']
        }

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(data['next_page'])
        item.set_latest_post_date(data['latest_post_date'])
        item.set_urls(data['links'])

        return item

    def parse_plurk_content(self, content):
        
        def parse_content(cont):
            soup = BeautifulSoup(cont, "html.parser")
            a_clear = soup.find_all('a')
            for a in a_clear:
                a.clear()
            return soup.text
        
        def parse_datetime(t):
            splitStr = t.split()
            abbr_to_num = {name: num for num, name in enumerate(calendar.month_abbr) if num}
            dt = splitStr[3] + '/' + str(abbr_to_num[splitStr[2]]) + '/' + splitStr[1] + ' ' + splitStr[4] 
            dt = datetime.strptime( dt, '%Y/%m/%d %H:%M:%S')   
            dt = dt + timedelta(hours=8)
            return  dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        
        
        def parse_comment(replies):
            replies_list = []
            for r in replies:
                reply = {
                        'comment_id': '',
                        'datetime': parse_datetime(r['posted']),
                        'author': str(r['user_id']),
                        'author_url': '',
                        'content': ' '.join(r['content_raw'].split()),
                        'source': 'plurk',
                        'metadata':{},
                        'comments':[] }
                replies_list.append(reply)
            return replies_list
                
        plrk = content['content']
        replies = content['replies']
        
        
        data = {
                'datetime':parse_datetime(plrk['posted']) ,
                'metadata': {'like_count': plrk['favorite_count'], 'replurkers_count':plrk['replurkers_count']},
                'author': str(plrk['user_id']),
                'title': '',
                'content': parse_content(plrk['content']),
                'comments': parse_comment(replies) }
        
        
        item = NEWS_PARSE_ITEM()
        item.set_url(content['url'])
        item.set_author(data['author'])
        item.set_date(data['datetime'])
        item.set_content(data['content'])
        item.set_comment(data['comments'])
        item.set_metadata(data['metadata'])
        return item
