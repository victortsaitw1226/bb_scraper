#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, timedelta
# from utils.parse_fb_comment import parse_comments

class BUSINESSTODAY:
    def __init__(self, server):
        print('init businesstoday.py')
        self.server = server

    def on_CRW2PAR_NTF_BUSINESSTODAY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_businesstoday_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())   ###
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_BUSINESSTODAY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_businesstoday_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_businesstoday_list(self, content):

        def parse_latest_post_date(soup):
            link_date = [datetime.strptime( dat.text, '%Y-%m-%d') for dat in soup.find_all('p','article__item-date')]
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
        
        def parse_next_page(current_page):
            page = re.search(r'page/(.*)/ajax',current_page).group(1)
            next_page = re.split('page/',current_page)[0] +'page/'+ str(int(page)+1) + '/ajax'
            return next_page
        
        soup = BeautifulSoup(content['list_html'], 'html.parser')

        links = [  {'url': li['href'] ,'metadata':[]}   for li in soup.find_all('a','article__item')]
        latest_post_date = parse_latest_post_date(soup)
        next_page = parse_next_page(content['current_page'])

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_businesstoday_content(self, content):
        
        def parse_datetime(soup):
            post_time = soup.find('p', class_='context__info-item context__info-item--date')
            date = datetime.strptime(post_time.text, '%Y-%m-%d %H:%M')
            return date.strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            return soup.find('p', class_='context__info-item context__info-item--author').text
        
        def parse_title(soup):
            title = soup.find('h1', class_='article__maintitle').text
            title = ' '.join(title.split())
            return title
        
        def parse_content(soup):
            content = []
            segment = soup.find('div', class_='cke_editable font__select-content')
            content = ''.join(x for x in segment.text)
            content = ''.join(content.split())
            return content

        def parse_metadata(soup):

            def parse_tag(soup):
                tags_content = []
                tags = soup.find_all('a', class_='context__tag')
                for tag in tags:
                    tags_content.append(tag.text)
                return tags_content

            # 文章分類、關鍵字
            metadata = {
                'category': soup.find('p', class_='context__info-item context__info-item--type').text,
                'tag': parse_tag(soup),
            }
            return metadata

        soup = BeautifulSoup(content['html'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content['url']
        date = parse_datetime(soup)
        author = parse_author(soup)
        title = parse_title(soup)
        content = parse_content(soup)
        metadata = parse_metadata(soup)
        # comment = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        # item.set_comment(comment)
        item.set_metadata(metadata)
        return item
