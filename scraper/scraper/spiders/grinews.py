# -*- coding: utf-8 -*-
import scrapy
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from scraper.items import NewsItem
from .redis_spiders import RedisSpider

class GrinewsSpider(RedisSpider):
    name = 'grinews'

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "grinews",
            "name": "grinews",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://grinews.com/news/category/newest/",
            "scrapy_key": "grinews:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')

        news = soup.find_all("div","article-img col-md-4")
        news_dates = soup.find_all("div","article-info col-md-8")

        dt_list=[]
        for i, matadata in (zip(news, news_dates)):
            auth=matadata.find("div","author").find("a")["href"]
            for time_ in matadata.find_all("div","date"):
                dt=datetime.strptime(time_.text, ' %Y / %m / %d')
                dt_list.append(dt)

            for url in i.find_all("a"):
                # Go to the article page
                yield scrapy.Request(url['href'],
                    meta=meta,
                    dont_filter=True,
                    callback=self.parse_article)

        latest_datetime = max(dt_list)

        past = datetime.now() - timedelta(seconds=meta['days_limit'])

        if latest_datetime < past:
            return


        current_page = soup.find("span","page-numbers current")
        if current_page is None:
            return

        next_page = current_page.find_next_sibling()
        if next_page is None:
            return


        yield scrapy.Request(next_page['href'],
            meta=meta,
            dont_filter=True,
            callback=self.parse)


    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')

        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = self.parse_author_url(soup)
        item['content'] = self.parse_content(soup)
        #item['comment'] = self.parse_comments(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = response.meta['media']
        item['proto'] =  'SETN_PARSE_ITEM'
        return item
        
    def parse_datetime(self, soup):
        time_ =soup.find("li","date").text
        dt = datetime.strptime(time_, " %Y/%m/%d")
        return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')

    def parse_author_url(self, soup):
        writer = []
        try:
            writer.append(soup.find("li","author").find("a")["href"])
        except:
            pass
        return writer

    def parse_author(self, soup):
        try:
            writer = soup.find("li","author").find("a").text
        except:
            writer = ''
        return writer

    def parse_title(self, soup):
        return soup.find("div","post-title").text.strip().replace(u'\u3000',
            u' ').replace(u'\xa0', u' ')

    def parse_content(self, soup):
        after_soup = soup
        [s.extract() for s in after_soup('strong')]
        content =soup.find("div","post-content").text
        return content.strip().replace(u'\u3000',
            u' ').replace(u'\xa0', u' ')

    def parse_metadata(self, soup):
        keys=[]
        try:
            for tag in soup.find('li',"cat").find_all("a"):
                keys.append(tag.text)
        except:
            pass
        category = soup.find('ul','post-categories').find('li').text
        return {
            'tag': keys,
            'category':category
        }

    def parse_comments(self, soup):
        coments_list = []
        comment_list = soup.find_all("ol","comment-list")
        comment = ""
        for i in comment_list:
            for content in i.find_all("p"):
                comment += content.text
            coments_list.append({
                "comment_id":'',
                 "author": i.find("cite","comment-author").text,
                 "author_url":'',
                 "content": comment,
                 "datetime": i.find("a","comment-date").text,
                 "source": "grinews",
                 "matadata": {},
                 "comments":[],
            })

        return coments_list
