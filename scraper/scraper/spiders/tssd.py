# -*- coding: utf-8 -*-
import scrapy
import traceback, sys
from dateutil.parser import parse as date_parser
from scraper.items import NewsItem
from .redis_spiders import RedisSpider
from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import json
import re

class TssdSpider(RedisSpider):
    name = "tssd"

    def start_requests(self):
        if isinstance(self, RedisSpider):
            return
        requests = [{
            "media": "tssd",
            "name": "tssd",
            "enabled": True,
            "days_limit": 3600 * 24,
            "interval": 3600,
            "url": "http://www.tssdnews.com.tw/?FID=63",
            "scrapy_key": "tssd:start_urls",
            "priority": 1
        }]
        for request in requests:
            yield scrapy.Request(request['url'],
                    meta=request,
                    dont_filter=True,
                    callback=self.parse)
 

    def parse(self, response):
        meta = response.meta
        soup = BeautifulSoup(response.body, 'html.parser')
        links = self.parse_links(soup)
        next_page = self.parse_next_page(soup)
        latest_datetime = self.parse_latest_post_date(links[0]['url'])

        for url in links:
            yield response.follow(url['url'],
                    meta = meta,
                    callback=self.parse_article)

        if next_page is None:
            return

        past = datetime.now() - timedelta(seconds=meta['days_limit'])
        if latest_datetime < past:
            return

        yield response.follow(next_page,
                dont_filter=True,
                meta=meta,
                callback=self.parse)

    def parse_next_page(self,soup):
        if soup.find('a', text='►') is not None:
            next_page = soup.find('a', text='►').get('href')
        else:
            next_page = None
        return next_page

    def parse_links(self,soup):
        links = []
        
        for ad in soup.find_all('dd', class_='only_1280'):
            ad.extract()
        
        for element in soup.find('div', id='story').find_all('dd'):
            link = element.a.get('href')
            links.append({
                'url': link,
                'metadata': {}
            })
        
        return links 

    def parse_latest_post_date(self,first_link):
        html = requests.get('http://www.tssdnews.com.tw'+first_link).text
        soup = BeautifulSoup(html, 'lxml')
        reg_time = re.compile(r'\d{4}/\d{2}/\d{2}', re.VERBOSE)
        post_time = soup.find('div', id='news_author').text
        time_ = reg_time.findall(post_time)
        dt = datetime.strptime(time_[0], '%Y/%m/%d')
        return dt
    

    def parse_article(self, response):
        soup = BeautifulSoup(response.body, 'html.parser')
        item = NewsItem()
        item['url'] = response.url
        item['author'] = self.parse_author(soup)
        item['article_title'] = self.parse_title(soup)
        item['author_url'] = []
        item['content'] = self.parse_content(soup)
        item['comment'] = []
        item['date'] = self.parse_datetime(soup)
        item['metadata'] = self.parse_metadata(soup)
        item['content_type'] = 0
        item['media'] = 'tssd'
        item['proto'] = 'TSSD_PARSE_ITEM'
        return item

    def parse_datetime(self,soup):
        reg_time = re.compile(r'\d{4}/\d{2}/\d{2}', re.VERBOSE)
        post_time = soup.find('div', id='news_author').text
        time_ = reg_time.findall(post_time)
        try:
            dt = datetime.strptime(time_[0], '%Y/%m/%d')
            return dt.strftime('%Y-%m-%dT%H:%M:%S+0800')
        except:
            return None

    def parse_author(self,soup):
        reg_time = re.compile(r'\d{4}/\d{2}/\d{2}', re.VERBOSE)
        author_block = soup.find('div', id='news_author').text
        time_ = reg_time.findall(author_block)[0]
        author = author_block.replace('【','').replace('】','').replace('（','').replace('）','').replace('◎','').replace('記者','').replace(time_,'')
        author = author.split('／')[0]
        author = author.split('/')[0]
        author = ''.join(author.split())
        return author

    def parse_title(self,soup):
        board = soup.find('div', id='news_title')
        return board.get_text(strip=True)

    def parse_content(self,soup):            
        for script in soup.find_all('script'):
            script.extract()
        contents = soup.find('div', class_='idx3').get_text(strip=True)
        return contents

    def parse_metadata(self,soup):
        metadata = {
            'category': ''
        }
        entry_crumb = soup.find('div', id='navbar').find_all('a')
        entry = [x.get_text() for x in entry_crumb]
        entry = list(dict.fromkeys(entry))
        metadata['category'] = entry[-1]
        return metadata
