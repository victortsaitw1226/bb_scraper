#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
import datetime

class KSNEWS:
    def __init__(self, server):
        print('init ksnews.py')
        self.server = server

    def on_CRW2PAR_NTF_KSNEWS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ksnews_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_days_limit(from_notify.get_days_limit())
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_KSNEWS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ksnews_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ksnews_list(self, content):     
        soup = BeautifulSoup(content, 'lxml') 

        next_page, latest_post_date, links = None, None, []

        try:         
            next_page = soup.find('a', {'class':'next'})['href']
        except:
            pass

        latest_post_date = datetime.datetime.strptime(
            soup.find('span', {'class':'date'}).text, '%Y年%m月%d日'
        ).strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        # links   
        p = soup.find_all('div', {'class':'ip_news_list'})
        for i in range(len(p)): 
            link = p[i].find('a')['href']
            links.append({
                            'url': link,
                            'metadata': {}
                            })
    
        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        return item

    def parse_ksnews_content(self, content):
        def parse_datetime(soup):
            return datetime.datetime.strptime(soup.find('span', {'class':'date'}).text, '%Y年%m月%d日').strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        def parse_author(soup):
            try:
                au = soup.find('section').text[0:200].replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')
                ind = au.index('記者')
                print(ind)
                au = au[ind+2:ind+5]
            except:
                au = ''
            return au

        def parse_title(soup):
            return soup.find('h1', {'class':'title_'}).text

        def parse_content(soup):
            return soup.find('section').text.replace('\n', '').replace('\t', '').replace('\r', '').replace('\xa0', '')

        def parse_metadata(soup):        
            return {'category': soup.find('ol', {'class':'bread_crumbs'}).text.split('\n')[3],
                    }
        
        soup = BeautifulSoup(content['html'], 'html.parser')
        # comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        
        url = content.get('url')
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        # item.set_comment(parse_comments(comment_html))
        item.set_metadata(parse_metadata(soup))
        
        return item
