#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class DCARD_GET_POST_LIST(Protocol):

    def __init__(self):
        super(DCARD_GET_POST_LIST, self).__init__('DCARD_GET_POST_LIST')
        self.update({
          'forum': ''
        })

    def set_forum(self, forum):
        self.set('forum', forum)

    def get_forum(self):
        return self.get('forum')
