#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime


class KAIROS:
    def __init__(self, server):
        print('init kairos.py')
        self.server = server

    def on_CRW2PAR_NTF_KAIROS_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_kairos_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_KAIROS_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_kairos_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_kairos_list(self, content):

        def parse_links(soup):
            links = soup.find_all('h3','post-title')
            links = [ {'url':li.find('a')['href'],'metadata':[]} for li in links]  
            return links
        
        def parse_next_page(soup):
            try:
                next_page = soup.find('ul','pages-numbers').find('li','the-next-page')
                next_page = next_page.find('a')['href']
            except:
                next_page = None
            return next_page
        
        def parse_latest_post_date(soup):
            pub_date = soup.find_all('span','date meta-item fa-before')
            link_date = []
            for date in pub_date:
                date = datetime.strptime( date.text , '%Y-%m-%d')
                link_date.append(date)
            latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
            return latest_post_date
                
        list_html = BeautifulSoup(content, "html.parser")
        links = parse_links(list_html)
        next_page = parse_next_page(list_html)
        latest_post_date = parse_latest_post_date(list_html)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)

        return item

    def parse_kairos_content(self, content):
        
        def parse_date(soup):     
            date = soup.find('head').find('meta',{'property':'article:published_time'})['content']
            date = datetime.strptime( date , '%Y-%m-%dT%H:%M:%S+00:00')
            date = date.strftime('%Y-%m-%dT%H:%M:%S+0000')
            return date
        
        def parse_content(soup):
            content = soup.find('div','entry-content entry clearfix')
            for ad in content.find_all('div','snippet'):
                ad.decompose()
                    
            content = '\n'.join(x.text for x in content.find_all('p'))        
            content = '\n'.join(content.split())
            content = content.replace('\n則留言','')
            return content
        
        def parse_author(soup):
            author = soup.find('span','meta-author')
            author = author.find('a','author-name').text.strip()
            return author
        
        def parse_category(soup):
            menubar = soup.find('div','main-menu header-menu')
            li = menubar.find_all('li')
            cat = [ l.find('a').text for l in li if 'tie-current-menu' in str(l['class'])]
            return cat[0]
        
        def parse_keywords(soup):
            try:
                tag_bar = soup.find('span','post-cat-wrap')
                tags = [t.text for t in tag_bar.find_all('a')]
            except:
                tags = []
            return tags
        
        
        # detail
        detail_html = BeautifulSoup(content['html'], 'html.parser')
        #comment_html =  BeautifulSoup(content['comment_html'], 'html.parser')
        url = content['url']
        title = detail_html.find('head').find('title').text
        title = ' '.join(title.split())
        date = parse_date(detail_html) 
        content = parse_content(detail_html)
        author = parse_author(detail_html)
        keywords = parse_keywords(detail_html)
        category = parse_category(detail_html)
        # share_count = detail_html.find('span','a2a_count').text
        # metadata = {'tag':keywords, 'share_count':share_count}
        metadata = {'tag':keywords, 'category':category,'share_count':''}
        #comment = parse_comments(comment_html)

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        
        return item
