#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from datetime import datetime, date, timedelta

class CNYES:
    def __init__(self, server):
        print('init cnyes.py')
        self.server = server

    def on_CRW2PAR_NTF_CNYES_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_cnyes_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())        
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_CNYES_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()
        if not content['html']:
            print('POST content[\'html\'] is None, ignoring...')
            return

        item = self.parse_cnyes_content(content)
        
        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', result['url'], result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)
 
    def parse_cnyes_list(self, html):        
        
        cnyes = 'https://news.cnyes.com'
        links = [{'url':cnyes+'/news/id/'+str(h['newsId']),'metadata':[]} for h in html['items']['data']]
        
        link_date = [datetime.fromtimestamp(h['publishAt']) for h in html['items']['data']]
        latest_post_date = max(link_date).strftime('%Y-%m-%dT%H:%M:%S+0800')
        
        if html['items']['next_page_url'] != None:
            next_page = cnyes + html['items']['next_page_url']
        else:
            start = (max(link_date).date()-timedelta(1)).strftime('%s')
            end = str(int(start)+86399)
            next_page = 'https://news.cnyes.com/api/v2/news?limit=30&startAt='+start+'&endAt='+end

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(next_page)
        item.set_latest_post_date(latest_post_date)
        item.set_urls(links)
        
        return item

    def parse_cnyes_content(self, content):

        soup = BeautifulSoup(content['html'], 'lxml')
                
        # detail
        url = soup.find('link', rel='canonical')['href']               
        date = soup.find("meta",  property="article:published_time")['content'].replace('+08:00','+0800')
        title = soup.find("meta",  property="og:title")['content'].split(' | ')[0]
        author = soup.find("span",  {'itemprop':'author'}).text.split('台')[0].split(' ')[0].replace('鉅亨網編譯','').replace('鉅亨網編輯','').replace('鉅亨網記者','') 
        content = soup.find("div",  {'itemprop':'articleBody'})
        for div in content.findAll('figure'): 
            div.decompose()
        content = content.text
        
        #comment
        # comment_html = BeautifulSoup(content_html['comment_html'], 'lxml')
        # comments = parse_comments(comment_html)

        #metadata
        try:
            keywords = soup.find("meta",  itemprop="keywords")['content'].split(',')
        except:
            keywords = []
        category = [s.text for s in soup.find("nav",  {'class':'_9QBS'}).findAll('span')]
        category = category[-1]
        metadata = {'tag': keywords, 'category':category}

        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(title)
        item.set_author(author)
        item.set_date(date)
        item.set_content(content)
        #item.set_comment(comment)
        item.set_metadata(metadata)
        return item

