#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import urllib.parse
import re
from bs4 import BeautifulSoup
from protocols.news_parse_item import NEWS_PARSE_ITEM
from protocols.news_parse_list import NEWS_PARSE_LIST
from protocols.par2mng_ntf_news_post_urls import PAR2MNG_NTF_NEWS_POST_URLS
from protocols.crw2par_ntf_news_post import CRW2PAR_NTF_NEWS_POST
from protocols.crw2par_ntf_news_index import CRW2PAR_NTF_NEWS_INDEX
from protocols.par2etl_ntf_new_data import PAR2ETL_NTF_NEW_DATA
from utils.parse_fb_comment import parse_comments
from datetime import datetime

class ETTODAY:
    def __init__(self, server):
        print('init ettoday.py')
        self.server = server

    def on_CRW2PAR_NTF_ETTODAY_INDEX(self, packet):
        from_notify = CRW2PAR_NTF_NEWS_INDEX()
        from_notify.parse(packet)
        content = from_notify.get_content()
        days_limit = from_notify.get_days_limit()
        name = from_notify.get_name()

        item = self.parse_ettoday_list(content)

        to_notify = PAR2MNG_NTF_NEWS_POST_URLS()
        to_notify.set_urls(item.get_urls())
        to_notify.set_media(from_notify.get_media())
        to_notify.set_next_page_url(item.get_next_page_url())
        to_notify.set_latest_post_date(item.get_latest_post_date())
        to_notify.set_name(name)
        to_notify.set_days_limit(days_limit)
        self.server.send(to_notify)

    def on_CRW2PAR_NTF_ETTODAY_POST(self, packet):
        '''
        Save To Mongo
        '''
        from_notify = CRW2PAR_NTF_NEWS_POST()
        from_notify.parse(packet)
        content = from_notify.get_content()

        item = self.parse_ettoday_content(content)

        result = item.to_dict()
        result['media'] = from_notify.get_media()
        url = self.server.mongo.upsert_post('history_v2', 'data', item.get('url'), result)
        if url is None:
            return
        to_notify = PAR2ETL_NTF_NEW_DATA()
        to_notify.set_url(url)
        self.server.send(to_notify)

    def parse_ettoday_list(self, content):        

        # parse links
        prefix = 'https://www.ettoday.net'
        links = []
        for html in content['html']:
            soup = BeautifulSoup(html, "lxml")    
            page = [prefix + i['href'] for i in soup.select('a')]
            page = list(set(page))
            for url in page:
                links.append({
                    'url': url, 
                    'metadata': {}}
                )

        # parse latest post date
        # def parse_datetime(soup):
        #     try:
        #         datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
        #     except:
        #         datetime = soup.find_all('time', {'class': 'news-time'})[0]['datetime']
        #     datetime = datetime[:22]+datetime[-2:] #remove ':'
        #     return datetime
        
        # html = requests.get(links[0]).text
        # soup = BeautifulSoup(html, 'html.parser')
        # latest_post_date = parse_datetime(soup)

        item = NEWS_PARSE_LIST()
        item.set_next_page_url(None)
        item.set_latest_post_date(content['latest_post_date'])
        item.set_urls(links)
        return item

    def parse_ettoday_content(self, content):

        def parse_datetime(soup):
            try:
                datetime = soup.find_all('time', {'class': 'date'})[0]['datetime']
            except:
                datetime = soup.find_all('time', {'class': 'news-time'})[0]['datetime']
            datetime = datetime[:22]+datetime[-2:] #remove ':'
            return datetime
        
        def parse_title(soup):
            return soup.select('h1')[0].text
        
        def parse_author(soup): 
            try:
                # try Columnist
                block = soup.find_all('div', {'class': 'penname_news clearfix'})
                for ele in block:
                    penname = ele.find_all('a', {'class': 'pic'})
                author = penname[0]['href'].split('/')[-1]
                return author
            
            except:
                script = re.search('"creator": ."[0-9]+....', str(soup.select('script')[0]))
                author = re.findall(re.compile(u"[\u4e00-\u9fa5]+"), str(script))
                return author[0] if len(author) else ''
            
        def parse_content(soup):
            article = soup.find('div', class_='story')
            paragraph = []
            for p in article.find_all('p'):
                text = p.text.strip()
                if len(text) == 0 or text[0]=='►':
                    continue
                paragraph.append(text)
                
            content = '\n'.join(paragraph)
            content = content.split('【更多鏡週刊相關報導】')[0]
            return content
        
        def parse_metadata(soup):
            metadata = {'category':'', 'fb_like_count': ''}
            metadata['category'] = soup.find('meta',{'property':'article:section'})['content']
            # label = soup.find_all('a', {'class': 'btn current'})
            # if len(label):
            #     metadata['category'] = label[0].text
            #     metadata['fb_like_count'] = fb_like_count_html.find('span',{'id':'u_0_1'}).text
            return metadata

        soup = BeautifulSoup(content['html'], 'lxml')
        # fb_like_count_html =  BeautifulSoup(content['fb_like_count'], 'lxml')
        # comment_html =  BeautifulSoup(content['comment_html'], 'lxml')

        url = content['url']
        
        item = NEWS_PARSE_ITEM()
        item.set_url(url)
        item.set_title(parse_title(soup))
        item.set_author(parse_author(soup))
        item.set_date(parse_datetime(soup))
        item.set_content(parse_content(soup))
        item.set_comment([])
        item.set_metadata(parse_metadata(soup))
        
        return item
