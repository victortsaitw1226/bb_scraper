import redis
import ujson
import time
from scutils.redis_queue import Base, RedisQueue, RedisPriorityQueue, RedisStack
#from scrapy_redis import PriorityQueue
from scrapy.http import Request

r = redis.StrictRedis()
redis_key = "youtube:start_urls"
q = RedisPriorityQueue(r, redis_key, encoding=ujson)
data = []
data.append({
    "url": 'https://www.youtube.com/all_comments?v={}'.format('lvIVoqPTMw0'), 
    "priority": 3, 
    'dont_redirect': True,
    'handle_httpstatus_list': [302, 303, 301]
})

for d in data:
    q.push(d, d['priority'])

