#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import os
import sys
import traceback
import datetime
from nameko.web.handlers import http
from nameko.events import EventDispatcher, event_handler, SINGLETON
from nameko.timer import timer
from extensions.redis import Redis
from extensions.logger import Logger
from extensions.elasticSearch import ElasticSearch
from extensions.browser import Browser
from extensions.mongo import Mongo
from extensions.evaluation import Evaluation
from extensions.mysql import MySQL
from nameko.dependency_providers import Config
from nameko.web.handlers import HttpRequestHandler, http
from werkzeug.wrappers import Response
from service.app import App
from urllib.parse import unquote

class Facade:
    name = 'facade'
    dispatch = EventDispatcher()
    config = Config()
    elasticsearch = ElasticSearch()
    redis = Redis()
    mongo = Mongo()
    #mysql = MySQL()
    log = Logger(
        name=os.environ.get('POD_NAME', 'manager'),
        env=os.environ.get('ENV', 'debug')
    )
    browser = Browser()
    evaluation = Evaluation()

    def split_attr(self, attributes):
        d = {}
        attributes = unquote(attributes)
        attrs = attributes.split('&')
        for attr in attrs:
            info = attr.split('=')
            d[info[0]] = '='.join(info[1:])
        return d

    def __init__(self):
        print('facade init')
        super(Facade, self).__init__()
        self.app = App(self)

    def send(self, protocol):
        proto = protocol.get_proto_name()
        self.dispatch(proto, str(protocol))

    @http('GET', '/data/reset')
    def reset(self, request):
        r = self.app.reset_data()
        return json.dumps(r)

    @http('GET', '/ping')
    def ping(self, request):
        return json.dumps({
          'pong': datetime.datetime.now().strftime("%H:%M:%S.%f - %b %d %Y")
        })

    # =========================================
    # ================ URLs ==================
    # =========================================

    @http('POST', '/url/list')
    def list_urls(self, request):
        name = request.get_data(as_text=True)
        media = name.split('=')[1]
        r = self.app.list_urls(media)
        return json.dumps(r)

    @http('POST', '/url/disable')
    def disable_urls(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        media = attr.get('media')
        if media:

            if 'all' == media:
                r = self.app.disable_all_urls()
                return json.dumps({
                  'result': 'disable all success'    
                })

            r = self.app.disable_urls_by_media(media)
            return json.dumps({
              'result': 'disable {} success'.format(media)    
            })
        url = attr.get('url')
        if url:
            r = self.app.disable_urls_by_url(url)
            return json.dumps({
                'result': 'disable {} success'.format(url)    
            })
        return json.dumps({
          'result': 'Cannot find media or url'    
        })

    @http('POST', '/url/enable')
    def enable_urls(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        media = attr.get('media')
        if media:

            if 'all' == media:
                r = self.app.enable_all_urls()
                return json.dumps({
                  'result': 'enable all success'    
                })

            r = self.app.enable_urls_by_media(media)
            return json.dumps({
              'result': 'enable {} success'.format(media)    
            })
        url = attr.get('url')
        if url:
            r = self.app.enable_urls_by_url(url)
            return json.dumps({
              'result': 'enable {} success'.format(url)    
            })
        return json.dumps({
          'result': 'Cannot find media or url'    
        })

    @http('POST', '/url/days_limit/change')
    def change_days_limit(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        media = attr.get('media')
        days_limit = int(attr.get('days_limit', 0))
        if days_limit < 1:
            return json.dumps({
                'result': 'days_limit must great than 0'    
            })

        if media:

            if 'all' == media:
                self.app.change_all_days_limit(days_limit)
                return json.dumps({
                    'result': 'change all success'
                })

            r = self.app.change_days_limit_by_media(media, days_limit)
            return json.dumps({
                'result': 'change days_limit {} by media {} success'.format(days_limit, media)
            })

        url = attr.get('url')
        if url:
            r = self.app.change_days_limit_by_url(url, days_limit)
            return json.dumps({
                'result': 'change days_limit {} by url {} success'.format(days_limit, url)
            })
        return json.dumps({
            'result': 'Cannot find media or url'
        })

    @http('POST', '/url/interval/change')
    def change_interval(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        media = attr.get('media')
        interval = int(attr.get('interval', 0))
        if interval < 1:
            return json.dumps({
                'result': 'interval must great than 0'    
            })

        if media:

            if 'all' == media:
                self.app.change_all_interval(interval)
                return json.dumps({
                    'result': 'change all success'
                })

            r = self.app.change_interval_by_media(media, interval)
            return json.dumps({
                'result': 'change interval {} by media {} success'.format(interval, media)
            })

        url = attr.get('url')
        if url:
            r = self.app.change_interval_by_url(url, interval)
            return json.dumps({
                'result': 'change interval {} by url {} success'.format(interval, url)
            })
        return json.dumps({
            'result': 'Cannot find media or url'
        })

    # =========================================
    # ================ Proxy ==================
    # =========================================

    @http('POST', '/proxy/add')
    def add_proxy(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        proxy = attr.get('proxy')
        self.redis.add_proxy(proxy)
        return json.dumps({
          'result': 'add proxy {} success'.format(proxy)
        })

    @http('GET', '/proxy/list')
    def list_proxy(self, request):
        r = self.redis.list_proxy()
        return json.dumps(r)


    # =========================================
    # ================ Lock ==================
    # =========================================

    @http('POST', '/lock/delete')
    def delete_lock(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        media = attr.get('media')
        locks = self.app.unlock_media(media)

        #for lock in self.app.list_locks(media):
        #    self.redis.force_unlock(lock)

        return json.dumps({
          'result': 'unlock {} success'.format(','.join(locks))
        })

    @http('GET', '/lock/ttl')
    def check_lock_ttl(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        lock = attr.get('lock')
        ttl = self.redis.get_ttl(lock)
        return json.dumps({
          'result': 'ttl {} sec of {}'.format(ttl, lock)
        })
    # ==========================================
    # ================== Date ==================
    # ==========================================

    @http('GET', '/data/bucket')
    def data_bucket(self, request):
        r = self.app.media_bucket_list()
        return json.dumps(r)

    # =========================================
    # ====== Facebook interactive count========
    # =========================================
    @http('POST', '/facebook/count')
    def facebook_count(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        url = attr.get('url')
        count = self.browser.count_fb(url)
        return json.dumps({
            'url': url,
            'count': int(count)
        })

    # =========================================
    # ============= Re-evaluation =============
    # =========================================
    @http('POST', '/data/evaluation')
    def data_evaluation(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        date = attr.get('date')
        self.app.data_evaluation(date)
        return json.dumps({
            'result': 'success'
        })

    # =====================================
    # ========= Add User To MySQL ========
    # ====================================
    @http('POST', '/line/add')
    def line_add_user(self, request):
        data = request.get_data(as_text=True)
        attr = self.split_attr(data)
        user_id = attr.get('user_id', '')
        name = attr.get('name', '')
        self.app.line_add_user(user_id, name)
        return json.dumps({
            'result': 'success'
        })

    @timer(3600)
    def update_fb_count(self):
        self.app.update_fb_count()

    #@timer(10)
    #def send_line_msg(self):
    #    self.app.send_line_msg()
