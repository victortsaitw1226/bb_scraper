#!/usr/bin/env python
# -*- coding: utf-8 -*-
from .protocol import Protocol

class CRW2PAR_NTF_DCARD_POST_LIST(Protocol):

    def __init__(self):
        super(CRW2PAR_NTF_DCARD_POST_LIST, self).__init__('CRW2PAR_NTF_DCARD_POST_LIST')
        self.update({
          'content': '',
          'days_limit': 0
        })

    def set_days_limit(self, days_limit):
        self.set('days_limit', days_limit)

    def get_days_limit(self):
        return self.get('days_limit')

    def set_content(self, content):
        self.set('content', content)

    def get_content(self):
        return self.get('content')
